package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PhongDatTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhongDat.class);
        PhongDat phongDat1 = new PhongDat();
        phongDat1.setId(1L);
        PhongDat phongDat2 = new PhongDat();
        phongDat2.setId(phongDat1.getId());
        assertThat(phongDat1).isEqualTo(phongDat2);
        phongDat2.setId(2L);
        assertThat(phongDat1).isNotEqualTo(phongDat2);
        phongDat1.setId(null);
        assertThat(phongDat1).isNotEqualTo(phongDat2);
    }
}
