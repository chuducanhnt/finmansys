package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PhongTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Phong.class);
        Phong phong1 = new Phong();
        phong1.setId(1L);
        Phong phong2 = new Phong();
        phong2.setId(phong1.getId());
        assertThat(phong1).isEqualTo(phong2);
        phong2.setId(2L);
        assertThat(phong1).isNotEqualTo(phong2);
        phong1.setId(null);
        assertThat(phong1).isNotEqualTo(phong2);
    }
}
