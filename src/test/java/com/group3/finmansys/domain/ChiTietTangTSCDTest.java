package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ChiTietTangTSCDTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiTietTangTSCD.class);
        ChiTietTangTSCD chiTietTangTSCD1 = new ChiTietTangTSCD();
        chiTietTangTSCD1.setId(1L);
        ChiTietTangTSCD chiTietTangTSCD2 = new ChiTietTangTSCD();
        chiTietTangTSCD2.setId(chiTietTangTSCD1.getId());
        assertThat(chiTietTangTSCD1).isEqualTo(chiTietTangTSCD2);
        chiTietTangTSCD2.setId(2L);
        assertThat(chiTietTangTSCD1).isNotEqualTo(chiTietTangTSCD2);
        chiTietTangTSCD1.setId(null);
        assertThat(chiTietTangTSCD1).isNotEqualTo(chiTietTangTSCD2);
    }
}
