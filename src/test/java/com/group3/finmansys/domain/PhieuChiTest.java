package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PhieuChiTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhieuChi.class);
        PhieuChi phieuChi1 = new PhieuChi();
        phieuChi1.setId(1L);
        PhieuChi phieuChi2 = new PhieuChi();
        phieuChi2.setId(phieuChi1.getId());
        assertThat(phieuChi1).isEqualTo(phieuChi2);
        phieuChi2.setId(2L);
        assertThat(phieuChi1).isNotEqualTo(phieuChi2);
        phieuChi1.setId(null);
        assertThat(phieuChi1).isNotEqualTo(phieuChi2);
    }
}
