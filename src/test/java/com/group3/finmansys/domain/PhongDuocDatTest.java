package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PhongDuocDatTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhongDuocDat.class);
        PhongDuocDat phongDuocDat1 = new PhongDuocDat();
        phongDuocDat1.setId(1L);
        PhongDuocDat phongDuocDat2 = new PhongDuocDat();
        phongDuocDat2.setId(phongDuocDat1.getId());
        assertThat(phongDuocDat1).isEqualTo(phongDuocDat2);
        phongDuocDat2.setId(2L);
        assertThat(phongDuocDat1).isNotEqualTo(phongDuocDat2);
        phongDuocDat1.setId(null);
        assertThat(phongDuocDat1).isNotEqualTo(phongDuocDat2);
    }
}
