package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PhieuThuTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhieuThu.class);
        PhieuThu phieuThu1 = new PhieuThu();
        phieuThu1.setId(1L);
        PhieuThu phieuThu2 = new PhieuThu();
        phieuThu2.setId(phieuThu1.getId());
        assertThat(phieuThu1).isEqualTo(phieuThu2);
        phieuThu2.setId(2L);
        assertThat(phieuThu1).isNotEqualTo(phieuThu2);
        phieuThu1.setId(null);
        assertThat(phieuThu1).isNotEqualTo(phieuThu2);
    }
}
