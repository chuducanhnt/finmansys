package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ChiTietGiamTSCDTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChiTietGiamTSCD.class);
        ChiTietGiamTSCD chiTietGiamTSCD1 = new ChiTietGiamTSCD();
        chiTietGiamTSCD1.setId(1L);
        ChiTietGiamTSCD chiTietGiamTSCD2 = new ChiTietGiamTSCD();
        chiTietGiamTSCD2.setId(chiTietGiamTSCD1.getId());
        assertThat(chiTietGiamTSCD1).isEqualTo(chiTietGiamTSCD2);
        chiTietGiamTSCD2.setId(2L);
        assertThat(chiTietGiamTSCD1).isNotEqualTo(chiTietGiamTSCD2);
        chiTietGiamTSCD1.setId(null);
        assertThat(chiTietGiamTSCD1).isNotEqualTo(chiTietGiamTSCD2);
    }
}
