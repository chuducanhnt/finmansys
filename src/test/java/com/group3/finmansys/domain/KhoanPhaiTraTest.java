package com.group3.finmansys.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class KhoanPhaiTraTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(KhoanPhaiTra.class);
        KhoanPhaiTra khoanPhaiTra1 = new KhoanPhaiTra();
        khoanPhaiTra1.setId(1L);
        KhoanPhaiTra khoanPhaiTra2 = new KhoanPhaiTra();
        khoanPhaiTra2.setId(khoanPhaiTra1.getId());
        assertThat(khoanPhaiTra1).isEqualTo(khoanPhaiTra2);
        khoanPhaiTra2.setId(2L);
        assertThat(khoanPhaiTra1).isNotEqualTo(khoanPhaiTra2);
        khoanPhaiTra1.setId(null);
        assertThat(khoanPhaiTra1).isNotEqualTo(khoanPhaiTra2);
    }
}
