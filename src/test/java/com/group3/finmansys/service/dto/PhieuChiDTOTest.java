package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PhieuChiDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhieuChiDTO.class);
        PhieuChiDTO phieuChiDTO1 = new PhieuChiDTO();
        phieuChiDTO1.setId(1L);
        PhieuChiDTO phieuChiDTO2 = new PhieuChiDTO();
        assertThat(phieuChiDTO1).isNotEqualTo(phieuChiDTO2);
        phieuChiDTO2.setId(phieuChiDTO1.getId());
        assertThat(phieuChiDTO1).isEqualTo(phieuChiDTO2);
        phieuChiDTO2.setId(2L);
        assertThat(phieuChiDTO1).isNotEqualTo(phieuChiDTO2);
        phieuChiDTO1.setId(null);
        assertThat(phieuChiDTO1).isNotEqualTo(phieuChiDTO2);
    }
}
