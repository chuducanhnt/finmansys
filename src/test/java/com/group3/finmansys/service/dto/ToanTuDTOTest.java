package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ToanTuDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ToanTuDTO.class);
        ToanTuDTO toanTuDTO1 = new ToanTuDTO();
        toanTuDTO1.setId(1L);
        ToanTuDTO toanTuDTO2 = new ToanTuDTO();
        assertThat(toanTuDTO1).isNotEqualTo(toanTuDTO2);
        toanTuDTO2.setId(toanTuDTO1.getId());
        assertThat(toanTuDTO1).isEqualTo(toanTuDTO2);
        toanTuDTO2.setId(2L);
        assertThat(toanTuDTO1).isNotEqualTo(toanTuDTO2);
        toanTuDTO1.setId(null);
        assertThat(toanTuDTO1).isNotEqualTo(toanTuDTO2);
    }
}
