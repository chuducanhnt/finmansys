package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DichVuDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DichVuDTO.class);
        DichVuDTO dichVuDTO1 = new DichVuDTO();
        dichVuDTO1.setId(1L);
        DichVuDTO dichVuDTO2 = new DichVuDTO();
        assertThat(dichVuDTO1).isNotEqualTo(dichVuDTO2);
        dichVuDTO2.setId(dichVuDTO1.getId());
        assertThat(dichVuDTO1).isEqualTo(dichVuDTO2);
        dichVuDTO2.setId(2L);
        assertThat(dichVuDTO1).isNotEqualTo(dichVuDTO2);
        dichVuDTO1.setId(null);
        assertThat(dichVuDTO1).isNotEqualTo(dichVuDTO2);
    }
}
