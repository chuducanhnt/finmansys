package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class NganSachNamDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NganSachNamDTO.class);
        NganSachNamDTO nganSachNamDTO1 = new NganSachNamDTO();
        nganSachNamDTO1.setId(1L);
        NganSachNamDTO nganSachNamDTO2 = new NganSachNamDTO();
        assertThat(nganSachNamDTO1).isNotEqualTo(nganSachNamDTO2);
        nganSachNamDTO2.setId(nganSachNamDTO1.getId());
        assertThat(nganSachNamDTO1).isEqualTo(nganSachNamDTO2);
        nganSachNamDTO2.setId(2L);
        assertThat(nganSachNamDTO1).isNotEqualTo(nganSachNamDTO2);
        nganSachNamDTO1.setId(null);
        assertThat(nganSachNamDTO1).isNotEqualTo(nganSachNamDTO2);
    }
}
