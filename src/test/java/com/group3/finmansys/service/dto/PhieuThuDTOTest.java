package com.group3.finmansys.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.group3.finmansys.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PhieuThuDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhieuThuDTO.class);
        PhieuThuDTO phieuThuDTO1 = new PhieuThuDTO();
        phieuThuDTO1.setId(1L);
        PhieuThuDTO phieuThuDTO2 = new PhieuThuDTO();
        assertThat(phieuThuDTO1).isNotEqualTo(phieuThuDTO2);
        phieuThuDTO2.setId(phieuThuDTO1.getId());
        assertThat(phieuThuDTO1).isEqualTo(phieuThuDTO2);
        phieuThuDTO2.setId(2L);
        assertThat(phieuThuDTO1).isNotEqualTo(phieuThuDTO2);
        phieuThuDTO1.setId(null);
        assertThat(phieuThuDTO1).isNotEqualTo(phieuThuDTO2);
    }
}
