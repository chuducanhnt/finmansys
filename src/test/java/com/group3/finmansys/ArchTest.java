package com.group3.finmansys;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.group3.finmansys");

        noClasses()
            .that()
            .resideInAnyPackage("com.group3.finmansys.service..")
            .or()
            .resideInAnyPackage("com.group3.finmansys.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..com.group3.finmansys.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
