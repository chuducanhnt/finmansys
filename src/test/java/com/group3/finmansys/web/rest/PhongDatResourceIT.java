package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.Phong;
import com.group3.finmansys.domain.PhongDat;
import com.group3.finmansys.domain.PhongDuocDat;
import com.group3.finmansys.repository.PhongDatRepository;
import com.group3.finmansys.service.criteria.PhongDatCriteria;
import com.group3.finmansys.service.dto.PhongDatDTO;
import com.group3.finmansys.service.mapper.PhongDatMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PhongDatResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class PhongDatResourceIT {

    private static final Instant DEFAULT_NGAY_DAT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_NGAY_DAT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/phong-dats";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PhongDatRepository phongDatRepository;

    @Autowired
    private PhongDatMapper phongDatMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPhongDatMockMvc;

    private PhongDat phongDat;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhongDat createEntity(EntityManager em) {
        PhongDat phongDat = new PhongDat().ngayDat(DEFAULT_NGAY_DAT);
        // Add required entity
        Phong phong;
        if (TestUtil.findAll(em, Phong.class).isEmpty()) {
            phong = PhongResourceIT.createEntity(em);
            em.persist(phong);
            em.flush();
        } else {
            phong = TestUtil.findAll(em, Phong.class).get(0);
        }
        phongDat.setPhong(phong);
        // Add required entity
        PhongDuocDat phongDuocDat;
        if (TestUtil.findAll(em, PhongDuocDat.class).isEmpty()) {
            phongDuocDat = PhongDuocDatResourceIT.createEntity(em);
            em.persist(phongDuocDat);
            em.flush();
        } else {
            phongDuocDat = TestUtil.findAll(em, PhongDuocDat.class).get(0);
        }
        phongDat.setPhongDuocDat(phongDuocDat);
        return phongDat;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhongDat createUpdatedEntity(EntityManager em) {
        PhongDat phongDat = new PhongDat().ngayDat(UPDATED_NGAY_DAT);
        // Add required entity
        Phong phong;
        if (TestUtil.findAll(em, Phong.class).isEmpty()) {
            phong = PhongResourceIT.createUpdatedEntity(em);
            em.persist(phong);
            em.flush();
        } else {
            phong = TestUtil.findAll(em, Phong.class).get(0);
        }
        phongDat.setPhong(phong);
        // Add required entity
        PhongDuocDat phongDuocDat;
        if (TestUtil.findAll(em, PhongDuocDat.class).isEmpty()) {
            phongDuocDat = PhongDuocDatResourceIT.createUpdatedEntity(em);
            em.persist(phongDuocDat);
            em.flush();
        } else {
            phongDuocDat = TestUtil.findAll(em, PhongDuocDat.class).get(0);
        }
        phongDat.setPhongDuocDat(phongDuocDat);
        return phongDat;
    }

    @BeforeEach
    public void initTest() {
        phongDat = createEntity(em);
    }

    @Test
    @Transactional
    void createPhongDat() throws Exception {
        int databaseSizeBeforeCreate = phongDatRepository.findAll().size();
        // Create the PhongDat
        PhongDatDTO phongDatDTO = phongDatMapper.toDto(phongDat);
        restPhongDatMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDatDTO)))
            .andExpect(status().isCreated());

        // Validate the PhongDat in the database
        List<PhongDat> phongDatList = phongDatRepository.findAll();
        assertThat(phongDatList).hasSize(databaseSizeBeforeCreate + 1);
        PhongDat testPhongDat = phongDatList.get(phongDatList.size() - 1);
        assertThat(testPhongDat.getNgayDat()).isEqualTo(DEFAULT_NGAY_DAT);
    }

    @Test
    @Transactional
    void createPhongDatWithExistingId() throws Exception {
        // Create the PhongDat with an existing ID
        phongDat.setId(1L);
        PhongDatDTO phongDatDTO = phongDatMapper.toDto(phongDat);

        int databaseSizeBeforeCreate = phongDatRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhongDatMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDatDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PhongDat in the database
        List<PhongDat> phongDatList = phongDatRepository.findAll();
        assertThat(phongDatList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNgayDatIsRequired() throws Exception {
        int databaseSizeBeforeTest = phongDatRepository.findAll().size();
        // set the field null
        phongDat.setNgayDat(null);

        // Create the PhongDat, which fails.
        PhongDatDTO phongDatDTO = phongDatMapper.toDto(phongDat);

        restPhongDatMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDatDTO)))
            .andExpect(status().isBadRequest());

        List<PhongDat> phongDatList = phongDatRepository.findAll();
        assertThat(phongDatList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPhongDats() throws Exception {
        // Initialize the database
        phongDatRepository.saveAndFlush(phongDat);

        // Get all the phongDatList
        restPhongDatMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phongDat.getId().intValue())))
            .andExpect(jsonPath("$.[*].ngayDat").value(hasItem(DEFAULT_NGAY_DAT.toString())));
    }

    @Test
    @Transactional
    void getPhongDat() throws Exception {
        // Initialize the database
        phongDatRepository.saveAndFlush(phongDat);

        // Get the phongDat
        restPhongDatMockMvc
            .perform(get(ENTITY_API_URL_ID, phongDat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(phongDat.getId().intValue()))
            .andExpect(jsonPath("$.ngayDat").value(DEFAULT_NGAY_DAT.toString()));
    }

    @Test
    @Transactional
    void getPhongDatsByIdFiltering() throws Exception {
        // Initialize the database
        phongDatRepository.saveAndFlush(phongDat);

        Long id = phongDat.getId();

        defaultPhongDatShouldBeFound("id.equals=" + id);
        defaultPhongDatShouldNotBeFound("id.notEquals=" + id);

        defaultPhongDatShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPhongDatShouldNotBeFound("id.greaterThan=" + id);

        defaultPhongDatShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPhongDatShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPhongDatsByNgayDatIsEqualToSomething() throws Exception {
        // Initialize the database
        phongDatRepository.saveAndFlush(phongDat);

        // Get all the phongDatList where ngayDat equals to DEFAULT_NGAY_DAT
        defaultPhongDatShouldBeFound("ngayDat.equals=" + DEFAULT_NGAY_DAT);

        // Get all the phongDatList where ngayDat equals to UPDATED_NGAY_DAT
        defaultPhongDatShouldNotBeFound("ngayDat.equals=" + UPDATED_NGAY_DAT);
    }

    @Test
    @Transactional
    void getAllPhongDatsByNgayDatIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phongDatRepository.saveAndFlush(phongDat);

        // Get all the phongDatList where ngayDat not equals to DEFAULT_NGAY_DAT
        defaultPhongDatShouldNotBeFound("ngayDat.notEquals=" + DEFAULT_NGAY_DAT);

        // Get all the phongDatList where ngayDat not equals to UPDATED_NGAY_DAT
        defaultPhongDatShouldBeFound("ngayDat.notEquals=" + UPDATED_NGAY_DAT);
    }

    @Test
    @Transactional
    void getAllPhongDatsByNgayDatIsInShouldWork() throws Exception {
        // Initialize the database
        phongDatRepository.saveAndFlush(phongDat);

        // Get all the phongDatList where ngayDat in DEFAULT_NGAY_DAT or UPDATED_NGAY_DAT
        defaultPhongDatShouldBeFound("ngayDat.in=" + DEFAULT_NGAY_DAT + "," + UPDATED_NGAY_DAT);

        // Get all the phongDatList where ngayDat equals to UPDATED_NGAY_DAT
        defaultPhongDatShouldNotBeFound("ngayDat.in=" + UPDATED_NGAY_DAT);
    }

    @Test
    @Transactional
    void getAllPhongDatsByNgayDatIsNullOrNotNull() throws Exception {
        // Initialize the database
        phongDatRepository.saveAndFlush(phongDat);

        // Get all the phongDatList where ngayDat is not null
        defaultPhongDatShouldBeFound("ngayDat.specified=true");

        // Get all the phongDatList where ngayDat is null
        defaultPhongDatShouldNotBeFound("ngayDat.specified=false");
    }

    @Test
    @Transactional
    void getAllPhongDatsByPhongIsEqualToSomething() throws Exception {
        // Initialize the database
        phongDatRepository.saveAndFlush(phongDat);
        Phong phong = PhongResourceIT.createEntity(em);
        em.persist(phong);
        em.flush();
        phongDat.setPhong(phong);
        phongDatRepository.saveAndFlush(phongDat);
        Long phongId = phong.getId();

        // Get all the phongDatList where phong equals to phongId
        defaultPhongDatShouldBeFound("phongId.equals=" + phongId);

        // Get all the phongDatList where phong equals to (phongId + 1)
        defaultPhongDatShouldNotBeFound("phongId.equals=" + (phongId + 1));
    }

    @Test
    @Transactional
    void getAllPhongDatsByPhongDuocDatIsEqualToSomething() throws Exception {
        // Initialize the database
        phongDatRepository.saveAndFlush(phongDat);
        PhongDuocDat phongDuocDat = PhongDuocDatResourceIT.createEntity(em);
        em.persist(phongDuocDat);
        em.flush();
        phongDat.setPhongDuocDat(phongDuocDat);
        phongDatRepository.saveAndFlush(phongDat);
        Long phongDuocDatId = phongDuocDat.getId();

        // Get all the phongDatList where phongDuocDat equals to phongDuocDatId
        defaultPhongDatShouldBeFound("phongDuocDatId.equals=" + phongDuocDatId);

        // Get all the phongDatList where phongDuocDat equals to (phongDuocDatId + 1)
        defaultPhongDatShouldNotBeFound("phongDuocDatId.equals=" + (phongDuocDatId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPhongDatShouldBeFound(String filter) throws Exception {
        restPhongDatMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phongDat.getId().intValue())))
            .andExpect(jsonPath("$.[*].ngayDat").value(hasItem(DEFAULT_NGAY_DAT.toString())));

        // Check, that the count call also returns 1
        restPhongDatMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPhongDatShouldNotBeFound(String filter) throws Exception {
        restPhongDatMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPhongDatMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPhongDat() throws Exception {
        // Get the phongDat
        restPhongDatMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPhongDat() throws Exception {
        // Initialize the database
        phongDatRepository.saveAndFlush(phongDat);

        int databaseSizeBeforeUpdate = phongDatRepository.findAll().size();

        // Update the phongDat
        PhongDat updatedPhongDat = phongDatRepository.findById(phongDat.getId()).get();
        // Disconnect from session so that the updates on updatedPhongDat are not directly saved in db
        em.detach(updatedPhongDat);
        updatedPhongDat.ngayDat(UPDATED_NGAY_DAT);
        PhongDatDTO phongDatDTO = phongDatMapper.toDto(updatedPhongDat);

        restPhongDatMockMvc
            .perform(
                put(ENTITY_API_URL_ID, phongDatDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(phongDatDTO))
            )
            .andExpect(status().isOk());

        // Validate the PhongDat in the database
        List<PhongDat> phongDatList = phongDatRepository.findAll();
        assertThat(phongDatList).hasSize(databaseSizeBeforeUpdate);
        PhongDat testPhongDat = phongDatList.get(phongDatList.size() - 1);
        assertThat(testPhongDat.getNgayDat()).isEqualTo(UPDATED_NGAY_DAT);
    }

    @Test
    @Transactional
    void putNonExistingPhongDat() throws Exception {
        int databaseSizeBeforeUpdate = phongDatRepository.findAll().size();
        phongDat.setId(count.incrementAndGet());

        // Create the PhongDat
        PhongDatDTO phongDatDTO = phongDatMapper.toDto(phongDat);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhongDatMockMvc
            .perform(
                put(ENTITY_API_URL_ID, phongDatDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(phongDatDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhongDat in the database
        List<PhongDat> phongDatList = phongDatRepository.findAll();
        assertThat(phongDatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPhongDat() throws Exception {
        int databaseSizeBeforeUpdate = phongDatRepository.findAll().size();
        phongDat.setId(count.incrementAndGet());

        // Create the PhongDat
        PhongDatDTO phongDatDTO = phongDatMapper.toDto(phongDat);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhongDatMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(phongDatDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhongDat in the database
        List<PhongDat> phongDatList = phongDatRepository.findAll();
        assertThat(phongDatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPhongDat() throws Exception {
        int databaseSizeBeforeUpdate = phongDatRepository.findAll().size();
        phongDat.setId(count.incrementAndGet());

        // Create the PhongDat
        PhongDatDTO phongDatDTO = phongDatMapper.toDto(phongDat);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhongDatMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phongDatDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the PhongDat in the database
        List<PhongDat> phongDatList = phongDatRepository.findAll();
        assertThat(phongDatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePhongDatWithPatch() throws Exception {
        // Initialize the database
        phongDatRepository.saveAndFlush(phongDat);

        int databaseSizeBeforeUpdate = phongDatRepository.findAll().size();

        // Update the phongDat using partial update
        PhongDat partialUpdatedPhongDat = new PhongDat();
        partialUpdatedPhongDat.setId(phongDat.getId());

        partialUpdatedPhongDat.ngayDat(UPDATED_NGAY_DAT);

        restPhongDatMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPhongDat.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPhongDat))
            )
            .andExpect(status().isOk());

        // Validate the PhongDat in the database
        List<PhongDat> phongDatList = phongDatRepository.findAll();
        assertThat(phongDatList).hasSize(databaseSizeBeforeUpdate);
        PhongDat testPhongDat = phongDatList.get(phongDatList.size() - 1);
        assertThat(testPhongDat.getNgayDat()).isEqualTo(UPDATED_NGAY_DAT);
    }

    @Test
    @Transactional
    void fullUpdatePhongDatWithPatch() throws Exception {
        // Initialize the database
        phongDatRepository.saveAndFlush(phongDat);

        int databaseSizeBeforeUpdate = phongDatRepository.findAll().size();

        // Update the phongDat using partial update
        PhongDat partialUpdatedPhongDat = new PhongDat();
        partialUpdatedPhongDat.setId(phongDat.getId());

        partialUpdatedPhongDat.ngayDat(UPDATED_NGAY_DAT);

        restPhongDatMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPhongDat.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPhongDat))
            )
            .andExpect(status().isOk());

        // Validate the PhongDat in the database
        List<PhongDat> phongDatList = phongDatRepository.findAll();
        assertThat(phongDatList).hasSize(databaseSizeBeforeUpdate);
        PhongDat testPhongDat = phongDatList.get(phongDatList.size() - 1);
        assertThat(testPhongDat.getNgayDat()).isEqualTo(UPDATED_NGAY_DAT);
    }

    @Test
    @Transactional
    void patchNonExistingPhongDat() throws Exception {
        int databaseSizeBeforeUpdate = phongDatRepository.findAll().size();
        phongDat.setId(count.incrementAndGet());

        // Create the PhongDat
        PhongDatDTO phongDatDTO = phongDatMapper.toDto(phongDat);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhongDatMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, phongDatDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(phongDatDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhongDat in the database
        List<PhongDat> phongDatList = phongDatRepository.findAll();
        assertThat(phongDatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPhongDat() throws Exception {
        int databaseSizeBeforeUpdate = phongDatRepository.findAll().size();
        phongDat.setId(count.incrementAndGet());

        // Create the PhongDat
        PhongDatDTO phongDatDTO = phongDatMapper.toDto(phongDat);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhongDatMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(phongDatDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhongDat in the database
        List<PhongDat> phongDatList = phongDatRepository.findAll();
        assertThat(phongDatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPhongDat() throws Exception {
        int databaseSizeBeforeUpdate = phongDatRepository.findAll().size();
        phongDat.setId(count.incrementAndGet());

        // Create the PhongDat
        PhongDatDTO phongDatDTO = phongDatMapper.toDto(phongDat);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhongDatMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(phongDatDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PhongDat in the database
        List<PhongDat> phongDatList = phongDatRepository.findAll();
        assertThat(phongDatList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePhongDat() throws Exception {
        // Initialize the database
        phongDatRepository.saveAndFlush(phongDat);

        int databaseSizeBeforeDelete = phongDatRepository.findAll().size();

        // Delete the phongDat
        restPhongDatMockMvc
            .perform(delete(ENTITY_API_URL_ID, phongDat.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PhongDat> phongDatList = phongDatRepository.findAll();
        assertThat(phongDatList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
