package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.KhachHang;
import com.group3.finmansys.domain.KhoanMucChiPhi;
import com.group3.finmansys.domain.KhoanPhaiTra;
import com.group3.finmansys.domain.PhieuChi;
import com.group3.finmansys.domain.QuyTien;
import com.group3.finmansys.repository.PhieuChiRepository;
import com.group3.finmansys.service.criteria.PhieuChiCriteria;
import com.group3.finmansys.service.dto.PhieuChiDTO;
import com.group3.finmansys.service.mapper.PhieuChiMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PhieuChiResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class PhieuChiResourceIT {

    private static final Instant DEFAULT_NGAY_CHUNG_TU = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_NGAY_CHUNG_TU = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Double DEFAULT_SO_TIEN = 1D;
    private static final Double UPDATED_SO_TIEN = 2D;
    private static final Double SMALLER_SO_TIEN = 1D - 1D;

    private static final String ENTITY_API_URL = "/api/phieu-chis";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PhieuChiRepository phieuChiRepository;

    @Autowired
    private PhieuChiMapper phieuChiMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPhieuChiMockMvc;

    private PhieuChi phieuChi;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhieuChi createEntity(EntityManager em) {
        PhieuChi phieuChi = new PhieuChi().ngayChungTu(DEFAULT_NGAY_CHUNG_TU).soTien(DEFAULT_SO_TIEN);
        // Add required entity
        KhachHang khachHang;
        if (TestUtil.findAll(em, KhachHang.class).isEmpty()) {
            khachHang = KhachHangResourceIT.createEntity(em);
            em.persist(khachHang);
            em.flush();
        } else {
            khachHang = TestUtil.findAll(em, KhachHang.class).get(0);
        }
        phieuChi.setKhachHang(khachHang);
        // Add required entity
        KhoanMucChiPhi khoanMucChiPhi;
        if (TestUtil.findAll(em, KhoanMucChiPhi.class).isEmpty()) {
            khoanMucChiPhi = KhoanMucChiPhiResourceIT.createEntity(em);
            em.persist(khoanMucChiPhi);
            em.flush();
        } else {
            khoanMucChiPhi = TestUtil.findAll(em, KhoanMucChiPhi.class).get(0);
        }
        phieuChi.setKhoanMucChiPhi(khoanMucChiPhi);
        // Add required entity
        QuyTien quyTien;
        if (TestUtil.findAll(em, QuyTien.class).isEmpty()) {
            quyTien = QuyTienResourceIT.createEntity(em);
            em.persist(quyTien);
            em.flush();
        } else {
            quyTien = TestUtil.findAll(em, QuyTien.class).get(0);
        }
        phieuChi.setQuyTien(quyTien);
        return phieuChi;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhieuChi createUpdatedEntity(EntityManager em) {
        PhieuChi phieuChi = new PhieuChi().ngayChungTu(UPDATED_NGAY_CHUNG_TU).soTien(UPDATED_SO_TIEN);
        // Add required entity
        KhachHang khachHang;
        if (TestUtil.findAll(em, KhachHang.class).isEmpty()) {
            khachHang = KhachHangResourceIT.createUpdatedEntity(em);
            em.persist(khachHang);
            em.flush();
        } else {
            khachHang = TestUtil.findAll(em, KhachHang.class).get(0);
        }
        phieuChi.setKhachHang(khachHang);
        // Add required entity
        KhoanMucChiPhi khoanMucChiPhi;
        if (TestUtil.findAll(em, KhoanMucChiPhi.class).isEmpty()) {
            khoanMucChiPhi = KhoanMucChiPhiResourceIT.createUpdatedEntity(em);
            em.persist(khoanMucChiPhi);
            em.flush();
        } else {
            khoanMucChiPhi = TestUtil.findAll(em, KhoanMucChiPhi.class).get(0);
        }
        phieuChi.setKhoanMucChiPhi(khoanMucChiPhi);
        // Add required entity
        QuyTien quyTien;
        if (TestUtil.findAll(em, QuyTien.class).isEmpty()) {
            quyTien = QuyTienResourceIT.createUpdatedEntity(em);
            em.persist(quyTien);
            em.flush();
        } else {
            quyTien = TestUtil.findAll(em, QuyTien.class).get(0);
        }
        phieuChi.setQuyTien(quyTien);
        return phieuChi;
    }

    @BeforeEach
    public void initTest() {
        phieuChi = createEntity(em);
    }

    @Test
    @Transactional
    void createPhieuChi() throws Exception {
        int databaseSizeBeforeCreate = phieuChiRepository.findAll().size();
        // Create the PhieuChi
        PhieuChiDTO phieuChiDTO = phieuChiMapper.toDto(phieuChi);
        restPhieuChiMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phieuChiDTO)))
            .andExpect(status().isCreated());

        // Validate the PhieuChi in the database
        List<PhieuChi> phieuChiList = phieuChiRepository.findAll();
        assertThat(phieuChiList).hasSize(databaseSizeBeforeCreate + 1);
        PhieuChi testPhieuChi = phieuChiList.get(phieuChiList.size() - 1);
        assertThat(testPhieuChi.getNgayChungTu()).isEqualTo(DEFAULT_NGAY_CHUNG_TU);
        assertThat(testPhieuChi.getSoTien()).isEqualTo(DEFAULT_SO_TIEN);
    }

    @Test
    @Transactional
    void createPhieuChiWithExistingId() throws Exception {
        // Create the PhieuChi with an existing ID
        phieuChi.setId(1L);
        PhieuChiDTO phieuChiDTO = phieuChiMapper.toDto(phieuChi);

        int databaseSizeBeforeCreate = phieuChiRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhieuChiMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phieuChiDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PhieuChi in the database
        List<PhieuChi> phieuChiList = phieuChiRepository.findAll();
        assertThat(phieuChiList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNgayChungTuIsRequired() throws Exception {
        int databaseSizeBeforeTest = phieuChiRepository.findAll().size();
        // set the field null
        phieuChi.setNgayChungTu(null);

        // Create the PhieuChi, which fails.
        PhieuChiDTO phieuChiDTO = phieuChiMapper.toDto(phieuChi);

        restPhieuChiMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phieuChiDTO)))
            .andExpect(status().isBadRequest());

        List<PhieuChi> phieuChiList = phieuChiRepository.findAll();
        assertThat(phieuChiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSoTienIsRequired() throws Exception {
        int databaseSizeBeforeTest = phieuChiRepository.findAll().size();
        // set the field null
        phieuChi.setSoTien(null);

        // Create the PhieuChi, which fails.
        PhieuChiDTO phieuChiDTO = phieuChiMapper.toDto(phieuChi);

        restPhieuChiMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phieuChiDTO)))
            .andExpect(status().isBadRequest());

        List<PhieuChi> phieuChiList = phieuChiRepository.findAll();
        assertThat(phieuChiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPhieuChis() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        // Get all the phieuChiList
        restPhieuChiMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phieuChi.getId().intValue())))
            .andExpect(jsonPath("$.[*].ngayChungTu").value(hasItem(DEFAULT_NGAY_CHUNG_TU.toString())))
            .andExpect(jsonPath("$.[*].soTien").value(hasItem(DEFAULT_SO_TIEN.doubleValue())));
    }

    @Test
    @Transactional
    void getPhieuChi() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        // Get the phieuChi
        restPhieuChiMockMvc
            .perform(get(ENTITY_API_URL_ID, phieuChi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(phieuChi.getId().intValue()))
            .andExpect(jsonPath("$.ngayChungTu").value(DEFAULT_NGAY_CHUNG_TU.toString()))
            .andExpect(jsonPath("$.soTien").value(DEFAULT_SO_TIEN.doubleValue()));
    }

    @Test
    @Transactional
    void getPhieuChisByIdFiltering() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        Long id = phieuChi.getId();

        defaultPhieuChiShouldBeFound("id.equals=" + id);
        defaultPhieuChiShouldNotBeFound("id.notEquals=" + id);

        defaultPhieuChiShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPhieuChiShouldNotBeFound("id.greaterThan=" + id);

        defaultPhieuChiShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPhieuChiShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPhieuChisByNgayChungTuIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        // Get all the phieuChiList where ngayChungTu equals to DEFAULT_NGAY_CHUNG_TU
        defaultPhieuChiShouldBeFound("ngayChungTu.equals=" + DEFAULT_NGAY_CHUNG_TU);

        // Get all the phieuChiList where ngayChungTu equals to UPDATED_NGAY_CHUNG_TU
        defaultPhieuChiShouldNotBeFound("ngayChungTu.equals=" + UPDATED_NGAY_CHUNG_TU);
    }

    @Test
    @Transactional
    void getAllPhieuChisByNgayChungTuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        // Get all the phieuChiList where ngayChungTu not equals to DEFAULT_NGAY_CHUNG_TU
        defaultPhieuChiShouldNotBeFound("ngayChungTu.notEquals=" + DEFAULT_NGAY_CHUNG_TU);

        // Get all the phieuChiList where ngayChungTu not equals to UPDATED_NGAY_CHUNG_TU
        defaultPhieuChiShouldBeFound("ngayChungTu.notEquals=" + UPDATED_NGAY_CHUNG_TU);
    }

    @Test
    @Transactional
    void getAllPhieuChisByNgayChungTuIsInShouldWork() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        // Get all the phieuChiList where ngayChungTu in DEFAULT_NGAY_CHUNG_TU or UPDATED_NGAY_CHUNG_TU
        defaultPhieuChiShouldBeFound("ngayChungTu.in=" + DEFAULT_NGAY_CHUNG_TU + "," + UPDATED_NGAY_CHUNG_TU);

        // Get all the phieuChiList where ngayChungTu equals to UPDATED_NGAY_CHUNG_TU
        defaultPhieuChiShouldNotBeFound("ngayChungTu.in=" + UPDATED_NGAY_CHUNG_TU);
    }

    @Test
    @Transactional
    void getAllPhieuChisByNgayChungTuIsNullOrNotNull() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        // Get all the phieuChiList where ngayChungTu is not null
        defaultPhieuChiShouldBeFound("ngayChungTu.specified=true");

        // Get all the phieuChiList where ngayChungTu is null
        defaultPhieuChiShouldNotBeFound("ngayChungTu.specified=false");
    }

    @Test
    @Transactional
    void getAllPhieuChisBySoTienIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        // Get all the phieuChiList where soTien equals to DEFAULT_SO_TIEN
        defaultPhieuChiShouldBeFound("soTien.equals=" + DEFAULT_SO_TIEN);

        // Get all the phieuChiList where soTien equals to UPDATED_SO_TIEN
        defaultPhieuChiShouldNotBeFound("soTien.equals=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllPhieuChisBySoTienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        // Get all the phieuChiList where soTien not equals to DEFAULT_SO_TIEN
        defaultPhieuChiShouldNotBeFound("soTien.notEquals=" + DEFAULT_SO_TIEN);

        // Get all the phieuChiList where soTien not equals to UPDATED_SO_TIEN
        defaultPhieuChiShouldBeFound("soTien.notEquals=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllPhieuChisBySoTienIsInShouldWork() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        // Get all the phieuChiList where soTien in DEFAULT_SO_TIEN or UPDATED_SO_TIEN
        defaultPhieuChiShouldBeFound("soTien.in=" + DEFAULT_SO_TIEN + "," + UPDATED_SO_TIEN);

        // Get all the phieuChiList where soTien equals to UPDATED_SO_TIEN
        defaultPhieuChiShouldNotBeFound("soTien.in=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllPhieuChisBySoTienIsNullOrNotNull() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        // Get all the phieuChiList where soTien is not null
        defaultPhieuChiShouldBeFound("soTien.specified=true");

        // Get all the phieuChiList where soTien is null
        defaultPhieuChiShouldNotBeFound("soTien.specified=false");
    }

    @Test
    @Transactional
    void getAllPhieuChisBySoTienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        // Get all the phieuChiList where soTien is greater than or equal to DEFAULT_SO_TIEN
        defaultPhieuChiShouldBeFound("soTien.greaterThanOrEqual=" + DEFAULT_SO_TIEN);

        // Get all the phieuChiList where soTien is greater than or equal to UPDATED_SO_TIEN
        defaultPhieuChiShouldNotBeFound("soTien.greaterThanOrEqual=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllPhieuChisBySoTienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        // Get all the phieuChiList where soTien is less than or equal to DEFAULT_SO_TIEN
        defaultPhieuChiShouldBeFound("soTien.lessThanOrEqual=" + DEFAULT_SO_TIEN);

        // Get all the phieuChiList where soTien is less than or equal to SMALLER_SO_TIEN
        defaultPhieuChiShouldNotBeFound("soTien.lessThanOrEqual=" + SMALLER_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllPhieuChisBySoTienIsLessThanSomething() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        // Get all the phieuChiList where soTien is less than DEFAULT_SO_TIEN
        defaultPhieuChiShouldNotBeFound("soTien.lessThan=" + DEFAULT_SO_TIEN);

        // Get all the phieuChiList where soTien is less than UPDATED_SO_TIEN
        defaultPhieuChiShouldBeFound("soTien.lessThan=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllPhieuChisBySoTienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        // Get all the phieuChiList where soTien is greater than DEFAULT_SO_TIEN
        defaultPhieuChiShouldNotBeFound("soTien.greaterThan=" + DEFAULT_SO_TIEN);

        // Get all the phieuChiList where soTien is greater than SMALLER_SO_TIEN
        defaultPhieuChiShouldBeFound("soTien.greaterThan=" + SMALLER_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllPhieuChisByKhachHangIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);
        KhachHang khachHang = KhachHangResourceIT.createEntity(em);
        em.persist(khachHang);
        em.flush();
        phieuChi.setKhachHang(khachHang);
        phieuChiRepository.saveAndFlush(phieuChi);
        Long khachHangId = khachHang.getId();

        // Get all the phieuChiList where khachHang equals to khachHangId
        defaultPhieuChiShouldBeFound("khachHangId.equals=" + khachHangId);

        // Get all the phieuChiList where khachHang equals to (khachHangId + 1)
        defaultPhieuChiShouldNotBeFound("khachHangId.equals=" + (khachHangId + 1));
    }

    @Test
    @Transactional
    void getAllPhieuChisByKhoanMucChiPhiIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);
        KhoanMucChiPhi khoanMucChiPhi = KhoanMucChiPhiResourceIT.createEntity(em);
        em.persist(khoanMucChiPhi);
        em.flush();
        phieuChi.setKhoanMucChiPhi(khoanMucChiPhi);
        phieuChiRepository.saveAndFlush(phieuChi);
        Long khoanMucChiPhiId = khoanMucChiPhi.getId();

        // Get all the phieuChiList where khoanMucChiPhi equals to khoanMucChiPhiId
        defaultPhieuChiShouldBeFound("khoanMucChiPhiId.equals=" + khoanMucChiPhiId);

        // Get all the phieuChiList where khoanMucChiPhi equals to (khoanMucChiPhiId + 1)
        defaultPhieuChiShouldNotBeFound("khoanMucChiPhiId.equals=" + (khoanMucChiPhiId + 1));
    }

    @Test
    @Transactional
    void getAllPhieuChisByKhoanPhaiTraIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);
        KhoanPhaiTra khoanPhaiTra = KhoanPhaiTraResourceIT.createEntity(em);
        em.persist(khoanPhaiTra);
        em.flush();
        phieuChi.setKhoanPhaiTra(khoanPhaiTra);
        phieuChiRepository.saveAndFlush(phieuChi);
        Long khoanPhaiTraId = khoanPhaiTra.getId();

        // Get all the phieuChiList where khoanPhaiTra equals to khoanPhaiTraId
        defaultPhieuChiShouldBeFound("khoanPhaiTraId.equals=" + khoanPhaiTraId);

        // Get all the phieuChiList where khoanPhaiTra equals to (khoanPhaiTraId + 1)
        defaultPhieuChiShouldNotBeFound("khoanPhaiTraId.equals=" + (khoanPhaiTraId + 1));
    }

    @Test
    @Transactional
    void getAllPhieuChisByQuyTienIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);
        QuyTien quyTien = QuyTienResourceIT.createEntity(em);
        em.persist(quyTien);
        em.flush();
        phieuChi.setQuyTien(quyTien);
        phieuChiRepository.saveAndFlush(phieuChi);
        Long quyTienId = quyTien.getId();

        // Get all the phieuChiList where quyTien equals to quyTienId
        defaultPhieuChiShouldBeFound("quyTienId.equals=" + quyTienId);

        // Get all the phieuChiList where quyTien equals to (quyTienId + 1)
        defaultPhieuChiShouldNotBeFound("quyTienId.equals=" + (quyTienId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPhieuChiShouldBeFound(String filter) throws Exception {
        restPhieuChiMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phieuChi.getId().intValue())))
            .andExpect(jsonPath("$.[*].ngayChungTu").value(hasItem(DEFAULT_NGAY_CHUNG_TU.toString())))
            .andExpect(jsonPath("$.[*].soTien").value(hasItem(DEFAULT_SO_TIEN.doubleValue())));

        // Check, that the count call also returns 1
        restPhieuChiMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPhieuChiShouldNotBeFound(String filter) throws Exception {
        restPhieuChiMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPhieuChiMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPhieuChi() throws Exception {
        // Get the phieuChi
        restPhieuChiMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPhieuChi() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        int databaseSizeBeforeUpdate = phieuChiRepository.findAll().size();

        // Update the phieuChi
        PhieuChi updatedPhieuChi = phieuChiRepository.findById(phieuChi.getId()).get();
        // Disconnect from session so that the updates on updatedPhieuChi are not directly saved in db
        em.detach(updatedPhieuChi);
        updatedPhieuChi.ngayChungTu(UPDATED_NGAY_CHUNG_TU).soTien(UPDATED_SO_TIEN);
        PhieuChiDTO phieuChiDTO = phieuChiMapper.toDto(updatedPhieuChi);

        restPhieuChiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, phieuChiDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(phieuChiDTO))
            )
            .andExpect(status().isOk());

        // Validate the PhieuChi in the database
        List<PhieuChi> phieuChiList = phieuChiRepository.findAll();
        assertThat(phieuChiList).hasSize(databaseSizeBeforeUpdate);
        PhieuChi testPhieuChi = phieuChiList.get(phieuChiList.size() - 1);
        assertThat(testPhieuChi.getNgayChungTu()).isEqualTo(UPDATED_NGAY_CHUNG_TU);
        assertThat(testPhieuChi.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void putNonExistingPhieuChi() throws Exception {
        int databaseSizeBeforeUpdate = phieuChiRepository.findAll().size();
        phieuChi.setId(count.incrementAndGet());

        // Create the PhieuChi
        PhieuChiDTO phieuChiDTO = phieuChiMapper.toDto(phieuChi);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhieuChiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, phieuChiDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(phieuChiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhieuChi in the database
        List<PhieuChi> phieuChiList = phieuChiRepository.findAll();
        assertThat(phieuChiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPhieuChi() throws Exception {
        int databaseSizeBeforeUpdate = phieuChiRepository.findAll().size();
        phieuChi.setId(count.incrementAndGet());

        // Create the PhieuChi
        PhieuChiDTO phieuChiDTO = phieuChiMapper.toDto(phieuChi);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhieuChiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(phieuChiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhieuChi in the database
        List<PhieuChi> phieuChiList = phieuChiRepository.findAll();
        assertThat(phieuChiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPhieuChi() throws Exception {
        int databaseSizeBeforeUpdate = phieuChiRepository.findAll().size();
        phieuChi.setId(count.incrementAndGet());

        // Create the PhieuChi
        PhieuChiDTO phieuChiDTO = phieuChiMapper.toDto(phieuChi);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhieuChiMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phieuChiDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the PhieuChi in the database
        List<PhieuChi> phieuChiList = phieuChiRepository.findAll();
        assertThat(phieuChiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePhieuChiWithPatch() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        int databaseSizeBeforeUpdate = phieuChiRepository.findAll().size();

        // Update the phieuChi using partial update
        PhieuChi partialUpdatedPhieuChi = new PhieuChi();
        partialUpdatedPhieuChi.setId(phieuChi.getId());

        partialUpdatedPhieuChi.ngayChungTu(UPDATED_NGAY_CHUNG_TU).soTien(UPDATED_SO_TIEN);

        restPhieuChiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPhieuChi.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPhieuChi))
            )
            .andExpect(status().isOk());

        // Validate the PhieuChi in the database
        List<PhieuChi> phieuChiList = phieuChiRepository.findAll();
        assertThat(phieuChiList).hasSize(databaseSizeBeforeUpdate);
        PhieuChi testPhieuChi = phieuChiList.get(phieuChiList.size() - 1);
        assertThat(testPhieuChi.getNgayChungTu()).isEqualTo(UPDATED_NGAY_CHUNG_TU);
        assertThat(testPhieuChi.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void fullUpdatePhieuChiWithPatch() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        int databaseSizeBeforeUpdate = phieuChiRepository.findAll().size();

        // Update the phieuChi using partial update
        PhieuChi partialUpdatedPhieuChi = new PhieuChi();
        partialUpdatedPhieuChi.setId(phieuChi.getId());

        partialUpdatedPhieuChi.ngayChungTu(UPDATED_NGAY_CHUNG_TU).soTien(UPDATED_SO_TIEN);

        restPhieuChiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPhieuChi.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPhieuChi))
            )
            .andExpect(status().isOk());

        // Validate the PhieuChi in the database
        List<PhieuChi> phieuChiList = phieuChiRepository.findAll();
        assertThat(phieuChiList).hasSize(databaseSizeBeforeUpdate);
        PhieuChi testPhieuChi = phieuChiList.get(phieuChiList.size() - 1);
        assertThat(testPhieuChi.getNgayChungTu()).isEqualTo(UPDATED_NGAY_CHUNG_TU);
        assertThat(testPhieuChi.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void patchNonExistingPhieuChi() throws Exception {
        int databaseSizeBeforeUpdate = phieuChiRepository.findAll().size();
        phieuChi.setId(count.incrementAndGet());

        // Create the PhieuChi
        PhieuChiDTO phieuChiDTO = phieuChiMapper.toDto(phieuChi);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhieuChiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, phieuChiDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(phieuChiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhieuChi in the database
        List<PhieuChi> phieuChiList = phieuChiRepository.findAll();
        assertThat(phieuChiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPhieuChi() throws Exception {
        int databaseSizeBeforeUpdate = phieuChiRepository.findAll().size();
        phieuChi.setId(count.incrementAndGet());

        // Create the PhieuChi
        PhieuChiDTO phieuChiDTO = phieuChiMapper.toDto(phieuChi);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhieuChiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(phieuChiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhieuChi in the database
        List<PhieuChi> phieuChiList = phieuChiRepository.findAll();
        assertThat(phieuChiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPhieuChi() throws Exception {
        int databaseSizeBeforeUpdate = phieuChiRepository.findAll().size();
        phieuChi.setId(count.incrementAndGet());

        // Create the PhieuChi
        PhieuChiDTO phieuChiDTO = phieuChiMapper.toDto(phieuChi);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhieuChiMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(phieuChiDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PhieuChi in the database
        List<PhieuChi> phieuChiList = phieuChiRepository.findAll();
        assertThat(phieuChiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePhieuChi() throws Exception {
        // Initialize the database
        phieuChiRepository.saveAndFlush(phieuChi);

        int databaseSizeBeforeDelete = phieuChiRepository.findAll().size();

        // Delete the phieuChi
        restPhieuChiMockMvc
            .perform(delete(ENTITY_API_URL_ID, phieuChi.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PhieuChi> phieuChiList = phieuChiRepository.findAll();
        assertThat(phieuChiList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
