package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.KhachSan;
import com.group3.finmansys.domain.NganSachNam;
import com.group3.finmansys.domain.NganSachThang;
import com.group3.finmansys.repository.NganSachNamRepository;
import com.group3.finmansys.service.criteria.NganSachNamCriteria;
import com.group3.finmansys.service.dto.NganSachNamDTO;
import com.group3.finmansys.service.mapper.NganSachNamMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link NganSachNamResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class NganSachNamResourceIT {

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final Integer DEFAULT_NAM = 1;
    private static final Integer UPDATED_NAM = 2;
    private static final Integer SMALLER_NAM = 1 - 1;

    private static final Double DEFAULT_SO_TIEN = 1D;
    private static final Double UPDATED_SO_TIEN = 2D;
    private static final Double SMALLER_SO_TIEN = 1D - 1D;

    private static final String ENTITY_API_URL = "/api/ngan-sach-nams";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private NganSachNamRepository nganSachNamRepository;

    @Autowired
    private NganSachNamMapper nganSachNamMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restNganSachNamMockMvc;

    private NganSachNam nganSachNam;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NganSachNam createEntity(EntityManager em) {
        NganSachNam nganSachNam = new NganSachNam().ten(DEFAULT_TEN).nam(DEFAULT_NAM).soTien(DEFAULT_SO_TIEN);
        // Add required entity
        KhachSan khachSan;
        if (TestUtil.findAll(em, KhachSan.class).isEmpty()) {
            khachSan = KhachSanResourceIT.createEntity(em);
            em.persist(khachSan);
            em.flush();
        } else {
            khachSan = TestUtil.findAll(em, KhachSan.class).get(0);
        }
        nganSachNam.setKhachSan(khachSan);
        return nganSachNam;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NganSachNam createUpdatedEntity(EntityManager em) {
        NganSachNam nganSachNam = new NganSachNam().ten(UPDATED_TEN).nam(UPDATED_NAM).soTien(UPDATED_SO_TIEN);
        // Add required entity
        KhachSan khachSan;
        if (TestUtil.findAll(em, KhachSan.class).isEmpty()) {
            khachSan = KhachSanResourceIT.createUpdatedEntity(em);
            em.persist(khachSan);
            em.flush();
        } else {
            khachSan = TestUtil.findAll(em, KhachSan.class).get(0);
        }
        nganSachNam.setKhachSan(khachSan);
        return nganSachNam;
    }

    @BeforeEach
    public void initTest() {
        nganSachNam = createEntity(em);
    }

    @Test
    @Transactional
    void createNganSachNam() throws Exception {
        int databaseSizeBeforeCreate = nganSachNamRepository.findAll().size();
        // Create the NganSachNam
        NganSachNamDTO nganSachNamDTO = nganSachNamMapper.toDto(nganSachNam);
        restNganSachNamMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(nganSachNamDTO))
            )
            .andExpect(status().isCreated());

        // Validate the NganSachNam in the database
        List<NganSachNam> nganSachNamList = nganSachNamRepository.findAll();
        assertThat(nganSachNamList).hasSize(databaseSizeBeforeCreate + 1);
        NganSachNam testNganSachNam = nganSachNamList.get(nganSachNamList.size() - 1);
        assertThat(testNganSachNam.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testNganSachNam.getNam()).isEqualTo(DEFAULT_NAM);
        assertThat(testNganSachNam.getSoTien()).isEqualTo(DEFAULT_SO_TIEN);
    }

    @Test
    @Transactional
    void createNganSachNamWithExistingId() throws Exception {
        // Create the NganSachNam with an existing ID
        nganSachNam.setId(1L);
        NganSachNamDTO nganSachNamDTO = nganSachNamMapper.toDto(nganSachNam);

        int databaseSizeBeforeCreate = nganSachNamRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restNganSachNamMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(nganSachNamDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the NganSachNam in the database
        List<NganSachNam> nganSachNamList = nganSachNamRepository.findAll();
        assertThat(nganSachNamList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = nganSachNamRepository.findAll().size();
        // set the field null
        nganSachNam.setTen(null);

        // Create the NganSachNam, which fails.
        NganSachNamDTO nganSachNamDTO = nganSachNamMapper.toDto(nganSachNam);

        restNganSachNamMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(nganSachNamDTO))
            )
            .andExpect(status().isBadRequest());

        List<NganSachNam> nganSachNamList = nganSachNamRepository.findAll();
        assertThat(nganSachNamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkNamIsRequired() throws Exception {
        int databaseSizeBeforeTest = nganSachNamRepository.findAll().size();
        // set the field null
        nganSachNam.setNam(null);

        // Create the NganSachNam, which fails.
        NganSachNamDTO nganSachNamDTO = nganSachNamMapper.toDto(nganSachNam);

        restNganSachNamMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(nganSachNamDTO))
            )
            .andExpect(status().isBadRequest());

        List<NganSachNam> nganSachNamList = nganSachNamRepository.findAll();
        assertThat(nganSachNamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSoTienIsRequired() throws Exception {
        int databaseSizeBeforeTest = nganSachNamRepository.findAll().size();
        // set the field null
        nganSachNam.setSoTien(null);

        // Create the NganSachNam, which fails.
        NganSachNamDTO nganSachNamDTO = nganSachNamMapper.toDto(nganSachNam);

        restNganSachNamMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(nganSachNamDTO))
            )
            .andExpect(status().isBadRequest());

        List<NganSachNam> nganSachNamList = nganSachNamRepository.findAll();
        assertThat(nganSachNamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllNganSachNams() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList
        restNganSachNamMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nganSachNam.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)))
            .andExpect(jsonPath("$.[*].soTien").value(hasItem(DEFAULT_SO_TIEN.doubleValue())));
    }

    @Test
    @Transactional
    void getNganSachNam() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get the nganSachNam
        restNganSachNamMockMvc
            .perform(get(ENTITY_API_URL_ID, nganSachNam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(nganSachNam.getId().intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.nam").value(DEFAULT_NAM))
            .andExpect(jsonPath("$.soTien").value(DEFAULT_SO_TIEN.doubleValue()));
    }

    @Test
    @Transactional
    void getNganSachNamsByIdFiltering() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        Long id = nganSachNam.getId();

        defaultNganSachNamShouldBeFound("id.equals=" + id);
        defaultNganSachNamShouldNotBeFound("id.notEquals=" + id);

        defaultNganSachNamShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultNganSachNamShouldNotBeFound("id.greaterThan=" + id);

        defaultNganSachNamShouldBeFound("id.lessThanOrEqual=" + id);
        defaultNganSachNamShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllNganSachNamsByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where ten equals to DEFAULT_TEN
        defaultNganSachNamShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the nganSachNamList where ten equals to UPDATED_TEN
        defaultNganSachNamShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllNganSachNamsByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where ten not equals to DEFAULT_TEN
        defaultNganSachNamShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the nganSachNamList where ten not equals to UPDATED_TEN
        defaultNganSachNamShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllNganSachNamsByTenIsInShouldWork() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultNganSachNamShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the nganSachNamList where ten equals to UPDATED_TEN
        defaultNganSachNamShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllNganSachNamsByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where ten is not null
        defaultNganSachNamShouldBeFound("ten.specified=true");

        // Get all the nganSachNamList where ten is null
        defaultNganSachNamShouldNotBeFound("ten.specified=false");
    }

    @Test
    @Transactional
    void getAllNganSachNamsByTenContainsSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where ten contains DEFAULT_TEN
        defaultNganSachNamShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the nganSachNamList where ten contains UPDATED_TEN
        defaultNganSachNamShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllNganSachNamsByTenNotContainsSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where ten does not contain DEFAULT_TEN
        defaultNganSachNamShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the nganSachNamList where ten does not contain UPDATED_TEN
        defaultNganSachNamShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllNganSachNamsByNamIsEqualToSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where nam equals to DEFAULT_NAM
        defaultNganSachNamShouldBeFound("nam.equals=" + DEFAULT_NAM);

        // Get all the nganSachNamList where nam equals to UPDATED_NAM
        defaultNganSachNamShouldNotBeFound("nam.equals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    void getAllNganSachNamsByNamIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where nam not equals to DEFAULT_NAM
        defaultNganSachNamShouldNotBeFound("nam.notEquals=" + DEFAULT_NAM);

        // Get all the nganSachNamList where nam not equals to UPDATED_NAM
        defaultNganSachNamShouldBeFound("nam.notEquals=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    void getAllNganSachNamsByNamIsInShouldWork() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where nam in DEFAULT_NAM or UPDATED_NAM
        defaultNganSachNamShouldBeFound("nam.in=" + DEFAULT_NAM + "," + UPDATED_NAM);

        // Get all the nganSachNamList where nam equals to UPDATED_NAM
        defaultNganSachNamShouldNotBeFound("nam.in=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    void getAllNganSachNamsByNamIsNullOrNotNull() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where nam is not null
        defaultNganSachNamShouldBeFound("nam.specified=true");

        // Get all the nganSachNamList where nam is null
        defaultNganSachNamShouldNotBeFound("nam.specified=false");
    }

    @Test
    @Transactional
    void getAllNganSachNamsByNamIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where nam is greater than or equal to DEFAULT_NAM
        defaultNganSachNamShouldBeFound("nam.greaterThanOrEqual=" + DEFAULT_NAM);

        // Get all the nganSachNamList where nam is greater than or equal to UPDATED_NAM
        defaultNganSachNamShouldNotBeFound("nam.greaterThanOrEqual=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    void getAllNganSachNamsByNamIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where nam is less than or equal to DEFAULT_NAM
        defaultNganSachNamShouldBeFound("nam.lessThanOrEqual=" + DEFAULT_NAM);

        // Get all the nganSachNamList where nam is less than or equal to SMALLER_NAM
        defaultNganSachNamShouldNotBeFound("nam.lessThanOrEqual=" + SMALLER_NAM);
    }

    @Test
    @Transactional
    void getAllNganSachNamsByNamIsLessThanSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where nam is less than DEFAULT_NAM
        defaultNganSachNamShouldNotBeFound("nam.lessThan=" + DEFAULT_NAM);

        // Get all the nganSachNamList where nam is less than UPDATED_NAM
        defaultNganSachNamShouldBeFound("nam.lessThan=" + UPDATED_NAM);
    }

    @Test
    @Transactional
    void getAllNganSachNamsByNamIsGreaterThanSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where nam is greater than DEFAULT_NAM
        defaultNganSachNamShouldNotBeFound("nam.greaterThan=" + DEFAULT_NAM);

        // Get all the nganSachNamList where nam is greater than SMALLER_NAM
        defaultNganSachNamShouldBeFound("nam.greaterThan=" + SMALLER_NAM);
    }

    @Test
    @Transactional
    void getAllNganSachNamsBySoTienIsEqualToSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where soTien equals to DEFAULT_SO_TIEN
        defaultNganSachNamShouldBeFound("soTien.equals=" + DEFAULT_SO_TIEN);

        // Get all the nganSachNamList where soTien equals to UPDATED_SO_TIEN
        defaultNganSachNamShouldNotBeFound("soTien.equals=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllNganSachNamsBySoTienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where soTien not equals to DEFAULT_SO_TIEN
        defaultNganSachNamShouldNotBeFound("soTien.notEquals=" + DEFAULT_SO_TIEN);

        // Get all the nganSachNamList where soTien not equals to UPDATED_SO_TIEN
        defaultNganSachNamShouldBeFound("soTien.notEquals=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllNganSachNamsBySoTienIsInShouldWork() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where soTien in DEFAULT_SO_TIEN or UPDATED_SO_TIEN
        defaultNganSachNamShouldBeFound("soTien.in=" + DEFAULT_SO_TIEN + "," + UPDATED_SO_TIEN);

        // Get all the nganSachNamList where soTien equals to UPDATED_SO_TIEN
        defaultNganSachNamShouldNotBeFound("soTien.in=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllNganSachNamsBySoTienIsNullOrNotNull() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where soTien is not null
        defaultNganSachNamShouldBeFound("soTien.specified=true");

        // Get all the nganSachNamList where soTien is null
        defaultNganSachNamShouldNotBeFound("soTien.specified=false");
    }

    @Test
    @Transactional
    void getAllNganSachNamsBySoTienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where soTien is greater than or equal to DEFAULT_SO_TIEN
        defaultNganSachNamShouldBeFound("soTien.greaterThanOrEqual=" + DEFAULT_SO_TIEN);

        // Get all the nganSachNamList where soTien is greater than or equal to UPDATED_SO_TIEN
        defaultNganSachNamShouldNotBeFound("soTien.greaterThanOrEqual=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllNganSachNamsBySoTienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where soTien is less than or equal to DEFAULT_SO_TIEN
        defaultNganSachNamShouldBeFound("soTien.lessThanOrEqual=" + DEFAULT_SO_TIEN);

        // Get all the nganSachNamList where soTien is less than or equal to SMALLER_SO_TIEN
        defaultNganSachNamShouldNotBeFound("soTien.lessThanOrEqual=" + SMALLER_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllNganSachNamsBySoTienIsLessThanSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where soTien is less than DEFAULT_SO_TIEN
        defaultNganSachNamShouldNotBeFound("soTien.lessThan=" + DEFAULT_SO_TIEN);

        // Get all the nganSachNamList where soTien is less than UPDATED_SO_TIEN
        defaultNganSachNamShouldBeFound("soTien.lessThan=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllNganSachNamsBySoTienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        // Get all the nganSachNamList where soTien is greater than DEFAULT_SO_TIEN
        defaultNganSachNamShouldNotBeFound("soTien.greaterThan=" + DEFAULT_SO_TIEN);

        // Get all the nganSachNamList where soTien is greater than SMALLER_SO_TIEN
        defaultNganSachNamShouldBeFound("soTien.greaterThan=" + SMALLER_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllNganSachNamsByKhachSanIsEqualToSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);
        KhachSan khachSan = KhachSanResourceIT.createEntity(em);
        khachSan.setId(1L);
        khachSan.setTen("CCCCCCCCCCCC");
        khachSan = em.merge(khachSan);
        em.flush();
        nganSachNam.setKhachSan(khachSan);
        nganSachNamRepository.saveAndFlush(nganSachNam);
        Long khachSanId = khachSan.getId();

        // Get all the nganSachNamList where khachSan equals to khachSanId
        defaultNganSachNamShouldBeFound("khachSanId.equals=" + khachSanId);

        // Get all the nganSachNamList where khachSan equals to (khachSanId + 1)
        defaultNganSachNamShouldNotBeFound("khachSanId.equals=" + (khachSanId + 1));
    }

    @Test
    @Transactional
    void getAllNganSachNamsByNganSachThangIsEqualToSomething() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);
        NganSachThang nganSachThang = NganSachThangResourceIT.createEntity(em);
        em.persist(nganSachThang);
        em.flush();
        nganSachNam.addNganSachThang(nganSachThang);
        nganSachNamRepository.saveAndFlush(nganSachNam);
        Long nganSachThangId = nganSachThang.getId();

        // Get all the nganSachNamList where nganSachThang equals to nganSachThangId
        defaultNganSachNamShouldBeFound("nganSachThangId.equals=" + nganSachThangId);

        // Get all the nganSachNamList where nganSachThang equals to (nganSachThangId + 1)
        defaultNganSachNamShouldNotBeFound("nganSachThangId.equals=" + (nganSachThangId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNganSachNamShouldBeFound(String filter) throws Exception {
        restNganSachNamMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nganSachNam.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].nam").value(hasItem(DEFAULT_NAM)))
            .andExpect(jsonPath("$.[*].soTien").value(hasItem(DEFAULT_SO_TIEN.doubleValue())));

        // Check, that the count call also returns 1
        restNganSachNamMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNganSachNamShouldNotBeFound(String filter) throws Exception {
        restNganSachNamMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNganSachNamMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingNganSachNam() throws Exception {
        // Get the nganSachNam
        restNganSachNamMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewNganSachNam() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        int databaseSizeBeforeUpdate = nganSachNamRepository.findAll().size();

        // Update the nganSachNam
        NganSachNam updatedNganSachNam = nganSachNamRepository.findById(nganSachNam.getId()).get();
        // Disconnect from session so that the updates on updatedNganSachNam are not directly saved in db
        em.detach(updatedNganSachNam);
        updatedNganSachNam.ten(UPDATED_TEN).nam(UPDATED_NAM).soTien(UPDATED_SO_TIEN);
        NganSachNamDTO nganSachNamDTO = nganSachNamMapper.toDto(updatedNganSachNam);

        restNganSachNamMockMvc
            .perform(
                put(ENTITY_API_URL_ID, nganSachNamDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(nganSachNamDTO))
            )
            .andExpect(status().isOk());

        // Validate the NganSachNam in the database
        List<NganSachNam> nganSachNamList = nganSachNamRepository.findAll();
        assertThat(nganSachNamList).hasSize(databaseSizeBeforeUpdate);
        NganSachNam testNganSachNam = nganSachNamList.get(nganSachNamList.size() - 1);
        assertThat(testNganSachNam.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testNganSachNam.getNam()).isEqualTo(UPDATED_NAM);
        assertThat(testNganSachNam.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void putNonExistingNganSachNam() throws Exception {
        int databaseSizeBeforeUpdate = nganSachNamRepository.findAll().size();
        nganSachNam.setId(count.incrementAndGet());

        // Create the NganSachNam
        NganSachNamDTO nganSachNamDTO = nganSachNamMapper.toDto(nganSachNam);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNganSachNamMockMvc
            .perform(
                put(ENTITY_API_URL_ID, nganSachNamDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(nganSachNamDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the NganSachNam in the database
        List<NganSachNam> nganSachNamList = nganSachNamRepository.findAll();
        assertThat(nganSachNamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchNganSachNam() throws Exception {
        int databaseSizeBeforeUpdate = nganSachNamRepository.findAll().size();
        nganSachNam.setId(count.incrementAndGet());

        // Create the NganSachNam
        NganSachNamDTO nganSachNamDTO = nganSachNamMapper.toDto(nganSachNam);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNganSachNamMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(nganSachNamDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the NganSachNam in the database
        List<NganSachNam> nganSachNamList = nganSachNamRepository.findAll();
        assertThat(nganSachNamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamNganSachNam() throws Exception {
        int databaseSizeBeforeUpdate = nganSachNamRepository.findAll().size();
        nganSachNam.setId(count.incrementAndGet());

        // Create the NganSachNam
        NganSachNamDTO nganSachNamDTO = nganSachNamMapper.toDto(nganSachNam);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNganSachNamMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(nganSachNamDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the NganSachNam in the database
        List<NganSachNam> nganSachNamList = nganSachNamRepository.findAll();
        assertThat(nganSachNamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateNganSachNamWithPatch() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        int databaseSizeBeforeUpdate = nganSachNamRepository.findAll().size();

        // Update the nganSachNam using partial update
        NganSachNam partialUpdatedNganSachNam = new NganSachNam();
        partialUpdatedNganSachNam.setId(nganSachNam.getId());

        partialUpdatedNganSachNam.ten(UPDATED_TEN).nam(UPDATED_NAM);

        restNganSachNamMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedNganSachNam.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedNganSachNam))
            )
            .andExpect(status().isOk());

        // Validate the NganSachNam in the database
        List<NganSachNam> nganSachNamList = nganSachNamRepository.findAll();
        assertThat(nganSachNamList).hasSize(databaseSizeBeforeUpdate);
        NganSachNam testNganSachNam = nganSachNamList.get(nganSachNamList.size() - 1);
        assertThat(testNganSachNam.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testNganSachNam.getNam()).isEqualTo(UPDATED_NAM);
        assertThat(testNganSachNam.getSoTien()).isEqualTo(DEFAULT_SO_TIEN);
    }

    @Test
    @Transactional
    void fullUpdateNganSachNamWithPatch() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        int databaseSizeBeforeUpdate = nganSachNamRepository.findAll().size();

        // Update the nganSachNam using partial update
        NganSachNam partialUpdatedNganSachNam = new NganSachNam();
        partialUpdatedNganSachNam.setId(nganSachNam.getId());

        partialUpdatedNganSachNam.ten(UPDATED_TEN).nam(UPDATED_NAM).soTien(UPDATED_SO_TIEN);

        restNganSachNamMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedNganSachNam.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedNganSachNam))
            )
            .andExpect(status().isOk());

        // Validate the NganSachNam in the database
        List<NganSachNam> nganSachNamList = nganSachNamRepository.findAll();
        assertThat(nganSachNamList).hasSize(databaseSizeBeforeUpdate);
        NganSachNam testNganSachNam = nganSachNamList.get(nganSachNamList.size() - 1);
        assertThat(testNganSachNam.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testNganSachNam.getNam()).isEqualTo(UPDATED_NAM);
        assertThat(testNganSachNam.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void patchNonExistingNganSachNam() throws Exception {
        int databaseSizeBeforeUpdate = nganSachNamRepository.findAll().size();
        nganSachNam.setId(count.incrementAndGet());

        // Create the NganSachNam
        NganSachNamDTO nganSachNamDTO = nganSachNamMapper.toDto(nganSachNam);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNganSachNamMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, nganSachNamDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(nganSachNamDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the NganSachNam in the database
        List<NganSachNam> nganSachNamList = nganSachNamRepository.findAll();
        assertThat(nganSachNamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchNganSachNam() throws Exception {
        int databaseSizeBeforeUpdate = nganSachNamRepository.findAll().size();
        nganSachNam.setId(count.incrementAndGet());

        // Create the NganSachNam
        NganSachNamDTO nganSachNamDTO = nganSachNamMapper.toDto(nganSachNam);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNganSachNamMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(nganSachNamDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the NganSachNam in the database
        List<NganSachNam> nganSachNamList = nganSachNamRepository.findAll();
        assertThat(nganSachNamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamNganSachNam() throws Exception {
        int databaseSizeBeforeUpdate = nganSachNamRepository.findAll().size();
        nganSachNam.setId(count.incrementAndGet());

        // Create the NganSachNam
        NganSachNamDTO nganSachNamDTO = nganSachNamMapper.toDto(nganSachNam);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNganSachNamMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(nganSachNamDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the NganSachNam in the database
        List<NganSachNam> nganSachNamList = nganSachNamRepository.findAll();
        assertThat(nganSachNamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteNganSachNam() throws Exception {
        // Initialize the database
        nganSachNamRepository.saveAndFlush(nganSachNam);

        int databaseSizeBeforeDelete = nganSachNamRepository.findAll().size();

        // Delete the nganSachNam
        restNganSachNamMockMvc
            .perform(delete(ENTITY_API_URL_ID, nganSachNam.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NganSachNam> nganSachNamList = nganSachNamRepository.findAll();
        assertThat(nganSachNamList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
