package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.KhachSan;
import com.group3.finmansys.domain.LoaiQuy;
import com.group3.finmansys.domain.MaKeToan;
import com.group3.finmansys.domain.PhieuChi;
import com.group3.finmansys.domain.PhieuThu;
import com.group3.finmansys.domain.QuyTien;
import com.group3.finmansys.repository.QuyTienRepository;
import com.group3.finmansys.service.criteria.QuyTienCriteria;
import com.group3.finmansys.service.dto.QuyTienDTO;
import com.group3.finmansys.service.mapper.QuyTienMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link QuyTienResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class QuyTienResourceIT {

    private static final Double DEFAULT_SO_TIEN = 1D;
    private static final Double UPDATED_SO_TIEN = 2D;
    private static final Double SMALLER_SO_TIEN = 1D - 1D;

    private static final String DEFAULT_GHI_CHU = "AAAAAAAAAA";
    private static final String UPDATED_GHI_CHU = "BBBBBBBBBB";

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/quy-tiens";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private QuyTienRepository quyTienRepository;

    @Autowired
    private QuyTienMapper quyTienMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restQuyTienMockMvc;

    private QuyTien quyTien;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static QuyTien createEntity(EntityManager em) {
        QuyTien quyTien = new QuyTien().soTien(DEFAULT_SO_TIEN).ghiChu(DEFAULT_GHI_CHU).ten(DEFAULT_TEN);
        // Add required entity
        LoaiQuy loaiQuy;
        if (TestUtil.findAll(em, LoaiQuy.class).isEmpty()) {
            loaiQuy = LoaiQuyResourceIT.createEntity(em);
            em.persist(loaiQuy);
            em.flush();
        } else {
            loaiQuy = TestUtil.findAll(em, LoaiQuy.class).get(0);
        }
        quyTien.setLoaiQuy(loaiQuy);
        // Add required entity
        MaKeToan maKeToan;
        if (TestUtil.findAll(em, MaKeToan.class).isEmpty()) {
            maKeToan = MaKeToanResourceIT.createEntity(em);
            em.persist(maKeToan);
            em.flush();
        } else {
            maKeToan = TestUtil.findAll(em, MaKeToan.class).get(0);
        }
        quyTien.setMaKeToan(maKeToan);
        // Add required entity
        KhachSan khachSan;
        if (TestUtil.findAll(em, KhachSan.class).isEmpty()) {
            khachSan = KhachSanResourceIT.createEntity(em);
            em.persist(khachSan);
            em.flush();
        } else {
            khachSan = TestUtil.findAll(em, KhachSan.class).get(0);
        }
        quyTien.setKhachSan(khachSan);
        return quyTien;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static QuyTien createUpdatedEntity(EntityManager em) {
        QuyTien quyTien = new QuyTien().soTien(UPDATED_SO_TIEN).ghiChu(UPDATED_GHI_CHU).ten(UPDATED_TEN);
        // Add required entity
        LoaiQuy loaiQuy;
        if (TestUtil.findAll(em, LoaiQuy.class).isEmpty()) {
            loaiQuy = LoaiQuyResourceIT.createUpdatedEntity(em);
            em.persist(loaiQuy);
            em.flush();
        } else {
            loaiQuy = TestUtil.findAll(em, LoaiQuy.class).get(0);
        }
        quyTien.setLoaiQuy(loaiQuy);
        // Add required entity
        MaKeToan maKeToan;
        if (TestUtil.findAll(em, MaKeToan.class).isEmpty()) {
            maKeToan = MaKeToanResourceIT.createUpdatedEntity(em);
            em.persist(maKeToan);
            em.flush();
        } else {
            maKeToan = TestUtil.findAll(em, MaKeToan.class).get(0);
        }
        quyTien.setMaKeToan(maKeToan);
        // Add required entity
        KhachSan khachSan;
        if (TestUtil.findAll(em, KhachSan.class).isEmpty()) {
            khachSan = KhachSanResourceIT.createUpdatedEntity(em);
            em.persist(khachSan);
            em.flush();
        } else {
            khachSan = TestUtil.findAll(em, KhachSan.class).get(0);
        }
        quyTien.setKhachSan(khachSan);
        return quyTien;
    }

    @BeforeEach
    public void initTest() {
        quyTien = createEntity(em);
    }

    @Test
    @Transactional
    void createQuyTien() throws Exception {
        int databaseSizeBeforeCreate = quyTienRepository.findAll().size();
        // Create the QuyTien
        QuyTienDTO quyTienDTO = quyTienMapper.toDto(quyTien);
        restQuyTienMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(quyTienDTO)))
            .andExpect(status().isCreated());

        // Validate the QuyTien in the database
        List<QuyTien> quyTienList = quyTienRepository.findAll();
        assertThat(quyTienList).hasSize(databaseSizeBeforeCreate + 1);
        QuyTien testQuyTien = quyTienList.get(quyTienList.size() - 1);
        assertThat(testQuyTien.getSoTien()).isEqualTo(DEFAULT_SO_TIEN);
        assertThat(testQuyTien.getGhiChu()).isEqualTo(DEFAULT_GHI_CHU);
        assertThat(testQuyTien.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    void createQuyTienWithExistingId() throws Exception {
        // Create the QuyTien with an existing ID
        quyTien.setId(1L);
        QuyTienDTO quyTienDTO = quyTienMapper.toDto(quyTien);

        int databaseSizeBeforeCreate = quyTienRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restQuyTienMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(quyTienDTO)))
            .andExpect(status().isBadRequest());

        // Validate the QuyTien in the database
        List<QuyTien> quyTienList = quyTienRepository.findAll();
        assertThat(quyTienList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkSoTienIsRequired() throws Exception {
        int databaseSizeBeforeTest = quyTienRepository.findAll().size();
        // set the field null
        quyTien.setSoTien(null);

        // Create the QuyTien, which fails.
        QuyTienDTO quyTienDTO = quyTienMapper.toDto(quyTien);

        restQuyTienMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(quyTienDTO)))
            .andExpect(status().isBadRequest());

        List<QuyTien> quyTienList = quyTienRepository.findAll();
        assertThat(quyTienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = quyTienRepository.findAll().size();
        // set the field null
        quyTien.setTen(null);

        // Create the QuyTien, which fails.
        QuyTienDTO quyTienDTO = quyTienMapper.toDto(quyTien);

        restQuyTienMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(quyTienDTO)))
            .andExpect(status().isBadRequest());

        List<QuyTien> quyTienList = quyTienRepository.findAll();
        assertThat(quyTienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllQuyTiens() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList
        restQuyTienMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(quyTien.getId().intValue())))
            .andExpect(jsonPath("$.[*].soTien").value(hasItem(DEFAULT_SO_TIEN.doubleValue())))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));
    }

    @Test
    @Transactional
    void getQuyTien() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get the quyTien
        restQuyTienMockMvc
            .perform(get(ENTITY_API_URL_ID, quyTien.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(quyTien.getId().intValue()))
            .andExpect(jsonPath("$.soTien").value(DEFAULT_SO_TIEN.doubleValue()))
            .andExpect(jsonPath("$.ghiChu").value(DEFAULT_GHI_CHU))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN));
    }

    @Test
    @Transactional
    void getQuyTiensByIdFiltering() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        Long id = quyTien.getId();

        defaultQuyTienShouldBeFound("id.equals=" + id);
        defaultQuyTienShouldNotBeFound("id.notEquals=" + id);

        defaultQuyTienShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultQuyTienShouldNotBeFound("id.greaterThan=" + id);

        defaultQuyTienShouldBeFound("id.lessThanOrEqual=" + id);
        defaultQuyTienShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllQuyTiensBySoTienIsEqualToSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where soTien equals to DEFAULT_SO_TIEN
        defaultQuyTienShouldBeFound("soTien.equals=" + DEFAULT_SO_TIEN);

        // Get all the quyTienList where soTien equals to UPDATED_SO_TIEN
        defaultQuyTienShouldNotBeFound("soTien.equals=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllQuyTiensBySoTienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where soTien not equals to DEFAULT_SO_TIEN
        defaultQuyTienShouldNotBeFound("soTien.notEquals=" + DEFAULT_SO_TIEN);

        // Get all the quyTienList where soTien not equals to UPDATED_SO_TIEN
        defaultQuyTienShouldBeFound("soTien.notEquals=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllQuyTiensBySoTienIsInShouldWork() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where soTien in DEFAULT_SO_TIEN or UPDATED_SO_TIEN
        defaultQuyTienShouldBeFound("soTien.in=" + DEFAULT_SO_TIEN + "," + UPDATED_SO_TIEN);

        // Get all the quyTienList where soTien equals to UPDATED_SO_TIEN
        defaultQuyTienShouldNotBeFound("soTien.in=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllQuyTiensBySoTienIsNullOrNotNull() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where soTien is not null
        defaultQuyTienShouldBeFound("soTien.specified=true");

        // Get all the quyTienList where soTien is null
        defaultQuyTienShouldNotBeFound("soTien.specified=false");
    }

    @Test
    @Transactional
    void getAllQuyTiensBySoTienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where soTien is greater than or equal to DEFAULT_SO_TIEN
        defaultQuyTienShouldBeFound("soTien.greaterThanOrEqual=" + DEFAULT_SO_TIEN);

        // Get all the quyTienList where soTien is greater than or equal to UPDATED_SO_TIEN
        defaultQuyTienShouldNotBeFound("soTien.greaterThanOrEqual=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllQuyTiensBySoTienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where soTien is less than or equal to DEFAULT_SO_TIEN
        defaultQuyTienShouldBeFound("soTien.lessThanOrEqual=" + DEFAULT_SO_TIEN);

        // Get all the quyTienList where soTien is less than or equal to SMALLER_SO_TIEN
        defaultQuyTienShouldNotBeFound("soTien.lessThanOrEqual=" + SMALLER_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllQuyTiensBySoTienIsLessThanSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where soTien is less than DEFAULT_SO_TIEN
        defaultQuyTienShouldNotBeFound("soTien.lessThan=" + DEFAULT_SO_TIEN);

        // Get all the quyTienList where soTien is less than UPDATED_SO_TIEN
        defaultQuyTienShouldBeFound("soTien.lessThan=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllQuyTiensBySoTienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where soTien is greater than DEFAULT_SO_TIEN
        defaultQuyTienShouldNotBeFound("soTien.greaterThan=" + DEFAULT_SO_TIEN);

        // Get all the quyTienList where soTien is greater than SMALLER_SO_TIEN
        defaultQuyTienShouldBeFound("soTien.greaterThan=" + SMALLER_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllQuyTiensByGhiChuIsEqualToSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where ghiChu equals to DEFAULT_GHI_CHU
        defaultQuyTienShouldBeFound("ghiChu.equals=" + DEFAULT_GHI_CHU);

        // Get all the quyTienList where ghiChu equals to UPDATED_GHI_CHU
        defaultQuyTienShouldNotBeFound("ghiChu.equals=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void getAllQuyTiensByGhiChuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where ghiChu not equals to DEFAULT_GHI_CHU
        defaultQuyTienShouldNotBeFound("ghiChu.notEquals=" + DEFAULT_GHI_CHU);

        // Get all the quyTienList where ghiChu not equals to UPDATED_GHI_CHU
        defaultQuyTienShouldBeFound("ghiChu.notEquals=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void getAllQuyTiensByGhiChuIsInShouldWork() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where ghiChu in DEFAULT_GHI_CHU or UPDATED_GHI_CHU
        defaultQuyTienShouldBeFound("ghiChu.in=" + DEFAULT_GHI_CHU + "," + UPDATED_GHI_CHU);

        // Get all the quyTienList where ghiChu equals to UPDATED_GHI_CHU
        defaultQuyTienShouldNotBeFound("ghiChu.in=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void getAllQuyTiensByGhiChuIsNullOrNotNull() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where ghiChu is not null
        defaultQuyTienShouldBeFound("ghiChu.specified=true");

        // Get all the quyTienList where ghiChu is null
        defaultQuyTienShouldNotBeFound("ghiChu.specified=false");
    }

    @Test
    @Transactional
    void getAllQuyTiensByGhiChuContainsSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where ghiChu contains DEFAULT_GHI_CHU
        defaultQuyTienShouldBeFound("ghiChu.contains=" + DEFAULT_GHI_CHU);

        // Get all the quyTienList where ghiChu contains UPDATED_GHI_CHU
        defaultQuyTienShouldNotBeFound("ghiChu.contains=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void getAllQuyTiensByGhiChuNotContainsSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where ghiChu does not contain DEFAULT_GHI_CHU
        defaultQuyTienShouldNotBeFound("ghiChu.doesNotContain=" + DEFAULT_GHI_CHU);

        // Get all the quyTienList where ghiChu does not contain UPDATED_GHI_CHU
        defaultQuyTienShouldBeFound("ghiChu.doesNotContain=" + UPDATED_GHI_CHU);
    }

    @Test
    @Transactional
    void getAllQuyTiensByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where ten equals to DEFAULT_TEN
        defaultQuyTienShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the quyTienList where ten equals to UPDATED_TEN
        defaultQuyTienShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllQuyTiensByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where ten not equals to DEFAULT_TEN
        defaultQuyTienShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the quyTienList where ten not equals to UPDATED_TEN
        defaultQuyTienShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllQuyTiensByTenIsInShouldWork() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultQuyTienShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the quyTienList where ten equals to UPDATED_TEN
        defaultQuyTienShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllQuyTiensByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where ten is not null
        defaultQuyTienShouldBeFound("ten.specified=true");

        // Get all the quyTienList where ten is null
        defaultQuyTienShouldNotBeFound("ten.specified=false");
    }

    @Test
    @Transactional
    void getAllQuyTiensByTenContainsSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where ten contains DEFAULT_TEN
        defaultQuyTienShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the quyTienList where ten contains UPDATED_TEN
        defaultQuyTienShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllQuyTiensByTenNotContainsSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        // Get all the quyTienList where ten does not contain DEFAULT_TEN
        defaultQuyTienShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the quyTienList where ten does not contain UPDATED_TEN
        defaultQuyTienShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllQuyTiensByLoaiQuyIsEqualToSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);
        LoaiQuy loaiQuy = LoaiQuyResourceIT.createEntity(em);
        loaiQuy.setMa("23");
        loaiQuy = em.merge(loaiQuy);
        em.flush();
        quyTien.setLoaiQuy(loaiQuy);
        quyTienRepository.saveAndFlush(quyTien);
        Long loaiQuyId = loaiQuy.getId();

        // Get all the quyTienList where loaiQuy equals to loaiQuyId
        defaultQuyTienShouldBeFound("loaiQuyId.equals=" + loaiQuyId);

        // Get all the quyTienList where loaiQuy equals to (loaiQuyId + 1)
        defaultQuyTienShouldNotBeFound("loaiQuyId.equals=" + (loaiQuyId + 1));
    }

    @Test
    @Transactional
    void getAllQuyTiensByMaKeToanIsEqualToSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);
        MaKeToan maKeToan = MaKeToanResourceIT.createEntity(em);
        maKeToan.setSoHieu(3D);
        maKeToan = em.merge(maKeToan);
        em.flush();
        quyTien.setMaKeToan(maKeToan);
        quyTienRepository.saveAndFlush(quyTien);
        Long maKeToanId = maKeToan.getId();

        // Get all the quyTienList where maKeToan equals to maKeToanId
        defaultQuyTienShouldBeFound("maKeToanId.equals=" + maKeToanId);

        // Get all the quyTienList where maKeToan equals to (maKeToanId + 1)
        defaultQuyTienShouldNotBeFound("maKeToanId.equals=" + (maKeToanId + 1));
    }

    @Test
    @Transactional
    void getAllQuyTiensByKhachSanIsEqualToSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);
        KhachSan khachSan = KhachSanResourceIT.createEntity(em);
        khachSan.setTen("IIIIII");
        khachSan = em.merge(khachSan);
        em.flush();
        quyTien.setKhachSan(khachSan);
        quyTienRepository.saveAndFlush(quyTien);
        Long khachSanId = khachSan.getId();

        // Get all the quyTienList where khachSan equals to khachSanId
        defaultQuyTienShouldBeFound("khachSanId.equals=" + khachSanId);

        // Get all the quyTienList where khachSan equals to (khachSanId + 1)
        defaultQuyTienShouldNotBeFound("khachSanId.equals=" + (khachSanId + 1));
    }

    @Test
    @Transactional
    void getAllQuyTiensByPhieuThuIsEqualToSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);
        PhieuThu phieuThu = PhieuThuResourceIT.createEntity(em);
        em.persist(phieuThu);
        em.flush();
        quyTien.addPhieuThu(phieuThu);
        quyTienRepository.saveAndFlush(quyTien);
        Long phieuThuId = phieuThu.getId();

        // Get all the quyTienList where phieuThu equals to phieuThuId
        defaultQuyTienShouldBeFound("phieuThuId.equals=" + phieuThuId);

        // Get all the quyTienList where phieuThu equals to (phieuThuId + 1)
        defaultQuyTienShouldNotBeFound("phieuThuId.equals=" + (phieuThuId + 1));
    }

    @Test
    @Transactional
    void getAllQuyTiensByPhieuChiIsEqualToSomething() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);
        PhieuChi phieuChi = PhieuChiResourceIT.createEntity(em);
        em.persist(phieuChi);
        em.flush();
        quyTien.addPhieuChi(phieuChi);
        quyTienRepository.saveAndFlush(quyTien);
        Long phieuChiId = phieuChi.getId();

        // Get all the quyTienList where phieuChi equals to phieuChiId
        defaultQuyTienShouldBeFound("phieuChiId.equals=" + phieuChiId);

        // Get all the quyTienList where phieuChi equals to (phieuChiId + 1)
        defaultQuyTienShouldNotBeFound("phieuChiId.equals=" + (phieuChiId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultQuyTienShouldBeFound(String filter) throws Exception {
        restQuyTienMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(quyTien.getId().intValue())))
            .andExpect(jsonPath("$.[*].soTien").value(hasItem(DEFAULT_SO_TIEN.doubleValue())))
            .andExpect(jsonPath("$.[*].ghiChu").value(hasItem(DEFAULT_GHI_CHU)))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));

        // Check, that the count call also returns 1
        restQuyTienMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultQuyTienShouldNotBeFound(String filter) throws Exception {
        restQuyTienMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restQuyTienMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingQuyTien() throws Exception {
        // Get the quyTien
        restQuyTienMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewQuyTien() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        int databaseSizeBeforeUpdate = quyTienRepository.findAll().size();

        // Update the quyTien
        QuyTien updatedQuyTien = quyTienRepository.findById(quyTien.getId()).get();
        // Disconnect from session so that the updates on updatedQuyTien are not directly saved in db
        em.detach(updatedQuyTien);
        updatedQuyTien.soTien(UPDATED_SO_TIEN).ghiChu(UPDATED_GHI_CHU).ten(UPDATED_TEN);
        QuyTienDTO quyTienDTO = quyTienMapper.toDto(updatedQuyTien);

        restQuyTienMockMvc
            .perform(
                put(ENTITY_API_URL_ID, quyTienDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(quyTienDTO))
            )
            .andExpect(status().isOk());

        // Validate the QuyTien in the database
        List<QuyTien> quyTienList = quyTienRepository.findAll();
        assertThat(quyTienList).hasSize(databaseSizeBeforeUpdate);
        QuyTien testQuyTien = quyTienList.get(quyTienList.size() - 1);
        assertThat(testQuyTien.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
        assertThat(testQuyTien.getGhiChu()).isEqualTo(UPDATED_GHI_CHU);
        assertThat(testQuyTien.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    void putNonExistingQuyTien() throws Exception {
        int databaseSizeBeforeUpdate = quyTienRepository.findAll().size();
        quyTien.setId(count.incrementAndGet());

        // Create the QuyTien
        QuyTienDTO quyTienDTO = quyTienMapper.toDto(quyTien);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restQuyTienMockMvc
            .perform(
                put(ENTITY_API_URL_ID, quyTienDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(quyTienDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the QuyTien in the database
        List<QuyTien> quyTienList = quyTienRepository.findAll();
        assertThat(quyTienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchQuyTien() throws Exception {
        int databaseSizeBeforeUpdate = quyTienRepository.findAll().size();
        quyTien.setId(count.incrementAndGet());

        // Create the QuyTien
        QuyTienDTO quyTienDTO = quyTienMapper.toDto(quyTien);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restQuyTienMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(quyTienDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the QuyTien in the database
        List<QuyTien> quyTienList = quyTienRepository.findAll();
        assertThat(quyTienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamQuyTien() throws Exception {
        int databaseSizeBeforeUpdate = quyTienRepository.findAll().size();
        quyTien.setId(count.incrementAndGet());

        // Create the QuyTien
        QuyTienDTO quyTienDTO = quyTienMapper.toDto(quyTien);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restQuyTienMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(quyTienDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the QuyTien in the database
        List<QuyTien> quyTienList = quyTienRepository.findAll();
        assertThat(quyTienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateQuyTienWithPatch() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        int databaseSizeBeforeUpdate = quyTienRepository.findAll().size();

        // Update the quyTien using partial update
        QuyTien partialUpdatedQuyTien = new QuyTien();
        partialUpdatedQuyTien.setId(quyTien.getId());

        partialUpdatedQuyTien.ghiChu(UPDATED_GHI_CHU).ten(UPDATED_TEN);

        restQuyTienMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedQuyTien.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedQuyTien))
            )
            .andExpect(status().isOk());

        // Validate the QuyTien in the database
        List<QuyTien> quyTienList = quyTienRepository.findAll();
        assertThat(quyTienList).hasSize(databaseSizeBeforeUpdate);
        QuyTien testQuyTien = quyTienList.get(quyTienList.size() - 1);
        assertThat(testQuyTien.getSoTien()).isEqualTo(DEFAULT_SO_TIEN);
        assertThat(testQuyTien.getGhiChu()).isEqualTo(UPDATED_GHI_CHU);
        assertThat(testQuyTien.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    void fullUpdateQuyTienWithPatch() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        int databaseSizeBeforeUpdate = quyTienRepository.findAll().size();

        // Update the quyTien using partial update
        QuyTien partialUpdatedQuyTien = new QuyTien();
        partialUpdatedQuyTien.setId(quyTien.getId());

        partialUpdatedQuyTien.soTien(UPDATED_SO_TIEN).ghiChu(UPDATED_GHI_CHU).ten(UPDATED_TEN);

        restQuyTienMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedQuyTien.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedQuyTien))
            )
            .andExpect(status().isOk());

        // Validate the QuyTien in the database
        List<QuyTien> quyTienList = quyTienRepository.findAll();
        assertThat(quyTienList).hasSize(databaseSizeBeforeUpdate);
        QuyTien testQuyTien = quyTienList.get(quyTienList.size() - 1);
        assertThat(testQuyTien.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
        assertThat(testQuyTien.getGhiChu()).isEqualTo(UPDATED_GHI_CHU);
        assertThat(testQuyTien.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    void patchNonExistingQuyTien() throws Exception {
        int databaseSizeBeforeUpdate = quyTienRepository.findAll().size();
        quyTien.setId(count.incrementAndGet());

        // Create the QuyTien
        QuyTienDTO quyTienDTO = quyTienMapper.toDto(quyTien);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restQuyTienMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, quyTienDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(quyTienDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the QuyTien in the database
        List<QuyTien> quyTienList = quyTienRepository.findAll();
        assertThat(quyTienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchQuyTien() throws Exception {
        int databaseSizeBeforeUpdate = quyTienRepository.findAll().size();
        quyTien.setId(count.incrementAndGet());

        // Create the QuyTien
        QuyTienDTO quyTienDTO = quyTienMapper.toDto(quyTien);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restQuyTienMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(quyTienDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the QuyTien in the database
        List<QuyTien> quyTienList = quyTienRepository.findAll();
        assertThat(quyTienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamQuyTien() throws Exception {
        int databaseSizeBeforeUpdate = quyTienRepository.findAll().size();
        quyTien.setId(count.incrementAndGet());

        // Create the QuyTien
        QuyTienDTO quyTienDTO = quyTienMapper.toDto(quyTien);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restQuyTienMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(quyTienDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the QuyTien in the database
        List<QuyTien> quyTienList = quyTienRepository.findAll();
        assertThat(quyTienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteQuyTien() throws Exception {
        // Initialize the database
        quyTienRepository.saveAndFlush(quyTien);

        int databaseSizeBeforeDelete = quyTienRepository.findAll().size();

        // Delete the quyTien
        restQuyTienMockMvc
            .perform(delete(ENTITY_API_URL_ID, quyTien.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<QuyTien> quyTienList = quyTienRepository.findAll();
        assertThat(quyTienList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
