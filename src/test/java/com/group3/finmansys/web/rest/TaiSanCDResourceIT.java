package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.KhachSan;
import com.group3.finmansys.domain.LoaiTaiSan;
import com.group3.finmansys.domain.TaiSanCD;
import com.group3.finmansys.repository.TaiSanCDRepository;
import com.group3.finmansys.service.criteria.TaiSanCDCriteria;
import com.group3.finmansys.service.dto.TaiSanCDDTO;
import com.group3.finmansys.service.mapper.TaiSanCDMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link TaiSanCDResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class TaiSanCDResourceIT {

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String DEFAULT_DON_VI_TUOI_THO = "AAAAAAAAAA";
    private static final String UPDATED_DON_VI_TUOI_THO = "BBBBBBBBBB";

    private static final Double DEFAULT_THOI_GIAN_SU_DUNG = 1D;
    private static final Double UPDATED_THOI_GIAN_SU_DUNG = 2D;
    private static final Double SMALLER_THOI_GIAN_SU_DUNG = 1D - 1D;

    private static final byte[] DEFAULT_HINH_ANH = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_HINH_ANH = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_HINH_ANH_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_HINH_ANH_CONTENT_TYPE = "image/png";

    private static final String ENTITY_API_URL = "/api/tai-san-cds";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private TaiSanCDRepository taiSanCDRepository;

    @Autowired
    private TaiSanCDMapper taiSanCDMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTaiSanCDMockMvc;

    private TaiSanCD taiSanCD;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TaiSanCD createEntity(EntityManager em) {
        TaiSanCD taiSanCD = new TaiSanCD()
            .ten(DEFAULT_TEN)
            .donViTuoiTho(DEFAULT_DON_VI_TUOI_THO)
            .thoiGianSuDung(DEFAULT_THOI_GIAN_SU_DUNG)
            .hinhAnh(DEFAULT_HINH_ANH)
            .hinhAnhContentType(DEFAULT_HINH_ANH_CONTENT_TYPE);
        // Add required entity
        KhachSan khachSan;
        if (TestUtil.findAll(em, KhachSan.class).isEmpty()) {
            khachSan = KhachSanResourceIT.createEntity(em);
            em.persist(khachSan);
            em.flush();
        } else {
            khachSan = TestUtil.findAll(em, KhachSan.class).get(0);
        }
        taiSanCD.setKhachSan(khachSan);
        // Add required entity
        LoaiTaiSan loaiTaiSan;
        if (TestUtil.findAll(em, LoaiTaiSan.class).isEmpty()) {
            loaiTaiSan = LoaiTaiSanResourceIT.createEntity(em);
            em.persist(loaiTaiSan);
            em.flush();
        } else {
            loaiTaiSan = TestUtil.findAll(em, LoaiTaiSan.class).get(0);
        }
        taiSanCD.setLoaiTaiSan(loaiTaiSan);
        return taiSanCD;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TaiSanCD createUpdatedEntity(EntityManager em) {
        TaiSanCD taiSanCD = new TaiSanCD()
            .ten(UPDATED_TEN)
            .donViTuoiTho(UPDATED_DON_VI_TUOI_THO)
            .thoiGianSuDung(UPDATED_THOI_GIAN_SU_DUNG)
            .hinhAnh(UPDATED_HINH_ANH)
            .hinhAnhContentType(UPDATED_HINH_ANH_CONTENT_TYPE);
        // Add required entity
        KhachSan khachSan;
        if (TestUtil.findAll(em, KhachSan.class).isEmpty()) {
            khachSan = KhachSanResourceIT.createUpdatedEntity(em);
            em.persist(khachSan);
            em.flush();
        } else {
            khachSan = TestUtil.findAll(em, KhachSan.class).get(0);
        }
        taiSanCD.setKhachSan(khachSan);
        // Add required entity
        LoaiTaiSan loaiTaiSan;
        if (TestUtil.findAll(em, LoaiTaiSan.class).isEmpty()) {
            loaiTaiSan = LoaiTaiSanResourceIT.createUpdatedEntity(em);
            em.persist(loaiTaiSan);
            em.flush();
        } else {
            loaiTaiSan = TestUtil.findAll(em, LoaiTaiSan.class).get(0);
        }
        taiSanCD.setLoaiTaiSan(loaiTaiSan);
        return taiSanCD;
    }

    @BeforeEach
    public void initTest() {
        taiSanCD = createEntity(em);
    }

    @Test
    @Transactional
    void createTaiSanCD() throws Exception {
        int databaseSizeBeforeCreate = taiSanCDRepository.findAll().size();
        // Create the TaiSanCD
        TaiSanCDDTO taiSanCDDTO = taiSanCDMapper.toDto(taiSanCD);
        restTaiSanCDMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(taiSanCDDTO)))
            .andExpect(status().isCreated());

        // Validate the TaiSanCD in the database
        List<TaiSanCD> taiSanCDList = taiSanCDRepository.findAll();
        assertThat(taiSanCDList).hasSize(databaseSizeBeforeCreate + 1);
        TaiSanCD testTaiSanCD = taiSanCDList.get(taiSanCDList.size() - 1);
        assertThat(testTaiSanCD.getTen()).isEqualTo(DEFAULT_TEN);
        assertThat(testTaiSanCD.getDonViTuoiTho()).isEqualTo(DEFAULT_DON_VI_TUOI_THO);
        assertThat(testTaiSanCD.getThoiGianSuDung()).isEqualTo(DEFAULT_THOI_GIAN_SU_DUNG);
        assertThat(testTaiSanCD.getHinhAnh()).isEqualTo(DEFAULT_HINH_ANH);
        assertThat(testTaiSanCD.getHinhAnhContentType()).isEqualTo(DEFAULT_HINH_ANH_CONTENT_TYPE);
    }

    @Test
    @Transactional
    void createTaiSanCDWithExistingId() throws Exception {
        // Create the TaiSanCD with an existing ID
        taiSanCD.setId(1L);
        TaiSanCDDTO taiSanCDDTO = taiSanCDMapper.toDto(taiSanCD);

        int databaseSizeBeforeCreate = taiSanCDRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restTaiSanCDMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(taiSanCDDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TaiSanCD in the database
        List<TaiSanCD> taiSanCDList = taiSanCDRepository.findAll();
        assertThat(taiSanCDList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = taiSanCDRepository.findAll().size();
        // set the field null
        taiSanCD.setTen(null);

        // Create the TaiSanCD, which fails.
        TaiSanCDDTO taiSanCDDTO = taiSanCDMapper.toDto(taiSanCD);

        restTaiSanCDMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(taiSanCDDTO)))
            .andExpect(status().isBadRequest());

        List<TaiSanCD> taiSanCDList = taiSanCDRepository.findAll();
        assertThat(taiSanCDList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDonViTuoiThoIsRequired() throws Exception {
        int databaseSizeBeforeTest = taiSanCDRepository.findAll().size();
        // set the field null
        taiSanCD.setDonViTuoiTho(null);

        // Create the TaiSanCD, which fails.
        TaiSanCDDTO taiSanCDDTO = taiSanCDMapper.toDto(taiSanCD);

        restTaiSanCDMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(taiSanCDDTO)))
            .andExpect(status().isBadRequest());

        List<TaiSanCD> taiSanCDList = taiSanCDRepository.findAll();
        assertThat(taiSanCDList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkThoiGianSuDungIsRequired() throws Exception {
        int databaseSizeBeforeTest = taiSanCDRepository.findAll().size();
        // set the field null
        taiSanCD.setThoiGianSuDung(null);

        // Create the TaiSanCD, which fails.
        TaiSanCDDTO taiSanCDDTO = taiSanCDMapper.toDto(taiSanCD);

        restTaiSanCDMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(taiSanCDDTO)))
            .andExpect(status().isBadRequest());

        List<TaiSanCD> taiSanCDList = taiSanCDRepository.findAll();
        assertThat(taiSanCDList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllTaiSanCDS() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList
        restTaiSanCDMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(taiSanCD.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].donViTuoiTho").value(hasItem(DEFAULT_DON_VI_TUOI_THO)))
            .andExpect(jsonPath("$.[*].thoiGianSuDung").value(hasItem(DEFAULT_THOI_GIAN_SU_DUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].hinhAnhContentType").value(hasItem(DEFAULT_HINH_ANH_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].hinhAnh").value(hasItem(Base64Utils.encodeToString(DEFAULT_HINH_ANH))));
    }

    @Test
    @Transactional
    void getTaiSanCD() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get the taiSanCD
        restTaiSanCDMockMvc
            .perform(get(ENTITY_API_URL_ID, taiSanCD.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(taiSanCD.getId().intValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN))
            .andExpect(jsonPath("$.donViTuoiTho").value(DEFAULT_DON_VI_TUOI_THO))
            .andExpect(jsonPath("$.thoiGianSuDung").value(DEFAULT_THOI_GIAN_SU_DUNG.doubleValue()))
            .andExpect(jsonPath("$.hinhAnhContentType").value(DEFAULT_HINH_ANH_CONTENT_TYPE))
            .andExpect(jsonPath("$.hinhAnh").value(Base64Utils.encodeToString(DEFAULT_HINH_ANH)));
    }

    @Test
    @Transactional
    void getTaiSanCDSByIdFiltering() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        Long id = taiSanCD.getId();

        defaultTaiSanCDShouldBeFound("id.equals=" + id);
        defaultTaiSanCDShouldNotBeFound("id.notEquals=" + id);

        defaultTaiSanCDShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTaiSanCDShouldNotBeFound("id.greaterThan=" + id);

        defaultTaiSanCDShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTaiSanCDShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where ten equals to DEFAULT_TEN
        defaultTaiSanCDShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the taiSanCDList where ten equals to UPDATED_TEN
        defaultTaiSanCDShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where ten not equals to DEFAULT_TEN
        defaultTaiSanCDShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the taiSanCDList where ten not equals to UPDATED_TEN
        defaultTaiSanCDShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByTenIsInShouldWork() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultTaiSanCDShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the taiSanCDList where ten equals to UPDATED_TEN
        defaultTaiSanCDShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where ten is not null
        defaultTaiSanCDShouldBeFound("ten.specified=true");

        // Get all the taiSanCDList where ten is null
        defaultTaiSanCDShouldNotBeFound("ten.specified=false");
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByTenContainsSomething() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where ten contains DEFAULT_TEN
        defaultTaiSanCDShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the taiSanCDList where ten contains UPDATED_TEN
        defaultTaiSanCDShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByTenNotContainsSomething() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where ten does not contain DEFAULT_TEN
        defaultTaiSanCDShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the taiSanCDList where ten does not contain UPDATED_TEN
        defaultTaiSanCDShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByDonViTuoiThoIsEqualToSomething() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where donViTuoiTho equals to DEFAULT_DON_VI_TUOI_THO
        defaultTaiSanCDShouldBeFound("donViTuoiTho.equals=" + DEFAULT_DON_VI_TUOI_THO);

        // Get all the taiSanCDList where donViTuoiTho equals to UPDATED_DON_VI_TUOI_THO
        defaultTaiSanCDShouldNotBeFound("donViTuoiTho.equals=" + UPDATED_DON_VI_TUOI_THO);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByDonViTuoiThoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where donViTuoiTho not equals to DEFAULT_DON_VI_TUOI_THO
        defaultTaiSanCDShouldNotBeFound("donViTuoiTho.notEquals=" + DEFAULT_DON_VI_TUOI_THO);

        // Get all the taiSanCDList where donViTuoiTho not equals to UPDATED_DON_VI_TUOI_THO
        defaultTaiSanCDShouldBeFound("donViTuoiTho.notEquals=" + UPDATED_DON_VI_TUOI_THO);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByDonViTuoiThoIsInShouldWork() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where donViTuoiTho in DEFAULT_DON_VI_TUOI_THO or UPDATED_DON_VI_TUOI_THO
        defaultTaiSanCDShouldBeFound("donViTuoiTho.in=" + DEFAULT_DON_VI_TUOI_THO + "," + UPDATED_DON_VI_TUOI_THO);

        // Get all the taiSanCDList where donViTuoiTho equals to UPDATED_DON_VI_TUOI_THO
        defaultTaiSanCDShouldNotBeFound("donViTuoiTho.in=" + UPDATED_DON_VI_TUOI_THO);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByDonViTuoiThoIsNullOrNotNull() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where donViTuoiTho is not null
        defaultTaiSanCDShouldBeFound("donViTuoiTho.specified=true");

        // Get all the taiSanCDList where donViTuoiTho is null
        defaultTaiSanCDShouldNotBeFound("donViTuoiTho.specified=false");
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByDonViTuoiThoContainsSomething() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where donViTuoiTho contains DEFAULT_DON_VI_TUOI_THO
        defaultTaiSanCDShouldBeFound("donViTuoiTho.contains=" + DEFAULT_DON_VI_TUOI_THO);

        // Get all the taiSanCDList where donViTuoiTho contains UPDATED_DON_VI_TUOI_THO
        defaultTaiSanCDShouldNotBeFound("donViTuoiTho.contains=" + UPDATED_DON_VI_TUOI_THO);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByDonViTuoiThoNotContainsSomething() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where donViTuoiTho does not contain DEFAULT_DON_VI_TUOI_THO
        defaultTaiSanCDShouldNotBeFound("donViTuoiTho.doesNotContain=" + DEFAULT_DON_VI_TUOI_THO);

        // Get all the taiSanCDList where donViTuoiTho does not contain UPDATED_DON_VI_TUOI_THO
        defaultTaiSanCDShouldBeFound("donViTuoiTho.doesNotContain=" + UPDATED_DON_VI_TUOI_THO);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByThoiGianSuDungIsEqualToSomething() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where thoiGianSuDung equals to DEFAULT_THOI_GIAN_SU_DUNG
        defaultTaiSanCDShouldBeFound("thoiGianSuDung.equals=" + DEFAULT_THOI_GIAN_SU_DUNG);

        // Get all the taiSanCDList where thoiGianSuDung equals to UPDATED_THOI_GIAN_SU_DUNG
        defaultTaiSanCDShouldNotBeFound("thoiGianSuDung.equals=" + UPDATED_THOI_GIAN_SU_DUNG);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByThoiGianSuDungIsNotEqualToSomething() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where thoiGianSuDung not equals to DEFAULT_THOI_GIAN_SU_DUNG
        defaultTaiSanCDShouldNotBeFound("thoiGianSuDung.notEquals=" + DEFAULT_THOI_GIAN_SU_DUNG);

        // Get all the taiSanCDList where thoiGianSuDung not equals to UPDATED_THOI_GIAN_SU_DUNG
        defaultTaiSanCDShouldBeFound("thoiGianSuDung.notEquals=" + UPDATED_THOI_GIAN_SU_DUNG);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByThoiGianSuDungIsInShouldWork() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where thoiGianSuDung in DEFAULT_THOI_GIAN_SU_DUNG or UPDATED_THOI_GIAN_SU_DUNG
        defaultTaiSanCDShouldBeFound("thoiGianSuDung.in=" + DEFAULT_THOI_GIAN_SU_DUNG + "," + UPDATED_THOI_GIAN_SU_DUNG);

        // Get all the taiSanCDList where thoiGianSuDung equals to UPDATED_THOI_GIAN_SU_DUNG
        defaultTaiSanCDShouldNotBeFound("thoiGianSuDung.in=" + UPDATED_THOI_GIAN_SU_DUNG);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByThoiGianSuDungIsNullOrNotNull() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where thoiGianSuDung is not null
        defaultTaiSanCDShouldBeFound("thoiGianSuDung.specified=true");

        // Get all the taiSanCDList where thoiGianSuDung is null
        defaultTaiSanCDShouldNotBeFound("thoiGianSuDung.specified=false");
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByThoiGianSuDungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where thoiGianSuDung is greater than or equal to DEFAULT_THOI_GIAN_SU_DUNG
        defaultTaiSanCDShouldBeFound("thoiGianSuDung.greaterThanOrEqual=" + DEFAULT_THOI_GIAN_SU_DUNG);

        // Get all the taiSanCDList where thoiGianSuDung is greater than or equal to UPDATED_THOI_GIAN_SU_DUNG
        defaultTaiSanCDShouldNotBeFound("thoiGianSuDung.greaterThanOrEqual=" + UPDATED_THOI_GIAN_SU_DUNG);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByThoiGianSuDungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where thoiGianSuDung is less than or equal to DEFAULT_THOI_GIAN_SU_DUNG
        defaultTaiSanCDShouldBeFound("thoiGianSuDung.lessThanOrEqual=" + DEFAULT_THOI_GIAN_SU_DUNG);

        // Get all the taiSanCDList where thoiGianSuDung is less than or equal to SMALLER_THOI_GIAN_SU_DUNG
        defaultTaiSanCDShouldNotBeFound("thoiGianSuDung.lessThanOrEqual=" + SMALLER_THOI_GIAN_SU_DUNG);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByThoiGianSuDungIsLessThanSomething() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where thoiGianSuDung is less than DEFAULT_THOI_GIAN_SU_DUNG
        defaultTaiSanCDShouldNotBeFound("thoiGianSuDung.lessThan=" + DEFAULT_THOI_GIAN_SU_DUNG);

        // Get all the taiSanCDList where thoiGianSuDung is less than UPDATED_THOI_GIAN_SU_DUNG
        defaultTaiSanCDShouldBeFound("thoiGianSuDung.lessThan=" + UPDATED_THOI_GIAN_SU_DUNG);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByThoiGianSuDungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        // Get all the taiSanCDList where thoiGianSuDung is greater than DEFAULT_THOI_GIAN_SU_DUNG
        defaultTaiSanCDShouldNotBeFound("thoiGianSuDung.greaterThan=" + DEFAULT_THOI_GIAN_SU_DUNG);

        // Get all the taiSanCDList where thoiGianSuDung is greater than SMALLER_THOI_GIAN_SU_DUNG
        defaultTaiSanCDShouldBeFound("thoiGianSuDung.greaterThan=" + SMALLER_THOI_GIAN_SU_DUNG);
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByKhachSanIsEqualToSomething() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);
        KhachSan khachSan = KhachSanResourceIT.createEntity(em);
        khachSan.setId(1L);
        khachSan.setTen("EEEEEEEEEEEEE");
        khachSan = em.merge(khachSan);
        em.flush();
        taiSanCD.setKhachSan(khachSan);
        taiSanCDRepository.saveAndFlush(taiSanCD);
        Long khachSanId = khachSan.getId();

        // Get all the taiSanCDList where khachSan equals to khachSanId
        defaultTaiSanCDShouldBeFound("khachSanId.equals=" + khachSanId);

        // Get all the taiSanCDList where khachSan equals to (khachSanId + 1)
        defaultTaiSanCDShouldNotBeFound("khachSanId.equals=" + (khachSanId + 1));
    }

    @Test
    @Transactional
    void getAllTaiSanCDSByLoaiTaiSanIsEqualToSomething() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);
        LoaiTaiSan loaiTaiSan = LoaiTaiSanResourceIT.createEntity(em);
        em.persist(loaiTaiSan);
        em.flush();
        taiSanCD.setLoaiTaiSan(loaiTaiSan);
        taiSanCDRepository.saveAndFlush(taiSanCD);
        Long loaiTaiSanId = loaiTaiSan.getId();

        // Get all the taiSanCDList where loaiTaiSan equals to loaiTaiSanId
        defaultTaiSanCDShouldBeFound("loaiTaiSanId.equals=" + loaiTaiSanId);

        // Get all the taiSanCDList where loaiTaiSan equals to (loaiTaiSanId + 1)
        defaultTaiSanCDShouldNotBeFound("loaiTaiSanId.equals=" + (loaiTaiSanId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTaiSanCDShouldBeFound(String filter) throws Exception {
        restTaiSanCDMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(taiSanCD.getId().intValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)))
            .andExpect(jsonPath("$.[*].donViTuoiTho").value(hasItem(DEFAULT_DON_VI_TUOI_THO)))
            .andExpect(jsonPath("$.[*].thoiGianSuDung").value(hasItem(DEFAULT_THOI_GIAN_SU_DUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].hinhAnhContentType").value(hasItem(DEFAULT_HINH_ANH_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].hinhAnh").value(hasItem(Base64Utils.encodeToString(DEFAULT_HINH_ANH))));

        // Check, that the count call also returns 1
        restTaiSanCDMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTaiSanCDShouldNotBeFound(String filter) throws Exception {
        restTaiSanCDMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTaiSanCDMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingTaiSanCD() throws Exception {
        // Get the taiSanCD
        restTaiSanCDMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewTaiSanCD() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        int databaseSizeBeforeUpdate = taiSanCDRepository.findAll().size();

        // Update the taiSanCD
        TaiSanCD updatedTaiSanCD = taiSanCDRepository.findById(taiSanCD.getId()).get();
        // Disconnect from session so that the updates on updatedTaiSanCD are not directly saved in db
        em.detach(updatedTaiSanCD);
        updatedTaiSanCD
            .ten(UPDATED_TEN)
            .donViTuoiTho(UPDATED_DON_VI_TUOI_THO)
            .thoiGianSuDung(UPDATED_THOI_GIAN_SU_DUNG)
            .hinhAnh(UPDATED_HINH_ANH)
            .hinhAnhContentType(UPDATED_HINH_ANH_CONTENT_TYPE);
        TaiSanCDDTO taiSanCDDTO = taiSanCDMapper.toDto(updatedTaiSanCD);

        restTaiSanCDMockMvc
            .perform(
                put(ENTITY_API_URL_ID, taiSanCDDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(taiSanCDDTO))
            )
            .andExpect(status().isOk());

        // Validate the TaiSanCD in the database
        List<TaiSanCD> taiSanCDList = taiSanCDRepository.findAll();
        assertThat(taiSanCDList).hasSize(databaseSizeBeforeUpdate);
        TaiSanCD testTaiSanCD = taiSanCDList.get(taiSanCDList.size() - 1);
        assertThat(testTaiSanCD.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testTaiSanCD.getDonViTuoiTho()).isEqualTo(UPDATED_DON_VI_TUOI_THO);
        assertThat(testTaiSanCD.getThoiGianSuDung()).isEqualTo(UPDATED_THOI_GIAN_SU_DUNG);
        assertThat(testTaiSanCD.getHinhAnh()).isEqualTo(UPDATED_HINH_ANH);
        assertThat(testTaiSanCD.getHinhAnhContentType()).isEqualTo(UPDATED_HINH_ANH_CONTENT_TYPE);
    }

    @Test
    @Transactional
    void putNonExistingTaiSanCD() throws Exception {
        int databaseSizeBeforeUpdate = taiSanCDRepository.findAll().size();
        taiSanCD.setId(count.incrementAndGet());

        // Create the TaiSanCD
        TaiSanCDDTO taiSanCDDTO = taiSanCDMapper.toDto(taiSanCD);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTaiSanCDMockMvc
            .perform(
                put(ENTITY_API_URL_ID, taiSanCDDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(taiSanCDDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TaiSanCD in the database
        List<TaiSanCD> taiSanCDList = taiSanCDRepository.findAll();
        assertThat(taiSanCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchTaiSanCD() throws Exception {
        int databaseSizeBeforeUpdate = taiSanCDRepository.findAll().size();
        taiSanCD.setId(count.incrementAndGet());

        // Create the TaiSanCD
        TaiSanCDDTO taiSanCDDTO = taiSanCDMapper.toDto(taiSanCD);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTaiSanCDMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(taiSanCDDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TaiSanCD in the database
        List<TaiSanCD> taiSanCDList = taiSanCDRepository.findAll();
        assertThat(taiSanCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamTaiSanCD() throws Exception {
        int databaseSizeBeforeUpdate = taiSanCDRepository.findAll().size();
        taiSanCD.setId(count.incrementAndGet());

        // Create the TaiSanCD
        TaiSanCDDTO taiSanCDDTO = taiSanCDMapper.toDto(taiSanCD);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTaiSanCDMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(taiSanCDDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the TaiSanCD in the database
        List<TaiSanCD> taiSanCDList = taiSanCDRepository.findAll();
        assertThat(taiSanCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateTaiSanCDWithPatch() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        int databaseSizeBeforeUpdate = taiSanCDRepository.findAll().size();

        // Update the taiSanCD using partial update
        TaiSanCD partialUpdatedTaiSanCD = new TaiSanCD();
        partialUpdatedTaiSanCD.setId(taiSanCD.getId());

        partialUpdatedTaiSanCD
            .ten(UPDATED_TEN)
            .thoiGianSuDung(UPDATED_THOI_GIAN_SU_DUNG)
            .hinhAnh(UPDATED_HINH_ANH)
            .hinhAnhContentType(UPDATED_HINH_ANH_CONTENT_TYPE);

        restTaiSanCDMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTaiSanCD.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTaiSanCD))
            )
            .andExpect(status().isOk());

        // Validate the TaiSanCD in the database
        List<TaiSanCD> taiSanCDList = taiSanCDRepository.findAll();
        assertThat(taiSanCDList).hasSize(databaseSizeBeforeUpdate);
        TaiSanCD testTaiSanCD = taiSanCDList.get(taiSanCDList.size() - 1);
        assertThat(testTaiSanCD.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testTaiSanCD.getDonViTuoiTho()).isEqualTo(DEFAULT_DON_VI_TUOI_THO);
        assertThat(testTaiSanCD.getThoiGianSuDung()).isEqualTo(UPDATED_THOI_GIAN_SU_DUNG);
        assertThat(testTaiSanCD.getHinhAnh()).isEqualTo(UPDATED_HINH_ANH);
        assertThat(testTaiSanCD.getHinhAnhContentType()).isEqualTo(UPDATED_HINH_ANH_CONTENT_TYPE);
    }

    @Test
    @Transactional
    void fullUpdateTaiSanCDWithPatch() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        int databaseSizeBeforeUpdate = taiSanCDRepository.findAll().size();

        // Update the taiSanCD using partial update
        TaiSanCD partialUpdatedTaiSanCD = new TaiSanCD();
        partialUpdatedTaiSanCD.setId(taiSanCD.getId());

        partialUpdatedTaiSanCD
            .ten(UPDATED_TEN)
            .donViTuoiTho(UPDATED_DON_VI_TUOI_THO)
            .thoiGianSuDung(UPDATED_THOI_GIAN_SU_DUNG)
            .hinhAnh(UPDATED_HINH_ANH)
            .hinhAnhContentType(UPDATED_HINH_ANH_CONTENT_TYPE);

        restTaiSanCDMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTaiSanCD.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTaiSanCD))
            )
            .andExpect(status().isOk());

        // Validate the TaiSanCD in the database
        List<TaiSanCD> taiSanCDList = taiSanCDRepository.findAll();
        assertThat(taiSanCDList).hasSize(databaseSizeBeforeUpdate);
        TaiSanCD testTaiSanCD = taiSanCDList.get(taiSanCDList.size() - 1);
        assertThat(testTaiSanCD.getTen()).isEqualTo(UPDATED_TEN);
        assertThat(testTaiSanCD.getDonViTuoiTho()).isEqualTo(UPDATED_DON_VI_TUOI_THO);
        assertThat(testTaiSanCD.getThoiGianSuDung()).isEqualTo(UPDATED_THOI_GIAN_SU_DUNG);
        assertThat(testTaiSanCD.getHinhAnh()).isEqualTo(UPDATED_HINH_ANH);
        assertThat(testTaiSanCD.getHinhAnhContentType()).isEqualTo(UPDATED_HINH_ANH_CONTENT_TYPE);
    }

    @Test
    @Transactional
    void patchNonExistingTaiSanCD() throws Exception {
        int databaseSizeBeforeUpdate = taiSanCDRepository.findAll().size();
        taiSanCD.setId(count.incrementAndGet());

        // Create the TaiSanCD
        TaiSanCDDTO taiSanCDDTO = taiSanCDMapper.toDto(taiSanCD);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTaiSanCDMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, taiSanCDDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(taiSanCDDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TaiSanCD in the database
        List<TaiSanCD> taiSanCDList = taiSanCDRepository.findAll();
        assertThat(taiSanCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchTaiSanCD() throws Exception {
        int databaseSizeBeforeUpdate = taiSanCDRepository.findAll().size();
        taiSanCD.setId(count.incrementAndGet());

        // Create the TaiSanCD
        TaiSanCDDTO taiSanCDDTO = taiSanCDMapper.toDto(taiSanCD);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTaiSanCDMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(taiSanCDDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TaiSanCD in the database
        List<TaiSanCD> taiSanCDList = taiSanCDRepository.findAll();
        assertThat(taiSanCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamTaiSanCD() throws Exception {
        int databaseSizeBeforeUpdate = taiSanCDRepository.findAll().size();
        taiSanCD.setId(count.incrementAndGet());

        // Create the TaiSanCD
        TaiSanCDDTO taiSanCDDTO = taiSanCDMapper.toDto(taiSanCD);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTaiSanCDMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(taiSanCDDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the TaiSanCD in the database
        List<TaiSanCD> taiSanCDList = taiSanCDRepository.findAll();
        assertThat(taiSanCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteTaiSanCD() throws Exception {
        // Initialize the database
        taiSanCDRepository.saveAndFlush(taiSanCD);

        int databaseSizeBeforeDelete = taiSanCDRepository.findAll().size();

        // Delete the taiSanCD
        restTaiSanCDMockMvc
            .perform(delete(ENTITY_API_URL_ID, taiSanCD.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TaiSanCD> taiSanCDList = taiSanCDRepository.findAll();
        assertThat(taiSanCDList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
