package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.KhachHang;
import com.group3.finmansys.domain.KhoanPhaiThu;
import com.group3.finmansys.domain.PhieuThu;
import com.group3.finmansys.domain.QuyTien;
import com.group3.finmansys.repository.PhieuThuRepository;
import com.group3.finmansys.service.criteria.PhieuThuCriteria;
import com.group3.finmansys.service.dto.PhieuThuDTO;
import com.group3.finmansys.service.mapper.PhieuThuMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PhieuThuResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class PhieuThuResourceIT {

    private static final Instant DEFAULT_NGAY_CHUNG_TU = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_NGAY_CHUNG_TU = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Double DEFAULT_SO_TIEN = 1D;
    private static final Double UPDATED_SO_TIEN = 2D;
    private static final Double SMALLER_SO_TIEN = 1D - 1D;

    private static final String ENTITY_API_URL = "/api/phieu-thus";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PhieuThuRepository phieuThuRepository;

    @Autowired
    private PhieuThuMapper phieuThuMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPhieuThuMockMvc;

    private PhieuThu phieuThu;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhieuThu createEntity(EntityManager em) {
        PhieuThu phieuThu = new PhieuThu().ngayChungTu(DEFAULT_NGAY_CHUNG_TU).soTien(DEFAULT_SO_TIEN);
        // Add required entity
        KhoanPhaiThu khoanPhaiThu;
        if (TestUtil.findAll(em, KhoanPhaiThu.class).isEmpty()) {
            khoanPhaiThu = KhoanPhaiThuResourceIT.createEntity(em);
            em.persist(khoanPhaiThu);
            em.flush();
        } else {
            khoanPhaiThu = TestUtil.findAll(em, KhoanPhaiThu.class).get(0);
        }
        phieuThu.setKhoanPhaiThu(khoanPhaiThu);
        // Add required entity
        QuyTien quyTien;
        if (TestUtil.findAll(em, QuyTien.class).isEmpty()) {
            quyTien = QuyTienResourceIT.createEntity(em);
            em.persist(quyTien);
            em.flush();
        } else {
            quyTien = TestUtil.findAll(em, QuyTien.class).get(0);
        }
        phieuThu.setQuyTien(quyTien);
        // Add required entity
        KhachHang khachHang;
        if (TestUtil.findAll(em, KhachHang.class).isEmpty()) {
            khachHang = KhachHangResourceIT.createEntity(em);
            em.persist(khachHang);
            em.flush();
        } else {
            khachHang = TestUtil.findAll(em, KhachHang.class).get(0);
        }
        phieuThu.setKhachHang(khachHang);
        return phieuThu;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhieuThu createUpdatedEntity(EntityManager em) {
        PhieuThu phieuThu = new PhieuThu().ngayChungTu(UPDATED_NGAY_CHUNG_TU).soTien(UPDATED_SO_TIEN);
        // Add required entity
        KhoanPhaiThu khoanPhaiThu;
        if (TestUtil.findAll(em, KhoanPhaiThu.class).isEmpty()) {
            khoanPhaiThu = KhoanPhaiThuResourceIT.createUpdatedEntity(em);
            em.persist(khoanPhaiThu);
            em.flush();
        } else {
            khoanPhaiThu = TestUtil.findAll(em, KhoanPhaiThu.class).get(0);
        }
        phieuThu.setKhoanPhaiThu(khoanPhaiThu);
        // Add required entity
        QuyTien quyTien;
        if (TestUtil.findAll(em, QuyTien.class).isEmpty()) {
            quyTien = QuyTienResourceIT.createUpdatedEntity(em);
            em.persist(quyTien);
            em.flush();
        } else {
            quyTien = TestUtil.findAll(em, QuyTien.class).get(0);
        }
        phieuThu.setQuyTien(quyTien);
        // Add required entity
        KhachHang khachHang;
        if (TestUtil.findAll(em, KhachHang.class).isEmpty()) {
            khachHang = KhachHangResourceIT.createUpdatedEntity(em);
            em.persist(khachHang);
            em.flush();
        } else {
            khachHang = TestUtil.findAll(em, KhachHang.class).get(0);
        }
        phieuThu.setKhachHang(khachHang);
        return phieuThu;
    }

    @BeforeEach
    public void initTest() {
        phieuThu = createEntity(em);
    }

    @Test
    @Transactional
    void createPhieuThu() throws Exception {
        int databaseSizeBeforeCreate = phieuThuRepository.findAll().size();
        // Create the PhieuThu
        PhieuThuDTO phieuThuDTO = phieuThuMapper.toDto(phieuThu);
        restPhieuThuMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phieuThuDTO)))
            .andExpect(status().isCreated());

        // Validate the PhieuThu in the database
        List<PhieuThu> phieuThuList = phieuThuRepository.findAll();
        assertThat(phieuThuList).hasSize(databaseSizeBeforeCreate + 1);
        PhieuThu testPhieuThu = phieuThuList.get(phieuThuList.size() - 1);
        assertThat(testPhieuThu.getNgayChungTu()).isEqualTo(DEFAULT_NGAY_CHUNG_TU);
        assertThat(testPhieuThu.getSoTien()).isEqualTo(DEFAULT_SO_TIEN);
    }

    @Test
    @Transactional
    void createPhieuThuWithExistingId() throws Exception {
        // Create the PhieuThu with an existing ID
        phieuThu.setId(1L);
        PhieuThuDTO phieuThuDTO = phieuThuMapper.toDto(phieuThu);

        int databaseSizeBeforeCreate = phieuThuRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhieuThuMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phieuThuDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PhieuThu in the database
        List<PhieuThu> phieuThuList = phieuThuRepository.findAll();
        assertThat(phieuThuList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNgayChungTuIsRequired() throws Exception {
        int databaseSizeBeforeTest = phieuThuRepository.findAll().size();
        // set the field null
        phieuThu.setNgayChungTu(null);

        // Create the PhieuThu, which fails.
        PhieuThuDTO phieuThuDTO = phieuThuMapper.toDto(phieuThu);

        restPhieuThuMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phieuThuDTO)))
            .andExpect(status().isBadRequest());

        List<PhieuThu> phieuThuList = phieuThuRepository.findAll();
        assertThat(phieuThuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSoTienIsRequired() throws Exception {
        int databaseSizeBeforeTest = phieuThuRepository.findAll().size();
        // set the field null
        phieuThu.setSoTien(null);

        // Create the PhieuThu, which fails.
        PhieuThuDTO phieuThuDTO = phieuThuMapper.toDto(phieuThu);

        restPhieuThuMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phieuThuDTO)))
            .andExpect(status().isBadRequest());

        List<PhieuThu> phieuThuList = phieuThuRepository.findAll();
        assertThat(phieuThuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPhieuThus() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        // Get all the phieuThuList
        restPhieuThuMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phieuThu.getId().intValue())))
            .andExpect(jsonPath("$.[*].ngayChungTu").value(hasItem(DEFAULT_NGAY_CHUNG_TU.toString())))
            .andExpect(jsonPath("$.[*].soTien").value(hasItem(DEFAULT_SO_TIEN.doubleValue())));
    }

    @Test
    @Transactional
    void getPhieuThu() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        // Get the phieuThu
        restPhieuThuMockMvc
            .perform(get(ENTITY_API_URL_ID, phieuThu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(phieuThu.getId().intValue()))
            .andExpect(jsonPath("$.ngayChungTu").value(DEFAULT_NGAY_CHUNG_TU.toString()))
            .andExpect(jsonPath("$.soTien").value(DEFAULT_SO_TIEN.doubleValue()));
    }

    @Test
    @Transactional
    void getPhieuThusByIdFiltering() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        Long id = phieuThu.getId();

        defaultPhieuThuShouldBeFound("id.equals=" + id);
        defaultPhieuThuShouldNotBeFound("id.notEquals=" + id);

        defaultPhieuThuShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPhieuThuShouldNotBeFound("id.greaterThan=" + id);

        defaultPhieuThuShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPhieuThuShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPhieuThusByNgayChungTuIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        // Get all the phieuThuList where ngayChungTu equals to DEFAULT_NGAY_CHUNG_TU
        defaultPhieuThuShouldBeFound("ngayChungTu.equals=" + DEFAULT_NGAY_CHUNG_TU);

        // Get all the phieuThuList where ngayChungTu equals to UPDATED_NGAY_CHUNG_TU
        defaultPhieuThuShouldNotBeFound("ngayChungTu.equals=" + UPDATED_NGAY_CHUNG_TU);
    }

    @Test
    @Transactional
    void getAllPhieuThusByNgayChungTuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        // Get all the phieuThuList where ngayChungTu not equals to DEFAULT_NGAY_CHUNG_TU
        defaultPhieuThuShouldNotBeFound("ngayChungTu.notEquals=" + DEFAULT_NGAY_CHUNG_TU);

        // Get all the phieuThuList where ngayChungTu not equals to UPDATED_NGAY_CHUNG_TU
        defaultPhieuThuShouldBeFound("ngayChungTu.notEquals=" + UPDATED_NGAY_CHUNG_TU);
    }

    @Test
    @Transactional
    void getAllPhieuThusByNgayChungTuIsInShouldWork() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        // Get all the phieuThuList where ngayChungTu in DEFAULT_NGAY_CHUNG_TU or UPDATED_NGAY_CHUNG_TU
        defaultPhieuThuShouldBeFound("ngayChungTu.in=" + DEFAULT_NGAY_CHUNG_TU + "," + UPDATED_NGAY_CHUNG_TU);

        // Get all the phieuThuList where ngayChungTu equals to UPDATED_NGAY_CHUNG_TU
        defaultPhieuThuShouldNotBeFound("ngayChungTu.in=" + UPDATED_NGAY_CHUNG_TU);
    }

    @Test
    @Transactional
    void getAllPhieuThusByNgayChungTuIsNullOrNotNull() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        // Get all the phieuThuList where ngayChungTu is not null
        defaultPhieuThuShouldBeFound("ngayChungTu.specified=true");

        // Get all the phieuThuList where ngayChungTu is null
        defaultPhieuThuShouldNotBeFound("ngayChungTu.specified=false");
    }

    @Test
    @Transactional
    void getAllPhieuThusBySoTienIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        // Get all the phieuThuList where soTien equals to DEFAULT_SO_TIEN
        defaultPhieuThuShouldBeFound("soTien.equals=" + DEFAULT_SO_TIEN);

        // Get all the phieuThuList where soTien equals to UPDATED_SO_TIEN
        defaultPhieuThuShouldNotBeFound("soTien.equals=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllPhieuThusBySoTienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        // Get all the phieuThuList where soTien not equals to DEFAULT_SO_TIEN
        defaultPhieuThuShouldNotBeFound("soTien.notEquals=" + DEFAULT_SO_TIEN);

        // Get all the phieuThuList where soTien not equals to UPDATED_SO_TIEN
        defaultPhieuThuShouldBeFound("soTien.notEquals=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllPhieuThusBySoTienIsInShouldWork() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        // Get all the phieuThuList where soTien in DEFAULT_SO_TIEN or UPDATED_SO_TIEN
        defaultPhieuThuShouldBeFound("soTien.in=" + DEFAULT_SO_TIEN + "," + UPDATED_SO_TIEN);

        // Get all the phieuThuList where soTien equals to UPDATED_SO_TIEN
        defaultPhieuThuShouldNotBeFound("soTien.in=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllPhieuThusBySoTienIsNullOrNotNull() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        // Get all the phieuThuList where soTien is not null
        defaultPhieuThuShouldBeFound("soTien.specified=true");

        // Get all the phieuThuList where soTien is null
        defaultPhieuThuShouldNotBeFound("soTien.specified=false");
    }

    @Test
    @Transactional
    void getAllPhieuThusBySoTienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        // Get all the phieuThuList where soTien is greater than or equal to DEFAULT_SO_TIEN
        defaultPhieuThuShouldBeFound("soTien.greaterThanOrEqual=" + DEFAULT_SO_TIEN);

        // Get all the phieuThuList where soTien is greater than or equal to UPDATED_SO_TIEN
        defaultPhieuThuShouldNotBeFound("soTien.greaterThanOrEqual=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllPhieuThusBySoTienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        // Get all the phieuThuList where soTien is less than or equal to DEFAULT_SO_TIEN
        defaultPhieuThuShouldBeFound("soTien.lessThanOrEqual=" + DEFAULT_SO_TIEN);

        // Get all the phieuThuList where soTien is less than or equal to SMALLER_SO_TIEN
        defaultPhieuThuShouldNotBeFound("soTien.lessThanOrEqual=" + SMALLER_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllPhieuThusBySoTienIsLessThanSomething() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        // Get all the phieuThuList where soTien is less than DEFAULT_SO_TIEN
        defaultPhieuThuShouldNotBeFound("soTien.lessThan=" + DEFAULT_SO_TIEN);

        // Get all the phieuThuList where soTien is less than UPDATED_SO_TIEN
        defaultPhieuThuShouldBeFound("soTien.lessThan=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllPhieuThusBySoTienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        // Get all the phieuThuList where soTien is greater than DEFAULT_SO_TIEN
        defaultPhieuThuShouldNotBeFound("soTien.greaterThan=" + DEFAULT_SO_TIEN);

        // Get all the phieuThuList where soTien is greater than SMALLER_SO_TIEN
        defaultPhieuThuShouldBeFound("soTien.greaterThan=" + SMALLER_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllPhieuThusByKhoanPhaiThuIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);
        KhoanPhaiThu khoanPhaiThu = KhoanPhaiThuResourceIT.createEntity(em);
        em.persist(khoanPhaiThu);
        em.flush();
        phieuThu.setKhoanPhaiThu(khoanPhaiThu);
        phieuThuRepository.saveAndFlush(phieuThu);
        Long khoanPhaiThuId = khoanPhaiThu.getId();

        // Get all the phieuThuList where khoanPhaiThu equals to khoanPhaiThuId
        defaultPhieuThuShouldBeFound("khoanPhaiThuId.equals=" + khoanPhaiThuId);

        // Get all the phieuThuList where khoanPhaiThu equals to (khoanPhaiThuId + 1)
        defaultPhieuThuShouldNotBeFound("khoanPhaiThuId.equals=" + (khoanPhaiThuId + 1));
    }

    @Test
    @Transactional
    void getAllPhieuThusByQuyTienIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);
        QuyTien quyTien = QuyTienResourceIT.createEntity(em);
        em.persist(quyTien);
        em.flush();
        phieuThu.setQuyTien(quyTien);
        phieuThuRepository.saveAndFlush(phieuThu);
        Long quyTienId = quyTien.getId();

        // Get all the phieuThuList where quyTien equals to quyTienId
        defaultPhieuThuShouldBeFound("quyTienId.equals=" + quyTienId);

        // Get all the phieuThuList where quyTien equals to (quyTienId + 1)
        defaultPhieuThuShouldNotBeFound("quyTienId.equals=" + (quyTienId + 1));
    }

    @Test
    @Transactional
    void getAllPhieuThusByKhachHangIsEqualToSomething() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);
        KhachHang khachHang = KhachHangResourceIT.createEntity(em);
        em.persist(khachHang);
        em.flush();
        phieuThu.setKhachHang(khachHang);
        phieuThuRepository.saveAndFlush(phieuThu);
        Long khachHangId = khachHang.getId();

        // Get all the phieuThuList where khachHang equals to khachHangId
        defaultPhieuThuShouldBeFound("khachHangId.equals=" + khachHangId);

        // Get all the phieuThuList where khachHang equals to (khachHangId + 1)
        defaultPhieuThuShouldNotBeFound("khachHangId.equals=" + (khachHangId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPhieuThuShouldBeFound(String filter) throws Exception {
        restPhieuThuMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phieuThu.getId().intValue())))
            .andExpect(jsonPath("$.[*].ngayChungTu").value(hasItem(DEFAULT_NGAY_CHUNG_TU.toString())))
            .andExpect(jsonPath("$.[*].soTien").value(hasItem(DEFAULT_SO_TIEN.doubleValue())));

        // Check, that the count call also returns 1
        restPhieuThuMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPhieuThuShouldNotBeFound(String filter) throws Exception {
        restPhieuThuMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPhieuThuMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPhieuThu() throws Exception {
        // Get the phieuThu
        restPhieuThuMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPhieuThu() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        int databaseSizeBeforeUpdate = phieuThuRepository.findAll().size();

        // Update the phieuThu
        PhieuThu updatedPhieuThu = phieuThuRepository.findById(phieuThu.getId()).get();
        // Disconnect from session so that the updates on updatedPhieuThu are not directly saved in db
        em.detach(updatedPhieuThu);
        updatedPhieuThu.ngayChungTu(UPDATED_NGAY_CHUNG_TU).soTien(UPDATED_SO_TIEN);
        PhieuThuDTO phieuThuDTO = phieuThuMapper.toDto(updatedPhieuThu);

        restPhieuThuMockMvc
            .perform(
                put(ENTITY_API_URL_ID, phieuThuDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(phieuThuDTO))
            )
            .andExpect(status().isOk());

        // Validate the PhieuThu in the database
        List<PhieuThu> phieuThuList = phieuThuRepository.findAll();
        assertThat(phieuThuList).hasSize(databaseSizeBeforeUpdate);
        PhieuThu testPhieuThu = phieuThuList.get(phieuThuList.size() - 1);
        assertThat(testPhieuThu.getNgayChungTu()).isEqualTo(UPDATED_NGAY_CHUNG_TU);
        assertThat(testPhieuThu.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void putNonExistingPhieuThu() throws Exception {
        int databaseSizeBeforeUpdate = phieuThuRepository.findAll().size();
        phieuThu.setId(count.incrementAndGet());

        // Create the PhieuThu
        PhieuThuDTO phieuThuDTO = phieuThuMapper.toDto(phieuThu);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhieuThuMockMvc
            .perform(
                put(ENTITY_API_URL_ID, phieuThuDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(phieuThuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhieuThu in the database
        List<PhieuThu> phieuThuList = phieuThuRepository.findAll();
        assertThat(phieuThuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPhieuThu() throws Exception {
        int databaseSizeBeforeUpdate = phieuThuRepository.findAll().size();
        phieuThu.setId(count.incrementAndGet());

        // Create the PhieuThu
        PhieuThuDTO phieuThuDTO = phieuThuMapper.toDto(phieuThu);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhieuThuMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(phieuThuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhieuThu in the database
        List<PhieuThu> phieuThuList = phieuThuRepository.findAll();
        assertThat(phieuThuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPhieuThu() throws Exception {
        int databaseSizeBeforeUpdate = phieuThuRepository.findAll().size();
        phieuThu.setId(count.incrementAndGet());

        // Create the PhieuThu
        PhieuThuDTO phieuThuDTO = phieuThuMapper.toDto(phieuThu);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhieuThuMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(phieuThuDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the PhieuThu in the database
        List<PhieuThu> phieuThuList = phieuThuRepository.findAll();
        assertThat(phieuThuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePhieuThuWithPatch() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        int databaseSizeBeforeUpdate = phieuThuRepository.findAll().size();

        // Update the phieuThu using partial update
        PhieuThu partialUpdatedPhieuThu = new PhieuThu();
        partialUpdatedPhieuThu.setId(phieuThu.getId());

        restPhieuThuMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPhieuThu.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPhieuThu))
            )
            .andExpect(status().isOk());

        // Validate the PhieuThu in the database
        List<PhieuThu> phieuThuList = phieuThuRepository.findAll();
        assertThat(phieuThuList).hasSize(databaseSizeBeforeUpdate);
        PhieuThu testPhieuThu = phieuThuList.get(phieuThuList.size() - 1);
        assertThat(testPhieuThu.getNgayChungTu()).isEqualTo(DEFAULT_NGAY_CHUNG_TU);
        assertThat(testPhieuThu.getSoTien()).isEqualTo(DEFAULT_SO_TIEN);
    }

    @Test
    @Transactional
    void fullUpdatePhieuThuWithPatch() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        int databaseSizeBeforeUpdate = phieuThuRepository.findAll().size();

        // Update the phieuThu using partial update
        PhieuThu partialUpdatedPhieuThu = new PhieuThu();
        partialUpdatedPhieuThu.setId(phieuThu.getId());

        partialUpdatedPhieuThu.ngayChungTu(UPDATED_NGAY_CHUNG_TU).soTien(UPDATED_SO_TIEN);

        restPhieuThuMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPhieuThu.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPhieuThu))
            )
            .andExpect(status().isOk());

        // Validate the PhieuThu in the database
        List<PhieuThu> phieuThuList = phieuThuRepository.findAll();
        assertThat(phieuThuList).hasSize(databaseSizeBeforeUpdate);
        PhieuThu testPhieuThu = phieuThuList.get(phieuThuList.size() - 1);
        assertThat(testPhieuThu.getNgayChungTu()).isEqualTo(UPDATED_NGAY_CHUNG_TU);
        assertThat(testPhieuThu.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void patchNonExistingPhieuThu() throws Exception {
        int databaseSizeBeforeUpdate = phieuThuRepository.findAll().size();
        phieuThu.setId(count.incrementAndGet());

        // Create the PhieuThu
        PhieuThuDTO phieuThuDTO = phieuThuMapper.toDto(phieuThu);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhieuThuMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, phieuThuDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(phieuThuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhieuThu in the database
        List<PhieuThu> phieuThuList = phieuThuRepository.findAll();
        assertThat(phieuThuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPhieuThu() throws Exception {
        int databaseSizeBeforeUpdate = phieuThuRepository.findAll().size();
        phieuThu.setId(count.incrementAndGet());

        // Create the PhieuThu
        PhieuThuDTO phieuThuDTO = phieuThuMapper.toDto(phieuThu);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhieuThuMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(phieuThuDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PhieuThu in the database
        List<PhieuThu> phieuThuList = phieuThuRepository.findAll();
        assertThat(phieuThuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPhieuThu() throws Exception {
        int databaseSizeBeforeUpdate = phieuThuRepository.findAll().size();
        phieuThu.setId(count.incrementAndGet());

        // Create the PhieuThu
        PhieuThuDTO phieuThuDTO = phieuThuMapper.toDto(phieuThu);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPhieuThuMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(phieuThuDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PhieuThu in the database
        List<PhieuThu> phieuThuList = phieuThuRepository.findAll();
        assertThat(phieuThuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePhieuThu() throws Exception {
        // Initialize the database
        phieuThuRepository.saveAndFlush(phieuThu);

        int databaseSizeBeforeDelete = phieuThuRepository.findAll().size();

        // Delete the phieuThu
        restPhieuThuMockMvc
            .perform(delete(ENTITY_API_URL_ID, phieuThu.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PhieuThu> phieuThuList = phieuThuRepository.findAll();
        assertThat(phieuThuList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
