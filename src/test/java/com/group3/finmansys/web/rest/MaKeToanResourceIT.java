package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.MaKeToan;
import com.group3.finmansys.repository.MaKeToanRepository;
import com.group3.finmansys.service.criteria.MaKeToanCriteria;
import com.group3.finmansys.service.dto.MaKeToanDTO;
import com.group3.finmansys.service.mapper.MaKeToanMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MaKeToanResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class MaKeToanResourceIT {

    private static final Double DEFAULT_SO_HIEU = 1D;
    private static final Double UPDATED_SO_HIEU = 2D;
    private static final Double SMALLER_SO_HIEU = 1D - 1D;

    private static final String DEFAULT_TEN = "AAAAAAAAAA";
    private static final String UPDATED_TEN = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/ma-ke-toans";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MaKeToanRepository maKeToanRepository;

    @Autowired
    private MaKeToanMapper maKeToanMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMaKeToanMockMvc;

    private MaKeToan maKeToan;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaKeToan createEntity(EntityManager em) {
        MaKeToan maKeToan = new MaKeToan().soHieu(DEFAULT_SO_HIEU).ten(DEFAULT_TEN);
        return maKeToan;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaKeToan createUpdatedEntity(EntityManager em) {
        MaKeToan maKeToan = new MaKeToan().soHieu(UPDATED_SO_HIEU).ten(UPDATED_TEN);
        return maKeToan;
    }

    @BeforeEach
    public void initTest() {
        maKeToan = createEntity(em);
    }

    @Test
    @Transactional
    void createMaKeToan() throws Exception {
        int databaseSizeBeforeCreate = maKeToanRepository.findAll().size();
        // Create the MaKeToan
        MaKeToanDTO maKeToanDTO = maKeToanMapper.toDto(maKeToan);
        restMaKeToanMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(maKeToanDTO)))
            .andExpect(status().isCreated());

        // Validate the MaKeToan in the database
        List<MaKeToan> maKeToanList = maKeToanRepository.findAll();
        assertThat(maKeToanList).hasSize(databaseSizeBeforeCreate + 1);
        MaKeToan testMaKeToan = maKeToanList.get(maKeToanList.size() - 1);
        assertThat(testMaKeToan.getSoHieu()).isEqualTo(DEFAULT_SO_HIEU);
        assertThat(testMaKeToan.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    void createMaKeToanWithExistingId() throws Exception {
        // Create the MaKeToan with an existing ID
        maKeToan.setId(1L);
        MaKeToanDTO maKeToanDTO = maKeToanMapper.toDto(maKeToan);

        int databaseSizeBeforeCreate = maKeToanRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMaKeToanMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(maKeToanDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MaKeToan in the database
        List<MaKeToan> maKeToanList = maKeToanRepository.findAll();
        assertThat(maKeToanList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkSoHieuIsRequired() throws Exception {
        int databaseSizeBeforeTest = maKeToanRepository.findAll().size();
        // set the field null
        maKeToan.setSoHieu(null);

        // Create the MaKeToan, which fails.
        MaKeToanDTO maKeToanDTO = maKeToanMapper.toDto(maKeToan);

        restMaKeToanMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(maKeToanDTO)))
            .andExpect(status().isBadRequest());

        List<MaKeToan> maKeToanList = maKeToanRepository.findAll();
        assertThat(maKeToanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTenIsRequired() throws Exception {
        int databaseSizeBeforeTest = maKeToanRepository.findAll().size();
        // set the field null
        maKeToan.setTen(null);

        // Create the MaKeToan, which fails.
        MaKeToanDTO maKeToanDTO = maKeToanMapper.toDto(maKeToan);

        restMaKeToanMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(maKeToanDTO)))
            .andExpect(status().isBadRequest());

        List<MaKeToan> maKeToanList = maKeToanRepository.findAll();
        assertThat(maKeToanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMaKeToans() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        // Get all the maKeToanList
        restMaKeToanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(maKeToan.getId().intValue())))
            .andExpect(jsonPath("$.[*].soHieu").value(hasItem(DEFAULT_SO_HIEU.doubleValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));
    }

    @Test
    @Transactional
    void getMaKeToan() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        // Get the maKeToan
        restMaKeToanMockMvc
            .perform(get(ENTITY_API_URL_ID, maKeToan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(maKeToan.getId().intValue()))
            .andExpect(jsonPath("$.soHieu").value(DEFAULT_SO_HIEU.doubleValue()))
            .andExpect(jsonPath("$.ten").value(DEFAULT_TEN));
    }

    @Test
    @Transactional
    void getMaKeToansByIdFiltering() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        Long id = maKeToan.getId();

        defaultMaKeToanShouldBeFound("id.equals=" + id);
        defaultMaKeToanShouldNotBeFound("id.notEquals=" + id);

        defaultMaKeToanShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultMaKeToanShouldNotBeFound("id.greaterThan=" + id);

        defaultMaKeToanShouldBeFound("id.lessThanOrEqual=" + id);
        defaultMaKeToanShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllMaKeToansBySoHieuIsEqualToSomething() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        // Get all the maKeToanList where soHieu equals to DEFAULT_SO_HIEU
        defaultMaKeToanShouldBeFound("soHieu.equals=" + DEFAULT_SO_HIEU);

        // Get all the maKeToanList where soHieu equals to UPDATED_SO_HIEU
        defaultMaKeToanShouldNotBeFound("soHieu.equals=" + UPDATED_SO_HIEU);
    }

    @Test
    @Transactional
    void getAllMaKeToansBySoHieuIsNotEqualToSomething() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        // Get all the maKeToanList where soHieu not equals to DEFAULT_SO_HIEU
        defaultMaKeToanShouldNotBeFound("soHieu.notEquals=" + DEFAULT_SO_HIEU);

        // Get all the maKeToanList where soHieu not equals to UPDATED_SO_HIEU
        defaultMaKeToanShouldBeFound("soHieu.notEquals=" + UPDATED_SO_HIEU);
    }

    @Test
    @Transactional
    void getAllMaKeToansBySoHieuIsInShouldWork() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        // Get all the maKeToanList where soHieu in DEFAULT_SO_HIEU or UPDATED_SO_HIEU
        defaultMaKeToanShouldBeFound("soHieu.in=" + DEFAULT_SO_HIEU + "," + UPDATED_SO_HIEU);

        // Get all the maKeToanList where soHieu equals to UPDATED_SO_HIEU
        defaultMaKeToanShouldNotBeFound("soHieu.in=" + UPDATED_SO_HIEU);
    }

    @Test
    @Transactional
    void getAllMaKeToansBySoHieuIsNullOrNotNull() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        // Get all the maKeToanList where soHieu is not null
        defaultMaKeToanShouldBeFound("soHieu.specified=true");

        // Get all the maKeToanList where soHieu is null
        defaultMaKeToanShouldNotBeFound("soHieu.specified=false");
    }

    @Test
    @Transactional
    void getAllMaKeToansBySoHieuIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        // Get all the maKeToanList where soHieu is greater than or equal to DEFAULT_SO_HIEU
        defaultMaKeToanShouldBeFound("soHieu.greaterThanOrEqual=" + DEFAULT_SO_HIEU);

        // Get all the maKeToanList where soHieu is greater than or equal to UPDATED_SO_HIEU
        defaultMaKeToanShouldNotBeFound("soHieu.greaterThanOrEqual=" + UPDATED_SO_HIEU);
    }

    @Test
    @Transactional
    void getAllMaKeToansBySoHieuIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        // Get all the maKeToanList where soHieu is less than or equal to DEFAULT_SO_HIEU
        defaultMaKeToanShouldBeFound("soHieu.lessThanOrEqual=" + DEFAULT_SO_HIEU);

        // Get all the maKeToanList where soHieu is less than or equal to SMALLER_SO_HIEU
        defaultMaKeToanShouldNotBeFound("soHieu.lessThanOrEqual=" + SMALLER_SO_HIEU);
    }

    @Test
    @Transactional
    void getAllMaKeToansBySoHieuIsLessThanSomething() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        // Get all the maKeToanList where soHieu is less than DEFAULT_SO_HIEU
        defaultMaKeToanShouldNotBeFound("soHieu.lessThan=" + DEFAULT_SO_HIEU);

        // Get all the maKeToanList where soHieu is less than UPDATED_SO_HIEU
        defaultMaKeToanShouldBeFound("soHieu.lessThan=" + UPDATED_SO_HIEU);
    }

    @Test
    @Transactional
    void getAllMaKeToansBySoHieuIsGreaterThanSomething() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        // Get all the maKeToanList where soHieu is greater than DEFAULT_SO_HIEU
        defaultMaKeToanShouldNotBeFound("soHieu.greaterThan=" + DEFAULT_SO_HIEU);

        // Get all the maKeToanList where soHieu is greater than SMALLER_SO_HIEU
        defaultMaKeToanShouldBeFound("soHieu.greaterThan=" + SMALLER_SO_HIEU);
    }

    @Test
    @Transactional
    void getAllMaKeToansByTenIsEqualToSomething() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        // Get all the maKeToanList where ten equals to DEFAULT_TEN
        defaultMaKeToanShouldBeFound("ten.equals=" + DEFAULT_TEN);

        // Get all the maKeToanList where ten equals to UPDATED_TEN
        defaultMaKeToanShouldNotBeFound("ten.equals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllMaKeToansByTenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        // Get all the maKeToanList where ten not equals to DEFAULT_TEN
        defaultMaKeToanShouldNotBeFound("ten.notEquals=" + DEFAULT_TEN);

        // Get all the maKeToanList where ten not equals to UPDATED_TEN
        defaultMaKeToanShouldBeFound("ten.notEquals=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllMaKeToansByTenIsInShouldWork() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        // Get all the maKeToanList where ten in DEFAULT_TEN or UPDATED_TEN
        defaultMaKeToanShouldBeFound("ten.in=" + DEFAULT_TEN + "," + UPDATED_TEN);

        // Get all the maKeToanList where ten equals to UPDATED_TEN
        defaultMaKeToanShouldNotBeFound("ten.in=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllMaKeToansByTenIsNullOrNotNull() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        // Get all the maKeToanList where ten is not null
        defaultMaKeToanShouldBeFound("ten.specified=true");

        // Get all the maKeToanList where ten is null
        defaultMaKeToanShouldNotBeFound("ten.specified=false");
    }

    @Test
    @Transactional
    void getAllMaKeToansByTenContainsSomething() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        // Get all the maKeToanList where ten contains DEFAULT_TEN
        defaultMaKeToanShouldBeFound("ten.contains=" + DEFAULT_TEN);

        // Get all the maKeToanList where ten contains UPDATED_TEN
        defaultMaKeToanShouldNotBeFound("ten.contains=" + UPDATED_TEN);
    }

    @Test
    @Transactional
    void getAllMaKeToansByTenNotContainsSomething() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        // Get all the maKeToanList where ten does not contain DEFAULT_TEN
        defaultMaKeToanShouldNotBeFound("ten.doesNotContain=" + DEFAULT_TEN);

        // Get all the maKeToanList where ten does not contain UPDATED_TEN
        defaultMaKeToanShouldBeFound("ten.doesNotContain=" + UPDATED_TEN);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMaKeToanShouldBeFound(String filter) throws Exception {
        restMaKeToanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(maKeToan.getId().intValue())))
            .andExpect(jsonPath("$.[*].soHieu").value(hasItem(DEFAULT_SO_HIEU.doubleValue())))
            .andExpect(jsonPath("$.[*].ten").value(hasItem(DEFAULT_TEN)));

        // Check, that the count call also returns 1
        restMaKeToanMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMaKeToanShouldNotBeFound(String filter) throws Exception {
        restMaKeToanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMaKeToanMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingMaKeToan() throws Exception {
        // Get the maKeToan
        restMaKeToanMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMaKeToan() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        int databaseSizeBeforeUpdate = maKeToanRepository.findAll().size();

        // Update the maKeToan
        MaKeToan updatedMaKeToan = maKeToanRepository.findById(maKeToan.getId()).get();
        // Disconnect from session so that the updates on updatedMaKeToan are not directly saved in db
        em.detach(updatedMaKeToan);
        updatedMaKeToan.soHieu(UPDATED_SO_HIEU).ten(UPDATED_TEN);
        MaKeToanDTO maKeToanDTO = maKeToanMapper.toDto(updatedMaKeToan);

        restMaKeToanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, maKeToanDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(maKeToanDTO))
            )
            .andExpect(status().isOk());

        // Validate the MaKeToan in the database
        List<MaKeToan> maKeToanList = maKeToanRepository.findAll();
        assertThat(maKeToanList).hasSize(databaseSizeBeforeUpdate);
        MaKeToan testMaKeToan = maKeToanList.get(maKeToanList.size() - 1);
        assertThat(testMaKeToan.getSoHieu()).isEqualTo(UPDATED_SO_HIEU);
        assertThat(testMaKeToan.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    void putNonExistingMaKeToan() throws Exception {
        int databaseSizeBeforeUpdate = maKeToanRepository.findAll().size();
        maKeToan.setId(count.incrementAndGet());

        // Create the MaKeToan
        MaKeToanDTO maKeToanDTO = maKeToanMapper.toDto(maKeToan);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMaKeToanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, maKeToanDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(maKeToanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MaKeToan in the database
        List<MaKeToan> maKeToanList = maKeToanRepository.findAll();
        assertThat(maKeToanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMaKeToan() throws Exception {
        int databaseSizeBeforeUpdate = maKeToanRepository.findAll().size();
        maKeToan.setId(count.incrementAndGet());

        // Create the MaKeToan
        MaKeToanDTO maKeToanDTO = maKeToanMapper.toDto(maKeToan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMaKeToanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(maKeToanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MaKeToan in the database
        List<MaKeToan> maKeToanList = maKeToanRepository.findAll();
        assertThat(maKeToanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMaKeToan() throws Exception {
        int databaseSizeBeforeUpdate = maKeToanRepository.findAll().size();
        maKeToan.setId(count.incrementAndGet());

        // Create the MaKeToan
        MaKeToanDTO maKeToanDTO = maKeToanMapper.toDto(maKeToan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMaKeToanMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(maKeToanDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the MaKeToan in the database
        List<MaKeToan> maKeToanList = maKeToanRepository.findAll();
        assertThat(maKeToanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMaKeToanWithPatch() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        int databaseSizeBeforeUpdate = maKeToanRepository.findAll().size();

        // Update the maKeToan using partial update
        MaKeToan partialUpdatedMaKeToan = new MaKeToan();
        partialUpdatedMaKeToan.setId(maKeToan.getId());

        restMaKeToanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMaKeToan.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMaKeToan))
            )
            .andExpect(status().isOk());

        // Validate the MaKeToan in the database
        List<MaKeToan> maKeToanList = maKeToanRepository.findAll();
        assertThat(maKeToanList).hasSize(databaseSizeBeforeUpdate);
        MaKeToan testMaKeToan = maKeToanList.get(maKeToanList.size() - 1);
        assertThat(testMaKeToan.getSoHieu()).isEqualTo(DEFAULT_SO_HIEU);
        assertThat(testMaKeToan.getTen()).isEqualTo(DEFAULT_TEN);
    }

    @Test
    @Transactional
    void fullUpdateMaKeToanWithPatch() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        int databaseSizeBeforeUpdate = maKeToanRepository.findAll().size();

        // Update the maKeToan using partial update
        MaKeToan partialUpdatedMaKeToan = new MaKeToan();
        partialUpdatedMaKeToan.setId(maKeToan.getId());

        partialUpdatedMaKeToan.soHieu(UPDATED_SO_HIEU).ten(UPDATED_TEN);

        restMaKeToanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMaKeToan.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMaKeToan))
            )
            .andExpect(status().isOk());

        // Validate the MaKeToan in the database
        List<MaKeToan> maKeToanList = maKeToanRepository.findAll();
        assertThat(maKeToanList).hasSize(databaseSizeBeforeUpdate);
        MaKeToan testMaKeToan = maKeToanList.get(maKeToanList.size() - 1);
        assertThat(testMaKeToan.getSoHieu()).isEqualTo(UPDATED_SO_HIEU);
        assertThat(testMaKeToan.getTen()).isEqualTo(UPDATED_TEN);
    }

    @Test
    @Transactional
    void patchNonExistingMaKeToan() throws Exception {
        int databaseSizeBeforeUpdate = maKeToanRepository.findAll().size();
        maKeToan.setId(count.incrementAndGet());

        // Create the MaKeToan
        MaKeToanDTO maKeToanDTO = maKeToanMapper.toDto(maKeToan);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMaKeToanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, maKeToanDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(maKeToanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MaKeToan in the database
        List<MaKeToan> maKeToanList = maKeToanRepository.findAll();
        assertThat(maKeToanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMaKeToan() throws Exception {
        int databaseSizeBeforeUpdate = maKeToanRepository.findAll().size();
        maKeToan.setId(count.incrementAndGet());

        // Create the MaKeToan
        MaKeToanDTO maKeToanDTO = maKeToanMapper.toDto(maKeToan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMaKeToanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(maKeToanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MaKeToan in the database
        List<MaKeToan> maKeToanList = maKeToanRepository.findAll();
        assertThat(maKeToanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMaKeToan() throws Exception {
        int databaseSizeBeforeUpdate = maKeToanRepository.findAll().size();
        maKeToan.setId(count.incrementAndGet());

        // Create the MaKeToan
        MaKeToanDTO maKeToanDTO = maKeToanMapper.toDto(maKeToan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMaKeToanMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(maKeToanDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MaKeToan in the database
        List<MaKeToan> maKeToanList = maKeToanRepository.findAll();
        assertThat(maKeToanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMaKeToan() throws Exception {
        // Initialize the database
        maKeToanRepository.saveAndFlush(maKeToan);

        int databaseSizeBeforeDelete = maKeToanRepository.findAll().size();

        // Delete the maKeToan
        restMaKeToanMockMvc
            .perform(delete(ENTITY_API_URL_ID, maKeToan.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MaKeToan> maKeToanList = maKeToanRepository.findAll();
        assertThat(maKeToanList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
