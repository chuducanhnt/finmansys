package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.ChiTietTangTSCD;
import com.group3.finmansys.domain.TaiSanCD;
import com.group3.finmansys.repository.ChiTietTangTSCDRepository;
import com.group3.finmansys.service.criteria.ChiTietTangTSCDCriteria;
import com.group3.finmansys.service.dto.ChiTietTangTSCDDTO;
import com.group3.finmansys.service.mapper.ChiTietTangTSCDMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ChiTietTangTSCDResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class ChiTietTangTSCDResourceIT {

    private static final Instant DEFAULT_NGAY_THU_MUA = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_NGAY_THU_MUA = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_SO_LUONG = 1;
    private static final Integer UPDATED_SO_LUONG = 2;
    private static final Integer SMALLER_SO_LUONG = 1 - 1;

    private static final Double DEFAULT_CHI_PHI_MUA = 1D;
    private static final Double UPDATED_CHI_PHI_MUA = 2D;
    private static final Double SMALLER_CHI_PHI_MUA = 1D - 1D;

    private static final String ENTITY_API_URL = "/api/chi-tiet-tang-tscds";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ChiTietTangTSCDRepository chiTietTangTSCDRepository;

    @Autowired
    private ChiTietTangTSCDMapper chiTietTangTSCDMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restChiTietTangTSCDMockMvc;

    private ChiTietTangTSCD chiTietTangTSCD;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiTietTangTSCD createEntity(EntityManager em) {
        ChiTietTangTSCD chiTietTangTSCD = new ChiTietTangTSCD()
            .ngayThuMua(DEFAULT_NGAY_THU_MUA)
            .soLuong(DEFAULT_SO_LUONG)
            .chiPhiMua(DEFAULT_CHI_PHI_MUA);
        // Add required entity
        TaiSanCD taiSanCD;
        if (TestUtil.findAll(em, TaiSanCD.class).isEmpty()) {
            taiSanCD = TaiSanCDResourceIT.createEntity(em);
            em.persist(taiSanCD);
            em.flush();
        } else {
            taiSanCD = TestUtil.findAll(em, TaiSanCD.class).get(0);
        }
        chiTietTangTSCD.setTaiSanCD(taiSanCD);
        return chiTietTangTSCD;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChiTietTangTSCD createUpdatedEntity(EntityManager em) {
        ChiTietTangTSCD chiTietTangTSCD = new ChiTietTangTSCD()
            .ngayThuMua(UPDATED_NGAY_THU_MUA)
            .soLuong(UPDATED_SO_LUONG)
            .chiPhiMua(UPDATED_CHI_PHI_MUA);
        // Add required entity
        TaiSanCD taiSanCD;
        if (TestUtil.findAll(em, TaiSanCD.class).isEmpty()) {
            taiSanCD = TaiSanCDResourceIT.createUpdatedEntity(em);
            em.persist(taiSanCD);
            em.flush();
        } else {
            taiSanCD = TestUtil.findAll(em, TaiSanCD.class).get(0);
        }
        chiTietTangTSCD.setTaiSanCD(taiSanCD);
        return chiTietTangTSCD;
    }

    @BeforeEach
    public void initTest() {
        chiTietTangTSCD = createEntity(em);
    }

    @Test
    @Transactional
    void createChiTietTangTSCD() throws Exception {
        int databaseSizeBeforeCreate = chiTietTangTSCDRepository.findAll().size();
        // Create the ChiTietTangTSCD
        ChiTietTangTSCDDTO chiTietTangTSCDDTO = chiTietTangTSCDMapper.toDto(chiTietTangTSCD);
        restChiTietTangTSCDMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chiTietTangTSCDDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ChiTietTangTSCD in the database
        List<ChiTietTangTSCD> chiTietTangTSCDList = chiTietTangTSCDRepository.findAll();
        assertThat(chiTietTangTSCDList).hasSize(databaseSizeBeforeCreate + 1);
        ChiTietTangTSCD testChiTietTangTSCD = chiTietTangTSCDList.get(chiTietTangTSCDList.size() - 1);
        assertThat(testChiTietTangTSCD.getNgayThuMua()).isEqualTo(DEFAULT_NGAY_THU_MUA);
        assertThat(testChiTietTangTSCD.getSoLuong()).isEqualTo(DEFAULT_SO_LUONG);
        assertThat(testChiTietTangTSCD.getChiPhiMua()).isEqualTo(DEFAULT_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void createChiTietTangTSCDWithExistingId() throws Exception {
        // Create the ChiTietTangTSCD with an existing ID
        chiTietTangTSCD.setId(1L);
        ChiTietTangTSCDDTO chiTietTangTSCDDTO = chiTietTangTSCDMapper.toDto(chiTietTangTSCD);

        int databaseSizeBeforeCreate = chiTietTangTSCDRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restChiTietTangTSCDMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chiTietTangTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChiTietTangTSCD in the database
        List<ChiTietTangTSCD> chiTietTangTSCDList = chiTietTangTSCDRepository.findAll();
        assertThat(chiTietTangTSCDList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNgayThuMuaIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiTietTangTSCDRepository.findAll().size();
        // set the field null
        chiTietTangTSCD.setNgayThuMua(null);

        // Create the ChiTietTangTSCD, which fails.
        ChiTietTangTSCDDTO chiTietTangTSCDDTO = chiTietTangTSCDMapper.toDto(chiTietTangTSCD);

        restChiTietTangTSCDMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chiTietTangTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        List<ChiTietTangTSCD> chiTietTangTSCDList = chiTietTangTSCDRepository.findAll();
        assertThat(chiTietTangTSCDList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSoLuongIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiTietTangTSCDRepository.findAll().size();
        // set the field null
        chiTietTangTSCD.setSoLuong(null);

        // Create the ChiTietTangTSCD, which fails.
        ChiTietTangTSCDDTO chiTietTangTSCDDTO = chiTietTangTSCDMapper.toDto(chiTietTangTSCD);

        restChiTietTangTSCDMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chiTietTangTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        List<ChiTietTangTSCD> chiTietTangTSCDList = chiTietTangTSCDRepository.findAll();
        assertThat(chiTietTangTSCDList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkChiPhiMuaIsRequired() throws Exception {
        int databaseSizeBeforeTest = chiTietTangTSCDRepository.findAll().size();
        // set the field null
        chiTietTangTSCD.setChiPhiMua(null);

        // Create the ChiTietTangTSCD, which fails.
        ChiTietTangTSCDDTO chiTietTangTSCDDTO = chiTietTangTSCDMapper.toDto(chiTietTangTSCD);

        restChiTietTangTSCDMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chiTietTangTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        List<ChiTietTangTSCD> chiTietTangTSCDList = chiTietTangTSCDRepository.findAll();
        assertThat(chiTietTangTSCDList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDS() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList
        restChiTietTangTSCDMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiTietTangTSCD.getId().intValue())))
            .andExpect(jsonPath("$.[*].ngayThuMua").value(hasItem(DEFAULT_NGAY_THU_MUA.toString())))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG)))
            .andExpect(jsonPath("$.[*].chiPhiMua").value(hasItem(DEFAULT_CHI_PHI_MUA.doubleValue())));
    }

    @Test
    @Transactional
    void getChiTietTangTSCD() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get the chiTietTangTSCD
        restChiTietTangTSCDMockMvc
            .perform(get(ENTITY_API_URL_ID, chiTietTangTSCD.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(chiTietTangTSCD.getId().intValue()))
            .andExpect(jsonPath("$.ngayThuMua").value(DEFAULT_NGAY_THU_MUA.toString()))
            .andExpect(jsonPath("$.soLuong").value(DEFAULT_SO_LUONG))
            .andExpect(jsonPath("$.chiPhiMua").value(DEFAULT_CHI_PHI_MUA.doubleValue()));
    }

    @Test
    @Transactional
    void getChiTietTangTSCDSByIdFiltering() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        Long id = chiTietTangTSCD.getId();

        defaultChiTietTangTSCDShouldBeFound("id.equals=" + id);
        defaultChiTietTangTSCDShouldNotBeFound("id.notEquals=" + id);

        defaultChiTietTangTSCDShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultChiTietTangTSCDShouldNotBeFound("id.greaterThan=" + id);

        defaultChiTietTangTSCDShouldBeFound("id.lessThanOrEqual=" + id);
        defaultChiTietTangTSCDShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSByNgayThuMuaIsEqualToSomething() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where ngayThuMua equals to DEFAULT_NGAY_THU_MUA
        defaultChiTietTangTSCDShouldBeFound("ngayThuMua.equals=" + DEFAULT_NGAY_THU_MUA);

        // Get all the chiTietTangTSCDList where ngayThuMua equals to UPDATED_NGAY_THU_MUA
        defaultChiTietTangTSCDShouldNotBeFound("ngayThuMua.equals=" + UPDATED_NGAY_THU_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSByNgayThuMuaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where ngayThuMua not equals to DEFAULT_NGAY_THU_MUA
        defaultChiTietTangTSCDShouldNotBeFound("ngayThuMua.notEquals=" + DEFAULT_NGAY_THU_MUA);

        // Get all the chiTietTangTSCDList where ngayThuMua not equals to UPDATED_NGAY_THU_MUA
        defaultChiTietTangTSCDShouldBeFound("ngayThuMua.notEquals=" + UPDATED_NGAY_THU_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSByNgayThuMuaIsInShouldWork() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where ngayThuMua in DEFAULT_NGAY_THU_MUA or UPDATED_NGAY_THU_MUA
        defaultChiTietTangTSCDShouldBeFound("ngayThuMua.in=" + DEFAULT_NGAY_THU_MUA + "," + UPDATED_NGAY_THU_MUA);

        // Get all the chiTietTangTSCDList where ngayThuMua equals to UPDATED_NGAY_THU_MUA
        defaultChiTietTangTSCDShouldNotBeFound("ngayThuMua.in=" + UPDATED_NGAY_THU_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSByNgayThuMuaIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where ngayThuMua is not null
        defaultChiTietTangTSCDShouldBeFound("ngayThuMua.specified=true");

        // Get all the chiTietTangTSCDList where ngayThuMua is null
        defaultChiTietTangTSCDShouldNotBeFound("ngayThuMua.specified=false");
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSBySoLuongIsEqualToSomething() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where soLuong equals to DEFAULT_SO_LUONG
        defaultChiTietTangTSCDShouldBeFound("soLuong.equals=" + DEFAULT_SO_LUONG);

        // Get all the chiTietTangTSCDList where soLuong equals to UPDATED_SO_LUONG
        defaultChiTietTangTSCDShouldNotBeFound("soLuong.equals=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSBySoLuongIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where soLuong not equals to DEFAULT_SO_LUONG
        defaultChiTietTangTSCDShouldNotBeFound("soLuong.notEquals=" + DEFAULT_SO_LUONG);

        // Get all the chiTietTangTSCDList where soLuong not equals to UPDATED_SO_LUONG
        defaultChiTietTangTSCDShouldBeFound("soLuong.notEquals=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSBySoLuongIsInShouldWork() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where soLuong in DEFAULT_SO_LUONG or UPDATED_SO_LUONG
        defaultChiTietTangTSCDShouldBeFound("soLuong.in=" + DEFAULT_SO_LUONG + "," + UPDATED_SO_LUONG);

        // Get all the chiTietTangTSCDList where soLuong equals to UPDATED_SO_LUONG
        defaultChiTietTangTSCDShouldNotBeFound("soLuong.in=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSBySoLuongIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where soLuong is not null
        defaultChiTietTangTSCDShouldBeFound("soLuong.specified=true");

        // Get all the chiTietTangTSCDList where soLuong is null
        defaultChiTietTangTSCDShouldNotBeFound("soLuong.specified=false");
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSBySoLuongIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where soLuong is greater than or equal to DEFAULT_SO_LUONG
        defaultChiTietTangTSCDShouldBeFound("soLuong.greaterThanOrEqual=" + DEFAULT_SO_LUONG);

        // Get all the chiTietTangTSCDList where soLuong is greater than or equal to UPDATED_SO_LUONG
        defaultChiTietTangTSCDShouldNotBeFound("soLuong.greaterThanOrEqual=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSBySoLuongIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where soLuong is less than or equal to DEFAULT_SO_LUONG
        defaultChiTietTangTSCDShouldBeFound("soLuong.lessThanOrEqual=" + DEFAULT_SO_LUONG);

        // Get all the chiTietTangTSCDList where soLuong is less than or equal to SMALLER_SO_LUONG
        defaultChiTietTangTSCDShouldNotBeFound("soLuong.lessThanOrEqual=" + SMALLER_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSBySoLuongIsLessThanSomething() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where soLuong is less than DEFAULT_SO_LUONG
        defaultChiTietTangTSCDShouldNotBeFound("soLuong.lessThan=" + DEFAULT_SO_LUONG);

        // Get all the chiTietTangTSCDList where soLuong is less than UPDATED_SO_LUONG
        defaultChiTietTangTSCDShouldBeFound("soLuong.lessThan=" + UPDATED_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSBySoLuongIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where soLuong is greater than DEFAULT_SO_LUONG
        defaultChiTietTangTSCDShouldNotBeFound("soLuong.greaterThan=" + DEFAULT_SO_LUONG);

        // Get all the chiTietTangTSCDList where soLuong is greater than SMALLER_SO_LUONG
        defaultChiTietTangTSCDShouldBeFound("soLuong.greaterThan=" + SMALLER_SO_LUONG);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSByChiPhiMuaIsEqualToSomething() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where chiPhiMua equals to DEFAULT_CHI_PHI_MUA
        defaultChiTietTangTSCDShouldBeFound("chiPhiMua.equals=" + DEFAULT_CHI_PHI_MUA);

        // Get all the chiTietTangTSCDList where chiPhiMua equals to UPDATED_CHI_PHI_MUA
        defaultChiTietTangTSCDShouldNotBeFound("chiPhiMua.equals=" + UPDATED_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSByChiPhiMuaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where chiPhiMua not equals to DEFAULT_CHI_PHI_MUA
        defaultChiTietTangTSCDShouldNotBeFound("chiPhiMua.notEquals=" + DEFAULT_CHI_PHI_MUA);

        // Get all the chiTietTangTSCDList where chiPhiMua not equals to UPDATED_CHI_PHI_MUA
        defaultChiTietTangTSCDShouldBeFound("chiPhiMua.notEquals=" + UPDATED_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSByChiPhiMuaIsInShouldWork() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where chiPhiMua in DEFAULT_CHI_PHI_MUA or UPDATED_CHI_PHI_MUA
        defaultChiTietTangTSCDShouldBeFound("chiPhiMua.in=" + DEFAULT_CHI_PHI_MUA + "," + UPDATED_CHI_PHI_MUA);

        // Get all the chiTietTangTSCDList where chiPhiMua equals to UPDATED_CHI_PHI_MUA
        defaultChiTietTangTSCDShouldNotBeFound("chiPhiMua.in=" + UPDATED_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSByChiPhiMuaIsNullOrNotNull() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where chiPhiMua is not null
        defaultChiTietTangTSCDShouldBeFound("chiPhiMua.specified=true");

        // Get all the chiTietTangTSCDList where chiPhiMua is null
        defaultChiTietTangTSCDShouldNotBeFound("chiPhiMua.specified=false");
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSByChiPhiMuaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where chiPhiMua is greater than or equal to DEFAULT_CHI_PHI_MUA
        defaultChiTietTangTSCDShouldBeFound("chiPhiMua.greaterThanOrEqual=" + DEFAULT_CHI_PHI_MUA);

        // Get all the chiTietTangTSCDList where chiPhiMua is greater than or equal to UPDATED_CHI_PHI_MUA
        defaultChiTietTangTSCDShouldNotBeFound("chiPhiMua.greaterThanOrEqual=" + UPDATED_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSByChiPhiMuaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where chiPhiMua is less than or equal to DEFAULT_CHI_PHI_MUA
        defaultChiTietTangTSCDShouldBeFound("chiPhiMua.lessThanOrEqual=" + DEFAULT_CHI_PHI_MUA);

        // Get all the chiTietTangTSCDList where chiPhiMua is less than or equal to SMALLER_CHI_PHI_MUA
        defaultChiTietTangTSCDShouldNotBeFound("chiPhiMua.lessThanOrEqual=" + SMALLER_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSByChiPhiMuaIsLessThanSomething() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where chiPhiMua is less than DEFAULT_CHI_PHI_MUA
        defaultChiTietTangTSCDShouldNotBeFound("chiPhiMua.lessThan=" + DEFAULT_CHI_PHI_MUA);

        // Get all the chiTietTangTSCDList where chiPhiMua is less than UPDATED_CHI_PHI_MUA
        defaultChiTietTangTSCDShouldBeFound("chiPhiMua.lessThan=" + UPDATED_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSByChiPhiMuaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        // Get all the chiTietTangTSCDList where chiPhiMua is greater than DEFAULT_CHI_PHI_MUA
        defaultChiTietTangTSCDShouldNotBeFound("chiPhiMua.greaterThan=" + DEFAULT_CHI_PHI_MUA);

        // Get all the chiTietTangTSCDList where chiPhiMua is greater than SMALLER_CHI_PHI_MUA
        defaultChiTietTangTSCDShouldBeFound("chiPhiMua.greaterThan=" + SMALLER_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void getAllChiTietTangTSCDSByTaiSanCDIsEqualToSomething() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);
        TaiSanCD taiSanCD = TaiSanCDResourceIT.createEntity(em);
        em.persist(taiSanCD);
        em.flush();
        chiTietTangTSCD.setTaiSanCD(taiSanCD);
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);
        Long taiSanCDId = taiSanCD.getId();

        // Get all the chiTietTangTSCDList where taiSanCD equals to taiSanCDId
        defaultChiTietTangTSCDShouldBeFound("taiSanCDId.equals=" + taiSanCDId);

        // Get all the chiTietTangTSCDList where taiSanCD equals to (taiSanCDId + 1)
        defaultChiTietTangTSCDShouldNotBeFound("taiSanCDId.equals=" + (taiSanCDId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultChiTietTangTSCDShouldBeFound(String filter) throws Exception {
        restChiTietTangTSCDMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chiTietTangTSCD.getId().intValue())))
            .andExpect(jsonPath("$.[*].ngayThuMua").value(hasItem(DEFAULT_NGAY_THU_MUA.toString())))
            .andExpect(jsonPath("$.[*].soLuong").value(hasItem(DEFAULT_SO_LUONG)))
            .andExpect(jsonPath("$.[*].chiPhiMua").value(hasItem(DEFAULT_CHI_PHI_MUA.doubleValue())));

        // Check, that the count call also returns 1
        restChiTietTangTSCDMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultChiTietTangTSCDShouldNotBeFound(String filter) throws Exception {
        restChiTietTangTSCDMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restChiTietTangTSCDMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingChiTietTangTSCD() throws Exception {
        // Get the chiTietTangTSCD
        restChiTietTangTSCDMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewChiTietTangTSCD() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        int databaseSizeBeforeUpdate = chiTietTangTSCDRepository.findAll().size();

        // Update the chiTietTangTSCD
        ChiTietTangTSCD updatedChiTietTangTSCD = chiTietTangTSCDRepository.findById(chiTietTangTSCD.getId()).get();
        // Disconnect from session so that the updates on updatedChiTietTangTSCD are not directly saved in db
        em.detach(updatedChiTietTangTSCD);
        updatedChiTietTangTSCD.ngayThuMua(UPDATED_NGAY_THU_MUA).soLuong(UPDATED_SO_LUONG).chiPhiMua(UPDATED_CHI_PHI_MUA);
        ChiTietTangTSCDDTO chiTietTangTSCDDTO = chiTietTangTSCDMapper.toDto(updatedChiTietTangTSCD);

        restChiTietTangTSCDMockMvc
            .perform(
                put(ENTITY_API_URL_ID, chiTietTangTSCDDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(chiTietTangTSCDDTO))
            )
            .andExpect(status().isOk());

        // Validate the ChiTietTangTSCD in the database
        List<ChiTietTangTSCD> chiTietTangTSCDList = chiTietTangTSCDRepository.findAll();
        assertThat(chiTietTangTSCDList).hasSize(databaseSizeBeforeUpdate);
        ChiTietTangTSCD testChiTietTangTSCD = chiTietTangTSCDList.get(chiTietTangTSCDList.size() - 1);
        assertThat(testChiTietTangTSCD.getNgayThuMua()).isEqualTo(UPDATED_NGAY_THU_MUA);
        assertThat(testChiTietTangTSCD.getSoLuong()).isEqualTo(UPDATED_SO_LUONG);
        assertThat(testChiTietTangTSCD.getChiPhiMua()).isEqualTo(UPDATED_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void putNonExistingChiTietTangTSCD() throws Exception {
        int databaseSizeBeforeUpdate = chiTietTangTSCDRepository.findAll().size();
        chiTietTangTSCD.setId(count.incrementAndGet());

        // Create the ChiTietTangTSCD
        ChiTietTangTSCDDTO chiTietTangTSCDDTO = chiTietTangTSCDMapper.toDto(chiTietTangTSCD);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChiTietTangTSCDMockMvc
            .perform(
                put(ENTITY_API_URL_ID, chiTietTangTSCDDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(chiTietTangTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChiTietTangTSCD in the database
        List<ChiTietTangTSCD> chiTietTangTSCDList = chiTietTangTSCDRepository.findAll();
        assertThat(chiTietTangTSCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchChiTietTangTSCD() throws Exception {
        int databaseSizeBeforeUpdate = chiTietTangTSCDRepository.findAll().size();
        chiTietTangTSCD.setId(count.incrementAndGet());

        // Create the ChiTietTangTSCD
        ChiTietTangTSCDDTO chiTietTangTSCDDTO = chiTietTangTSCDMapper.toDto(chiTietTangTSCD);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChiTietTangTSCDMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(chiTietTangTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChiTietTangTSCD in the database
        List<ChiTietTangTSCD> chiTietTangTSCDList = chiTietTangTSCDRepository.findAll();
        assertThat(chiTietTangTSCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamChiTietTangTSCD() throws Exception {
        int databaseSizeBeforeUpdate = chiTietTangTSCDRepository.findAll().size();
        chiTietTangTSCD.setId(count.incrementAndGet());

        // Create the ChiTietTangTSCD
        ChiTietTangTSCDDTO chiTietTangTSCDDTO = chiTietTangTSCDMapper.toDto(chiTietTangTSCD);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChiTietTangTSCDMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chiTietTangTSCDDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ChiTietTangTSCD in the database
        List<ChiTietTangTSCD> chiTietTangTSCDList = chiTietTangTSCDRepository.findAll();
        assertThat(chiTietTangTSCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateChiTietTangTSCDWithPatch() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        int databaseSizeBeforeUpdate = chiTietTangTSCDRepository.findAll().size();

        // Update the chiTietTangTSCD using partial update
        ChiTietTangTSCD partialUpdatedChiTietTangTSCD = new ChiTietTangTSCD();
        partialUpdatedChiTietTangTSCD.setId(chiTietTangTSCD.getId());

        partialUpdatedChiTietTangTSCD.ngayThuMua(UPDATED_NGAY_THU_MUA).soLuong(UPDATED_SO_LUONG);

        restChiTietTangTSCDMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedChiTietTangTSCD.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedChiTietTangTSCD))
            )
            .andExpect(status().isOk());

        // Validate the ChiTietTangTSCD in the database
        List<ChiTietTangTSCD> chiTietTangTSCDList = chiTietTangTSCDRepository.findAll();
        assertThat(chiTietTangTSCDList).hasSize(databaseSizeBeforeUpdate);
        ChiTietTangTSCD testChiTietTangTSCD = chiTietTangTSCDList.get(chiTietTangTSCDList.size() - 1);
        assertThat(testChiTietTangTSCD.getNgayThuMua()).isEqualTo(UPDATED_NGAY_THU_MUA);
        assertThat(testChiTietTangTSCD.getSoLuong()).isEqualTo(UPDATED_SO_LUONG);
        assertThat(testChiTietTangTSCD.getChiPhiMua()).isEqualTo(DEFAULT_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void fullUpdateChiTietTangTSCDWithPatch() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        int databaseSizeBeforeUpdate = chiTietTangTSCDRepository.findAll().size();

        // Update the chiTietTangTSCD using partial update
        ChiTietTangTSCD partialUpdatedChiTietTangTSCD = new ChiTietTangTSCD();
        partialUpdatedChiTietTangTSCD.setId(chiTietTangTSCD.getId());

        partialUpdatedChiTietTangTSCD.ngayThuMua(UPDATED_NGAY_THU_MUA).soLuong(UPDATED_SO_LUONG).chiPhiMua(UPDATED_CHI_PHI_MUA);

        restChiTietTangTSCDMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedChiTietTangTSCD.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedChiTietTangTSCD))
            )
            .andExpect(status().isOk());

        // Validate the ChiTietTangTSCD in the database
        List<ChiTietTangTSCD> chiTietTangTSCDList = chiTietTangTSCDRepository.findAll();
        assertThat(chiTietTangTSCDList).hasSize(databaseSizeBeforeUpdate);
        ChiTietTangTSCD testChiTietTangTSCD = chiTietTangTSCDList.get(chiTietTangTSCDList.size() - 1);
        assertThat(testChiTietTangTSCD.getNgayThuMua()).isEqualTo(UPDATED_NGAY_THU_MUA);
        assertThat(testChiTietTangTSCD.getSoLuong()).isEqualTo(UPDATED_SO_LUONG);
        assertThat(testChiTietTangTSCD.getChiPhiMua()).isEqualTo(UPDATED_CHI_PHI_MUA);
    }

    @Test
    @Transactional
    void patchNonExistingChiTietTangTSCD() throws Exception {
        int databaseSizeBeforeUpdate = chiTietTangTSCDRepository.findAll().size();
        chiTietTangTSCD.setId(count.incrementAndGet());

        // Create the ChiTietTangTSCD
        ChiTietTangTSCDDTO chiTietTangTSCDDTO = chiTietTangTSCDMapper.toDto(chiTietTangTSCD);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChiTietTangTSCDMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, chiTietTangTSCDDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(chiTietTangTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChiTietTangTSCD in the database
        List<ChiTietTangTSCD> chiTietTangTSCDList = chiTietTangTSCDRepository.findAll();
        assertThat(chiTietTangTSCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchChiTietTangTSCD() throws Exception {
        int databaseSizeBeforeUpdate = chiTietTangTSCDRepository.findAll().size();
        chiTietTangTSCD.setId(count.incrementAndGet());

        // Create the ChiTietTangTSCD
        ChiTietTangTSCDDTO chiTietTangTSCDDTO = chiTietTangTSCDMapper.toDto(chiTietTangTSCD);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChiTietTangTSCDMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(chiTietTangTSCDDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChiTietTangTSCD in the database
        List<ChiTietTangTSCD> chiTietTangTSCDList = chiTietTangTSCDRepository.findAll();
        assertThat(chiTietTangTSCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamChiTietTangTSCD() throws Exception {
        int databaseSizeBeforeUpdate = chiTietTangTSCDRepository.findAll().size();
        chiTietTangTSCD.setId(count.incrementAndGet());

        // Create the ChiTietTangTSCD
        ChiTietTangTSCDDTO chiTietTangTSCDDTO = chiTietTangTSCDMapper.toDto(chiTietTangTSCD);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChiTietTangTSCDMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(chiTietTangTSCDDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ChiTietTangTSCD in the database
        List<ChiTietTangTSCD> chiTietTangTSCDList = chiTietTangTSCDRepository.findAll();
        assertThat(chiTietTangTSCDList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteChiTietTangTSCD() throws Exception {
        // Initialize the database
        chiTietTangTSCDRepository.saveAndFlush(chiTietTangTSCD);

        int databaseSizeBeforeDelete = chiTietTangTSCDRepository.findAll().size();

        // Delete the chiTietTangTSCD
        restChiTietTangTSCDMockMvc
            .perform(delete(ENTITY_API_URL_ID, chiTietTangTSCD.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ChiTietTangTSCD> chiTietTangTSCDList = chiTietTangTSCDRepository.findAll();
        assertThat(chiTietTangTSCDList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
