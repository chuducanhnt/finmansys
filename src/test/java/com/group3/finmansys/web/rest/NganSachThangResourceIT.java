package com.group3.finmansys.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.group3.finmansys.IntegrationTest;
import com.group3.finmansys.domain.KhoanMucChiPhi;
import com.group3.finmansys.domain.NganSachNam;
import com.group3.finmansys.domain.NganSachThang;
import com.group3.finmansys.repository.NganSachThangRepository;
import com.group3.finmansys.service.criteria.NganSachThangCriteria;
import com.group3.finmansys.service.dto.NganSachThangDTO;
import com.group3.finmansys.service.mapper.NganSachThangMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link NganSachThangResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class NganSachThangResourceIT {

    private static final Integer DEFAULT_THANG = 1;
    private static final Integer UPDATED_THANG = 2;
    private static final Integer SMALLER_THANG = 1 - 1;

    private static final Double DEFAULT_SO_TIEN = 1D;
    private static final Double UPDATED_SO_TIEN = 2D;
    private static final Double SMALLER_SO_TIEN = 1D - 1D;

    private static final String ENTITY_API_URL = "/api/ngan-sach-thangs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private NganSachThangRepository nganSachThangRepository;

    @Autowired
    private NganSachThangMapper nganSachThangMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restNganSachThangMockMvc;

    private NganSachThang nganSachThang;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NganSachThang createEntity(EntityManager em) {
        NganSachThang nganSachThang = new NganSachThang().thang(DEFAULT_THANG).soTien(DEFAULT_SO_TIEN);
        // Add required entity
        NganSachNam nganSachNam;
        if (TestUtil.findAll(em, NganSachNam.class).isEmpty()) {
            nganSachNam = NganSachNamResourceIT.createEntity(em);
            em.persist(nganSachNam);
            em.flush();
        } else {
            nganSachNam = TestUtil.findAll(em, NganSachNam.class).get(0);
        }
        nganSachThang.setNganSachNam(nganSachNam);
        // Add required entity
        KhoanMucChiPhi khoanMucChiPhi;
        if (TestUtil.findAll(em, KhoanMucChiPhi.class).isEmpty()) {
            khoanMucChiPhi = KhoanMucChiPhiResourceIT.createEntity(em);
            em.persist(khoanMucChiPhi);
            em.flush();
        } else {
            khoanMucChiPhi = TestUtil.findAll(em, KhoanMucChiPhi.class).get(0);
        }
        nganSachThang.setKhoanMucChiPhi(khoanMucChiPhi);
        return nganSachThang;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NganSachThang createUpdatedEntity(EntityManager em) {
        NganSachThang nganSachThang = new NganSachThang().thang(UPDATED_THANG).soTien(UPDATED_SO_TIEN);
        // Add required entity
        NganSachNam nganSachNam;
        if (TestUtil.findAll(em, NganSachNam.class).isEmpty()) {
            nganSachNam = NganSachNamResourceIT.createUpdatedEntity(em);
            em.persist(nganSachNam);
            em.flush();
        } else {
            nganSachNam = TestUtil.findAll(em, NganSachNam.class).get(0);
        }
        nganSachThang.setNganSachNam(nganSachNam);
        // Add required entity
        KhoanMucChiPhi khoanMucChiPhi;
        if (TestUtil.findAll(em, KhoanMucChiPhi.class).isEmpty()) {
            khoanMucChiPhi = KhoanMucChiPhiResourceIT.createUpdatedEntity(em);
            em.persist(khoanMucChiPhi);
            em.flush();
        } else {
            khoanMucChiPhi = TestUtil.findAll(em, KhoanMucChiPhi.class).get(0);
        }
        nganSachThang.setKhoanMucChiPhi(khoanMucChiPhi);
        return nganSachThang;
    }

    @BeforeEach
    public void initTest() {
        nganSachThang = createEntity(em);
    }

    @Test
    @Transactional
    void createNganSachThang() throws Exception {
        int databaseSizeBeforeCreate = nganSachThangRepository.findAll().size();
        // Create the NganSachThang
        NganSachThangDTO nganSachThangDTO = nganSachThangMapper.toDto(nganSachThang);
        restNganSachThangMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(nganSachThangDTO))
            )
            .andExpect(status().isCreated());

        // Validate the NganSachThang in the database
        List<NganSachThang> nganSachThangList = nganSachThangRepository.findAll();
        assertThat(nganSachThangList).hasSize(databaseSizeBeforeCreate + 1);
        NganSachThang testNganSachThang = nganSachThangList.get(nganSachThangList.size() - 1);
        assertThat(testNganSachThang.getThang()).isEqualTo(DEFAULT_THANG);
        assertThat(testNganSachThang.getSoTien()).isEqualTo(DEFAULT_SO_TIEN);
    }

    @Test
    @Transactional
    void createNganSachThangWithExistingId() throws Exception {
        // Create the NganSachThang with an existing ID
        nganSachThang.setId(1L);
        NganSachThangDTO nganSachThangDTO = nganSachThangMapper.toDto(nganSachThang);

        int databaseSizeBeforeCreate = nganSachThangRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restNganSachThangMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(nganSachThangDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the NganSachThang in the database
        List<NganSachThang> nganSachThangList = nganSachThangRepository.findAll();
        assertThat(nganSachThangList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkThangIsRequired() throws Exception {
        int databaseSizeBeforeTest = nganSachThangRepository.findAll().size();
        // set the field null
        nganSachThang.setThang(null);

        // Create the NganSachThang, which fails.
        NganSachThangDTO nganSachThangDTO = nganSachThangMapper.toDto(nganSachThang);

        restNganSachThangMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(nganSachThangDTO))
            )
            .andExpect(status().isBadRequest());

        List<NganSachThang> nganSachThangList = nganSachThangRepository.findAll();
        assertThat(nganSachThangList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSoTienIsRequired() throws Exception {
        int databaseSizeBeforeTest = nganSachThangRepository.findAll().size();
        // set the field null
        nganSachThang.setSoTien(null);

        // Create the NganSachThang, which fails.
        NganSachThangDTO nganSachThangDTO = nganSachThangMapper.toDto(nganSachThang);

        restNganSachThangMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(nganSachThangDTO))
            )
            .andExpect(status().isBadRequest());

        List<NganSachThang> nganSachThangList = nganSachThangRepository.findAll();
        assertThat(nganSachThangList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllNganSachThangs() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList
        restNganSachThangMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nganSachThang.getId().intValue())))
            .andExpect(jsonPath("$.[*].thang").value(hasItem(DEFAULT_THANG)))
            .andExpect(jsonPath("$.[*].soTien").value(hasItem(DEFAULT_SO_TIEN.doubleValue())));
    }

    @Test
    @Transactional
    void getNganSachThang() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get the nganSachThang
        restNganSachThangMockMvc
            .perform(get(ENTITY_API_URL_ID, nganSachThang.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(nganSachThang.getId().intValue()))
            .andExpect(jsonPath("$.thang").value(DEFAULT_THANG))
            .andExpect(jsonPath("$.soTien").value(DEFAULT_SO_TIEN.doubleValue()));
    }

    @Test
    @Transactional
    void getNganSachThangsByIdFiltering() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        Long id = nganSachThang.getId();

        defaultNganSachThangShouldBeFound("id.equals=" + id);
        defaultNganSachThangShouldNotBeFound("id.notEquals=" + id);

        defaultNganSachThangShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultNganSachThangShouldNotBeFound("id.greaterThan=" + id);

        defaultNganSachThangShouldBeFound("id.lessThanOrEqual=" + id);
        defaultNganSachThangShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllNganSachThangsByThangIsEqualToSomething() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList where thang equals to DEFAULT_THANG
        defaultNganSachThangShouldBeFound("thang.equals=" + DEFAULT_THANG);

        // Get all the nganSachThangList where thang equals to UPDATED_THANG
        defaultNganSachThangShouldNotBeFound("thang.equals=" + UPDATED_THANG);
    }

    @Test
    @Transactional
    void getAllNganSachThangsByThangIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList where thang not equals to DEFAULT_THANG
        defaultNganSachThangShouldNotBeFound("thang.notEquals=" + DEFAULT_THANG);

        // Get all the nganSachThangList where thang not equals to UPDATED_THANG
        defaultNganSachThangShouldBeFound("thang.notEquals=" + UPDATED_THANG);
    }

    @Test
    @Transactional
    void getAllNganSachThangsByThangIsInShouldWork() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList where thang in DEFAULT_THANG or UPDATED_THANG
        defaultNganSachThangShouldBeFound("thang.in=" + DEFAULT_THANG + "," + UPDATED_THANG);

        // Get all the nganSachThangList where thang equals to UPDATED_THANG
        defaultNganSachThangShouldNotBeFound("thang.in=" + UPDATED_THANG);
    }

    @Test
    @Transactional
    void getAllNganSachThangsByThangIsNullOrNotNull() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList where thang is not null
        defaultNganSachThangShouldBeFound("thang.specified=true");

        // Get all the nganSachThangList where thang is null
        defaultNganSachThangShouldNotBeFound("thang.specified=false");
    }

    @Test
    @Transactional
    void getAllNganSachThangsByThangIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList where thang is greater than or equal to DEFAULT_THANG
        defaultNganSachThangShouldBeFound("thang.greaterThanOrEqual=" + DEFAULT_THANG);

        // Get all the nganSachThangList where thang is greater than or equal to (DEFAULT_THANG + 1)
        defaultNganSachThangShouldNotBeFound("thang.greaterThanOrEqual=" + (DEFAULT_THANG + 1));
    }

    @Test
    @Transactional
    void getAllNganSachThangsByThangIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList where thang is less than or equal to DEFAULT_THANG
        defaultNganSachThangShouldBeFound("thang.lessThanOrEqual=" + DEFAULT_THANG);

        // Get all the nganSachThangList where thang is less than or equal to SMALLER_THANG
        defaultNganSachThangShouldNotBeFound("thang.lessThanOrEqual=" + SMALLER_THANG);
    }

    @Test
    @Transactional
    void getAllNganSachThangsByThangIsLessThanSomething() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList where thang is less than DEFAULT_THANG
        defaultNganSachThangShouldNotBeFound("thang.lessThan=" + DEFAULT_THANG);

        // Get all the nganSachThangList where thang is less than (DEFAULT_THANG + 1)
        defaultNganSachThangShouldBeFound("thang.lessThan=" + (DEFAULT_THANG + 1));
    }

    @Test
    @Transactional
    void getAllNganSachThangsByThangIsGreaterThanSomething() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList where thang is greater than DEFAULT_THANG
        defaultNganSachThangShouldNotBeFound("thang.greaterThan=" + DEFAULT_THANG);

        // Get all the nganSachThangList where thang is greater than SMALLER_THANG
        defaultNganSachThangShouldBeFound("thang.greaterThan=" + SMALLER_THANG);
    }

    @Test
    @Transactional
    void getAllNganSachThangsBySoTienIsEqualToSomething() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList where soTien equals to DEFAULT_SO_TIEN
        defaultNganSachThangShouldBeFound("soTien.equals=" + DEFAULT_SO_TIEN);

        // Get all the nganSachThangList where soTien equals to UPDATED_SO_TIEN
        defaultNganSachThangShouldNotBeFound("soTien.equals=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllNganSachThangsBySoTienIsNotEqualToSomething() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList where soTien not equals to DEFAULT_SO_TIEN
        defaultNganSachThangShouldNotBeFound("soTien.notEquals=" + DEFAULT_SO_TIEN);

        // Get all the nganSachThangList where soTien not equals to UPDATED_SO_TIEN
        defaultNganSachThangShouldBeFound("soTien.notEquals=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllNganSachThangsBySoTienIsInShouldWork() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList where soTien in DEFAULT_SO_TIEN or UPDATED_SO_TIEN
        defaultNganSachThangShouldBeFound("soTien.in=" + DEFAULT_SO_TIEN + "," + UPDATED_SO_TIEN);

        // Get all the nganSachThangList where soTien equals to UPDATED_SO_TIEN
        defaultNganSachThangShouldNotBeFound("soTien.in=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllNganSachThangsBySoTienIsNullOrNotNull() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList where soTien is not null
        defaultNganSachThangShouldBeFound("soTien.specified=true");

        // Get all the nganSachThangList where soTien is null
        defaultNganSachThangShouldNotBeFound("soTien.specified=false");
    }

    @Test
    @Transactional
    void getAllNganSachThangsBySoTienIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList where soTien is greater than or equal to DEFAULT_SO_TIEN
        defaultNganSachThangShouldBeFound("soTien.greaterThanOrEqual=" + DEFAULT_SO_TIEN);

        // Get all the nganSachThangList where soTien is greater than or equal to UPDATED_SO_TIEN
        defaultNganSachThangShouldNotBeFound("soTien.greaterThanOrEqual=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllNganSachThangsBySoTienIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList where soTien is less than or equal to DEFAULT_SO_TIEN
        defaultNganSachThangShouldBeFound("soTien.lessThanOrEqual=" + DEFAULT_SO_TIEN);

        // Get all the nganSachThangList where soTien is less than or equal to SMALLER_SO_TIEN
        defaultNganSachThangShouldNotBeFound("soTien.lessThanOrEqual=" + SMALLER_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllNganSachThangsBySoTienIsLessThanSomething() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList where soTien is less than DEFAULT_SO_TIEN
        defaultNganSachThangShouldNotBeFound("soTien.lessThan=" + DEFAULT_SO_TIEN);

        // Get all the nganSachThangList where soTien is less than UPDATED_SO_TIEN
        defaultNganSachThangShouldBeFound("soTien.lessThan=" + UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllNganSachThangsBySoTienIsGreaterThanSomething() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        // Get all the nganSachThangList where soTien is greater than DEFAULT_SO_TIEN
        defaultNganSachThangShouldNotBeFound("soTien.greaterThan=" + DEFAULT_SO_TIEN);

        // Get all the nganSachThangList where soTien is greater than SMALLER_SO_TIEN
        defaultNganSachThangShouldBeFound("soTien.greaterThan=" + SMALLER_SO_TIEN);
    }

    @Test
    @Transactional
    void getAllNganSachThangsByNganSachNamIsEqualToSomething() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);
        NganSachNam nganSachNam = NganSachNamResourceIT.createEntity(em);
        em.persist(nganSachNam);
        em.flush();
        nganSachThang.setNganSachNam(nganSachNam);
        nganSachThangRepository.saveAndFlush(nganSachThang);
        Long nganSachNamId = nganSachNam.getId();

        // Get all the nganSachThangList where nganSachNam equals to nganSachNamId
        defaultNganSachThangShouldBeFound("nganSachNamId.equals=" + nganSachNamId);

        // Get all the nganSachThangList where nganSachNam equals to (nganSachNamId + 1)
        defaultNganSachThangShouldNotBeFound("nganSachNamId.equals=" + (nganSachNamId + 1));
    }

    @Test
    @Transactional
    void getAllNganSachThangsByKhoanMucChiPhiIsEqualToSomething() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);
        KhoanMucChiPhi khoanMucChiPhi = KhoanMucChiPhiResourceIT.createEntity(em);
        khoanMucChiPhi.setTen("PPPPPPPP");
        khoanMucChiPhi = em.merge(khoanMucChiPhi);
        em.flush();
        nganSachThang.setKhoanMucChiPhi(khoanMucChiPhi);
        nganSachThangRepository.saveAndFlush(nganSachThang);
        Long khoanMucChiPhiId = khoanMucChiPhi.getId();

        // Get all the nganSachThangList where khoanMucChiPhi equals to khoanMucChiPhiId
        defaultNganSachThangShouldBeFound("khoanMucChiPhiId.equals=" + khoanMucChiPhiId);

        // Get all the nganSachThangList where khoanMucChiPhi equals to (khoanMucChiPhiId + 1)
        defaultNganSachThangShouldNotBeFound("khoanMucChiPhiId.equals=" + (khoanMucChiPhiId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNganSachThangShouldBeFound(String filter) throws Exception {
        restNganSachThangMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nganSachThang.getId().intValue())))
            .andExpect(jsonPath("$.[*].thang").value(hasItem(DEFAULT_THANG)))
            .andExpect(jsonPath("$.[*].soTien").value(hasItem(DEFAULT_SO_TIEN.doubleValue())));

        // Check, that the count call also returns 1
        restNganSachThangMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNganSachThangShouldNotBeFound(String filter) throws Exception {
        restNganSachThangMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNganSachThangMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingNganSachThang() throws Exception {
        // Get the nganSachThang
        restNganSachThangMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewNganSachThang() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        int databaseSizeBeforeUpdate = nganSachThangRepository.findAll().size();

        // Update the nganSachThang
        NganSachThang updatedNganSachThang = nganSachThangRepository.findById(nganSachThang.getId()).get();
        // Disconnect from session so that the updates on updatedNganSachThang are not directly saved in db
        em.detach(updatedNganSachThang);
        updatedNganSachThang.thang(UPDATED_THANG).soTien(UPDATED_SO_TIEN);
        NganSachThangDTO nganSachThangDTO = nganSachThangMapper.toDto(updatedNganSachThang);

        restNganSachThangMockMvc
            .perform(
                put(ENTITY_API_URL_ID, nganSachThangDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(nganSachThangDTO))
            )
            .andExpect(status().isOk());

        // Validate the NganSachThang in the database
        List<NganSachThang> nganSachThangList = nganSachThangRepository.findAll();
        assertThat(nganSachThangList).hasSize(databaseSizeBeforeUpdate);
        NganSachThang testNganSachThang = nganSachThangList.get(nganSachThangList.size() - 1);
        assertThat(testNganSachThang.getThang()).isEqualTo(UPDATED_THANG);
        assertThat(testNganSachThang.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void putNonExistingNganSachThang() throws Exception {
        int databaseSizeBeforeUpdate = nganSachThangRepository.findAll().size();
        nganSachThang.setId(count.incrementAndGet());

        // Create the NganSachThang
        NganSachThangDTO nganSachThangDTO = nganSachThangMapper.toDto(nganSachThang);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNganSachThangMockMvc
            .perform(
                put(ENTITY_API_URL_ID, nganSachThangDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(nganSachThangDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the NganSachThang in the database
        List<NganSachThang> nganSachThangList = nganSachThangRepository.findAll();
        assertThat(nganSachThangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchNganSachThang() throws Exception {
        int databaseSizeBeforeUpdate = nganSachThangRepository.findAll().size();
        nganSachThang.setId(count.incrementAndGet());

        // Create the NganSachThang
        NganSachThangDTO nganSachThangDTO = nganSachThangMapper.toDto(nganSachThang);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNganSachThangMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(nganSachThangDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the NganSachThang in the database
        List<NganSachThang> nganSachThangList = nganSachThangRepository.findAll();
        assertThat(nganSachThangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamNganSachThang() throws Exception {
        int databaseSizeBeforeUpdate = nganSachThangRepository.findAll().size();
        nganSachThang.setId(count.incrementAndGet());

        // Create the NganSachThang
        NganSachThangDTO nganSachThangDTO = nganSachThangMapper.toDto(nganSachThang);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNganSachThangMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(nganSachThangDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the NganSachThang in the database
        List<NganSachThang> nganSachThangList = nganSachThangRepository.findAll();
        assertThat(nganSachThangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateNganSachThangWithPatch() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        int databaseSizeBeforeUpdate = nganSachThangRepository.findAll().size();

        // Update the nganSachThang using partial update
        NganSachThang partialUpdatedNganSachThang = new NganSachThang();
        partialUpdatedNganSachThang.setId(nganSachThang.getId());

        partialUpdatedNganSachThang.soTien(UPDATED_SO_TIEN);

        restNganSachThangMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedNganSachThang.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedNganSachThang))
            )
            .andExpect(status().isOk());

        // Validate the NganSachThang in the database
        List<NganSachThang> nganSachThangList = nganSachThangRepository.findAll();
        assertThat(nganSachThangList).hasSize(databaseSizeBeforeUpdate);
        NganSachThang testNganSachThang = nganSachThangList.get(nganSachThangList.size() - 1);
        assertThat(testNganSachThang.getThang()).isEqualTo(DEFAULT_THANG);
        assertThat(testNganSachThang.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void fullUpdateNganSachThangWithPatch() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        int databaseSizeBeforeUpdate = nganSachThangRepository.findAll().size();

        // Update the nganSachThang using partial update
        NganSachThang partialUpdatedNganSachThang = new NganSachThang();
        partialUpdatedNganSachThang.setId(nganSachThang.getId());

        partialUpdatedNganSachThang.thang(UPDATED_THANG).soTien(UPDATED_SO_TIEN);

        restNganSachThangMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedNganSachThang.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedNganSachThang))
            )
            .andExpect(status().isOk());

        // Validate the NganSachThang in the database
        List<NganSachThang> nganSachThangList = nganSachThangRepository.findAll();
        assertThat(nganSachThangList).hasSize(databaseSizeBeforeUpdate);
        NganSachThang testNganSachThang = nganSachThangList.get(nganSachThangList.size() - 1);
        assertThat(testNganSachThang.getThang()).isEqualTo(UPDATED_THANG);
        assertThat(testNganSachThang.getSoTien()).isEqualTo(UPDATED_SO_TIEN);
    }

    @Test
    @Transactional
    void patchNonExistingNganSachThang() throws Exception {
        int databaseSizeBeforeUpdate = nganSachThangRepository.findAll().size();
        nganSachThang.setId(count.incrementAndGet());

        // Create the NganSachThang
        NganSachThangDTO nganSachThangDTO = nganSachThangMapper.toDto(nganSachThang);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNganSachThangMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, nganSachThangDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(nganSachThangDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the NganSachThang in the database
        List<NganSachThang> nganSachThangList = nganSachThangRepository.findAll();
        assertThat(nganSachThangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchNganSachThang() throws Exception {
        int databaseSizeBeforeUpdate = nganSachThangRepository.findAll().size();
        nganSachThang.setId(count.incrementAndGet());

        // Create the NganSachThang
        NganSachThangDTO nganSachThangDTO = nganSachThangMapper.toDto(nganSachThang);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNganSachThangMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(nganSachThangDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the NganSachThang in the database
        List<NganSachThang> nganSachThangList = nganSachThangRepository.findAll();
        assertThat(nganSachThangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamNganSachThang() throws Exception {
        int databaseSizeBeforeUpdate = nganSachThangRepository.findAll().size();
        nganSachThang.setId(count.incrementAndGet());

        // Create the NganSachThang
        NganSachThangDTO nganSachThangDTO = nganSachThangMapper.toDto(nganSachThang);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNganSachThangMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(nganSachThangDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the NganSachThang in the database
        List<NganSachThang> nganSachThangList = nganSachThangRepository.findAll();
        assertThat(nganSachThangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteNganSachThang() throws Exception {
        // Initialize the database
        nganSachThangRepository.saveAndFlush(nganSachThang);

        int databaseSizeBeforeDelete = nganSachThangRepository.findAll().size();

        // Delete the nganSachThang
        restNganSachThangMockMvc
            .perform(delete(ENTITY_API_URL_ID, nganSachThang.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NganSachThang> nganSachThangList = nganSachThangRepository.findAll();
        assertThat(nganSachThangList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
