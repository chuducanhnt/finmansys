import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { LoaiTaiSanDetailComponent } from './loai-tai-san-detail.component';

describe('Component Tests', () => {
  describe('LoaiTaiSan Management Detail Component', () => {
    let comp: LoaiTaiSanDetailComponent;
    let fixture: ComponentFixture<LoaiTaiSanDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [LoaiTaiSanDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ loaiTaiSan: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(LoaiTaiSanDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(LoaiTaiSanDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load loaiTaiSan on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.loaiTaiSan).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
