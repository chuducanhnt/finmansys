import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ILoaiTaiSan } from '../loai-tai-san.model';
import { LoaiTaiSanService } from '../service/loai-tai-san.service';

@Component({
  templateUrl: './loai-tai-san-delete-dialog.component.html',
})
export class LoaiTaiSanDeleteDialogComponent {
  loaiTaiSan?: ILoaiTaiSan;

  constructor(protected loaiTaiSanService: LoaiTaiSanService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.loaiTaiSanService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
