import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ILoaiTaiSan, LoaiTaiSan } from '../loai-tai-san.model';

import { LoaiTaiSanService } from './loai-tai-san.service';

describe('Service Tests', () => {
  describe('LoaiTaiSan Service', () => {
    let service: LoaiTaiSanService;
    let httpMock: HttpTestingController;
    let elemDefault: ILoaiTaiSan;
    let expectedResult: ILoaiTaiSan | ILoaiTaiSan[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(LoaiTaiSanService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        ten: 'AAAAAAA',
        moTa: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a LoaiTaiSan', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new LoaiTaiSan()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a LoaiTaiSan', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ten: 'BBBBBB',
            moTa: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a LoaiTaiSan', () => {
        const patchObject = Object.assign(
          {
            moTa: 'BBBBBB',
          },
          new LoaiTaiSan()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of LoaiTaiSan', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ten: 'BBBBBB',
            moTa: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a LoaiTaiSan', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addLoaiTaiSanToCollectionIfMissing', () => {
        it('should add a LoaiTaiSan to an empty array', () => {
          const loaiTaiSan: ILoaiTaiSan = { id: 123 };
          expectedResult = service.addLoaiTaiSanToCollectionIfMissing([], loaiTaiSan);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(loaiTaiSan);
        });

        it('should not add a LoaiTaiSan to an array that contains it', () => {
          const loaiTaiSan: ILoaiTaiSan = { id: 123 };
          const loaiTaiSanCollection: ILoaiTaiSan[] = [
            {
              ...loaiTaiSan,
            },
            { id: 456 },
          ];
          expectedResult = service.addLoaiTaiSanToCollectionIfMissing(loaiTaiSanCollection, loaiTaiSan);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a LoaiTaiSan to an array that doesn't contain it", () => {
          const loaiTaiSan: ILoaiTaiSan = { id: 123 };
          const loaiTaiSanCollection: ILoaiTaiSan[] = [{ id: 456 }];
          expectedResult = service.addLoaiTaiSanToCollectionIfMissing(loaiTaiSanCollection, loaiTaiSan);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(loaiTaiSan);
        });

        it('should add only unique LoaiTaiSan to an array', () => {
          const loaiTaiSanArray: ILoaiTaiSan[] = [{ id: 123 }, { id: 456 }, { id: 20936 }];
          const loaiTaiSanCollection: ILoaiTaiSan[] = [{ id: 123 }];
          expectedResult = service.addLoaiTaiSanToCollectionIfMissing(loaiTaiSanCollection, ...loaiTaiSanArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const loaiTaiSan: ILoaiTaiSan = { id: 123 };
          const loaiTaiSan2: ILoaiTaiSan = { id: 456 };
          expectedResult = service.addLoaiTaiSanToCollectionIfMissing([], loaiTaiSan, loaiTaiSan2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(loaiTaiSan);
          expect(expectedResult).toContain(loaiTaiSan2);
        });

        it('should accept null and undefined values', () => {
          const loaiTaiSan: ILoaiTaiSan = { id: 123 };
          expectedResult = service.addLoaiTaiSanToCollectionIfMissing([], null, loaiTaiSan, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(loaiTaiSan);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
