import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ILoaiTaiSan, getLoaiTaiSanIdentifier } from '../loai-tai-san.model';

export type EntityResponseType = HttpResponse<ILoaiTaiSan>;
export type EntityArrayResponseType = HttpResponse<ILoaiTaiSan[]>;

@Injectable({ providedIn: 'root' })
export class LoaiTaiSanService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/loai-tai-sans');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(loaiTaiSan: ILoaiTaiSan): Observable<EntityResponseType> {
    return this.http.post<ILoaiTaiSan>(this.resourceUrl, loaiTaiSan, { observe: 'response' });
  }

  update(loaiTaiSan: ILoaiTaiSan): Observable<EntityResponseType> {
    return this.http.put<ILoaiTaiSan>(`${this.resourceUrl}/${getLoaiTaiSanIdentifier(loaiTaiSan) as number}`, loaiTaiSan, {
      observe: 'response',
    });
  }

  partialUpdate(loaiTaiSan: ILoaiTaiSan): Observable<EntityResponseType> {
    return this.http.patch<ILoaiTaiSan>(`${this.resourceUrl}/${getLoaiTaiSanIdentifier(loaiTaiSan) as number}`, loaiTaiSan, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ILoaiTaiSan>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ILoaiTaiSan[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addLoaiTaiSanToCollectionIfMissing(
    loaiTaiSanCollection: ILoaiTaiSan[],
    ...loaiTaiSansToCheck: (ILoaiTaiSan | null | undefined)[]
  ): ILoaiTaiSan[] {
    const loaiTaiSans: ILoaiTaiSan[] = loaiTaiSansToCheck.filter(isPresent);
    if (loaiTaiSans.length > 0) {
      const loaiTaiSanCollectionIdentifiers = loaiTaiSanCollection.map(loaiTaiSanItem => getLoaiTaiSanIdentifier(loaiTaiSanItem)!);
      const loaiTaiSansToAdd = loaiTaiSans.filter(loaiTaiSanItem => {
        const loaiTaiSanIdentifier = getLoaiTaiSanIdentifier(loaiTaiSanItem);
        if (loaiTaiSanIdentifier == null || loaiTaiSanCollectionIdentifiers.includes(loaiTaiSanIdentifier)) {
          return false;
        }
        loaiTaiSanCollectionIdentifiers.push(loaiTaiSanIdentifier);
        return true;
      });
      return [...loaiTaiSansToAdd, ...loaiTaiSanCollection];
    }
    return loaiTaiSanCollection;
  }
}
