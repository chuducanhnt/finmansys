export interface IKhachSan {
  id?: number;
  ten?: string;
  diaChi?: string | null;
  email?: string | null;
}

export class KhachSan implements IKhachSan {
  constructor(public id?: number, public ten?: string, public diaChi?: string | null, public email?: string | null) {}
}

export function getKhachSanIdentifier(khachSan: IKhachSan): number | undefined {
  return khachSan.id;
}
