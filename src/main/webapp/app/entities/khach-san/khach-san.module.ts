import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { KhachSanComponent } from './list/khach-san.component';
import { KhachSanDetailComponent } from './detail/khach-san-detail.component';
import { KhachSanUpdateComponent } from './update/khach-san-update.component';
import { KhachSanDeleteDialogComponent } from './delete/khach-san-delete-dialog.component';
import { KhachSanRoutingModule } from './route/khach-san-routing.module';

@NgModule({
  imports: [SharedModule, KhachSanRoutingModule],
  declarations: [KhachSanComponent, KhachSanDetailComponent, KhachSanUpdateComponent, KhachSanDeleteDialogComponent],
  entryComponents: [KhachSanDeleteDialogComponent],
})
export class KhachSanModule {}
