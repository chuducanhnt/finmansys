import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IKhachSan, KhachSan } from '../khach-san.model';

import { KhachSanService } from './khach-san.service';

describe('Service Tests', () => {
  describe('KhachSan Service', () => {
    let service: KhachSanService;
    let httpMock: HttpTestingController;
    let elemDefault: IKhachSan;
    let expectedResult: IKhachSan | IKhachSan[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(KhachSanService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        ten: 'AAAAAAA',
        diaChi: 'AAAAAAA',
        email: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a KhachSan', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new KhachSan()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a KhachSan', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ten: 'BBBBBB',
            diaChi: 'BBBBBB',
            email: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a KhachSan', () => {
        const patchObject = Object.assign(
          {
            ten: 'BBBBBB',
            email: 'BBBBBB',
          },
          new KhachSan()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of KhachSan', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ten: 'BBBBBB',
            diaChi: 'BBBBBB',
            email: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a KhachSan', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addKhachSanToCollectionIfMissing', () => {
        it('should add a KhachSan to an empty array', () => {
          const khachSan: IKhachSan = { id: 123 };
          expectedResult = service.addKhachSanToCollectionIfMissing([], khachSan);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(khachSan);
        });

        it('should not add a KhachSan to an array that contains it', () => {
          const khachSan: IKhachSan = { id: 123 };
          const khachSanCollection: IKhachSan[] = [
            {
              ...khachSan,
            },
            { id: 456 },
          ];
          expectedResult = service.addKhachSanToCollectionIfMissing(khachSanCollection, khachSan);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a KhachSan to an array that doesn't contain it", () => {
          const khachSan: IKhachSan = { id: 123 };
          const khachSanCollection: IKhachSan[] = [{ id: 456 }];
          expectedResult = service.addKhachSanToCollectionIfMissing(khachSanCollection, khachSan);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(khachSan);
        });

        it('should add only unique KhachSan to an array', () => {
          const khachSanArray: IKhachSan[] = [{ id: 123 }, { id: 456 }, { id: 76369 }];
          const khachSanCollection: IKhachSan[] = [{ id: 123 }];
          expectedResult = service.addKhachSanToCollectionIfMissing(khachSanCollection, ...khachSanArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const khachSan: IKhachSan = { id: 123 };
          const khachSan2: IKhachSan = { id: 456 };
          expectedResult = service.addKhachSanToCollectionIfMissing([], khachSan, khachSan2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(khachSan);
          expect(expectedResult).toContain(khachSan2);
        });

        it('should accept null and undefined values', () => {
          const khachSan: IKhachSan = { id: 123 };
          expectedResult = service.addKhachSanToCollectionIfMissing([], null, khachSan, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(khachSan);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
