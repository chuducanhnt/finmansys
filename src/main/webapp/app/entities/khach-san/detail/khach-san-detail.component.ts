import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IKhachSan } from '../khach-san.model';

@Component({
  selector: 'jhi-khach-san-detail',
  templateUrl: './khach-san-detail.component.html',
})
export class KhachSanDetailComponent implements OnInit {
  khachSan: IKhachSan | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ khachSan }) => {
      this.khachSan = khachSan;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
