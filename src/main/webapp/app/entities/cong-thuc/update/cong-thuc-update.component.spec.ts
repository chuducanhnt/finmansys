jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { CongThucService } from '../service/cong-thuc.service';
import { ICongThuc, CongThuc } from '../cong-thuc.model';

import { CongThucUpdateComponent } from './cong-thuc-update.component';

describe('Component Tests', () => {
  describe('CongThuc Management Update Component', () => {
    let comp: CongThucUpdateComponent;
    let fixture: ComponentFixture<CongThucUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let congThucService: CongThucService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [CongThucUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(CongThucUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CongThucUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      congThucService = TestBed.inject(CongThucService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call CongThuc query and add missing value', () => {
        const congThuc: ICongThuc = { id: 456 };
        const capCha: ICongThuc = { id: 26537 };
        congThuc.capCha = capCha;

        const congThucCollection: ICongThuc[] = [{ id: 48960 }];
        spyOn(congThucService, 'query').and.returnValue(of(new HttpResponse({ body: congThucCollection })));
        const additionalCongThucs = [capCha];
        const expectedCollection: ICongThuc[] = [...additionalCongThucs, ...congThucCollection];
        spyOn(congThucService, 'addCongThucToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ congThuc });
        comp.ngOnInit();

        expect(congThucService.query).toHaveBeenCalled();
        expect(congThucService.addCongThucToCollectionIfMissing).toHaveBeenCalledWith(congThucCollection, ...additionalCongThucs);
        expect(comp.congThucsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const congThuc: ICongThuc = { id: 456 };
        const capCha: ICongThuc = { id: 689 };
        congThuc.capCha = capCha;

        activatedRoute.data = of({ congThuc });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(congThuc));
        expect(comp.congThucsSharedCollection).toContain(capCha);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const congThuc = { id: 123 };
        spyOn(congThucService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ congThuc });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: congThuc }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(congThucService.update).toHaveBeenCalledWith(congThuc);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const congThuc = new CongThuc();
        spyOn(congThucService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ congThuc });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: congThuc }));
        saveSubject.complete();

        // THEN
        expect(congThucService.create).toHaveBeenCalledWith(congThuc);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const congThuc = { id: 123 };
        spyOn(congThucService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ congThuc });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(congThucService.update).toHaveBeenCalledWith(congThuc);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackCongThucById', () => {
        it('Should return tracked CongThuc primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackCongThucById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
