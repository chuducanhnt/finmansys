import { IToanTu } from 'app/entities/toan-tu/toan-tu.model';

export interface ICongThuc {
  id?: number;
  maSo?: number;
  tenChiTieu?: string;
  cap?: number;
  soDauKy?: number | null;
  soCuoiKy?: number | null;
  toanTus?: IToanTu[] | null;
  capCha?: ICongThuc | null;
  capCons?: ICongThuc[] | null;
}

export class CongThuc implements ICongThuc {
  constructor(
    public id?: number,
    public maSo?: number,
    public tenChiTieu?: string,
    public cap?: number,
    public soDauKy?: number | null,
    public soCuoiKy?: number | null,
    public toanTus?: IToanTu[] | null,
    public capCha?: ICongThuc | null,
    public capCons?: ICongThuc[] | null
  ) {}
}

export function getCongThucIdentifier(congThuc: ICongThuc): number | undefined {
  return congThuc.id;
}
