jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ToanTuService } from '../service/toan-tu.service';
import { IToanTu, ToanTu } from '../toan-tu.model';
import { IMaKeToan } from 'app/entities/ma-ke-toan/ma-ke-toan.model';
import { MaKeToanService } from 'app/entities/ma-ke-toan/service/ma-ke-toan.service';
import { ICongThuc } from 'app/entities/cong-thuc/cong-thuc.model';
import { CongThucService } from 'app/entities/cong-thuc/service/cong-thuc.service';

import { ToanTuUpdateComponent } from './toan-tu-update.component';

describe('Component Tests', () => {
  describe('ToanTu Management Update Component', () => {
    let comp: ToanTuUpdateComponent;
    let fixture: ComponentFixture<ToanTuUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let toanTuService: ToanTuService;
    let maKeToanService: MaKeToanService;
    let congThucService: CongThucService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [ToanTuUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(ToanTuUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ToanTuUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      toanTuService = TestBed.inject(ToanTuService);
      maKeToanService = TestBed.inject(MaKeToanService);
      congThucService = TestBed.inject(CongThucService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call MaKeToan query and add missing value', () => {
        const toanTu: IToanTu = { id: 456 };
        const maKeToan: IMaKeToan = { id: 53263 };
        toanTu.maKeToan = maKeToan;

        const maKeToanCollection: IMaKeToan[] = [{ id: 10718 }];
        spyOn(maKeToanService, 'query').and.returnValue(of(new HttpResponse({ body: maKeToanCollection })));
        const additionalMaKeToans = [maKeToan];
        const expectedCollection: IMaKeToan[] = [...additionalMaKeToans, ...maKeToanCollection];
        spyOn(maKeToanService, 'addMaKeToanToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ toanTu });
        comp.ngOnInit();

        expect(maKeToanService.query).toHaveBeenCalled();
        expect(maKeToanService.addMaKeToanToCollectionIfMissing).toHaveBeenCalledWith(maKeToanCollection, ...additionalMaKeToans);
        expect(comp.maKeToansSharedCollection).toEqual(expectedCollection);
      });

      it('Should call CongThuc query and add missing value', () => {
        const toanTu: IToanTu = { id: 456 };
        const congThuc: ICongThuc = { id: 36393 };
        toanTu.congThuc = congThuc;

        const congThucCollection: ICongThuc[] = [{ id: 67700 }];
        spyOn(congThucService, 'query').and.returnValue(of(new HttpResponse({ body: congThucCollection })));
        const additionalCongThucs = [congThuc];
        const expectedCollection: ICongThuc[] = [...additionalCongThucs, ...congThucCollection];
        spyOn(congThucService, 'addCongThucToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ toanTu });
        comp.ngOnInit();

        expect(congThucService.query).toHaveBeenCalled();
        expect(congThucService.addCongThucToCollectionIfMissing).toHaveBeenCalledWith(congThucCollection, ...additionalCongThucs);
        expect(comp.congThucsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const toanTu: IToanTu = { id: 456 };
        const maKeToan: IMaKeToan = { id: 25204 };
        toanTu.maKeToan = maKeToan;
        const congThuc: ICongThuc = { id: 29119 };
        toanTu.congThuc = congThuc;

        activatedRoute.data = of({ toanTu });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(toanTu));
        expect(comp.maKeToansSharedCollection).toContain(maKeToan);
        expect(comp.congThucsSharedCollection).toContain(congThuc);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const toanTu = { id: 123 };
        spyOn(toanTuService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ toanTu });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: toanTu }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(toanTuService.update).toHaveBeenCalledWith(toanTu);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const toanTu = new ToanTu();
        spyOn(toanTuService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ toanTu });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: toanTu }));
        saveSubject.complete();

        // THEN
        expect(toanTuService.create).toHaveBeenCalledWith(toanTu);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const toanTu = { id: 123 };
        spyOn(toanTuService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ toanTu });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(toanTuService.update).toHaveBeenCalledWith(toanTu);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackMaKeToanById', () => {
        it('Should return tracked MaKeToan primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackMaKeToanById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackCongThucById', () => {
        it('Should return tracked CongThuc primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackCongThucById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
