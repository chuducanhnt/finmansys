import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IToanTu, ToanTu } from '../toan-tu.model';
import { ToanTuService } from '../service/toan-tu.service';
import { IMaKeToan } from 'app/entities/ma-ke-toan/ma-ke-toan.model';
import { MaKeToanService } from 'app/entities/ma-ke-toan/service/ma-ke-toan.service';
import { ICongThuc } from 'app/entities/cong-thuc/cong-thuc.model';
import { CongThucService } from 'app/entities/cong-thuc/service/cong-thuc.service';

@Component({
  selector: 'jhi-toan-tu-update',
  templateUrl: './toan-tu-update.component.html',
})
export class ToanTuUpdateComponent implements OnInit {
  isSaving = false;

  maKeToansSharedCollection: IMaKeToan[] = [];
  congThucsSharedCollection: ICongThuc[] = [];

  editForm = this.fb.group({
    id: [],
    dau: [null, [Validators.required]],
    maKeToan: [null, Validators.required],
    congThuc: [null, Validators.required],
  });

  constructor(
    protected toanTuService: ToanTuService,
    protected maKeToanService: MaKeToanService,
    protected congThucService: CongThucService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ toanTu }) => {
      this.updateForm(toanTu);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const toanTu = this.createFromForm();
    if (toanTu.id !== undefined) {
      this.subscribeToSaveResponse(this.toanTuService.update(toanTu));
    } else {
      this.subscribeToSaveResponse(this.toanTuService.create(toanTu));
    }
  }

  trackMaKeToanById(index: number, item: IMaKeToan): number {
    return item.id!;
  }

  trackCongThucById(index: number, item: ICongThuc): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IToanTu>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(toanTu: IToanTu): void {
    this.editForm.patchValue({
      id: toanTu.id,
      dau: toanTu.dau,
      maKeToan: toanTu.maKeToan,
      congThuc: toanTu.congThuc,
    });

    this.maKeToansSharedCollection = this.maKeToanService.addMaKeToanToCollectionIfMissing(this.maKeToansSharedCollection, toanTu.maKeToan);
    this.congThucsSharedCollection = this.congThucService.addCongThucToCollectionIfMissing(this.congThucsSharedCollection, toanTu.congThuc);
  }

  protected loadRelationshipsOptions(): void {
    this.maKeToanService
      .query({
        page: 0,
        size: 10000,
        sort: ['id,asc'],
      })
      .pipe(map((res: HttpResponse<IMaKeToan[]>) => res.body ?? []))
      .pipe(
        map((maKeToans: IMaKeToan[]) =>
          this.maKeToanService.addMaKeToanToCollectionIfMissing(maKeToans, this.editForm.get('maKeToan')!.value)
        )
      )
      .subscribe((maKeToans: IMaKeToan[]) => (this.maKeToansSharedCollection = maKeToans));

    this.congThucService
      .query({
        page: 0,
        size: 10000,
        sort: ['id,asc'],
      })
      .pipe(map((res: HttpResponse<ICongThuc[]>) => res.body ?? []))
      .pipe(
        map((congThucs: ICongThuc[]) =>
          this.congThucService.addCongThucToCollectionIfMissing(congThucs, this.editForm.get('congThuc')!.value)
        )
      )
      .subscribe((congThucs: ICongThuc[]) => (this.congThucsSharedCollection = congThucs));
  }

  protected createFromForm(): IToanTu {
    return {
      ...new ToanTu(),
      id: this.editForm.get(['id'])!.value,
      dau: this.editForm.get(['dau'])!.value,
      maKeToan: this.editForm.get(['maKeToan'])!.value,
      congThuc: this.editForm.get(['congThuc'])!.value,
    };
  }
}
