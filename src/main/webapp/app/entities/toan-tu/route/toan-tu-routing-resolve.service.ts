import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IToanTu, ToanTu } from '../toan-tu.model';
import { ToanTuService } from '../service/toan-tu.service';

@Injectable({ providedIn: 'root' })
export class ToanTuRoutingResolveService implements Resolve<IToanTu> {
  constructor(protected service: ToanTuService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IToanTu> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((toanTu: HttpResponse<ToanTu>) => {
          if (toanTu.body) {
            return of(toanTu.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ToanTu());
  }
}
