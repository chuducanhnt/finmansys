import { IMaKeToan } from 'app/entities/ma-ke-toan/ma-ke-toan.model';
import { ICongThuc } from 'app/entities/cong-thuc/cong-thuc.model';

export interface IToanTu {
  id?: number;
  dau?: string;
  maKeToan?: IMaKeToan;
  congThuc?: ICongThuc;
}

export class ToanTu implements IToanTu {
  constructor(public id?: number, public dau?: string, public maKeToan?: IMaKeToan, public congThuc?: ICongThuc) {}
}

export function getToanTuIdentifier(toanTu: IToanTu): number | undefined {
  return toanTu.id;
}
