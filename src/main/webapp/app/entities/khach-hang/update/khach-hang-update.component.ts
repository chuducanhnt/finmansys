import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IKhachHang, KhachHang } from '../khach-hang.model';
import { KhachHangService } from '../service/khach-hang.service';

@Component({
  selector: 'jhi-khach-hang-update',
  templateUrl: './khach-hang-update.component.html',
})
export class KhachHangUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    ten: [null, [Validators.required]],
    diaChi: [],
    email: [],
    sdt: [],
  });

  constructor(protected khachHangService: KhachHangService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ khachHang }) => {
      this.updateForm(khachHang);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const khachHang = this.createFromForm();
    if (khachHang.id !== undefined) {
      this.subscribeToSaveResponse(this.khachHangService.update(khachHang));
    } else {
      this.subscribeToSaveResponse(this.khachHangService.create(khachHang));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IKhachHang>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(khachHang: IKhachHang): void {
    this.editForm.patchValue({
      id: khachHang.id,
      ten: khachHang.ten,
      diaChi: khachHang.diaChi,
      email: khachHang.email,
      sdt: khachHang.sdt,
    });
  }

  protected createFromForm(): IKhachHang {
    return {
      ...new KhachHang(),
      id: this.editForm.get(['id'])!.value,
      ten: this.editForm.get(['ten'])!.value,
      diaChi: this.editForm.get(['diaChi'])!.value,
      email: this.editForm.get(['email'])!.value,
      sdt: this.editForm.get(['sdt'])!.value,
    };
  }
}
