import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPhieuChi } from '../phieu-chi.model';
import { PhieuChiService } from '../service/phieu-chi.service';

@Component({
  templateUrl: './phieu-chi-delete-dialog.component.html',
})
export class PhieuChiDeleteDialogComponent {
  phieuChi?: IPhieuChi;

  constructor(protected phieuChiService: PhieuChiService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.phieuChiService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
