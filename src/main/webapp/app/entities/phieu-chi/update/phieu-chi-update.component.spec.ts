jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { PhieuChiService } from '../service/phieu-chi.service';
import { IPhieuChi, PhieuChi } from '../phieu-chi.model';
import { IKhachHang } from 'app/entities/khach-hang/khach-hang.model';
import { KhachHangService } from 'app/entities/khach-hang/service/khach-hang.service';
import { IKhoanMucChiPhi } from 'app/entities/khoan-muc-chi-phi/khoan-muc-chi-phi.model';
import { KhoanMucChiPhiService } from 'app/entities/khoan-muc-chi-phi/service/khoan-muc-chi-phi.service';
import { IKhoanPhaiTra } from 'app/entities/khoan-phai-tra/khoan-phai-tra.model';
import { KhoanPhaiTraService } from 'app/entities/khoan-phai-tra/service/khoan-phai-tra.service';
import { IQuyTien } from 'app/entities/quy-tien/quy-tien.model';
import { QuyTienService } from 'app/entities/quy-tien/service/quy-tien.service';

import { PhieuChiUpdateComponent } from './phieu-chi-update.component';

describe('Component Tests', () => {
  describe('PhieuChi Management Update Component', () => {
    let comp: PhieuChiUpdateComponent;
    let fixture: ComponentFixture<PhieuChiUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let phieuChiService: PhieuChiService;
    let khachHangService: KhachHangService;
    let khoanMucChiPhiService: KhoanMucChiPhiService;
    let khoanPhaiTraService: KhoanPhaiTraService;
    let quyTienService: QuyTienService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [PhieuChiUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(PhieuChiUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PhieuChiUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      phieuChiService = TestBed.inject(PhieuChiService);
      khachHangService = TestBed.inject(KhachHangService);
      khoanMucChiPhiService = TestBed.inject(KhoanMucChiPhiService);
      khoanPhaiTraService = TestBed.inject(KhoanPhaiTraService);
      quyTienService = TestBed.inject(QuyTienService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call KhachHang query and add missing value', () => {
        const phieuChi: IPhieuChi = { id: 456 };
        const khachHang: IKhachHang = { id: 71646 };
        phieuChi.khachHang = khachHang;

        const khachHangCollection: IKhachHang[] = [{ id: 29324 }];
        spyOn(khachHangService, 'query').and.returnValue(of(new HttpResponse({ body: khachHangCollection })));
        const additionalKhachHangs = [khachHang];
        const expectedCollection: IKhachHang[] = [...additionalKhachHangs, ...khachHangCollection];
        spyOn(khachHangService, 'addKhachHangToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ phieuChi });
        comp.ngOnInit();

        expect(khachHangService.query).toHaveBeenCalled();
        expect(khachHangService.addKhachHangToCollectionIfMissing).toHaveBeenCalledWith(khachHangCollection, ...additionalKhachHangs);
        expect(comp.khachHangsSharedCollection).toEqual(expectedCollection);
      });

      it('Should call KhoanMucChiPhi query and add missing value', () => {
        const phieuChi: IPhieuChi = { id: 456 };
        const khoanMucChiPhi: IKhoanMucChiPhi = { id: 30455 };
        phieuChi.khoanMucChiPhi = khoanMucChiPhi;

        const khoanMucChiPhiCollection: IKhoanMucChiPhi[] = [{ id: 60591 }];
        spyOn(khoanMucChiPhiService, 'query').and.returnValue(of(new HttpResponse({ body: khoanMucChiPhiCollection })));
        const additionalKhoanMucChiPhis = [khoanMucChiPhi];
        const expectedCollection: IKhoanMucChiPhi[] = [...additionalKhoanMucChiPhis, ...khoanMucChiPhiCollection];
        spyOn(khoanMucChiPhiService, 'addKhoanMucChiPhiToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ phieuChi });
        comp.ngOnInit();

        expect(khoanMucChiPhiService.query).toHaveBeenCalled();
        expect(khoanMucChiPhiService.addKhoanMucChiPhiToCollectionIfMissing).toHaveBeenCalledWith(
          khoanMucChiPhiCollection,
          ...additionalKhoanMucChiPhis
        );
        expect(comp.khoanMucChiPhisSharedCollection).toEqual(expectedCollection);
      });

      it('Should call KhoanPhaiTra query and add missing value', () => {
        const phieuChi: IPhieuChi = { id: 456 };
        const khoanPhaiTra: IKhoanPhaiTra = { id: 90582 };
        phieuChi.khoanPhaiTra = khoanPhaiTra;

        const khoanPhaiTraCollection: IKhoanPhaiTra[] = [{ id: 99377 }];
        spyOn(khoanPhaiTraService, 'query').and.returnValue(of(new HttpResponse({ body: khoanPhaiTraCollection })));
        const additionalKhoanPhaiTras = [khoanPhaiTra];
        const expectedCollection: IKhoanPhaiTra[] = [...additionalKhoanPhaiTras, ...khoanPhaiTraCollection];
        spyOn(khoanPhaiTraService, 'addKhoanPhaiTraToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ phieuChi });
        comp.ngOnInit();

        expect(khoanPhaiTraService.query).toHaveBeenCalled();
        expect(khoanPhaiTraService.addKhoanPhaiTraToCollectionIfMissing).toHaveBeenCalledWith(
          khoanPhaiTraCollection,
          ...additionalKhoanPhaiTras
        );
        expect(comp.khoanPhaiTrasSharedCollection).toEqual(expectedCollection);
      });

      it('Should call QuyTien query and add missing value', () => {
        const phieuChi: IPhieuChi = { id: 456 };
        const quyTien: IQuyTien = { id: 43658 };
        phieuChi.quyTien = quyTien;

        const quyTienCollection: IQuyTien[] = [{ id: 37904 }];
        spyOn(quyTienService, 'query').and.returnValue(of(new HttpResponse({ body: quyTienCollection })));
        const additionalQuyTiens = [quyTien];
        const expectedCollection: IQuyTien[] = [...additionalQuyTiens, ...quyTienCollection];
        spyOn(quyTienService, 'addQuyTienToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ phieuChi });
        comp.ngOnInit();

        expect(quyTienService.query).toHaveBeenCalled();
        expect(quyTienService.addQuyTienToCollectionIfMissing).toHaveBeenCalledWith(quyTienCollection, ...additionalQuyTiens);
        expect(comp.quyTiensSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const phieuChi: IPhieuChi = { id: 456 };
        const khachHang: IKhachHang = { id: 89279 };
        phieuChi.khachHang = khachHang;
        const khoanMucChiPhi: IKhoanMucChiPhi = { id: 38896 };
        phieuChi.khoanMucChiPhi = khoanMucChiPhi;
        const khoanPhaiTra: IKhoanPhaiTra = { id: 45020 };
        phieuChi.khoanPhaiTra = khoanPhaiTra;
        const quyTien: IQuyTien = { id: 74086 };
        phieuChi.quyTien = quyTien;

        activatedRoute.data = of({ phieuChi });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(phieuChi));
        expect(comp.khachHangsSharedCollection).toContain(khachHang);
        expect(comp.khoanMucChiPhisSharedCollection).toContain(khoanMucChiPhi);
        expect(comp.khoanPhaiTrasSharedCollection).toContain(khoanPhaiTra);
        expect(comp.quyTiensSharedCollection).toContain(quyTien);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const phieuChi = { id: 123 };
        spyOn(phieuChiService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ phieuChi });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: phieuChi }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(phieuChiService.update).toHaveBeenCalledWith(phieuChi);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const phieuChi = new PhieuChi();
        spyOn(phieuChiService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ phieuChi });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: phieuChi }));
        saveSubject.complete();

        // THEN
        expect(phieuChiService.create).toHaveBeenCalledWith(phieuChi);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const phieuChi = { id: 123 };
        spyOn(phieuChiService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ phieuChi });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(phieuChiService.update).toHaveBeenCalledWith(phieuChi);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackKhachHangById', () => {
        it('Should return tracked KhachHang primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackKhachHangById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackKhoanMucChiPhiById', () => {
        it('Should return tracked KhoanMucChiPhi primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackKhoanMucChiPhiById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackKhoanPhaiTraById', () => {
        it('Should return tracked KhoanPhaiTra primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackKhoanPhaiTraById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackQuyTienById', () => {
        it('Should return tracked QuyTien primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackQuyTienById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
