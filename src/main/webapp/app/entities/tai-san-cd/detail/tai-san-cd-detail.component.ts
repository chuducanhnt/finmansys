import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITaiSanCD } from '../tai-san-cd.model';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-tai-san-cd-detail',
  templateUrl: './tai-san-cd-detail.component.html',
})
export class TaiSanCDDetailComponent implements OnInit {
  taiSanCD: ITaiSanCD | null = null;

  constructor(protected dataUtils: DataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ taiSanCD }) => {
      this.taiSanCD = taiSanCD;
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  previousState(): void {
    window.history.back();
  }
}
