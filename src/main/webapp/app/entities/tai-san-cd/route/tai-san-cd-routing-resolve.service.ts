import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ITaiSanCD, TaiSanCD } from '../tai-san-cd.model';
import { TaiSanCDService } from '../service/tai-san-cd.service';

@Injectable({ providedIn: 'root' })
export class TaiSanCDRoutingResolveService implements Resolve<ITaiSanCD> {
  constructor(protected service: TaiSanCDService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITaiSanCD> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((taiSanCD: HttpResponse<TaiSanCD>) => {
          if (taiSanCD.body) {
            return of(taiSanCD.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TaiSanCD());
  }
}
