jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { NganSachThangService } from '../service/ngan-sach-thang.service';
import { INganSachThang, NganSachThang } from '../ngan-sach-thang.model';
import { INganSachNam } from 'app/entities/ngan-sach-nam/ngan-sach-nam.model';
import { NganSachNamService } from 'app/entities/ngan-sach-nam/service/ngan-sach-nam.service';
import { IKhoanMucChiPhi } from 'app/entities/khoan-muc-chi-phi/khoan-muc-chi-phi.model';
import { KhoanMucChiPhiService } from 'app/entities/khoan-muc-chi-phi/service/khoan-muc-chi-phi.service';

import { NganSachThangUpdateComponent } from './ngan-sach-thang-update.component';

describe('Component Tests', () => {
  describe('NganSachThang Management Update Component', () => {
    let comp: NganSachThangUpdateComponent;
    let fixture: ComponentFixture<NganSachThangUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let nganSachThangService: NganSachThangService;
    let nganSachNamService: NganSachNamService;
    let khoanMucChiPhiService: KhoanMucChiPhiService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [NganSachThangUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(NganSachThangUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(NganSachThangUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      nganSachThangService = TestBed.inject(NganSachThangService);
      nganSachNamService = TestBed.inject(NganSachNamService);
      khoanMucChiPhiService = TestBed.inject(KhoanMucChiPhiService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call NganSachNam query and add missing value', () => {
        const nganSachThang: INganSachThang = { id: 456 };
        const nganSachNam: INganSachNam = { id: 99274 };
        nganSachThang.nganSachNam = nganSachNam;

        const nganSachNamCollection: INganSachNam[] = [{ id: 65929 }];
        spyOn(nganSachNamService, 'query').and.returnValue(of(new HttpResponse({ body: nganSachNamCollection })));
        const additionalNganSachNams = [nganSachNam];
        const expectedCollection: INganSachNam[] = [...additionalNganSachNams, ...nganSachNamCollection];
        spyOn(nganSachNamService, 'addNganSachNamToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ nganSachThang });
        comp.ngOnInit();

        expect(nganSachNamService.query).toHaveBeenCalled();
        expect(nganSachNamService.addNganSachNamToCollectionIfMissing).toHaveBeenCalledWith(
          nganSachNamCollection,
          ...additionalNganSachNams
        );
        expect(comp.nganSachNamsSharedCollection).toEqual(expectedCollection);
      });

      it('Should call KhoanMucChiPhi query and add missing value', () => {
        const nganSachThang: INganSachThang = { id: 456 };
        const khoanMucChiPhi: IKhoanMucChiPhi = { id: 30455 };
        nganSachThang.khoanMucChiPhi = khoanMucChiPhi;

        const khoanMucChiPhiCollection: IKhoanMucChiPhi[] = [{ id: 60591 }];
        spyOn(khoanMucChiPhiService, 'query').and.returnValue(of(new HttpResponse({ body: khoanMucChiPhiCollection })));
        const additionalKhoanMucChiPhis = [khoanMucChiPhi];
        const expectedCollection: IKhoanMucChiPhi[] = [...additionalKhoanMucChiPhis, ...khoanMucChiPhiCollection];
        spyOn(khoanMucChiPhiService, 'addKhoanMucChiPhiToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ nganSachThang });
        comp.ngOnInit();

        expect(khoanMucChiPhiService.query).toHaveBeenCalled();
        expect(khoanMucChiPhiService.addKhoanMucChiPhiToCollectionIfMissing).toHaveBeenCalledWith(
          khoanMucChiPhiCollection,
          ...additionalKhoanMucChiPhis
        );
        expect(comp.khoanMucChiPhisSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const nganSachThang: INganSachThang = { id: 456 };
        const nganSachNam: INganSachNam = { id: 47179 };
        nganSachThang.nganSachNam = nganSachNam;
        const khoanMucChiPhi: IKhoanMucChiPhi = { id: 38896 };
        nganSachThang.khoanMucChiPhi = khoanMucChiPhi;

        activatedRoute.data = of({ nganSachThang });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(nganSachThang));
        expect(comp.nganSachNamsSharedCollection).toContain(nganSachNam);
        expect(comp.khoanMucChiPhisSharedCollection).toContain(khoanMucChiPhi);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const nganSachThang = { id: 123 };
        spyOn(nganSachThangService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ nganSachThang });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: nganSachThang }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(nganSachThangService.update).toHaveBeenCalledWith(nganSachThang);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const nganSachThang = new NganSachThang();
        spyOn(nganSachThangService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ nganSachThang });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: nganSachThang }));
        saveSubject.complete();

        // THEN
        expect(nganSachThangService.create).toHaveBeenCalledWith(nganSachThang);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const nganSachThang = { id: 123 };
        spyOn(nganSachThangService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ nganSachThang });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(nganSachThangService.update).toHaveBeenCalledWith(nganSachThang);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackNganSachNamById', () => {
        it('Should return tracked NganSachNam primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackNganSachNamById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackKhoanMucChiPhiById', () => {
        it('Should return tracked KhoanMucChiPhi primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackKhoanMucChiPhiById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
