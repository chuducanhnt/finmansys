import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { INganSachThang, getNganSachThangIdentifier } from '../ngan-sach-thang.model';

export type EntityResponseType = HttpResponse<INganSachThang>;
export type EntityArrayResponseType = HttpResponse<INganSachThang[]>;

@Injectable({ providedIn: 'root' })
export class NganSachThangService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/ngan-sach-thangs');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(nganSachThang: INganSachThang): Observable<EntityResponseType> {
    return this.http.post<INganSachThang>(this.resourceUrl, nganSachThang, { observe: 'response' });
  }

  update(nganSachThang: INganSachThang): Observable<EntityResponseType> {
    return this.http.put<INganSachThang>(`${this.resourceUrl}/${getNganSachThangIdentifier(nganSachThang) as number}`, nganSachThang, {
      observe: 'response',
    });
  }

  partialUpdate(nganSachThang: INganSachThang): Observable<EntityResponseType> {
    return this.http.patch<INganSachThang>(`${this.resourceUrl}/${getNganSachThangIdentifier(nganSachThang) as number}`, nganSachThang, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<INganSachThang>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<INganSachThang[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addNganSachThangToCollectionIfMissing(
    nganSachThangCollection: INganSachThang[],
    ...nganSachThangsToCheck: (INganSachThang | null | undefined)[]
  ): INganSachThang[] {
    const nganSachThangs: INganSachThang[] = nganSachThangsToCheck.filter(isPresent);
    if (nganSachThangs.length > 0) {
      const nganSachThangCollectionIdentifiers = nganSachThangCollection.map(
        nganSachThangItem => getNganSachThangIdentifier(nganSachThangItem)!
      );
      const nganSachThangsToAdd = nganSachThangs.filter(nganSachThangItem => {
        const nganSachThangIdentifier = getNganSachThangIdentifier(nganSachThangItem);
        if (nganSachThangIdentifier == null || nganSachThangCollectionIdentifiers.includes(nganSachThangIdentifier)) {
          return false;
        }
        nganSachThangCollectionIdentifiers.push(nganSachThangIdentifier);
        return true;
      });
      return [...nganSachThangsToAdd, ...nganSachThangCollection];
    }
    return nganSachThangCollection;
  }
}
