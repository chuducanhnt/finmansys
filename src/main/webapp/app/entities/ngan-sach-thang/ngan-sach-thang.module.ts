import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { NganSachThangComponent } from './list/ngan-sach-thang.component';
import { NganSachThangDetailComponent } from './detail/ngan-sach-thang-detail.component';
import { NganSachThangUpdateComponent } from './update/ngan-sach-thang-update.component';
import { NganSachThangDeleteDialogComponent } from './delete/ngan-sach-thang-delete-dialog.component';
import { NganSachThangRoutingModule } from './route/ngan-sach-thang-routing.module';

@NgModule({
  imports: [SharedModule, NganSachThangRoutingModule],
  declarations: [NganSachThangComponent, NganSachThangDetailComponent, NganSachThangUpdateComponent, NganSachThangDeleteDialogComponent],
  entryComponents: [NganSachThangDeleteDialogComponent],
})
export class NganSachThangModule {}
