jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { INganSachThang, NganSachThang } from '../ngan-sach-thang.model';
import { NganSachThangService } from '../service/ngan-sach-thang.service';

import { NganSachThangRoutingResolveService } from './ngan-sach-thang-routing-resolve.service';

describe('Service Tests', () => {
  describe('NganSachThang routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: NganSachThangRoutingResolveService;
    let service: NganSachThangService;
    let resultNganSachThang: INganSachThang | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(NganSachThangRoutingResolveService);
      service = TestBed.inject(NganSachThangService);
      resultNganSachThang = undefined;
    });

    describe('resolve', () => {
      it('should return INganSachThang returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultNganSachThang = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultNganSachThang).toEqual({ id: 123 });
      });

      it('should return new INganSachThang if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultNganSachThang = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultNganSachThang).toEqual(new NganSachThang());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultNganSachThang = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultNganSachThang).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
