import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PhongComponent } from '../list/phong.component';
import { PhongDetailComponent } from '../detail/phong-detail.component';
import { PhongUpdateComponent } from '../update/phong-update.component';
import { PhongRoutingResolveService } from './phong-routing-resolve.service';

const phongRoute: Routes = [
  {
    path: '',
    component: PhongComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PhongDetailComponent,
    resolve: {
      phong: PhongRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PhongUpdateComponent,
    resolve: {
      phong: PhongRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PhongUpdateComponent,
    resolve: {
      phong: PhongRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(phongRoute)],
  exports: [RouterModule],
})
export class PhongRoutingModule {}
