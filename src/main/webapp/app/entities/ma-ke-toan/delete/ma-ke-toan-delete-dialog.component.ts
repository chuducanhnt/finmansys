import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IMaKeToan } from '../ma-ke-toan.model';
import { MaKeToanService } from '../service/ma-ke-toan.service';

@Component({
  templateUrl: './ma-ke-toan-delete-dialog.component.html',
})
export class MaKeToanDeleteDialogComponent {
  maKeToan?: IMaKeToan;

  constructor(protected maKeToanService: MaKeToanService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.maKeToanService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
