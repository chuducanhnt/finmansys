jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { MaKeToanService } from '../service/ma-ke-toan.service';
import { IMaKeToan, MaKeToan } from '../ma-ke-toan.model';

import { MaKeToanUpdateComponent } from './ma-ke-toan-update.component';

describe('Component Tests', () => {
  describe('MaKeToan Management Update Component', () => {
    let comp: MaKeToanUpdateComponent;
    let fixture: ComponentFixture<MaKeToanUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let maKeToanService: MaKeToanService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [MaKeToanUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(MaKeToanUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MaKeToanUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      maKeToanService = TestBed.inject(MaKeToanService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const maKeToan: IMaKeToan = { id: 456 };

        activatedRoute.data = of({ maKeToan });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(maKeToan));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const maKeToan = { id: 123 };
        spyOn(maKeToanService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ maKeToan });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: maKeToan }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(maKeToanService.update).toHaveBeenCalledWith(maKeToan);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const maKeToan = new MaKeToan();
        spyOn(maKeToanService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ maKeToan });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: maKeToan }));
        saveSubject.complete();

        // THEN
        expect(maKeToanService.create).toHaveBeenCalledWith(maKeToan);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const maKeToan = { id: 123 };
        spyOn(maKeToanService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ maKeToan });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(maKeToanService.update).toHaveBeenCalledWith(maKeToan);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
