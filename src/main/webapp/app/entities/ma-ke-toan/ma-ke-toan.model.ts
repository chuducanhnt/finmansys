export interface IMaKeToan {
  id?: number;
  soHieu?: number;
  ten?: string;
}

export class MaKeToan implements IMaKeToan {
  constructor(public id?: number, public soHieu?: number, public ten?: string) {}
}

export function getMaKeToanIdentifier(maKeToan: IMaKeToan): number | undefined {
  return maKeToan.id;
}
