import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IMaKeToan, MaKeToan } from '../ma-ke-toan.model';

import { MaKeToanService } from './ma-ke-toan.service';

describe('Service Tests', () => {
  describe('MaKeToan Service', () => {
    let service: MaKeToanService;
    let httpMock: HttpTestingController;
    let elemDefault: IMaKeToan;
    let expectedResult: IMaKeToan | IMaKeToan[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(MaKeToanService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        soHieu: 0,
        ten: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a MaKeToan', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new MaKeToan()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a MaKeToan', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            soHieu: 1,
            ten: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a MaKeToan', () => {
        const patchObject = Object.assign({}, new MaKeToan());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of MaKeToan', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            soHieu: 1,
            ten: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a MaKeToan', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addMaKeToanToCollectionIfMissing', () => {
        it('should add a MaKeToan to an empty array', () => {
          const maKeToan: IMaKeToan = { id: 123 };
          expectedResult = service.addMaKeToanToCollectionIfMissing([], maKeToan);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(maKeToan);
        });

        it('should not add a MaKeToan to an array that contains it', () => {
          const maKeToan: IMaKeToan = { id: 123 };
          const maKeToanCollection: IMaKeToan[] = [
            {
              ...maKeToan,
            },
            { id: 456 },
          ];
          expectedResult = service.addMaKeToanToCollectionIfMissing(maKeToanCollection, maKeToan);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a MaKeToan to an array that doesn't contain it", () => {
          const maKeToan: IMaKeToan = { id: 123 };
          const maKeToanCollection: IMaKeToan[] = [{ id: 456 }];
          expectedResult = service.addMaKeToanToCollectionIfMissing(maKeToanCollection, maKeToan);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(maKeToan);
        });

        it('should add only unique MaKeToan to an array', () => {
          const maKeToanArray: IMaKeToan[] = [{ id: 123 }, { id: 456 }, { id: 84231 }];
          const maKeToanCollection: IMaKeToan[] = [{ id: 123 }];
          expectedResult = service.addMaKeToanToCollectionIfMissing(maKeToanCollection, ...maKeToanArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const maKeToan: IMaKeToan = { id: 123 };
          const maKeToan2: IMaKeToan = { id: 456 };
          expectedResult = service.addMaKeToanToCollectionIfMissing([], maKeToan, maKeToan2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(maKeToan);
          expect(expectedResult).toContain(maKeToan2);
        });

        it('should accept null and undefined values', () => {
          const maKeToan: IMaKeToan = { id: 123 };
          expectedResult = service.addMaKeToanToCollectionIfMissing([], null, maKeToan, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(maKeToan);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
