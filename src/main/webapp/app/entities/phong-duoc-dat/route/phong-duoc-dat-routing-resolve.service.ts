import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPhongDuocDat, PhongDuocDat } from '../phong-duoc-dat.model';
import { PhongDuocDatService } from '../service/phong-duoc-dat.service';

@Injectable({ providedIn: 'root' })
export class PhongDuocDatRoutingResolveService implements Resolve<IPhongDuocDat> {
  constructor(protected service: PhongDuocDatService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPhongDuocDat> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((phongDuocDat: HttpResponse<PhongDuocDat>) => {
          if (phongDuocDat.body) {
            return of(phongDuocDat.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PhongDuocDat());
  }
}
