jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IPhongDuocDat, PhongDuocDat } from '../phong-duoc-dat.model';
import { PhongDuocDatService } from '../service/phong-duoc-dat.service';

import { PhongDuocDatRoutingResolveService } from './phong-duoc-dat-routing-resolve.service';

describe('Service Tests', () => {
  describe('PhongDuocDat routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: PhongDuocDatRoutingResolveService;
    let service: PhongDuocDatService;
    let resultPhongDuocDat: IPhongDuocDat | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(PhongDuocDatRoutingResolveService);
      service = TestBed.inject(PhongDuocDatService);
      resultPhongDuocDat = undefined;
    });

    describe('resolve', () => {
      it('should return IPhongDuocDat returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPhongDuocDat = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultPhongDuocDat).toEqual({ id: 123 });
      });

      it('should return new IPhongDuocDat if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPhongDuocDat = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultPhongDuocDat).toEqual(new PhongDuocDat());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPhongDuocDat = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultPhongDuocDat).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
