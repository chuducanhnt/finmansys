import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IPhongDuocDat, PhongDuocDat } from '../phong-duoc-dat.model';
import { PhongDuocDatService } from '../service/phong-duoc-dat.service';
import { IKhachHang } from 'app/entities/khach-hang/khach-hang.model';
import { KhachHangService } from 'app/entities/khach-hang/service/khach-hang.service';

@Component({
  selector: 'jhi-phong-duoc-dat-update',
  templateUrl: './phong-duoc-dat-update.component.html',
})
export class PhongDuocDatUpdateComponent implements OnInit {
  isSaving = false;

  khachHangsSharedCollection: IKhachHang[] = [];

  editForm = this.fb.group({
    id: [],
    ngayDat: [null, [Validators.required]],
    khuyenMai: [null, [Validators.min(0), Validators.max(100)]],
    checkIn: [null, [Validators.required]],
    checkOut: [null, [Validators.required]],
    gia: [null, [Validators.required, Validators.min(1)]],
    khachHang: [null, Validators.required],
  });

  constructor(
    protected phongDuocDatService: PhongDuocDatService,
    protected khachHangService: KhachHangService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ phongDuocDat }) => {
      if (phongDuocDat.id === undefined) {
        const today = dayjs().startOf('day');
        phongDuocDat.ngayDat = today;
        phongDuocDat.checkIn = today;
        phongDuocDat.checkOut = today;
      }

      this.updateForm(phongDuocDat);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const phongDuocDat = this.createFromForm();
    if (phongDuocDat.id !== undefined) {
      this.subscribeToSaveResponse(this.phongDuocDatService.update(phongDuocDat));
    } else {
      this.subscribeToSaveResponse(this.phongDuocDatService.create(phongDuocDat));
    }
  }

  trackKhachHangById(index: number, item: IKhachHang): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPhongDuocDat>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(phongDuocDat: IPhongDuocDat): void {
    this.editForm.patchValue({
      id: phongDuocDat.id,
      ngayDat: phongDuocDat.ngayDat ? phongDuocDat.ngayDat.format(DATE_TIME_FORMAT) : null,
      khuyenMai: phongDuocDat.khuyenMai,
      checkIn: phongDuocDat.checkIn ? phongDuocDat.checkIn.format(DATE_TIME_FORMAT) : null,
      checkOut: phongDuocDat.checkOut ? phongDuocDat.checkOut.format(DATE_TIME_FORMAT) : null,
      gia: phongDuocDat.gia,
      khachHang: phongDuocDat.khachHang,
    });

    this.khachHangsSharedCollection = this.khachHangService.addKhachHangToCollectionIfMissing(
      this.khachHangsSharedCollection,
      phongDuocDat.khachHang
    );
  }

  protected loadRelationshipsOptions(): void {
    this.khachHangService
      .query({ page: 0, size: 10000, sort: ['id,asc'] })
      .pipe(map((res: HttpResponse<IKhachHang[]>) => res.body ?? []))
      .pipe(
        map((khachHangs: IKhachHang[]) =>
          this.khachHangService.addKhachHangToCollectionIfMissing(khachHangs, this.editForm.get('khachHang')!.value)
        )
      )
      .subscribe((khachHangs: IKhachHang[]) => (this.khachHangsSharedCollection = khachHangs));
  }

  protected createFromForm(): IPhongDuocDat {
    return {
      ...new PhongDuocDat(),
      id: this.editForm.get(['id'])!.value,
      ngayDat: this.editForm.get(['ngayDat'])!.value ? dayjs(this.editForm.get(['ngayDat'])!.value, DATE_TIME_FORMAT) : undefined,
      khuyenMai: this.editForm.get(['khuyenMai'])!.value,
      checkIn: this.editForm.get(['checkIn'])!.value ? dayjs(this.editForm.get(['checkIn'])!.value, DATE_TIME_FORMAT) : undefined,
      checkOut: this.editForm.get(['checkOut'])!.value ? dayjs(this.editForm.get(['checkOut'])!.value, DATE_TIME_FORMAT) : undefined,
      gia: this.editForm.get(['gia'])!.value,
      khachHang: this.editForm.get(['khachHang'])!.value,
    };
  }
}
