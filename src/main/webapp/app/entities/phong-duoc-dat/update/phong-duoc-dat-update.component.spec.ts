jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { PhongDuocDatService } from '../service/phong-duoc-dat.service';
import { IPhongDuocDat, PhongDuocDat } from '../phong-duoc-dat.model';
import { IKhachHang } from 'app/entities/khach-hang/khach-hang.model';
import { KhachHangService } from 'app/entities/khach-hang/service/khach-hang.service';

import { PhongDuocDatUpdateComponent } from './phong-duoc-dat-update.component';

describe('Component Tests', () => {
  describe('PhongDuocDat Management Update Component', () => {
    let comp: PhongDuocDatUpdateComponent;
    let fixture: ComponentFixture<PhongDuocDatUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let phongDuocDatService: PhongDuocDatService;
    let khachHangService: KhachHangService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [PhongDuocDatUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(PhongDuocDatUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PhongDuocDatUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      phongDuocDatService = TestBed.inject(PhongDuocDatService);
      khachHangService = TestBed.inject(KhachHangService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call KhachHang query and add missing value', () => {
        const phongDuocDat: IPhongDuocDat = { id: 456 };
        const khachHang: IKhachHang = { id: 17887 };
        phongDuocDat.khachHang = khachHang;

        const khachHangCollection: IKhachHang[] = [{ id: 91021 }];
        spyOn(khachHangService, 'query').and.returnValue(of(new HttpResponse({ body: khachHangCollection })));
        const additionalKhachHangs = [khachHang];
        const expectedCollection: IKhachHang[] = [...additionalKhachHangs, ...khachHangCollection];
        spyOn(khachHangService, 'addKhachHangToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ phongDuocDat });
        comp.ngOnInit();

        expect(khachHangService.query).toHaveBeenCalled();
        expect(khachHangService.addKhachHangToCollectionIfMissing).toHaveBeenCalledWith(khachHangCollection, ...additionalKhachHangs);
        expect(comp.khachHangsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const phongDuocDat: IPhongDuocDat = { id: 456 };
        const khachHang: IKhachHang = { id: 67750 };
        phongDuocDat.khachHang = khachHang;

        activatedRoute.data = of({ phongDuocDat });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(phongDuocDat));
        expect(comp.khachHangsSharedCollection).toContain(khachHang);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const phongDuocDat = { id: 123 };
        spyOn(phongDuocDatService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ phongDuocDat });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: phongDuocDat }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(phongDuocDatService.update).toHaveBeenCalledWith(phongDuocDat);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const phongDuocDat = new PhongDuocDat();
        spyOn(phongDuocDatService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ phongDuocDat });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: phongDuocDat }));
        saveSubject.complete();

        // THEN
        expect(phongDuocDatService.create).toHaveBeenCalledWith(phongDuocDat);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const phongDuocDat = { id: 123 };
        spyOn(phongDuocDatService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ phongDuocDat });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(phongDuocDatService.update).toHaveBeenCalledWith(phongDuocDat);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackKhachHangById', () => {
        it('Should return tracked KhachHang primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackKhachHangById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
