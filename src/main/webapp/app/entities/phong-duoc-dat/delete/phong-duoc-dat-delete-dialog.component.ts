import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPhongDuocDat } from '../phong-duoc-dat.model';
import { PhongDuocDatService } from '../service/phong-duoc-dat.service';

@Component({
  templateUrl: './phong-duoc-dat-delete-dialog.component.html',
})
export class PhongDuocDatDeleteDialogComponent {
  phongDuocDat?: IPhongDuocDat;

  constructor(protected phongDuocDatService: PhongDuocDatService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.phongDuocDatService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
