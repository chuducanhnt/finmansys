jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IKhoanMucChiPhi, KhoanMucChiPhi } from '../khoan-muc-chi-phi.model';
import { KhoanMucChiPhiService } from '../service/khoan-muc-chi-phi.service';

import { KhoanMucChiPhiRoutingResolveService } from './khoan-muc-chi-phi-routing-resolve.service';

describe('Service Tests', () => {
  describe('KhoanMucChiPhi routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: KhoanMucChiPhiRoutingResolveService;
    let service: KhoanMucChiPhiService;
    let resultKhoanMucChiPhi: IKhoanMucChiPhi | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(KhoanMucChiPhiRoutingResolveService);
      service = TestBed.inject(KhoanMucChiPhiService);
      resultKhoanMucChiPhi = undefined;
    });

    describe('resolve', () => {
      it('should return IKhoanMucChiPhi returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultKhoanMucChiPhi = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultKhoanMucChiPhi).toEqual({ id: 123 });
      });

      it('should return new IKhoanMucChiPhi if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultKhoanMucChiPhi = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultKhoanMucChiPhi).toEqual(new KhoanMucChiPhi());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultKhoanMucChiPhi = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultKhoanMucChiPhi).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
