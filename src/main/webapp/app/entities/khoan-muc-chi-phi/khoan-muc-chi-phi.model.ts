export interface IKhoanMucChiPhi {
  id?: number;
  ten?: string;
  moTa?: string | null;
}

export class KhoanMucChiPhi implements IKhoanMucChiPhi {
  constructor(public id?: number, public ten?: string, public moTa?: string | null) {}
}

export function getKhoanMucChiPhiIdentifier(khoanMucChiPhi: IKhoanMucChiPhi): number | undefined {
  return khoanMucChiPhi.id;
}
