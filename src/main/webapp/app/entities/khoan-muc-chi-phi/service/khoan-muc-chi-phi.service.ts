import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IKhoanMucChiPhi, getKhoanMucChiPhiIdentifier } from '../khoan-muc-chi-phi.model';

export type EntityResponseType = HttpResponse<IKhoanMucChiPhi>;
export type EntityArrayResponseType = HttpResponse<IKhoanMucChiPhi[]>;

@Injectable({ providedIn: 'root' })
export class KhoanMucChiPhiService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/khoan-muc-chi-phis');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(khoanMucChiPhi: IKhoanMucChiPhi): Observable<EntityResponseType> {
    return this.http.post<IKhoanMucChiPhi>(this.resourceUrl, khoanMucChiPhi, { observe: 'response' });
  }

  update(khoanMucChiPhi: IKhoanMucChiPhi): Observable<EntityResponseType> {
    return this.http.put<IKhoanMucChiPhi>(`${this.resourceUrl}/${getKhoanMucChiPhiIdentifier(khoanMucChiPhi) as number}`, khoanMucChiPhi, {
      observe: 'response',
    });
  }

  partialUpdate(khoanMucChiPhi: IKhoanMucChiPhi): Observable<EntityResponseType> {
    return this.http.patch<IKhoanMucChiPhi>(
      `${this.resourceUrl}/${getKhoanMucChiPhiIdentifier(khoanMucChiPhi) as number}`,
      khoanMucChiPhi,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IKhoanMucChiPhi>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IKhoanMucChiPhi[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addKhoanMucChiPhiToCollectionIfMissing(
    khoanMucChiPhiCollection: IKhoanMucChiPhi[],
    ...khoanMucChiPhisToCheck: (IKhoanMucChiPhi | null | undefined)[]
  ): IKhoanMucChiPhi[] {
    const khoanMucChiPhis: IKhoanMucChiPhi[] = khoanMucChiPhisToCheck.filter(isPresent);
    if (khoanMucChiPhis.length > 0) {
      const khoanMucChiPhiCollectionIdentifiers = khoanMucChiPhiCollection.map(
        khoanMucChiPhiItem => getKhoanMucChiPhiIdentifier(khoanMucChiPhiItem)!
      );
      const khoanMucChiPhisToAdd = khoanMucChiPhis.filter(khoanMucChiPhiItem => {
        const khoanMucChiPhiIdentifier = getKhoanMucChiPhiIdentifier(khoanMucChiPhiItem);
        if (khoanMucChiPhiIdentifier == null || khoanMucChiPhiCollectionIdentifiers.includes(khoanMucChiPhiIdentifier)) {
          return false;
        }
        khoanMucChiPhiCollectionIdentifiers.push(khoanMucChiPhiIdentifier);
        return true;
      });
      return [...khoanMucChiPhisToAdd, ...khoanMucChiPhiCollection];
    }
    return khoanMucChiPhiCollection;
  }
}
