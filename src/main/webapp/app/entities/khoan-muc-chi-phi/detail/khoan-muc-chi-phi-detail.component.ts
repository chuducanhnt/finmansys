import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IKhoanMucChiPhi } from '../khoan-muc-chi-phi.model';

@Component({
  selector: 'jhi-khoan-muc-chi-phi-detail',
  templateUrl: './khoan-muc-chi-phi-detail.component.html',
})
export class KhoanMucChiPhiDetailComponent implements OnInit {
  khoanMucChiPhi: IKhoanMucChiPhi | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ khoanMucChiPhi }) => {
      this.khoanMucChiPhi = khoanMucChiPhi;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
