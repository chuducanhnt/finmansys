import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDichVuSuDung, getDichVuSuDungIdentifier } from '../dich-vu-su-dung.model';

export type EntityResponseType = HttpResponse<IDichVuSuDung>;
export type EntityArrayResponseType = HttpResponse<IDichVuSuDung[]>;

@Injectable({ providedIn: 'root' })
export class DichVuSuDungService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/dich-vu-su-dungs');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(dichVuSuDung: IDichVuSuDung): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(dichVuSuDung);
    return this.http
      .post<IDichVuSuDung>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(dichVuSuDung: IDichVuSuDung): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(dichVuSuDung);
    return this.http
      .put<IDichVuSuDung>(`${this.resourceUrl}/${getDichVuSuDungIdentifier(dichVuSuDung) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(dichVuSuDung: IDichVuSuDung): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(dichVuSuDung);
    return this.http
      .patch<IDichVuSuDung>(`${this.resourceUrl}/${getDichVuSuDungIdentifier(dichVuSuDung) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDichVuSuDung>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDichVuSuDung[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addDichVuSuDungToCollectionIfMissing(
    dichVuSuDungCollection: IDichVuSuDung[],
    ...dichVuSuDungsToCheck: (IDichVuSuDung | null | undefined)[]
  ): IDichVuSuDung[] {
    const dichVuSuDungs: IDichVuSuDung[] = dichVuSuDungsToCheck.filter(isPresent);
    if (dichVuSuDungs.length > 0) {
      const dichVuSuDungCollectionIdentifiers = dichVuSuDungCollection.map(
        dichVuSuDungItem => getDichVuSuDungIdentifier(dichVuSuDungItem)!
      );
      const dichVuSuDungsToAdd = dichVuSuDungs.filter(dichVuSuDungItem => {
        const dichVuSuDungIdentifier = getDichVuSuDungIdentifier(dichVuSuDungItem);
        if (dichVuSuDungIdentifier == null || dichVuSuDungCollectionIdentifiers.includes(dichVuSuDungIdentifier)) {
          return false;
        }
        dichVuSuDungCollectionIdentifiers.push(dichVuSuDungIdentifier);
        return true;
      });
      return [...dichVuSuDungsToAdd, ...dichVuSuDungCollection];
    }
    return dichVuSuDungCollection;
  }

  protected convertDateFromClient(dichVuSuDung: IDichVuSuDung): IDichVuSuDung {
    return Object.assign({}, dichVuSuDung, {
      ngaySuDung: dichVuSuDung.ngaySuDung?.isValid() ? dichVuSuDung.ngaySuDung.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.ngaySuDung = res.body.ngaySuDung ? dayjs(res.body.ngaySuDung) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((dichVuSuDung: IDichVuSuDung) => {
        dichVuSuDung.ngaySuDung = dichVuSuDung.ngaySuDung ? dayjs(dichVuSuDung.ngaySuDung) : undefined;
      });
    }
    return res;
  }
}
