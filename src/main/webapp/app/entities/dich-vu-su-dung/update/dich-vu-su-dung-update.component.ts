import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IDichVuSuDung, DichVuSuDung } from '../dich-vu-su-dung.model';
import { DichVuSuDungService } from '../service/dich-vu-su-dung.service';
import { IDichVu } from 'app/entities/dich-vu/dich-vu.model';
import { DichVuService } from 'app/entities/dich-vu/service/dich-vu.service';
import { IPhongDuocDat } from 'app/entities/phong-duoc-dat/phong-duoc-dat.model';
import { PhongDuocDatService } from 'app/entities/phong-duoc-dat/service/phong-duoc-dat.service';

@Component({
  selector: 'jhi-dich-vu-su-dung-update',
  templateUrl: './dich-vu-su-dung-update.component.html',
})
export class DichVuSuDungUpdateComponent implements OnInit {
  isSaving = false;

  dichVusSharedCollection: IDichVu[] = [];
  phongDuocDatsSharedCollection: IPhongDuocDat[] = [];

  editForm = this.fb.group({
    id: [],
    soLuong: [null, [Validators.required, Validators.min(1)]],
    gia: [null, [Validators.required, Validators.min(1)]],
    ngaySuDung: [null, [Validators.required]],
    dichVu: [null, Validators.required],
    phongDuocDat: [null, Validators.required],
  });

  constructor(
    protected dichVuSuDungService: DichVuSuDungService,
    protected dichVuService: DichVuService,
    protected phongDuocDatService: PhongDuocDatService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dichVuSuDung }) => {
      if (dichVuSuDung.id === undefined) {
        const today = dayjs().startOf('day');
        dichVuSuDung.ngaySuDung = today;
      }

      this.updateForm(dichVuSuDung);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const dichVuSuDung = this.createFromForm();
    if (dichVuSuDung.id !== undefined) {
      this.subscribeToSaveResponse(this.dichVuSuDungService.update(dichVuSuDung));
    } else {
      this.subscribeToSaveResponse(this.dichVuSuDungService.create(dichVuSuDung));
    }
  }

  trackDichVuById(index: number, item: IDichVu): number {
    return item.id!;
  }

  trackPhongDuocDatById(index: number, item: IPhongDuocDat): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDichVuSuDung>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(dichVuSuDung: IDichVuSuDung): void {
    this.editForm.patchValue({
      id: dichVuSuDung.id,
      soLuong: dichVuSuDung.soLuong,
      gia: dichVuSuDung.gia,
      ngaySuDung: dichVuSuDung.ngaySuDung ? dichVuSuDung.ngaySuDung.format(DATE_TIME_FORMAT) : null,
      dichVu: dichVuSuDung.dichVu,
      phongDuocDat: dichVuSuDung.phongDuocDat,
    });

    this.dichVusSharedCollection = this.dichVuService.addDichVuToCollectionIfMissing(this.dichVusSharedCollection, dichVuSuDung.dichVu);
    this.phongDuocDatsSharedCollection = this.phongDuocDatService.addPhongDuocDatToCollectionIfMissing(
      this.phongDuocDatsSharedCollection,
      dichVuSuDung.phongDuocDat
    );
  }

  protected loadRelationshipsOptions(): void {
    this.dichVuService
      .query()
      .pipe(map((res: HttpResponse<IDichVu[]>) => res.body ?? []))
      .pipe(map((dichVus: IDichVu[]) => this.dichVuService.addDichVuToCollectionIfMissing(dichVus, this.editForm.get('dichVu')!.value)))
      .subscribe((dichVus: IDichVu[]) => (this.dichVusSharedCollection = dichVus));

    this.phongDuocDatService
      .query()
      .pipe(map((res: HttpResponse<IPhongDuocDat[]>) => res.body ?? []))
      .pipe(
        map((phongDuocDats: IPhongDuocDat[]) =>
          this.phongDuocDatService.addPhongDuocDatToCollectionIfMissing(phongDuocDats, this.editForm.get('phongDuocDat')!.value)
        )
      )
      .subscribe((phongDuocDats: IPhongDuocDat[]) => (this.phongDuocDatsSharedCollection = phongDuocDats));
  }

  protected createFromForm(): IDichVuSuDung {
    return {
      ...new DichVuSuDung(),
      id: this.editForm.get(['id'])!.value,
      soLuong: this.editForm.get(['soLuong'])!.value,
      gia: this.editForm.get(['gia'])!.value,
      ngaySuDung: this.editForm.get(['ngaySuDung'])!.value ? dayjs(this.editForm.get(['ngaySuDung'])!.value, DATE_TIME_FORMAT) : undefined,
      dichVu: this.editForm.get(['dichVu'])!.value,
      phongDuocDat: this.editForm.get(['phongDuocDat'])!.value,
    };
  }
}
