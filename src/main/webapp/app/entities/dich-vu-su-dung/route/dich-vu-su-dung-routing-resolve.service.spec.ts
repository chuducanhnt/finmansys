jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IDichVuSuDung, DichVuSuDung } from '../dich-vu-su-dung.model';
import { DichVuSuDungService } from '../service/dich-vu-su-dung.service';

import { DichVuSuDungRoutingResolveService } from './dich-vu-su-dung-routing-resolve.service';

describe('Service Tests', () => {
  describe('DichVuSuDung routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: DichVuSuDungRoutingResolveService;
    let service: DichVuSuDungService;
    let resultDichVuSuDung: IDichVuSuDung | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(DichVuSuDungRoutingResolveService);
      service = TestBed.inject(DichVuSuDungService);
      resultDichVuSuDung = undefined;
    });

    describe('resolve', () => {
      it('should return IDichVuSuDung returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDichVuSuDung = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultDichVuSuDung).toEqual({ id: 123 });
      });

      it('should return new IDichVuSuDung if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDichVuSuDung = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultDichVuSuDung).toEqual(new DichVuSuDung());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDichVuSuDung = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultDichVuSuDung).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
