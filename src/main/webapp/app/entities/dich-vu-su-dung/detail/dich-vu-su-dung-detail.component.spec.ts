import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DichVuSuDungDetailComponent } from './dich-vu-su-dung-detail.component';

describe('Component Tests', () => {
  describe('DichVuSuDung Management Detail Component', () => {
    let comp: DichVuSuDungDetailComponent;
    let fixture: ComponentFixture<DichVuSuDungDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [DichVuSuDungDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ dichVuSuDung: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(DichVuSuDungDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DichVuSuDungDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load dichVuSuDung on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.dichVuSuDung).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
