import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PhieuThuComponent } from '../list/phieu-thu.component';
import { PhieuThuDetailComponent } from '../detail/phieu-thu-detail.component';
import { PhieuThuUpdateComponent } from '../update/phieu-thu-update.component';
import { PhieuThuRoutingResolveService } from './phieu-thu-routing-resolve.service';

const phieuThuRoute: Routes = [
  {
    path: '',
    component: PhieuThuComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PhieuThuDetailComponent,
    resolve: {
      phieuThu: PhieuThuRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PhieuThuUpdateComponent,
    resolve: {
      phieuThu: PhieuThuRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PhieuThuUpdateComponent,
    resolve: {
      phieuThu: PhieuThuRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(phieuThuRoute)],
  exports: [RouterModule],
})
export class PhieuThuRoutingModule {}
