import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PhieuThuDetailComponent } from './phieu-thu-detail.component';

describe('Component Tests', () => {
  describe('PhieuThu Management Detail Component', () => {
    let comp: PhieuThuDetailComponent;
    let fixture: ComponentFixture<PhieuThuDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [PhieuThuDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ phieuThu: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(PhieuThuDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PhieuThuDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load phieuThu on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.phieuThu).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
