export interface IDichVu {
  id?: number;
  ten?: string;
  gia?: number;
  moTa?: string | null;
}

export class DichVu implements IDichVu {
  constructor(public id?: number, public ten?: string, public gia?: number, public moTa?: string | null) {}
}

export function getDichVuIdentifier(dichVu: IDichVu): number | undefined {
  return dichVu.id;
}
