import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IDichVu } from '../dich-vu.model';
import { DichVuService } from '../service/dich-vu.service';

@Component({
  templateUrl: './dich-vu-delete-dialog.component.html',
})
export class DichVuDeleteDialogComponent {
  dichVu?: IDichVu;

  constructor(protected dichVuService: DichVuService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.dichVuService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
