import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { KhoanPhaiThuComponent } from '../list/khoan-phai-thu.component';
import { KhoanPhaiThuDetailComponent } from '../detail/khoan-phai-thu-detail.component';
import { KhoanPhaiThuUpdateComponent } from '../update/khoan-phai-thu-update.component';
import { KhoanPhaiThuRoutingResolveService } from './khoan-phai-thu-routing-resolve.service';

const khoanPhaiThuRoute: Routes = [
  {
    path: '',
    component: KhoanPhaiThuComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: KhoanPhaiThuDetailComponent,
    resolve: {
      khoanPhaiThu: KhoanPhaiThuRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: KhoanPhaiThuUpdateComponent,
    resolve: {
      khoanPhaiThu: KhoanPhaiThuRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: KhoanPhaiThuUpdateComponent,
    resolve: {
      khoanPhaiThu: KhoanPhaiThuRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(khoanPhaiThuRoute)],
  exports: [RouterModule],
})
export class KhoanPhaiThuRoutingModule {}
