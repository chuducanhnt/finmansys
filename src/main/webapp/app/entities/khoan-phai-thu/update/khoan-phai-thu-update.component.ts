import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IKhoanPhaiThu, KhoanPhaiThu } from '../khoan-phai-thu.model';
import { KhoanPhaiThuService } from '../service/khoan-phai-thu.service';
import { IKhachHang } from 'app/entities/khach-hang/khach-hang.model';
import { KhachHangService } from 'app/entities/khach-hang/service/khach-hang.service';
import { IMaKeToan } from 'app/entities/ma-ke-toan/ma-ke-toan.model';
import { MaKeToanService } from 'app/entities/ma-ke-toan/service/ma-ke-toan.service';
import { IPhongDuocDat } from 'app/entities/phong-duoc-dat/phong-duoc-dat.model';
import { PhongDuocDatService } from 'app/entities/phong-duoc-dat/service/phong-duoc-dat.service';

@Component({
  selector: 'jhi-khoan-phai-thu-update',
  templateUrl: './khoan-phai-thu-update.component.html',
})
export class KhoanPhaiThuUpdateComponent implements OnInit {
  isSaving = false;

  khachHangsSharedCollection: IKhachHang[] = [];
  maKeToansSharedCollection: IMaKeToan[] = [];
  phongDuocDatsSharedCollection: IPhongDuocDat[] = [];

  editForm = this.fb.group({
    id: [],
    soTien: [null, [Validators.required, Validators.min(1)]],
    ngayTao: [null, [Validators.required]],
    ngayPhaiThu: [null, [Validators.required]],
    ghiChu: [],
    khachHang: [null, Validators.required],
    maKeToan: [null, Validators.required],
    phongDuocDat: [],
  });

  constructor(
    protected khoanPhaiThuService: KhoanPhaiThuService,
    protected khachHangService: KhachHangService,
    protected maKeToanService: MaKeToanService,
    protected phongDuocDatService: PhongDuocDatService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ khoanPhaiThu }) => {
      if (khoanPhaiThu.id === undefined) {
        const today = dayjs().startOf('day');
        khoanPhaiThu.ngayTao = today;
        khoanPhaiThu.ngayPhaiThu = today;
      }

      this.updateForm(khoanPhaiThu);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const khoanPhaiThu = this.createFromForm();
    if (khoanPhaiThu.id !== undefined) {
      this.subscribeToSaveResponse(this.khoanPhaiThuService.update(khoanPhaiThu));
    } else {
      this.subscribeToSaveResponse(this.khoanPhaiThuService.create(khoanPhaiThu));
    }
  }

  trackKhachHangById(index: number, item: IKhachHang): number {
    return item.id!;
  }

  trackMaKeToanById(index: number, item: IMaKeToan): number {
    return item.id!;
  }

  trackPhongDuocDatById(index: number, item: IPhongDuocDat): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IKhoanPhaiThu>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(khoanPhaiThu: IKhoanPhaiThu): void {
    this.editForm.patchValue({
      id: khoanPhaiThu.id,
      soTien: khoanPhaiThu.soTien,
      ngayTao: khoanPhaiThu.ngayTao ? khoanPhaiThu.ngayTao.format(DATE_TIME_FORMAT) : null,
      ngayPhaiThu: khoanPhaiThu.ngayPhaiThu ? khoanPhaiThu.ngayPhaiThu.format(DATE_TIME_FORMAT) : null,
      ghiChu: khoanPhaiThu.ghiChu,
      khachHang: khoanPhaiThu.khachHang,
      maKeToan: khoanPhaiThu.maKeToan,
      phongDuocDat: khoanPhaiThu.phongDuocDat,
    });

    this.khachHangsSharedCollection = this.khachHangService.addKhachHangToCollectionIfMissing(
      this.khachHangsSharedCollection,
      khoanPhaiThu.khachHang
    );
    this.maKeToansSharedCollection = this.maKeToanService.addMaKeToanToCollectionIfMissing(
      this.maKeToansSharedCollection,
      khoanPhaiThu.maKeToan
    );
    this.phongDuocDatsSharedCollection = this.phongDuocDatService.addPhongDuocDatToCollectionIfMissing(
      this.phongDuocDatsSharedCollection,
      khoanPhaiThu.phongDuocDat
    );
  }

  protected loadRelationshipsOptions(): void {
    this.khachHangService
      .query({ page: 0, size: 10000, sort: ['id,asc'] })
      .pipe(map((res: HttpResponse<IKhachHang[]>) => res.body ?? []))
      .pipe(
        map((khachHangs: IKhachHang[]) =>
          this.khachHangService.addKhachHangToCollectionIfMissing(khachHangs, this.editForm.get('khachHang')!.value)
        )
      )
      .subscribe((khachHangs: IKhachHang[]) => (this.khachHangsSharedCollection = khachHangs));

    this.maKeToanService
      .query({ page: 0, size: 10000, sort: ['id,asc'] })
      .pipe(map((res: HttpResponse<IMaKeToan[]>) => res.body ?? []))
      .pipe(
        map((maKeToans: IMaKeToan[]) =>
          this.maKeToanService.addMaKeToanToCollectionIfMissing(maKeToans, this.editForm.get('maKeToan')!.value)
        )
      )
      .subscribe((maKeToans: IMaKeToan[]) => (this.maKeToansSharedCollection = maKeToans));

    this.phongDuocDatService
      .query({ page: 0, size: 10000, sort: ['id,asc'] })
      .pipe(map((res: HttpResponse<IPhongDuocDat[]>) => res.body ?? []))
      .pipe(
        map((phongDuocDats: IPhongDuocDat[]) =>
          this.phongDuocDatService.addPhongDuocDatToCollectionIfMissing(phongDuocDats, this.editForm.get('phongDuocDat')!.value)
        )
      )
      .subscribe((phongDuocDats: IPhongDuocDat[]) => (this.phongDuocDatsSharedCollection = phongDuocDats));
  }

  protected createFromForm(): IKhoanPhaiThu {
    return {
      ...new KhoanPhaiThu(),
      id: this.editForm.get(['id'])!.value,
      soTien: this.editForm.get(['soTien'])!.value,
      ngayTao: this.editForm.get(['ngayTao'])!.value ? dayjs(this.editForm.get(['ngayTao'])!.value, DATE_TIME_FORMAT) : undefined,
      ngayPhaiThu: this.editForm.get(['ngayPhaiThu'])!.value
        ? dayjs(this.editForm.get(['ngayPhaiThu'])!.value, DATE_TIME_FORMAT)
        : undefined,
      ghiChu: this.editForm.get(['ghiChu'])!.value,
      khachHang: this.editForm.get(['khachHang'])!.value,
      maKeToan: this.editForm.get(['maKeToan'])!.value,
      phongDuocDat: this.editForm.get(['phongDuocDat'])!.value,
    };
  }
}
