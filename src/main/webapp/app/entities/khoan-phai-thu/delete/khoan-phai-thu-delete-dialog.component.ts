import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IKhoanPhaiThu } from '../khoan-phai-thu.model';
import { KhoanPhaiThuService } from '../service/khoan-phai-thu.service';

@Component({
  templateUrl: './khoan-phai-thu-delete-dialog.component.html',
})
export class KhoanPhaiThuDeleteDialogComponent {
  khoanPhaiThu?: IKhoanPhaiThu;

  constructor(protected khoanPhaiThuService: KhoanPhaiThuService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.khoanPhaiThuService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
