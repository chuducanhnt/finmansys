import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IQuyTien, QuyTien } from '../quy-tien.model';

import { QuyTienService } from './quy-tien.service';

describe('Service Tests', () => {
  describe('QuyTien Service', () => {
    let service: QuyTienService;
    let httpMock: HttpTestingController;
    let elemDefault: IQuyTien;
    let expectedResult: IQuyTien | IQuyTien[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(QuyTienService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        soTien: 0,
        ghiChu: 'AAAAAAA',
        ten: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a QuyTien', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new QuyTien()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a QuyTien', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            soTien: 1,
            ghiChu: 'BBBBBB',
            ten: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a QuyTien', () => {
        const patchObject = Object.assign(
          {
            ghiChu: 'BBBBBB',
            ten: 'BBBBBB',
          },
          new QuyTien()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of QuyTien', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            soTien: 1,
            ghiChu: 'BBBBBB',
            ten: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a QuyTien', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addQuyTienToCollectionIfMissing', () => {
        it('should add a QuyTien to an empty array', () => {
          const quyTien: IQuyTien = { id: 123 };
          expectedResult = service.addQuyTienToCollectionIfMissing([], quyTien);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(quyTien);
        });

        it('should not add a QuyTien to an array that contains it', () => {
          const quyTien: IQuyTien = { id: 123 };
          const quyTienCollection: IQuyTien[] = [
            {
              ...quyTien,
            },
            { id: 456 },
          ];
          expectedResult = service.addQuyTienToCollectionIfMissing(quyTienCollection, quyTien);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a QuyTien to an array that doesn't contain it", () => {
          const quyTien: IQuyTien = { id: 123 };
          const quyTienCollection: IQuyTien[] = [{ id: 456 }];
          expectedResult = service.addQuyTienToCollectionIfMissing(quyTienCollection, quyTien);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(quyTien);
        });

        it('should add only unique QuyTien to an array', () => {
          const quyTienArray: IQuyTien[] = [{ id: 123 }, { id: 456 }, { id: 59681 }];
          const quyTienCollection: IQuyTien[] = [{ id: 123 }];
          expectedResult = service.addQuyTienToCollectionIfMissing(quyTienCollection, ...quyTienArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const quyTien: IQuyTien = { id: 123 };
          const quyTien2: IQuyTien = { id: 456 };
          expectedResult = service.addQuyTienToCollectionIfMissing([], quyTien, quyTien2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(quyTien);
          expect(expectedResult).toContain(quyTien2);
        });

        it('should accept null and undefined values', () => {
          const quyTien: IQuyTien = { id: 123 };
          expectedResult = service.addQuyTienToCollectionIfMissing([], null, quyTien, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(quyTien);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
