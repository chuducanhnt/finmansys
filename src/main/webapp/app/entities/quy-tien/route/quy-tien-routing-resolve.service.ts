import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IQuyTien, QuyTien } from '../quy-tien.model';
import { QuyTienService } from '../service/quy-tien.service';

@Injectable({ providedIn: 'root' })
export class QuyTienRoutingResolveService implements Resolve<IQuyTien> {
  constructor(protected service: QuyTienService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IQuyTien> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((quyTien: HttpResponse<QuyTien>) => {
          if (quyTien.body) {
            return of(quyTien.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new QuyTien());
  }
}
