import { ILoaiQuy } from 'app/entities/loai-quy/loai-quy.model';
import { IMaKeToan } from 'app/entities/ma-ke-toan/ma-ke-toan.model';
import { IKhachSan } from 'app/entities/khach-san/khach-san.model';
import { IPhieuThu } from 'app/entities/phieu-thu/phieu-thu.model';
import { IPhieuChi } from 'app/entities/phieu-chi/phieu-chi.model';

export interface IQuyTien {
  id?: number;
  soTien?: number;
  ghiChu?: string | null;
  ten?: string;
  loaiQuy?: ILoaiQuy;
  maKeToan?: IMaKeToan;
  khachSan?: IKhachSan;
  phieuThus?: IPhieuThu[] | null;
  phieuChis?: IPhieuChi[] | null;
}

export class QuyTien implements IQuyTien {
  constructor(
    public id?: number,
    public soTien?: number,
    public ghiChu?: string | null,
    public ten?: string,
    public loaiQuy?: ILoaiQuy,
    public maKeToan?: IMaKeToan,
    public khachSan?: IKhachSan,
    public phieuThus?: IPhieuThu[] | null,
    public phieuChis?: IPhieuChi[] | null
  ) {}
}

export function getQuyTienIdentifier(quyTien: IQuyTien): number | undefined {
  return quyTien.id;
}
