import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IQuyTien } from '../quy-tien.model';
import { QuyTienService } from '../service/quy-tien.service';

@Component({
  templateUrl: './quy-tien-delete-dialog.component.html',
})
export class QuyTienDeleteDialogComponent {
  quyTien?: IQuyTien;

  constructor(protected quyTienService: QuyTienService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.quyTienService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
