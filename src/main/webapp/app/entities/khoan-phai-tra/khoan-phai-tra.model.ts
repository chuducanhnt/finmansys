import * as dayjs from 'dayjs';
import { IKhachHang } from 'app/entities/khach-hang/khach-hang.model';
import { IMaKeToan } from 'app/entities/ma-ke-toan/ma-ke-toan.model';
import { IPhieuChi } from 'app/entities/phieu-chi/phieu-chi.model';

export interface IKhoanPhaiTra {
  id?: number;
  ngayTao?: dayjs.Dayjs;
  ngayPhaiTra?: dayjs.Dayjs;
  soTien?: number;
  ghiChu?: string | null;
  khachHang?: IKhachHang;
  maKeToan?: IMaKeToan;
  dsPhieuChis?: IPhieuChi[] | null;
}

export class KhoanPhaiTra implements IKhoanPhaiTra {
  constructor(
    public id?: number,
    public ngayTao?: dayjs.Dayjs,
    public ngayPhaiTra?: dayjs.Dayjs,
    public soTien?: number,
    public ghiChu?: string | null,
    public khachHang?: IKhachHang,
    public maKeToan?: IMaKeToan,
    public dsPhieuChis?: IPhieuChi[] | null
  ) {}
}

export function getKhoanPhaiTraIdentifier(khoanPhaiTra: IKhoanPhaiTra): number | undefined {
  return khoanPhaiTra.id;
}
