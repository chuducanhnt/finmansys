import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { KhoanPhaiTraDetailComponent } from './khoan-phai-tra-detail.component';

describe('Component Tests', () => {
  describe('KhoanPhaiTra Management Detail Component', () => {
    let comp: KhoanPhaiTraDetailComponent;
    let fixture: ComponentFixture<KhoanPhaiTraDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [KhoanPhaiTraDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ khoanPhaiTra: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(KhoanPhaiTraDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(KhoanPhaiTraDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load khoanPhaiTra on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.khoanPhaiTra).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
