import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IChiTietGiamTSCD, ChiTietGiamTSCD } from '../chi-tiet-giam-tscd.model';
import { ChiTietGiamTSCDService } from '../service/chi-tiet-giam-tscd.service';

@Injectable({ providedIn: 'root' })
export class ChiTietGiamTSCDRoutingResolveService implements Resolve<IChiTietGiamTSCD> {
  constructor(protected service: ChiTietGiamTSCDService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IChiTietGiamTSCD> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((chiTietGiamTSCD: HttpResponse<ChiTietGiamTSCD>) => {
          if (chiTietGiamTSCD.body) {
            return of(chiTietGiamTSCD.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ChiTietGiamTSCD());
  }
}
