jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IChiTietGiamTSCD, ChiTietGiamTSCD } from '../chi-tiet-giam-tscd.model';
import { ChiTietGiamTSCDService } from '../service/chi-tiet-giam-tscd.service';

import { ChiTietGiamTSCDRoutingResolveService } from './chi-tiet-giam-tscd-routing-resolve.service';

describe('Service Tests', () => {
  describe('ChiTietGiamTSCD routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: ChiTietGiamTSCDRoutingResolveService;
    let service: ChiTietGiamTSCDService;
    let resultChiTietGiamTSCD: IChiTietGiamTSCD | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(ChiTietGiamTSCDRoutingResolveService);
      service = TestBed.inject(ChiTietGiamTSCDService);
      resultChiTietGiamTSCD = undefined;
    });

    describe('resolve', () => {
      it('should return IChiTietGiamTSCD returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultChiTietGiamTSCD = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultChiTietGiamTSCD).toEqual({ id: 123 });
      });

      it('should return new IChiTietGiamTSCD if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultChiTietGiamTSCD = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultChiTietGiamTSCD).toEqual(new ChiTietGiamTSCD());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultChiTietGiamTSCD = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultChiTietGiamTSCD).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
