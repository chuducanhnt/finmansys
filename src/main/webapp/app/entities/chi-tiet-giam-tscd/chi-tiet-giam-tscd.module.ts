import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { ChiTietGiamTSCDComponent } from './list/chi-tiet-giam-tscd.component';
import { ChiTietGiamTSCDDetailComponent } from './detail/chi-tiet-giam-tscd-detail.component';
import { ChiTietGiamTSCDUpdateComponent } from './update/chi-tiet-giam-tscd-update.component';
import { ChiTietGiamTSCDDeleteDialogComponent } from './delete/chi-tiet-giam-tscd-delete-dialog.component';
import { ChiTietGiamTSCDRoutingModule } from './route/chi-tiet-giam-tscd-routing.module';

@NgModule({
  imports: [SharedModule, ChiTietGiamTSCDRoutingModule],
  declarations: [
    ChiTietGiamTSCDComponent,
    ChiTietGiamTSCDDetailComponent,
    ChiTietGiamTSCDUpdateComponent,
    ChiTietGiamTSCDDeleteDialogComponent,
  ],
  entryComponents: [ChiTietGiamTSCDDeleteDialogComponent],
})
export class ChiTietGiamTSCDModule {}
