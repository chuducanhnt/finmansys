import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ChiTietGiamTSCDDetailComponent } from './chi-tiet-giam-tscd-detail.component';

describe('Component Tests', () => {
  describe('ChiTietGiamTSCD Management Detail Component', () => {
    let comp: ChiTietGiamTSCDDetailComponent;
    let fixture: ComponentFixture<ChiTietGiamTSCDDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [ChiTietGiamTSCDDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ chiTietGiamTSCD: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(ChiTietGiamTSCDDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ChiTietGiamTSCDDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load chiTietGiamTSCD on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.chiTietGiamTSCD).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
