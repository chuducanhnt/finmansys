import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IChiTietGiamTSCD } from '../chi-tiet-giam-tscd.model';

@Component({
  selector: 'jhi-chi-tiet-giam-tscd-detail',
  templateUrl: './chi-tiet-giam-tscd-detail.component.html',
})
export class ChiTietGiamTSCDDetailComponent implements OnInit {
  chiTietGiamTSCD: IChiTietGiamTSCD | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ chiTietGiamTSCD }) => {
      this.chiTietGiamTSCD = chiTietGiamTSCD;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
