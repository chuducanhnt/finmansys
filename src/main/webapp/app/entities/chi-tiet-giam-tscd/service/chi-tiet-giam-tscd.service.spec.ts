import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IChiTietGiamTSCD, ChiTietGiamTSCD } from '../chi-tiet-giam-tscd.model';

import { ChiTietGiamTSCDService } from './chi-tiet-giam-tscd.service';

describe('Service Tests', () => {
  describe('ChiTietGiamTSCD Service', () => {
    let service: ChiTietGiamTSCDService;
    let httpMock: HttpTestingController;
    let elemDefault: IChiTietGiamTSCD;
    let expectedResult: IChiTietGiamTSCD | IChiTietGiamTSCD[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(ChiTietGiamTSCDService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        ngayMua: currentDate,
        soLuong: 0,
        khauHao: 0,
        soTienThanhLy: 0,
        chiPhiMua: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            ngayMua: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ChiTietGiamTSCD', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            ngayMua: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayMua: currentDate,
          },
          returnedFromService
        );

        service.create(new ChiTietGiamTSCD()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ChiTietGiamTSCD', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ngayMua: currentDate.format(DATE_TIME_FORMAT),
            soLuong: 1,
            khauHao: 1,
            soTienThanhLy: 1,
            chiPhiMua: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayMua: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a ChiTietGiamTSCD', () => {
        const patchObject = Object.assign(
          {
            soLuong: 1,
          },
          new ChiTietGiamTSCD()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            ngayMua: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ChiTietGiamTSCD', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ngayMua: currentDate.format(DATE_TIME_FORMAT),
            soLuong: 1,
            khauHao: 1,
            soTienThanhLy: 1,
            chiPhiMua: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            ngayMua: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ChiTietGiamTSCD', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addChiTietGiamTSCDToCollectionIfMissing', () => {
        it('should add a ChiTietGiamTSCD to an empty array', () => {
          const chiTietGiamTSCD: IChiTietGiamTSCD = { id: 123 };
          expectedResult = service.addChiTietGiamTSCDToCollectionIfMissing([], chiTietGiamTSCD);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(chiTietGiamTSCD);
        });

        it('should not add a ChiTietGiamTSCD to an array that contains it', () => {
          const chiTietGiamTSCD: IChiTietGiamTSCD = { id: 123 };
          const chiTietGiamTSCDCollection: IChiTietGiamTSCD[] = [
            {
              ...chiTietGiamTSCD,
            },
            { id: 456 },
          ];
          expectedResult = service.addChiTietGiamTSCDToCollectionIfMissing(chiTietGiamTSCDCollection, chiTietGiamTSCD);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a ChiTietGiamTSCD to an array that doesn't contain it", () => {
          const chiTietGiamTSCD: IChiTietGiamTSCD = { id: 123 };
          const chiTietGiamTSCDCollection: IChiTietGiamTSCD[] = [{ id: 456 }];
          expectedResult = service.addChiTietGiamTSCDToCollectionIfMissing(chiTietGiamTSCDCollection, chiTietGiamTSCD);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(chiTietGiamTSCD);
        });

        it('should add only unique ChiTietGiamTSCD to an array', () => {
          const chiTietGiamTSCDArray: IChiTietGiamTSCD[] = [{ id: 123 }, { id: 456 }, { id: 53225 }];
          const chiTietGiamTSCDCollection: IChiTietGiamTSCD[] = [{ id: 123 }];
          expectedResult = service.addChiTietGiamTSCDToCollectionIfMissing(chiTietGiamTSCDCollection, ...chiTietGiamTSCDArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const chiTietGiamTSCD: IChiTietGiamTSCD = { id: 123 };
          const chiTietGiamTSCD2: IChiTietGiamTSCD = { id: 456 };
          expectedResult = service.addChiTietGiamTSCDToCollectionIfMissing([], chiTietGiamTSCD, chiTietGiamTSCD2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(chiTietGiamTSCD);
          expect(expectedResult).toContain(chiTietGiamTSCD2);
        });

        it('should accept null and undefined values', () => {
          const chiTietGiamTSCD: IChiTietGiamTSCD = { id: 123 };
          expectedResult = service.addChiTietGiamTSCDToCollectionIfMissing([], null, chiTietGiamTSCD, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(chiTietGiamTSCD);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
