import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IChiTietGiamTSCD, getChiTietGiamTSCDIdentifier } from '../chi-tiet-giam-tscd.model';

export type EntityResponseType = HttpResponse<IChiTietGiamTSCD>;
export type EntityArrayResponseType = HttpResponse<IChiTietGiamTSCD[]>;

@Injectable({ providedIn: 'root' })
export class ChiTietGiamTSCDService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/chi-tiet-giam-tscds');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(chiTietGiamTSCD: IChiTietGiamTSCD): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(chiTietGiamTSCD);
    return this.http
      .post<IChiTietGiamTSCD>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(chiTietGiamTSCD: IChiTietGiamTSCD): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(chiTietGiamTSCD);
    return this.http
      .put<IChiTietGiamTSCD>(`${this.resourceUrl}/${getChiTietGiamTSCDIdentifier(chiTietGiamTSCD) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(chiTietGiamTSCD: IChiTietGiamTSCD): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(chiTietGiamTSCD);
    return this.http
      .patch<IChiTietGiamTSCD>(`${this.resourceUrl}/${getChiTietGiamTSCDIdentifier(chiTietGiamTSCD) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IChiTietGiamTSCD>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IChiTietGiamTSCD[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addChiTietGiamTSCDToCollectionIfMissing(
    chiTietGiamTSCDCollection: IChiTietGiamTSCD[],
    ...chiTietGiamTSCDSToCheck: (IChiTietGiamTSCD | null | undefined)[]
  ): IChiTietGiamTSCD[] {
    const chiTietGiamTSCDS: IChiTietGiamTSCD[] = chiTietGiamTSCDSToCheck.filter(isPresent);
    if (chiTietGiamTSCDS.length > 0) {
      const chiTietGiamTSCDCollectionIdentifiers = chiTietGiamTSCDCollection.map(
        chiTietGiamTSCDItem => getChiTietGiamTSCDIdentifier(chiTietGiamTSCDItem)!
      );
      const chiTietGiamTSCDSToAdd = chiTietGiamTSCDS.filter(chiTietGiamTSCDItem => {
        const chiTietGiamTSCDIdentifier = getChiTietGiamTSCDIdentifier(chiTietGiamTSCDItem);
        if (chiTietGiamTSCDIdentifier == null || chiTietGiamTSCDCollectionIdentifiers.includes(chiTietGiamTSCDIdentifier)) {
          return false;
        }
        chiTietGiamTSCDCollectionIdentifiers.push(chiTietGiamTSCDIdentifier);
        return true;
      });
      return [...chiTietGiamTSCDSToAdd, ...chiTietGiamTSCDCollection];
    }
    return chiTietGiamTSCDCollection;
  }

  protected convertDateFromClient(chiTietGiamTSCD: IChiTietGiamTSCD): IChiTietGiamTSCD {
    return Object.assign({}, chiTietGiamTSCD, {
      ngayMua: chiTietGiamTSCD.ngayMua?.isValid() ? chiTietGiamTSCD.ngayMua.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.ngayMua = res.body.ngayMua ? dayjs(res.body.ngayMua) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((chiTietGiamTSCD: IChiTietGiamTSCD) => {
        chiTietGiamTSCD.ngayMua = chiTietGiamTSCD.ngayMua ? dayjs(chiTietGiamTSCD.ngayMua) : undefined;
      });
    }
    return res;
  }
}
