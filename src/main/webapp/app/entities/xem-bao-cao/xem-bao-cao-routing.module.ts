import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { BangCanDoiKeToanComponent } from 'app/entities/xem-bao-cao/bang-can-doi-ke-toan/bang-can-doi-ke-toan.component';
import { BaoCaoDoanhThuComponent } from 'app/entities/xem-bao-cao/bao-cao-doanh-thu/bao-cao-doanh-thu.component';

const routes: Routes = [
  {
    path: 'bang-can-doi-ke-toan',
    component: BangCanDoiKeToanComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'bao-cao-doanh-thu',
    component: BaoCaoDoanhThuComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class XemBaoCaoRoutingModule {}
