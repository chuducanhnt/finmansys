import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BangCanDoiKeToanComponent } from './bang-can-doi-ke-toan.component';

describe('BangCanDoiKeToanComponent', () => {
  let component: BangCanDoiKeToanComponent;
  let fixture: ComponentFixture<BangCanDoiKeToanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BangCanDoiKeToanComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BangCanDoiKeToanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
