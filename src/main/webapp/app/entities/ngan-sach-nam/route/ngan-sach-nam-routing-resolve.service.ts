import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { INganSachNam, NganSachNam } from '../ngan-sach-nam.model';
import { NganSachNamService } from '../service/ngan-sach-nam.service';

@Injectable({ providedIn: 'root' })
export class NganSachNamRoutingResolveService implements Resolve<INganSachNam> {
  constructor(protected service: NganSachNamService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<INganSachNam> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((nganSachNam: HttpResponse<NganSachNam>) => {
          if (nganSachNam.body) {
            return of(nganSachNam.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new NganSachNam());
  }
}
