import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { INganSachNam, NganSachNam } from '../ngan-sach-nam.model';

import { NganSachNamService } from './ngan-sach-nam.service';

describe('Service Tests', () => {
  describe('NganSachNam Service', () => {
    let service: NganSachNamService;
    let httpMock: HttpTestingController;
    let elemDefault: INganSachNam;
    let expectedResult: INganSachNam | INganSachNam[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(NganSachNamService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        ten: 'AAAAAAA',
        nam: 0,
        soTien: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a NganSachNam', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new NganSachNam()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a NganSachNam', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ten: 'BBBBBB',
            nam: 1,
            soTien: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a NganSachNam', () => {
        const patchObject = Object.assign(
          {
            ten: 'BBBBBB',
            nam: 1,
          },
          new NganSachNam()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of NganSachNam', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ten: 'BBBBBB',
            nam: 1,
            soTien: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a NganSachNam', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addNganSachNamToCollectionIfMissing', () => {
        it('should add a NganSachNam to an empty array', () => {
          const nganSachNam: INganSachNam = { id: 123 };
          expectedResult = service.addNganSachNamToCollectionIfMissing([], nganSachNam);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(nganSachNam);
        });

        it('should not add a NganSachNam to an array that contains it', () => {
          const nganSachNam: INganSachNam = { id: 123 };
          const nganSachNamCollection: INganSachNam[] = [
            {
              ...nganSachNam,
            },
            { id: 456 },
          ];
          expectedResult = service.addNganSachNamToCollectionIfMissing(nganSachNamCollection, nganSachNam);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a NganSachNam to an array that doesn't contain it", () => {
          const nganSachNam: INganSachNam = { id: 123 };
          const nganSachNamCollection: INganSachNam[] = [{ id: 456 }];
          expectedResult = service.addNganSachNamToCollectionIfMissing(nganSachNamCollection, nganSachNam);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(nganSachNam);
        });

        it('should add only unique NganSachNam to an array', () => {
          const nganSachNamArray: INganSachNam[] = [{ id: 123 }, { id: 456 }, { id: 11785 }];
          const nganSachNamCollection: INganSachNam[] = [{ id: 123 }];
          expectedResult = service.addNganSachNamToCollectionIfMissing(nganSachNamCollection, ...nganSachNamArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const nganSachNam: INganSachNam = { id: 123 };
          const nganSachNam2: INganSachNam = { id: 456 };
          expectedResult = service.addNganSachNamToCollectionIfMissing([], nganSachNam, nganSachNam2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(nganSachNam);
          expect(expectedResult).toContain(nganSachNam2);
        });

        it('should accept null and undefined values', () => {
          const nganSachNam: INganSachNam = { id: 123 };
          expectedResult = service.addNganSachNamToCollectionIfMissing([], null, nganSachNam, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(nganSachNam);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
