jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { NganSachNamService } from '../service/ngan-sach-nam.service';
import { INganSachNam, NganSachNam } from '../ngan-sach-nam.model';
import { IKhachSan } from 'app/entities/khach-san/khach-san.model';
import { KhachSanService } from 'app/entities/khach-san/service/khach-san.service';

import { NganSachNamUpdateComponent } from './ngan-sach-nam-update.component';

describe('Component Tests', () => {
  describe('NganSachNam Management Update Component', () => {
    let comp: NganSachNamUpdateComponent;
    let fixture: ComponentFixture<NganSachNamUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let nganSachNamService: NganSachNamService;
    let khachSanService: KhachSanService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [NganSachNamUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(NganSachNamUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(NganSachNamUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      nganSachNamService = TestBed.inject(NganSachNamService);
      khachSanService = TestBed.inject(KhachSanService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call KhachSan query and add missing value', () => {
        const nganSachNam: INganSachNam = { id: 456 };
        const khachSan: IKhachSan = { id: 93831 };
        nganSachNam.khachSan = khachSan;

        const khachSanCollection: IKhachSan[] = [{ id: 19492 }];
        spyOn(khachSanService, 'query').and.returnValue(of(new HttpResponse({ body: khachSanCollection })));
        const additionalKhachSans = [khachSan];
        const expectedCollection: IKhachSan[] = [...additionalKhachSans, ...khachSanCollection];
        spyOn(khachSanService, 'addKhachSanToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ nganSachNam });
        comp.ngOnInit();

        expect(khachSanService.query).toHaveBeenCalled();
        expect(khachSanService.addKhachSanToCollectionIfMissing).toHaveBeenCalledWith(khachSanCollection, ...additionalKhachSans);
        expect(comp.khachSansSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const nganSachNam: INganSachNam = { id: 456 };
        const khachSan: IKhachSan = { id: 52768 };
        nganSachNam.khachSan = khachSan;

        activatedRoute.data = of({ nganSachNam });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(nganSachNam));
        expect(comp.khachSansSharedCollection).toContain(khachSan);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const nganSachNam = { id: 123 };
        spyOn(nganSachNamService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ nganSachNam });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: nganSachNam }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(nganSachNamService.update).toHaveBeenCalledWith(nganSachNam);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const nganSachNam = new NganSachNam();
        spyOn(nganSachNamService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ nganSachNam });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: nganSachNam }));
        saveSubject.complete();

        // THEN
        expect(nganSachNamService.create).toHaveBeenCalledWith(nganSachNam);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const nganSachNam = { id: 123 };
        spyOn(nganSachNamService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ nganSachNam });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(nganSachNamService.update).toHaveBeenCalledWith(nganSachNam);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackKhachSanById', () => {
        it('Should return tracked KhachSan primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackKhachSanById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
