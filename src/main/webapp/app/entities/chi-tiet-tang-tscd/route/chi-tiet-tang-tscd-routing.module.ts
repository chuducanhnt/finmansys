import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ChiTietTangTSCDComponent } from '../list/chi-tiet-tang-tscd.component';
import { ChiTietTangTSCDDetailComponent } from '../detail/chi-tiet-tang-tscd-detail.component';
import { ChiTietTangTSCDUpdateComponent } from '../update/chi-tiet-tang-tscd-update.component';
import { ChiTietTangTSCDRoutingResolveService } from './chi-tiet-tang-tscd-routing-resolve.service';

const chiTietTangTSCDRoute: Routes = [
  {
    path: '',
    component: ChiTietTangTSCDComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ChiTietTangTSCDDetailComponent,
    resolve: {
      chiTietTangTSCD: ChiTietTangTSCDRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ChiTietTangTSCDUpdateComponent,
    resolve: {
      chiTietTangTSCD: ChiTietTangTSCDRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ChiTietTangTSCDUpdateComponent,
    resolve: {
      chiTietTangTSCD: ChiTietTangTSCDRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(chiTietTangTSCDRoute)],
  exports: [RouterModule],
})
export class ChiTietTangTSCDRoutingModule {}
