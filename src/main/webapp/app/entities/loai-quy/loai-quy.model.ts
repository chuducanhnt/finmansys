export interface ILoaiQuy {
  id?: number;
  ma?: string;
  ten?: string;
}

export class LoaiQuy implements ILoaiQuy {
  constructor(public id?: number, public ma?: string, public ten?: string) {}
}

export function getLoaiQuyIdentifier(loaiQuy: ILoaiQuy): number | undefined {
  return loaiQuy.id;
}
