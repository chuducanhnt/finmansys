jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { ILoaiQuy, LoaiQuy } from '../loai-quy.model';
import { LoaiQuyService } from '../service/loai-quy.service';

import { LoaiQuyRoutingResolveService } from './loai-quy-routing-resolve.service';

describe('Service Tests', () => {
  describe('LoaiQuy routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: LoaiQuyRoutingResolveService;
    let service: LoaiQuyService;
    let resultLoaiQuy: ILoaiQuy | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(LoaiQuyRoutingResolveService);
      service = TestBed.inject(LoaiQuyService);
      resultLoaiQuy = undefined;
    });

    describe('resolve', () => {
      it('should return ILoaiQuy returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultLoaiQuy = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultLoaiQuy).toEqual({ id: 123 });
      });

      it('should return new ILoaiQuy if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultLoaiQuy = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultLoaiQuy).toEqual(new LoaiQuy());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultLoaiQuy = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultLoaiQuy).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
