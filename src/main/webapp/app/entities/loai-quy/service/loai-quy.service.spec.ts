import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ILoaiQuy, LoaiQuy } from '../loai-quy.model';

import { LoaiQuyService } from './loai-quy.service';

describe('Service Tests', () => {
  describe('LoaiQuy Service', () => {
    let service: LoaiQuyService;
    let httpMock: HttpTestingController;
    let elemDefault: ILoaiQuy;
    let expectedResult: ILoaiQuy | ILoaiQuy[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(LoaiQuyService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        ma: 'AAAAAAA',
        ten: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a LoaiQuy', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new LoaiQuy()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a LoaiQuy', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ma: 'BBBBBB',
            ten: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a LoaiQuy', () => {
        const patchObject = Object.assign({}, new LoaiQuy());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of LoaiQuy', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            ma: 'BBBBBB',
            ten: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a LoaiQuy', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addLoaiQuyToCollectionIfMissing', () => {
        it('should add a LoaiQuy to an empty array', () => {
          const loaiQuy: ILoaiQuy = { id: 123 };
          expectedResult = service.addLoaiQuyToCollectionIfMissing([], loaiQuy);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(loaiQuy);
        });

        it('should not add a LoaiQuy to an array that contains it', () => {
          const loaiQuy: ILoaiQuy = { id: 123 };
          const loaiQuyCollection: ILoaiQuy[] = [
            {
              ...loaiQuy,
            },
            { id: 456 },
          ];
          expectedResult = service.addLoaiQuyToCollectionIfMissing(loaiQuyCollection, loaiQuy);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a LoaiQuy to an array that doesn't contain it", () => {
          const loaiQuy: ILoaiQuy = { id: 123 };
          const loaiQuyCollection: ILoaiQuy[] = [{ id: 456 }];
          expectedResult = service.addLoaiQuyToCollectionIfMissing(loaiQuyCollection, loaiQuy);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(loaiQuy);
        });

        it('should add only unique LoaiQuy to an array', () => {
          const loaiQuyArray: ILoaiQuy[] = [{ id: 123 }, { id: 456 }, { id: 96406 }];
          const loaiQuyCollection: ILoaiQuy[] = [{ id: 123 }];
          expectedResult = service.addLoaiQuyToCollectionIfMissing(loaiQuyCollection, ...loaiQuyArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const loaiQuy: ILoaiQuy = { id: 123 };
          const loaiQuy2: ILoaiQuy = { id: 456 };
          expectedResult = service.addLoaiQuyToCollectionIfMissing([], loaiQuy, loaiQuy2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(loaiQuy);
          expect(expectedResult).toContain(loaiQuy2);
        });

        it('should accept null and undefined values', () => {
          const loaiQuy: ILoaiQuy = { id: 123 };
          expectedResult = service.addLoaiQuyToCollectionIfMissing([], null, loaiQuy, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(loaiQuy);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
