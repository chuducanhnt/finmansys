import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ILoaiQuy, LoaiQuy } from '../loai-quy.model';
import { LoaiQuyService } from '../service/loai-quy.service';

@Component({
  selector: 'jhi-loai-quy-update',
  templateUrl: './loai-quy-update.component.html',
})
export class LoaiQuyUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    ma: [null, [Validators.required]],
    ten: [null, [Validators.required]],
  });

  constructor(protected loaiQuyService: LoaiQuyService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ loaiQuy }) => {
      this.updateForm(loaiQuy);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const loaiQuy = this.createFromForm();
    if (loaiQuy.id !== undefined) {
      this.subscribeToSaveResponse(this.loaiQuyService.update(loaiQuy));
    } else {
      this.subscribeToSaveResponse(this.loaiQuyService.create(loaiQuy));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILoaiQuy>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(loaiQuy: ILoaiQuy): void {
    this.editForm.patchValue({
      id: loaiQuy.id,
      ma: loaiQuy.ma,
      ten: loaiQuy.ten,
    });
  }

  protected createFromForm(): ILoaiQuy {
    return {
      ...new LoaiQuy(),
      id: this.editForm.get(['id'])!.value,
      ma: this.editForm.get(['ma'])!.value,
      ten: this.editForm.get(['ten'])!.value,
    };
  }
}
