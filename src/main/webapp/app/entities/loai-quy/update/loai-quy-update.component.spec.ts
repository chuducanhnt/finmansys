jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { LoaiQuyService } from '../service/loai-quy.service';
import { ILoaiQuy, LoaiQuy } from '../loai-quy.model';

import { LoaiQuyUpdateComponent } from './loai-quy-update.component';

describe('Component Tests', () => {
  describe('LoaiQuy Management Update Component', () => {
    let comp: LoaiQuyUpdateComponent;
    let fixture: ComponentFixture<LoaiQuyUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let loaiQuyService: LoaiQuyService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [LoaiQuyUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(LoaiQuyUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(LoaiQuyUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      loaiQuyService = TestBed.inject(LoaiQuyService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const loaiQuy: ILoaiQuy = { id: 456 };

        activatedRoute.data = of({ loaiQuy });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(loaiQuy));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const loaiQuy = { id: 123 };
        spyOn(loaiQuyService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ loaiQuy });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: loaiQuy }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(loaiQuyService.update).toHaveBeenCalledWith(loaiQuy);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const loaiQuy = new LoaiQuy();
        spyOn(loaiQuyService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ loaiQuy });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: loaiQuy }));
        saveSubject.complete();

        // THEN
        expect(loaiQuyService.create).toHaveBeenCalledWith(loaiQuy);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const loaiQuy = { id: 123 };
        spyOn(loaiQuyService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ loaiQuy });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(loaiQuyService.update).toHaveBeenCalledWith(loaiQuy);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
