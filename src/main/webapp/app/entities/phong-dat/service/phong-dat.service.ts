import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPhongDat, getPhongDatIdentifier } from '../phong-dat.model';

export type EntityResponseType = HttpResponse<IPhongDat>;
export type EntityArrayResponseType = HttpResponse<IPhongDat[]>;

@Injectable({ providedIn: 'root' })
export class PhongDatService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/phong-dats');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(phongDat: IPhongDat): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(phongDat);
    return this.http
      .post<IPhongDat>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(phongDat: IPhongDat): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(phongDat);
    return this.http
      .put<IPhongDat>(`${this.resourceUrl}/${getPhongDatIdentifier(phongDat) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(phongDat: IPhongDat): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(phongDat);
    return this.http
      .patch<IPhongDat>(`${this.resourceUrl}/${getPhongDatIdentifier(phongDat) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPhongDat>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPhongDat[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPhongDatToCollectionIfMissing(phongDatCollection: IPhongDat[], ...phongDatsToCheck: (IPhongDat | null | undefined)[]): IPhongDat[] {
    const phongDats: IPhongDat[] = phongDatsToCheck.filter(isPresent);
    if (phongDats.length > 0) {
      const phongDatCollectionIdentifiers = phongDatCollection.map(phongDatItem => getPhongDatIdentifier(phongDatItem)!);
      const phongDatsToAdd = phongDats.filter(phongDatItem => {
        const phongDatIdentifier = getPhongDatIdentifier(phongDatItem);
        if (phongDatIdentifier == null || phongDatCollectionIdentifiers.includes(phongDatIdentifier)) {
          return false;
        }
        phongDatCollectionIdentifiers.push(phongDatIdentifier);
        return true;
      });
      return [...phongDatsToAdd, ...phongDatCollection];
    }
    return phongDatCollection;
  }

  protected convertDateFromClient(phongDat: IPhongDat): IPhongDat {
    return Object.assign({}, phongDat, {
      ngayDat: phongDat.ngayDat?.isValid() ? phongDat.ngayDat.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.ngayDat = res.body.ngayDat ? dayjs(res.body.ngayDat) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((phongDat: IPhongDat) => {
        phongDat.ngayDat = phongDat.ngayDat ? dayjs(phongDat.ngayDat) : undefined;
      });
    }
    return res;
  }
}
