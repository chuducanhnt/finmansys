import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPhongDat } from '../phong-dat.model';

@Component({
  selector: 'jhi-phong-dat-detail',
  templateUrl: './phong-dat-detail.component.html',
})
export class PhongDatDetailComponent implements OnInit {
  phongDat: IPhongDat | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ phongDat }) => {
      this.phongDat = phongDat;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
