package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.ChiTietTangTSCDRepository;
import com.group3.finmansys.service.ChiTietTangTSCDQueryService;
import com.group3.finmansys.service.ChiTietTangTSCDService;
import com.group3.finmansys.service.criteria.ChiTietTangTSCDCriteria;
import com.group3.finmansys.service.dto.ChiTietTangTSCDDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.ChiTietTangTSCD}.
 */
@RestController
@RequestMapping("/api")
public class ChiTietTangTSCDResource {

    private final Logger log = LoggerFactory.getLogger(ChiTietTangTSCDResource.class);

    private static final String ENTITY_NAME = "chiTietTangTSCD";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChiTietTangTSCDService chiTietTangTSCDService;

    private final ChiTietTangTSCDRepository chiTietTangTSCDRepository;

    private final ChiTietTangTSCDQueryService chiTietTangTSCDQueryService;

    public ChiTietTangTSCDResource(
        ChiTietTangTSCDService chiTietTangTSCDService,
        ChiTietTangTSCDRepository chiTietTangTSCDRepository,
        ChiTietTangTSCDQueryService chiTietTangTSCDQueryService
    ) {
        this.chiTietTangTSCDService = chiTietTangTSCDService;
        this.chiTietTangTSCDRepository = chiTietTangTSCDRepository;
        this.chiTietTangTSCDQueryService = chiTietTangTSCDQueryService;
    }

    /**
     * {@code POST  /chi-tiet-tang-tscds} : Create a new chiTietTangTSCD.
     *
     * @param chiTietTangTSCDDTO the chiTietTangTSCDDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new chiTietTangTSCDDTO, or with status {@code 400 (Bad Request)} if the chiTietTangTSCD has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/chi-tiet-tang-tscds")
    public ResponseEntity<ChiTietTangTSCDDTO> createChiTietTangTSCD(@Valid @RequestBody ChiTietTangTSCDDTO chiTietTangTSCDDTO)
        throws URISyntaxException {
        log.debug("REST request to save ChiTietTangTSCD : {}", chiTietTangTSCDDTO);
        if (chiTietTangTSCDDTO.getId() != null) {
            throw new BadRequestAlertException("A new chiTietTangTSCD cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChiTietTangTSCDDTO result = chiTietTangTSCDService.save(chiTietTangTSCDDTO);
        return ResponseEntity
            .created(new URI("/api/chi-tiet-tang-tscds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /chi-tiet-tang-tscds/:id} : Updates an existing chiTietTangTSCD.
     *
     * @param id the id of the chiTietTangTSCDDTO to save.
     * @param chiTietTangTSCDDTO the chiTietTangTSCDDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chiTietTangTSCDDTO,
     * or with status {@code 400 (Bad Request)} if the chiTietTangTSCDDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the chiTietTangTSCDDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/chi-tiet-tang-tscds/{id}")
    public ResponseEntity<ChiTietTangTSCDDTO> updateChiTietTangTSCD(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ChiTietTangTSCDDTO chiTietTangTSCDDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ChiTietTangTSCD : {}, {}", id, chiTietTangTSCDDTO);
        if (chiTietTangTSCDDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, chiTietTangTSCDDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!chiTietTangTSCDRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ChiTietTangTSCDDTO result = chiTietTangTSCDService.save(chiTietTangTSCDDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chiTietTangTSCDDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /chi-tiet-tang-tscds/:id} : Partial updates given fields of an existing chiTietTangTSCD, field will ignore if it is null
     *
     * @param id the id of the chiTietTangTSCDDTO to save.
     * @param chiTietTangTSCDDTO the chiTietTangTSCDDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated chiTietTangTSCDDTO,
     * or with status {@code 400 (Bad Request)} if the chiTietTangTSCDDTO is not valid,
     * or with status {@code 404 (Not Found)} if the chiTietTangTSCDDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the chiTietTangTSCDDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/chi-tiet-tang-tscds/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<ChiTietTangTSCDDTO> partialUpdateChiTietTangTSCD(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ChiTietTangTSCDDTO chiTietTangTSCDDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ChiTietTangTSCD partially : {}, {}", id, chiTietTangTSCDDTO);
        if (chiTietTangTSCDDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, chiTietTangTSCDDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!chiTietTangTSCDRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ChiTietTangTSCDDTO> result = chiTietTangTSCDService.partialUpdate(chiTietTangTSCDDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chiTietTangTSCDDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /chi-tiet-tang-tscds} : get all the chiTietTangTSCDS.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of chiTietTangTSCDS in body.
     */
    @GetMapping("/chi-tiet-tang-tscds")
    public ResponseEntity<List<ChiTietTangTSCDDTO>> getAllChiTietTangTSCDS(ChiTietTangTSCDCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ChiTietTangTSCDS by criteria: {}", criteria);
        Page<ChiTietTangTSCDDTO> page = chiTietTangTSCDQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /chi-tiet-tang-tscds/count} : count all the chiTietTangTSCDS.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/chi-tiet-tang-tscds/count")
    public ResponseEntity<Long> countChiTietTangTSCDS(ChiTietTangTSCDCriteria criteria) {
        log.debug("REST request to count ChiTietTangTSCDS by criteria: {}", criteria);
        return ResponseEntity.ok().body(chiTietTangTSCDQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /chi-tiet-tang-tscds/:id} : get the "id" chiTietTangTSCD.
     *
     * @param id the id of the chiTietTangTSCDDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the chiTietTangTSCDDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/chi-tiet-tang-tscds/{id}")
    public ResponseEntity<ChiTietTangTSCDDTO> getChiTietTangTSCD(@PathVariable Long id) {
        log.debug("REST request to get ChiTietTangTSCD : {}", id);
        Optional<ChiTietTangTSCDDTO> chiTietTangTSCDDTO = chiTietTangTSCDService.findOne(id);
        return ResponseUtil.wrapOrNotFound(chiTietTangTSCDDTO);
    }

    /**
     * {@code DELETE  /chi-tiet-tang-tscds/:id} : delete the "id" chiTietTangTSCD.
     *
     * @param id the id of the chiTietTangTSCDDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/chi-tiet-tang-tscds/{id}")
    public ResponseEntity<Void> deleteChiTietTangTSCD(@PathVariable Long id) {
        log.debug("REST request to delete ChiTietTangTSCD : {}", id);
        chiTietTangTSCDService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
