/**
 * View Models used by Spring MVC REST controllers.
 */
package com.group3.finmansys.web.rest.vm;
