package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.KhachSanRepository;
import com.group3.finmansys.service.KhachSanQueryService;
import com.group3.finmansys.service.KhachSanService;
import com.group3.finmansys.service.criteria.KhachSanCriteria;
import com.group3.finmansys.service.dto.KhachSanDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.KhachSan}.
 */
@RestController
@RequestMapping("/api")
public class KhachSanResource {

    private final Logger log = LoggerFactory.getLogger(KhachSanResource.class);

    private static final String ENTITY_NAME = "khachSan";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KhachSanService khachSanService;

    private final KhachSanRepository khachSanRepository;

    private final KhachSanQueryService khachSanQueryService;

    public KhachSanResource(
        KhachSanService khachSanService,
        KhachSanRepository khachSanRepository,
        KhachSanQueryService khachSanQueryService
    ) {
        this.khachSanService = khachSanService;
        this.khachSanRepository = khachSanRepository;
        this.khachSanQueryService = khachSanQueryService;
    }

    /**
     * {@code POST  /khach-sans} : Create a new khachSan.
     *
     * @param khachSanDTO the khachSanDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new khachSanDTO, or with status {@code 400 (Bad Request)} if the khachSan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/khach-sans")
    public ResponseEntity<KhachSanDTO> createKhachSan(@Valid @RequestBody KhachSanDTO khachSanDTO) throws URISyntaxException {
        log.debug("REST request to save KhachSan : {}", khachSanDTO);
        if (khachSanDTO.getId() != null) {
            throw new BadRequestAlertException("A new khachSan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KhachSanDTO result = khachSanService.save(khachSanDTO);
        return ResponseEntity
            .created(new URI("/api/khach-sans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /khach-sans/:id} : Updates an existing khachSan.
     *
     * @param id the id of the khachSanDTO to save.
     * @param khachSanDTO the khachSanDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated khachSanDTO,
     * or with status {@code 400 (Bad Request)} if the khachSanDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the khachSanDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/khach-sans/{id}")
    public ResponseEntity<KhachSanDTO> updateKhachSan(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody KhachSanDTO khachSanDTO
    ) throws URISyntaxException {
        log.debug("REST request to update KhachSan : {}, {}", id, khachSanDTO);
        if (khachSanDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, khachSanDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!khachSanRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        KhachSanDTO result = khachSanService.save(khachSanDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, khachSanDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /khach-sans/:id} : Partial updates given fields of an existing khachSan, field will ignore if it is null
     *
     * @param id the id of the khachSanDTO to save.
     * @param khachSanDTO the khachSanDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated khachSanDTO,
     * or with status {@code 400 (Bad Request)} if the khachSanDTO is not valid,
     * or with status {@code 404 (Not Found)} if the khachSanDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the khachSanDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/khach-sans/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<KhachSanDTO> partialUpdateKhachSan(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody KhachSanDTO khachSanDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update KhachSan partially : {}, {}", id, khachSanDTO);
        if (khachSanDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, khachSanDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!khachSanRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<KhachSanDTO> result = khachSanService.partialUpdate(khachSanDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, khachSanDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /khach-sans} : get all the khachSans.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of khachSans in body.
     */
    @GetMapping("/khach-sans")
    public ResponseEntity<List<KhachSanDTO>> getAllKhachSans(KhachSanCriteria criteria, Pageable pageable) {
        log.debug("REST request to get KhachSans by criteria: {}", criteria);
        Page<KhachSanDTO> page = khachSanQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /khach-sans/count} : count all the khachSans.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/khach-sans/count")
    public ResponseEntity<Long> countKhachSans(KhachSanCriteria criteria) {
        log.debug("REST request to count KhachSans by criteria: {}", criteria);
        return ResponseEntity.ok().body(khachSanQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /khach-sans/:id} : get the "id" khachSan.
     *
     * @param id the id of the khachSanDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the khachSanDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/khach-sans/{id}")
    public ResponseEntity<KhachSanDTO> getKhachSan(@PathVariable Long id) {
        log.debug("REST request to get KhachSan : {}", id);
        Optional<KhachSanDTO> khachSanDTO = khachSanService.findOne(id);
        return ResponseUtil.wrapOrNotFound(khachSanDTO);
    }

    /**
     * {@code DELETE  /khach-sans/:id} : delete the "id" khachSan.
     *
     * @param id the id of the khachSanDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/khach-sans/{id}")
    public ResponseEntity<Void> deleteKhachSan(@PathVariable Long id) {
        log.debug("REST request to delete KhachSan : {}", id);
        khachSanService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
