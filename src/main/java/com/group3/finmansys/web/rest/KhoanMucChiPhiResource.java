package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.KhoanMucChiPhiRepository;
import com.group3.finmansys.service.KhoanMucChiPhiQueryService;
import com.group3.finmansys.service.KhoanMucChiPhiService;
import com.group3.finmansys.service.criteria.KhoanMucChiPhiCriteria;
import com.group3.finmansys.service.dto.KhoanMucChiPhiDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.KhoanMucChiPhi}.
 */
@RestController
@RequestMapping("/api")
public class KhoanMucChiPhiResource {

    private final Logger log = LoggerFactory.getLogger(KhoanMucChiPhiResource.class);

    private static final String ENTITY_NAME = "khoanMucChiPhi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KhoanMucChiPhiService khoanMucChiPhiService;

    private final KhoanMucChiPhiRepository khoanMucChiPhiRepository;

    private final KhoanMucChiPhiQueryService khoanMucChiPhiQueryService;

    public KhoanMucChiPhiResource(
        KhoanMucChiPhiService khoanMucChiPhiService,
        KhoanMucChiPhiRepository khoanMucChiPhiRepository,
        KhoanMucChiPhiQueryService khoanMucChiPhiQueryService
    ) {
        this.khoanMucChiPhiService = khoanMucChiPhiService;
        this.khoanMucChiPhiRepository = khoanMucChiPhiRepository;
        this.khoanMucChiPhiQueryService = khoanMucChiPhiQueryService;
    }

    /**
     * {@code POST  /khoan-muc-chi-phis} : Create a new khoanMucChiPhi.
     *
     * @param khoanMucChiPhiDTO the khoanMucChiPhiDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new khoanMucChiPhiDTO, or with status {@code 400 (Bad Request)} if the khoanMucChiPhi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/khoan-muc-chi-phis")
    public ResponseEntity<KhoanMucChiPhiDTO> createKhoanMucChiPhi(@Valid @RequestBody KhoanMucChiPhiDTO khoanMucChiPhiDTO)
        throws URISyntaxException {
        log.debug("REST request to save KhoanMucChiPhi : {}", khoanMucChiPhiDTO);
        if (khoanMucChiPhiDTO.getId() != null) {
            throw new BadRequestAlertException("A new khoanMucChiPhi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KhoanMucChiPhiDTO result = khoanMucChiPhiService.save(khoanMucChiPhiDTO);
        return ResponseEntity
            .created(new URI("/api/khoan-muc-chi-phis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /khoan-muc-chi-phis/:id} : Updates an existing khoanMucChiPhi.
     *
     * @param id the id of the khoanMucChiPhiDTO to save.
     * @param khoanMucChiPhiDTO the khoanMucChiPhiDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated khoanMucChiPhiDTO,
     * or with status {@code 400 (Bad Request)} if the khoanMucChiPhiDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the khoanMucChiPhiDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/khoan-muc-chi-phis/{id}")
    public ResponseEntity<KhoanMucChiPhiDTO> updateKhoanMucChiPhi(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody KhoanMucChiPhiDTO khoanMucChiPhiDTO
    ) throws URISyntaxException {
        log.debug("REST request to update KhoanMucChiPhi : {}, {}", id, khoanMucChiPhiDTO);
        if (khoanMucChiPhiDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, khoanMucChiPhiDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!khoanMucChiPhiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        KhoanMucChiPhiDTO result = khoanMucChiPhiService.save(khoanMucChiPhiDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, khoanMucChiPhiDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /khoan-muc-chi-phis/:id} : Partial updates given fields of an existing khoanMucChiPhi, field will ignore if it is null
     *
     * @param id the id of the khoanMucChiPhiDTO to save.
     * @param khoanMucChiPhiDTO the khoanMucChiPhiDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated khoanMucChiPhiDTO,
     * or with status {@code 400 (Bad Request)} if the khoanMucChiPhiDTO is not valid,
     * or with status {@code 404 (Not Found)} if the khoanMucChiPhiDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the khoanMucChiPhiDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/khoan-muc-chi-phis/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<KhoanMucChiPhiDTO> partialUpdateKhoanMucChiPhi(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody KhoanMucChiPhiDTO khoanMucChiPhiDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update KhoanMucChiPhi partially : {}, {}", id, khoanMucChiPhiDTO);
        if (khoanMucChiPhiDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, khoanMucChiPhiDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!khoanMucChiPhiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<KhoanMucChiPhiDTO> result = khoanMucChiPhiService.partialUpdate(khoanMucChiPhiDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, khoanMucChiPhiDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /khoan-muc-chi-phis} : get all the khoanMucChiPhis.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of khoanMucChiPhis in body.
     */
    @GetMapping("/khoan-muc-chi-phis")
    public ResponseEntity<List<KhoanMucChiPhiDTO>> getAllKhoanMucChiPhis(KhoanMucChiPhiCriteria criteria, Pageable pageable) {
        log.debug("REST request to get KhoanMucChiPhis by criteria: {}", criteria);
        Page<KhoanMucChiPhiDTO> page = khoanMucChiPhiQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /khoan-muc-chi-phis/count} : count all the khoanMucChiPhis.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/khoan-muc-chi-phis/count")
    public ResponseEntity<Long> countKhoanMucChiPhis(KhoanMucChiPhiCriteria criteria) {
        log.debug("REST request to count KhoanMucChiPhis by criteria: {}", criteria);
        return ResponseEntity.ok().body(khoanMucChiPhiQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /khoan-muc-chi-phis/:id} : get the "id" khoanMucChiPhi.
     *
     * @param id the id of the khoanMucChiPhiDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the khoanMucChiPhiDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/khoan-muc-chi-phis/{id}")
    public ResponseEntity<KhoanMucChiPhiDTO> getKhoanMucChiPhi(@PathVariable Long id) {
        log.debug("REST request to get KhoanMucChiPhi : {}", id);
        Optional<KhoanMucChiPhiDTO> khoanMucChiPhiDTO = khoanMucChiPhiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(khoanMucChiPhiDTO);
    }

    /**
     * {@code DELETE  /khoan-muc-chi-phis/:id} : delete the "id" khoanMucChiPhi.
     *
     * @param id the id of the khoanMucChiPhiDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/khoan-muc-chi-phis/{id}")
    public ResponseEntity<Void> deleteKhoanMucChiPhi(@PathVariable Long id) {
        log.debug("REST request to delete KhoanMucChiPhi : {}", id);
        khoanMucChiPhiService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
