package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.MaKeToanRepository;
import com.group3.finmansys.service.MaKeToanQueryService;
import com.group3.finmansys.service.MaKeToanService;
import com.group3.finmansys.service.criteria.MaKeToanCriteria;
import com.group3.finmansys.service.dto.MaKeToanDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.MaKeToan}.
 */
@RestController
@RequestMapping("/api")
public class MaKeToanResource {

    private final Logger log = LoggerFactory.getLogger(MaKeToanResource.class);

    private static final String ENTITY_NAME = "maKeToan";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MaKeToanService maKeToanService;

    private final MaKeToanRepository maKeToanRepository;

    private final MaKeToanQueryService maKeToanQueryService;

    public MaKeToanResource(
        MaKeToanService maKeToanService,
        MaKeToanRepository maKeToanRepository,
        MaKeToanQueryService maKeToanQueryService
    ) {
        this.maKeToanService = maKeToanService;
        this.maKeToanRepository = maKeToanRepository;
        this.maKeToanQueryService = maKeToanQueryService;
    }

    /**
     * {@code POST  /ma-ke-toans} : Create a new maKeToan.
     *
     * @param maKeToanDTO the maKeToanDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new maKeToanDTO, or with status {@code 400 (Bad Request)} if the maKeToan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ma-ke-toans")
    public ResponseEntity<MaKeToanDTO> createMaKeToan(@Valid @RequestBody MaKeToanDTO maKeToanDTO) throws URISyntaxException {
        log.debug("REST request to save MaKeToan : {}", maKeToanDTO);
        if (maKeToanDTO.getId() != null) {
            throw new BadRequestAlertException("A new maKeToan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MaKeToanDTO result = maKeToanService.save(maKeToanDTO);
        return ResponseEntity
            .created(new URI("/api/ma-ke-toans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ma-ke-toans/:id} : Updates an existing maKeToan.
     *
     * @param id the id of the maKeToanDTO to save.
     * @param maKeToanDTO the maKeToanDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated maKeToanDTO,
     * or with status {@code 400 (Bad Request)} if the maKeToanDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the maKeToanDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ma-ke-toans/{id}")
    public ResponseEntity<MaKeToanDTO> updateMaKeToan(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody MaKeToanDTO maKeToanDTO
    ) throws URISyntaxException {
        log.debug("REST request to update MaKeToan : {}, {}", id, maKeToanDTO);
        if (maKeToanDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, maKeToanDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!maKeToanRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MaKeToanDTO result = maKeToanService.save(maKeToanDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, maKeToanDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /ma-ke-toans/:id} : Partial updates given fields of an existing maKeToan, field will ignore if it is null
     *
     * @param id the id of the maKeToanDTO to save.
     * @param maKeToanDTO the maKeToanDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated maKeToanDTO,
     * or with status {@code 400 (Bad Request)} if the maKeToanDTO is not valid,
     * or with status {@code 404 (Not Found)} if the maKeToanDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the maKeToanDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/ma-ke-toans/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<MaKeToanDTO> partialUpdateMaKeToan(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody MaKeToanDTO maKeToanDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update MaKeToan partially : {}, {}", id, maKeToanDTO);
        if (maKeToanDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, maKeToanDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!maKeToanRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MaKeToanDTO> result = maKeToanService.partialUpdate(maKeToanDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, maKeToanDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /ma-ke-toans} : get all the maKeToans.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of maKeToans in body.
     */
    @GetMapping("/ma-ke-toans")
    public ResponseEntity<List<MaKeToanDTO>> getAllMaKeToans(MaKeToanCriteria criteria, Pageable pageable) {
        log.debug("REST request to get MaKeToans by criteria: {}", criteria);
        Page<MaKeToanDTO> page = maKeToanQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /ma-ke-toans/count} : count all the maKeToans.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/ma-ke-toans/count")
    public ResponseEntity<Long> countMaKeToans(MaKeToanCriteria criteria) {
        log.debug("REST request to count MaKeToans by criteria: {}", criteria);
        return ResponseEntity.ok().body(maKeToanQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ma-ke-toans/:id} : get the "id" maKeToan.
     *
     * @param id the id of the maKeToanDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the maKeToanDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ma-ke-toans/{id}")
    public ResponseEntity<MaKeToanDTO> getMaKeToan(@PathVariable Long id) {
        log.debug("REST request to get MaKeToan : {}", id);
        Optional<MaKeToanDTO> maKeToanDTO = maKeToanService.findOne(id);
        return ResponseUtil.wrapOrNotFound(maKeToanDTO);
    }

    /**
     * {@code DELETE  /ma-ke-toans/:id} : delete the "id" maKeToan.
     *
     * @param id the id of the maKeToanDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ma-ke-toans/{id}")
    public ResponseEntity<Void> deleteMaKeToan(@PathVariable Long id) {
        log.debug("REST request to delete MaKeToan : {}", id);
        maKeToanService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
