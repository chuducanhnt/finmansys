package com.group3.finmansys.web.rest;

import com.group3.finmansys.repository.KhoanPhaiThuRepository;
import com.group3.finmansys.service.KhoanPhaiThuQueryService;
import com.group3.finmansys.service.KhoanPhaiThuService;
import com.group3.finmansys.service.criteria.KhoanPhaiThuCriteria;
import com.group3.finmansys.service.dto.KhoanPhaiThuDTO;
import com.group3.finmansys.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.group3.finmansys.domain.KhoanPhaiThu}.
 */
@RestController
@RequestMapping("/api")
public class KhoanPhaiThuResource {

    private final Logger log = LoggerFactory.getLogger(KhoanPhaiThuResource.class);

    private static final String ENTITY_NAME = "khoanPhaiThu";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KhoanPhaiThuService khoanPhaiThuService;

    private final KhoanPhaiThuRepository khoanPhaiThuRepository;

    private final KhoanPhaiThuQueryService khoanPhaiThuQueryService;

    public KhoanPhaiThuResource(
        KhoanPhaiThuService khoanPhaiThuService,
        KhoanPhaiThuRepository khoanPhaiThuRepository,
        KhoanPhaiThuQueryService khoanPhaiThuQueryService
    ) {
        this.khoanPhaiThuService = khoanPhaiThuService;
        this.khoanPhaiThuRepository = khoanPhaiThuRepository;
        this.khoanPhaiThuQueryService = khoanPhaiThuQueryService;
    }

    /**
     * {@code POST  /khoan-phai-thus} : Create a new khoanPhaiThu.
     *
     * @param khoanPhaiThuDTO the khoanPhaiThuDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new khoanPhaiThuDTO, or with status {@code 400 (Bad Request)} if the khoanPhaiThu has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/khoan-phai-thus")
    public ResponseEntity<KhoanPhaiThuDTO> createKhoanPhaiThu(@Valid @RequestBody KhoanPhaiThuDTO khoanPhaiThuDTO)
        throws URISyntaxException {
        log.debug("REST request to save KhoanPhaiThu : {}", khoanPhaiThuDTO);
        if (khoanPhaiThuDTO.getId() != null) {
            throw new BadRequestAlertException("A new khoanPhaiThu cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KhoanPhaiThuDTO result = khoanPhaiThuService.save(khoanPhaiThuDTO);
        return ResponseEntity
            .created(new URI("/api/khoan-phai-thus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /khoan-phai-thus/:id} : Updates an existing khoanPhaiThu.
     *
     * @param id the id of the khoanPhaiThuDTO to save.
     * @param khoanPhaiThuDTO the khoanPhaiThuDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated khoanPhaiThuDTO,
     * or with status {@code 400 (Bad Request)} if the khoanPhaiThuDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the khoanPhaiThuDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/khoan-phai-thus/{id}")
    public ResponseEntity<KhoanPhaiThuDTO> updateKhoanPhaiThu(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody KhoanPhaiThuDTO khoanPhaiThuDTO
    ) throws URISyntaxException {
        log.debug("REST request to update KhoanPhaiThu : {}, {}", id, khoanPhaiThuDTO);
        if (khoanPhaiThuDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, khoanPhaiThuDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!khoanPhaiThuRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        KhoanPhaiThuDTO result = khoanPhaiThuService.save(khoanPhaiThuDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, khoanPhaiThuDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /khoan-phai-thus/:id} : Partial updates given fields of an existing khoanPhaiThu, field will ignore if it is null
     *
     * @param id the id of the khoanPhaiThuDTO to save.
     * @param khoanPhaiThuDTO the khoanPhaiThuDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated khoanPhaiThuDTO,
     * or with status {@code 400 (Bad Request)} if the khoanPhaiThuDTO is not valid,
     * or with status {@code 404 (Not Found)} if the khoanPhaiThuDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the khoanPhaiThuDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/khoan-phai-thus/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<KhoanPhaiThuDTO> partialUpdateKhoanPhaiThu(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody KhoanPhaiThuDTO khoanPhaiThuDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update KhoanPhaiThu partially : {}, {}", id, khoanPhaiThuDTO);
        if (khoanPhaiThuDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, khoanPhaiThuDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!khoanPhaiThuRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<KhoanPhaiThuDTO> result = khoanPhaiThuService.partialUpdate(khoanPhaiThuDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, khoanPhaiThuDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /khoan-phai-thus} : get all the khoanPhaiThus.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of khoanPhaiThus in body.
     */
    @GetMapping("/khoan-phai-thus")
    public ResponseEntity<List<KhoanPhaiThuDTO>> getAllKhoanPhaiThus(KhoanPhaiThuCriteria criteria, Pageable pageable) {
        log.debug("REST request to get KhoanPhaiThus by criteria: {}", criteria);
        Page<KhoanPhaiThuDTO> page = khoanPhaiThuQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /khoan-phai-thus/count} : count all the khoanPhaiThus.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/khoan-phai-thus/count")
    public ResponseEntity<Long> countKhoanPhaiThus(KhoanPhaiThuCriteria criteria) {
        log.debug("REST request to count KhoanPhaiThus by criteria: {}", criteria);
        return ResponseEntity.ok().body(khoanPhaiThuQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /khoan-phai-thus/:id} : get the "id" khoanPhaiThu.
     *
     * @param id the id of the khoanPhaiThuDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the khoanPhaiThuDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/khoan-phai-thus/{id}")
    public ResponseEntity<KhoanPhaiThuDTO> getKhoanPhaiThu(@PathVariable Long id) {
        log.debug("REST request to get KhoanPhaiThu : {}", id);
        Optional<KhoanPhaiThuDTO> khoanPhaiThuDTO = khoanPhaiThuService.findOne(id);
        return ResponseUtil.wrapOrNotFound(khoanPhaiThuDTO);
    }

    /**
     * {@code DELETE  /khoan-phai-thus/:id} : delete the "id" khoanPhaiThu.
     *
     * @param id the id of the khoanPhaiThuDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/khoan-phai-thus/{id}")
    public ResponseEntity<Void> deleteKhoanPhaiThu(@PathVariable Long id) {
        log.debug("REST request to delete KhoanPhaiThu : {}", id);
        khoanPhaiThuService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
