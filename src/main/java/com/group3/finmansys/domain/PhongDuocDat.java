package com.group3.finmansys.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A PhongDuocDat.
 */
@Entity
@Table(name = "phong_duoc_dat")
public class PhongDuocDat implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "ngay_dat", nullable = false)
    private Instant ngayDat;

    @Column(name = "khuyen_mai")
    private Double khuyenMai;

    @NotNull
    @Column(name = "check_in", nullable = false)
    private Instant checkIn;

    @NotNull
    @Column(name = "check_out", nullable = false)
    private Instant checkOut;

    @NotNull
    @DecimalMin(value = "1")
    @Column(name = "gia", nullable = false)
    private Double gia;

    @ManyToOne(optional = false)
    @NotNull
    private KhachHang khachHang;

    @OneToMany(mappedBy = "phongDuocDat")
    @JsonIgnoreProperties(value = { "phong", "phongDuocDat" }, allowSetters = true)
    private Set<PhongDat> phongDats = new HashSet<>();

    @OneToMany(mappedBy = "phongDuocDat")
    @JsonIgnoreProperties(value = { "dichVu", "phongDuocDat" }, allowSetters = true)
    private Set<DichVuSuDung> dichVuSuDungs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PhongDuocDat id(Long id) {
        this.id = id;
        return this;
    }

    public Instant getNgayDat() {
        return this.ngayDat;
    }

    public PhongDuocDat ngayDat(Instant ngayDat) {
        this.ngayDat = ngayDat;
        return this;
    }

    public void setNgayDat(Instant ngayDat) {
        this.ngayDat = ngayDat;
    }

    public Double getKhuyenMai() {
        return this.khuyenMai;
    }

    public PhongDuocDat khuyenMai(Double khuyenMai) {
        this.khuyenMai = khuyenMai;
        return this;
    }

    public void setKhuyenMai(Double khuyenMai) {
        this.khuyenMai = khuyenMai;
    }

    public Instant getCheckIn() {
        return this.checkIn;
    }

    public PhongDuocDat checkIn(Instant checkIn) {
        this.checkIn = checkIn;
        return this;
    }

    public void setCheckIn(Instant checkIn) {
        this.checkIn = checkIn;
    }

    public Instant getCheckOut() {
        return this.checkOut;
    }

    public PhongDuocDat checkOut(Instant checkOut) {
        this.checkOut = checkOut;
        return this;
    }

    public void setCheckOut(Instant checkOut) {
        this.checkOut = checkOut;
    }

    public Double getGia() {
        return this.gia;
    }

    public PhongDuocDat gia(Double gia) {
        this.gia = gia;
        return this;
    }

    public void setGia(Double gia) {
        this.gia = gia;
    }

    public KhachHang getKhachHang() {
        return this.khachHang;
    }

    public PhongDuocDat khachHang(KhachHang khachHang) {
        this.setKhachHang(khachHang);
        return this;
    }

    public void setKhachHang(KhachHang khachHang) {
        this.khachHang = khachHang;
    }

    public Set<PhongDat> getPhongDats() {
        return this.phongDats;
    }

    public PhongDuocDat phongDats(Set<PhongDat> phongDats) {
        this.setPhongDats(phongDats);
        return this;
    }

    public PhongDuocDat addPhongDat(PhongDat phongDat) {
        this.phongDats.add(phongDat);
        phongDat.setPhongDuocDat(this);
        return this;
    }

    public PhongDuocDat removePhongDat(PhongDat phongDat) {
        this.phongDats.remove(phongDat);
        phongDat.setPhongDuocDat(null);
        return this;
    }

    public void setPhongDats(Set<PhongDat> phongDats) {
        if (this.phongDats != null) {
            this.phongDats.forEach(i -> i.setPhongDuocDat(null));
        }
        if (phongDats != null) {
            phongDats.forEach(i -> i.setPhongDuocDat(this));
        }
        this.phongDats = phongDats;
    }

    public Set<DichVuSuDung> getDichVuSuDungs() {
        return this.dichVuSuDungs;
    }

    public PhongDuocDat dichVuSuDungs(Set<DichVuSuDung> dichVuSuDungs) {
        this.setDichVuSuDungs(dichVuSuDungs);
        return this;
    }

    public PhongDuocDat addDichVuSuDung(DichVuSuDung dichVuSuDung) {
        this.dichVuSuDungs.add(dichVuSuDung);
        dichVuSuDung.setPhongDuocDat(this);
        return this;
    }

    public PhongDuocDat removeDichVuSuDung(DichVuSuDung dichVuSuDung) {
        this.dichVuSuDungs.remove(dichVuSuDung);
        dichVuSuDung.setPhongDuocDat(null);
        return this;
    }

    public void setDichVuSuDungs(Set<DichVuSuDung> dichVuSuDungs) {
        if (this.dichVuSuDungs != null) {
            this.dichVuSuDungs.forEach(i -> i.setPhongDuocDat(null));
        }
        if (dichVuSuDungs != null) {
            dichVuSuDungs.forEach(i -> i.setPhongDuocDat(this));
        }
        this.dichVuSuDungs = dichVuSuDungs;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PhongDuocDat)) {
            return false;
        }
        return id != null && id.equals(((PhongDuocDat) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhongDuocDat{" +
            "id=" + getId() +
            ", ngayDat='" + getNgayDat() + "'" +
            ", khuyenMai=" + getKhuyenMai() +
            ", checkIn='" + getCheckIn() + "'" +
            ", checkOut='" + getCheckOut() + "'" +
            ", gia=" + getGia() +
            "}";
    }
}
