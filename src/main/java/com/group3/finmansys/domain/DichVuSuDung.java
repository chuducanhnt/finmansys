package com.group3.finmansys.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A DichVuSuDung.
 */
@Entity
@Table(name = "dich_vu_su_dung")
public class DichVuSuDung implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Min(value = 1)
    @Column(name = "so_luong", nullable = false)
    private Integer soLuong;

    @NotNull
    @DecimalMin(value = "1")
    @Column(name = "gia", nullable = false)
    private Double gia;

    @NotNull
    @Column(name = "ngay_su_dung", nullable = false)
    private Instant ngaySuDung;

    @ManyToOne(optional = false)
    @NotNull
    private DichVu dichVu;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "khachHang", "phongDats", "dichVuSuDungs" }, allowSetters = true)
    private PhongDuocDat phongDuocDat;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DichVuSuDung id(Long id) {
        this.id = id;
        return this;
    }

    public Integer getSoLuong() {
        return this.soLuong;
    }

    public DichVuSuDung soLuong(Integer soLuong) {
        this.soLuong = soLuong;
        return this;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }

    public Double getGia() {
        return this.gia;
    }

    public DichVuSuDung gia(Double gia) {
        this.gia = gia;
        return this;
    }

    public void setGia(Double gia) {
        this.gia = gia;
    }

    public Instant getNgaySuDung() {
        return this.ngaySuDung;
    }

    public DichVuSuDung ngaySuDung(Instant ngaySuDung) {
        this.ngaySuDung = ngaySuDung;
        return this;
    }

    public void setNgaySuDung(Instant ngaySuDung) {
        this.ngaySuDung = ngaySuDung;
    }

    public DichVu getDichVu() {
        return this.dichVu;
    }

    public DichVuSuDung dichVu(DichVu dichVu) {
        this.setDichVu(dichVu);
        return this;
    }

    public void setDichVu(DichVu dichVu) {
        this.dichVu = dichVu;
    }

    public PhongDuocDat getPhongDuocDat() {
        return this.phongDuocDat;
    }

    public DichVuSuDung phongDuocDat(PhongDuocDat phongDuocDat) {
        this.setPhongDuocDat(phongDuocDat);
        return this;
    }

    public void setPhongDuocDat(PhongDuocDat phongDuocDat) {
        this.phongDuocDat = phongDuocDat;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DichVuSuDung)) {
            return false;
        }
        return id != null && id.equals(((DichVuSuDung) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DichVuSuDung{" +
            "id=" + getId() +
            ", soLuong=" + getSoLuong() +
            ", gia=" + getGia() +
            ", ngaySuDung='" + getNgaySuDung() + "'" +
            "}";
    }
}
