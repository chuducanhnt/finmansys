package com.group3.finmansys.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A ChiTietGiamTSCD.
 */
@Entity
@Table(name = "chi_tiet_giamtscd")
public class ChiTietGiamTSCD implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "ngay_mua", nullable = false)
    private Instant ngayMua;

    @NotNull
    @Min(value = 1)
    @Column(name = "so_luong", nullable = false)
    private Integer soLuong;

    @DecimalMin(value = "0")
    @Column(name = "khau_hao")
    private Double khauHao;

    @NotNull
    @DecimalMin(value = "1")
    @Column(name = "so_tien_thanh_ly", nullable = false)
    private Double soTienThanhLy;

    @NotNull
    @DecimalMin(value = "1")
    @Column(name = "chi_phi_mua", nullable = false)
    private Double chiPhiMua;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "khachSan", "loaiTaiSan" }, allowSetters = true)
    private TaiSanCD taiSanCD;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ChiTietGiamTSCD id(Long id) {
        this.id = id;
        return this;
    }

    public Instant getNgayMua() {
        return this.ngayMua;
    }

    public ChiTietGiamTSCD ngayMua(Instant ngayMua) {
        this.ngayMua = ngayMua;
        return this;
    }

    public void setNgayMua(Instant ngayMua) {
        this.ngayMua = ngayMua;
    }

    public Integer getSoLuong() {
        return this.soLuong;
    }

    public ChiTietGiamTSCD soLuong(Integer soLuong) {
        this.soLuong = soLuong;
        return this;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }

    public Double getKhauHao() {
        return this.khauHao;
    }

    public ChiTietGiamTSCD khauHao(Double khauHao) {
        this.khauHao = khauHao;
        return this;
    }

    public void setKhauHao(Double khauHao) {
        this.khauHao = khauHao;
    }

    public Double getSoTienThanhLy() {
        return this.soTienThanhLy;
    }

    public ChiTietGiamTSCD soTienThanhLy(Double soTienThanhLy) {
        this.soTienThanhLy = soTienThanhLy;
        return this;
    }

    public void setSoTienThanhLy(Double soTienThanhLy) {
        this.soTienThanhLy = soTienThanhLy;
    }

    public Double getChiPhiMua() {
        return this.chiPhiMua;
    }

    public ChiTietGiamTSCD chiPhiMua(Double chiPhiMua) {
        this.chiPhiMua = chiPhiMua;
        return this;
    }

    public void setChiPhiMua(Double chiPhiMua) {
        this.chiPhiMua = chiPhiMua;
    }

    public TaiSanCD getTaiSanCD() {
        return this.taiSanCD;
    }

    public ChiTietGiamTSCD taiSanCD(TaiSanCD taiSanCD) {
        this.setTaiSanCD(taiSanCD);
        return this;
    }

    public void setTaiSanCD(TaiSanCD taiSanCD) {
        this.taiSanCD = taiSanCD;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChiTietGiamTSCD)) {
            return false;
        }
        return id != null && id.equals(((ChiTietGiamTSCD) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ChiTietGiamTSCD{" +
            "id=" + getId() +
            ", ngayMua='" + getNgayMua() + "'" +
            ", soLuong=" + getSoLuong() +
            ", khauHao=" + getKhauHao() +
            ", soTienThanhLy=" + getSoTienThanhLy() +
            ", chiPhiMua=" + getChiPhiMua() +
            "}";
    }
}
