package com.group3.finmansys.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A TaiSanCD.
 */
@Entity
@Table(name = "tai_sancd")
public class TaiSanCD implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "ten", length = 50, nullable = false)
    private String ten;

    @NotNull
    @Column(name = "don_vi_tuoi_tho", nullable = false)
    private String donViTuoiTho;

    @NotNull
    @DecimalMin(value = "1")
    @Column(name = "thoi_gian_su_dung", nullable = false)
    private Double thoiGianSuDung;

    @Lob
    @Column(name = "hinh_anh")
    private byte[] hinhAnh;

    @Column(name = "hinh_anh_content_type")
    private String hinhAnhContentType;

    @ManyToOne(optional = false)
    @NotNull
    private KhachSan khachSan;

    @ManyToOne(optional = false)
    @NotNull
    private LoaiTaiSan loaiTaiSan;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TaiSanCD id(Long id) {
        this.id = id;
        return this;
    }

    public String getTen() {
        return this.ten;
    }

    public TaiSanCD ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getDonViTuoiTho() {
        return this.donViTuoiTho;
    }

    public TaiSanCD donViTuoiTho(String donViTuoiTho) {
        this.donViTuoiTho = donViTuoiTho;
        return this;
    }

    public void setDonViTuoiTho(String donViTuoiTho) {
        this.donViTuoiTho = donViTuoiTho;
    }

    public Double getThoiGianSuDung() {
        return this.thoiGianSuDung;
    }

    public TaiSanCD thoiGianSuDung(Double thoiGianSuDung) {
        this.thoiGianSuDung = thoiGianSuDung;
        return this;
    }

    public void setThoiGianSuDung(Double thoiGianSuDung) {
        this.thoiGianSuDung = thoiGianSuDung;
    }

    public byte[] getHinhAnh() {
        return this.hinhAnh;
    }

    public TaiSanCD hinhAnh(byte[] hinhAnh) {
        this.hinhAnh = hinhAnh;
        return this;
    }

    public void setHinhAnh(byte[] hinhAnh) {
        this.hinhAnh = hinhAnh;
    }

    public String getHinhAnhContentType() {
        return this.hinhAnhContentType;
    }

    public TaiSanCD hinhAnhContentType(String hinhAnhContentType) {
        this.hinhAnhContentType = hinhAnhContentType;
        return this;
    }

    public void setHinhAnhContentType(String hinhAnhContentType) {
        this.hinhAnhContentType = hinhAnhContentType;
    }

    public KhachSan getKhachSan() {
        return this.khachSan;
    }

    public TaiSanCD khachSan(KhachSan khachSan) {
        this.setKhachSan(khachSan);
        return this;
    }

    public void setKhachSan(KhachSan khachSan) {
        this.khachSan = khachSan;
    }

    public LoaiTaiSan getLoaiTaiSan() {
        return this.loaiTaiSan;
    }

    public TaiSanCD loaiTaiSan(LoaiTaiSan loaiTaiSan) {
        this.setLoaiTaiSan(loaiTaiSan);
        return this;
    }

    public void setLoaiTaiSan(LoaiTaiSan loaiTaiSan) {
        this.loaiTaiSan = loaiTaiSan;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TaiSanCD)) {
            return false;
        }
        return id != null && id.equals(((TaiSanCD) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TaiSanCD{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", donViTuoiTho='" + getDonViTuoiTho() + "'" +
            ", thoiGianSuDung=" + getThoiGianSuDung() +
            ", hinhAnh='" + getHinhAnh() + "'" +
            ", hinhAnhContentType='" + getHinhAnhContentType() + "'" +
            "}";
    }
}
