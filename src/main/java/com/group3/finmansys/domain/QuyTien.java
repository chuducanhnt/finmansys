package com.group3.finmansys.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A QuyTien.
 */
@Entity
@Table(name = "quy_tien")
public class QuyTien implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @DecimalMin(value = "1")
    @Column(name = "so_tien", nullable = false)
    private Double soTien;

    @Column(name = "ghi_chu")
    private String ghiChu;

    @NotNull
    @Column(name = "ten", nullable = false)
    private String ten;

    @ManyToOne(optional = false)
    @NotNull
    private LoaiQuy loaiQuy;

    @ManyToOne(optional = false)
    @NotNull
    private MaKeToan maKeToan;

    @ManyToOne(optional = false)
    @NotNull
    private KhachSan khachSan;

    @OneToMany(mappedBy = "quyTien")
    @JsonIgnoreProperties(value = { "phongDuocDat", "khoanPhaiThu", "quyTien" }, allowSetters = true)
    private Set<PhieuThu> phieuThus = new HashSet<>();

    @OneToMany(mappedBy = "quyTien")
    @JsonIgnoreProperties(value = { "khachHang", "maKeToan", "khoanMucChiPhi", "khoanPhaiTra", "quyTien" }, allowSetters = true)
    private Set<PhieuChi> phieuChis = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public QuyTien id(Long id) {
        this.id = id;
        return this;
    }

    public Double getSoTien() {
        return this.soTien;
    }

    public QuyTien soTien(Double soTien) {
        this.soTien = soTien;
        return this;
    }

    public void setSoTien(Double soTien) {
        this.soTien = soTien;
    }

    public String getGhiChu() {
        return this.ghiChu;
    }

    public QuyTien ghiChu(String ghiChu) {
        this.ghiChu = ghiChu;
        return this;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public String getTen() {
        return this.ten;
    }

    public QuyTien ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public LoaiQuy getLoaiQuy() {
        return this.loaiQuy;
    }

    public QuyTien loaiQuy(LoaiQuy loaiQuy) {
        this.setLoaiQuy(loaiQuy);
        return this;
    }

    public void setLoaiQuy(LoaiQuy loaiQuy) {
        this.loaiQuy = loaiQuy;
    }

    public MaKeToan getMaKeToan() {
        return this.maKeToan;
    }

    public QuyTien maKeToan(MaKeToan maKeToan) {
        this.setMaKeToan(maKeToan);
        return this;
    }

    public void setMaKeToan(MaKeToan maKeToan) {
        this.maKeToan = maKeToan;
    }

    public KhachSan getKhachSan() {
        return this.khachSan;
    }

    public QuyTien khachSan(KhachSan khachSan) {
        this.setKhachSan(khachSan);
        return this;
    }

    public void setKhachSan(KhachSan khachSan) {
        this.khachSan = khachSan;
    }

    public Set<PhieuThu> getPhieuThus() {
        return this.phieuThus;
    }

    public QuyTien phieuThus(Set<PhieuThu> phieuThus) {
        this.setPhieuThus(phieuThus);
        return this;
    }

    public QuyTien addPhieuThu(PhieuThu phieuThu) {
        this.phieuThus.add(phieuThu);
        phieuThu.setQuyTien(this);
        return this;
    }

    public QuyTien removePhieuThu(PhieuThu phieuThu) {
        this.phieuThus.remove(phieuThu);
        phieuThu.setQuyTien(null);
        return this;
    }

    public void setPhieuThus(Set<PhieuThu> phieuThus) {
        if (this.phieuThus != null) {
            this.phieuThus.forEach(i -> i.setQuyTien(null));
        }
        if (phieuThus != null) {
            phieuThus.forEach(i -> i.setQuyTien(this));
        }
        this.phieuThus = phieuThus;
    }

    public Set<PhieuChi> getPhieuChis() {
        return this.phieuChis;
    }

    public QuyTien phieuChis(Set<PhieuChi> phieuChis) {
        this.setPhieuChis(phieuChis);
        return this;
    }

    public QuyTien addPhieuChi(PhieuChi phieuChi) {
        this.phieuChis.add(phieuChi);
        phieuChi.setQuyTien(this);
        return this;
    }

    public QuyTien removePhieuChi(PhieuChi phieuChi) {
        this.phieuChis.remove(phieuChi);
        phieuChi.setQuyTien(null);
        return this;
    }

    public void setPhieuChis(Set<PhieuChi> phieuChis) {
        if (this.phieuChis != null) {
            this.phieuChis.forEach(i -> i.setQuyTien(null));
        }
        if (phieuChis != null) {
            phieuChis.forEach(i -> i.setQuyTien(this));
        }
        this.phieuChis = phieuChis;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof QuyTien)) {
            return false;
        }
        return id != null && id.equals(((QuyTien) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "QuyTien{" +
            "id=" + getId() +
            ", soTien=" + getSoTien() +
            ", ghiChu='" + getGhiChu() + "'" +
            ", ten='" + getTen() + "'" +
            "}";
    }
}
