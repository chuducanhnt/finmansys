package com.group3.finmansys.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A LoaiTaiSan.
 */
@Entity
@Table(name = "loai_tai_san")
public class LoaiTaiSan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ten")
    private String ten;

    @Column(name = "mo_ta")
    private String moTa;

    @ManyToOne(optional = false)
    @NotNull
    private MaKeToan maKeToan;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LoaiTaiSan id(Long id) {
        this.id = id;
        return this;
    }

    public String getTen() {
        return this.ten;
    }

    public LoaiTaiSan ten(String ten) {
        this.ten = ten;
        return this;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMoTa() {
        return this.moTa;
    }

    public LoaiTaiSan moTa(String moTa) {
        this.moTa = moTa;
        return this;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public MaKeToan getMaKeToan() {
        return this.maKeToan;
    }

    public LoaiTaiSan maKeToan(MaKeToan maKeToan) {
        this.setMaKeToan(maKeToan);
        return this;
    }

    public void setMaKeToan(MaKeToan maKeToan) {
        this.maKeToan = maKeToan;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LoaiTaiSan)) {
            return false;
        }
        return id != null && id.equals(((LoaiTaiSan) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LoaiTaiSan{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", moTa='" + getMoTa() + "'" +
            "}";
    }
}
