package com.group3.finmansys.repository;

import com.group3.finmansys.domain.KhoanPhaiThu;
import com.group3.finmansys.domain.KhoanPhaiTra;
import java.time.Instant;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the KhoanPhaiTra entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KhoanPhaiTraRepository extends JpaRepository<KhoanPhaiTra, Long>, JpaSpecificationExecutor<KhoanPhaiTra> {
    @Query("SELECT kpt FROM KhoanPhaiTra kpt WHERE kpt.ngayTao <= ?1 AND kpt.maKeToan.soHieu = ?2")
    List<KhoanPhaiTra> getBangCanDoiKeToan(Instant start, Double soHieu);
}
