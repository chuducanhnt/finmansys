package com.group3.finmansys.repository;

import com.group3.finmansys.domain.LoaiTaiSan;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the LoaiTaiSan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LoaiTaiSanRepository extends JpaRepository<LoaiTaiSan, Long>, JpaSpecificationExecutor<LoaiTaiSan> {}
