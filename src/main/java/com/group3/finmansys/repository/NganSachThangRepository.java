package com.group3.finmansys.repository;

import com.group3.finmansys.domain.NganSachThang;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the NganSachThang entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NganSachThangRepository extends JpaRepository<NganSachThang, Long>, JpaSpecificationExecutor<NganSachThang> {}
