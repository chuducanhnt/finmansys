package com.group3.finmansys.repository;

import com.group3.finmansys.domain.CongThuc;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the CongThuc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CongThucRepository extends JpaRepository<CongThuc, Long>, JpaSpecificationExecutor<CongThuc> {}
