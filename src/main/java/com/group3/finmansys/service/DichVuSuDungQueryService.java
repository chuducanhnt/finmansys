package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.DichVuSuDung;
import com.group3.finmansys.repository.DichVuSuDungRepository;
import com.group3.finmansys.service.criteria.DichVuSuDungCriteria;
import com.group3.finmansys.service.dto.DichVuSuDungDTO;
import com.group3.finmansys.service.mapper.DichVuSuDungMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link DichVuSuDung} entities in the database.
 * The main input is a {@link DichVuSuDungCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DichVuSuDungDTO} or a {@link Page} of {@link DichVuSuDungDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DichVuSuDungQueryService extends QueryService<DichVuSuDung> {

    private final Logger log = LoggerFactory.getLogger(DichVuSuDungQueryService.class);

    private final DichVuSuDungRepository dichVuSuDungRepository;

    private final DichVuSuDungMapper dichVuSuDungMapper;

    public DichVuSuDungQueryService(DichVuSuDungRepository dichVuSuDungRepository, DichVuSuDungMapper dichVuSuDungMapper) {
        this.dichVuSuDungRepository = dichVuSuDungRepository;
        this.dichVuSuDungMapper = dichVuSuDungMapper;
    }

    /**
     * Return a {@link List} of {@link DichVuSuDungDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DichVuSuDungDTO> findByCriteria(DichVuSuDungCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DichVuSuDung> specification = createSpecification(criteria);
        return dichVuSuDungMapper.toDto(dichVuSuDungRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DichVuSuDungDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DichVuSuDungDTO> findByCriteria(DichVuSuDungCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DichVuSuDung> specification = createSpecification(criteria);
        return dichVuSuDungRepository.findAll(specification, page).map(dichVuSuDungMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DichVuSuDungCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DichVuSuDung> specification = createSpecification(criteria);
        return dichVuSuDungRepository.count(specification);
    }

    /**
     * Function to convert {@link DichVuSuDungCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DichVuSuDung> createSpecification(DichVuSuDungCriteria criteria) {
        Specification<DichVuSuDung> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), DichVuSuDung_.id));
            }
            if (criteria.getSoLuong() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoLuong(), DichVuSuDung_.soLuong));
            }
            if (criteria.getGia() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGia(), DichVuSuDung_.gia));
            }
            if (criteria.getNgaySuDung() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgaySuDung(), DichVuSuDung_.ngaySuDung));
            }
            if (criteria.getDichVuId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getDichVuId(), root -> root.join(DichVuSuDung_.dichVu, JoinType.LEFT).get(DichVu_.id))
                    );
            }
            if (criteria.getPhongDuocDatId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPhongDuocDatId(),
                            root -> root.join(DichVuSuDung_.phongDuocDat, JoinType.LEFT).get(PhongDuocDat_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
