package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.DichVu;
import com.group3.finmansys.repository.DichVuRepository;
import com.group3.finmansys.service.criteria.DichVuCriteria;
import com.group3.finmansys.service.dto.DichVuDTO;
import com.group3.finmansys.service.mapper.DichVuMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link DichVu} entities in the database.
 * The main input is a {@link DichVuCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DichVuDTO} or a {@link Page} of {@link DichVuDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DichVuQueryService extends QueryService<DichVu> {

    private final Logger log = LoggerFactory.getLogger(DichVuQueryService.class);

    private final DichVuRepository dichVuRepository;

    private final DichVuMapper dichVuMapper;

    public DichVuQueryService(DichVuRepository dichVuRepository, DichVuMapper dichVuMapper) {
        this.dichVuRepository = dichVuRepository;
        this.dichVuMapper = dichVuMapper;
    }

    /**
     * Return a {@link List} of {@link DichVuDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DichVuDTO> findByCriteria(DichVuCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DichVu> specification = createSpecification(criteria);
        return dichVuMapper.toDto(dichVuRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DichVuDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DichVuDTO> findByCriteria(DichVuCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DichVu> specification = createSpecification(criteria);
        return dichVuRepository.findAll(specification, page).map(dichVuMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DichVuCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DichVu> specification = createSpecification(criteria);
        return dichVuRepository.count(specification);
    }

    /**
     * Function to convert {@link DichVuCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DichVu> createSpecification(DichVuCriteria criteria) {
        Specification<DichVu> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), DichVu_.id));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), DichVu_.ten));
            }
            if (criteria.getGia() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGia(), DichVu_.gia));
            }
            if (criteria.getMoTa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMoTa(), DichVu_.moTa));
            }
        }
        return specification;
    }
}
