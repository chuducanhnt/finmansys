package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.LoaiTaiSan} entity.
 */
public class LoaiTaiSanDTO implements Serializable {

    private Long id;

    private String ten;

    private String moTa;

    private MaKeToanDTO maKeToan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public MaKeToanDTO getMaKeToan() {
        return maKeToan;
    }

    public void setMaKeToan(MaKeToanDTO maKeToan) {
        this.maKeToan = maKeToan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LoaiTaiSanDTO)) {
            return false;
        }

        LoaiTaiSanDTO loaiTaiSanDTO = (LoaiTaiSanDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, loaiTaiSanDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LoaiTaiSanDTO{" +
            "id=" + getId() +
            ", ten='" + getTen() + "'" +
            ", moTa='" + getMoTa() + "'" +
            ", maKeToan=" + getMaKeToan() +
            "}";
    }
}
