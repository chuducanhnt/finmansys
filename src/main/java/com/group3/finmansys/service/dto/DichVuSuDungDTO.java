package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.DichVuSuDung} entity.
 */
public class DichVuSuDungDTO implements Serializable {

    private Long id;

    @NotNull
    @Min(value = 1)
    private Integer soLuong;

    @NotNull
    @DecimalMin(value = "1")
    private Double gia;

    @NotNull
    private Instant ngaySuDung;

    private DichVuDTO dichVu;

    private PhongDuocDatDTO phongDuocDat;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }

    public Double getGia() {
        return gia;
    }

    public void setGia(Double gia) {
        this.gia = gia;
    }

    public Instant getNgaySuDung() {
        return ngaySuDung;
    }

    public void setNgaySuDung(Instant ngaySuDung) {
        this.ngaySuDung = ngaySuDung;
    }

    public DichVuDTO getDichVu() {
        return dichVu;
    }

    public void setDichVu(DichVuDTO dichVu) {
        this.dichVu = dichVu;
    }

    public PhongDuocDatDTO getPhongDuocDat() {
        return phongDuocDat;
    }

    public void setPhongDuocDat(PhongDuocDatDTO phongDuocDat) {
        this.phongDuocDat = phongDuocDat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DichVuSuDungDTO)) {
            return false;
        }

        DichVuSuDungDTO dichVuSuDungDTO = (DichVuSuDungDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, dichVuSuDungDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DichVuSuDungDTO{" +
            "id=" + getId() +
            ", soLuong=" + getSoLuong() +
            ", gia=" + getGia() +
            ", ngaySuDung='" + getNgaySuDung() + "'" +
            ", dichVu=" + getDichVu() +
            ", phongDuocDat=" + getPhongDuocDat() +
            "}";
    }
}
