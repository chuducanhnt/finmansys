package com.group3.finmansys.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.group3.finmansys.domain.ToanTu} entity.
 */
public class ToanTuDTO implements Serializable {

    private Long id;

    @NotNull
    private String dau;

    private MaKeToanDTO maKeToan;

    private CongThucDTO congThuc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDau() {
        return dau;
    }

    public void setDau(String dau) {
        this.dau = dau;
    }

    public MaKeToanDTO getMaKeToan() {
        return maKeToan;
    }

    public void setMaKeToan(MaKeToanDTO maKeToan) {
        this.maKeToan = maKeToan;
    }

    public CongThucDTO getCongThuc() {
        return congThuc;
    }

    public void setCongThuc(CongThucDTO congThuc) {
        this.congThuc = congThuc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ToanTuDTO)) {
            return false;
        }

        ToanTuDTO toanTuDTO = (ToanTuDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, toanTuDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ToanTuDTO{" +
            "id=" + getId() +
            ", dau='" + getDau() + "'" +
            ", maKeToan=" + getMaKeToan() +
            ", congThuc=" + getCongThuc() +
            "}";
    }
}
