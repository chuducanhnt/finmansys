package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.PhongDat;
import com.group3.finmansys.repository.PhongDatRepository;
import com.group3.finmansys.service.criteria.PhongDatCriteria;
import com.group3.finmansys.service.dto.PhongDatDTO;
import com.group3.finmansys.service.mapper.PhongDatMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link PhongDat} entities in the database.
 * The main input is a {@link PhongDatCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PhongDatDTO} or a {@link Page} of {@link PhongDatDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PhongDatQueryService extends QueryService<PhongDat> {

    private final Logger log = LoggerFactory.getLogger(PhongDatQueryService.class);

    private final PhongDatRepository phongDatRepository;

    private final PhongDatMapper phongDatMapper;

    public PhongDatQueryService(PhongDatRepository phongDatRepository, PhongDatMapper phongDatMapper) {
        this.phongDatRepository = phongDatRepository;
        this.phongDatMapper = phongDatMapper;
    }

    /**
     * Return a {@link List} of {@link PhongDatDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PhongDatDTO> findByCriteria(PhongDatCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PhongDat> specification = createSpecification(criteria);
        return phongDatMapper.toDto(phongDatRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PhongDatDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PhongDatDTO> findByCriteria(PhongDatCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PhongDat> specification = createSpecification(criteria);
        return phongDatRepository.findAll(specification, page).map(phongDatMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PhongDatCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PhongDat> specification = createSpecification(criteria);
        return phongDatRepository.count(specification);
    }

    /**
     * Function to convert {@link PhongDatCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PhongDat> createSpecification(PhongDatCriteria criteria) {
        Specification<PhongDat> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PhongDat_.id));
            }
            if (criteria.getNgayDat() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayDat(), PhongDat_.ngayDat));
            }
            if (criteria.getPhongId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getPhongId(), root -> root.join(PhongDat_.phong, JoinType.LEFT).get(Phong_.id))
                    );
            }
            if (criteria.getPhongDuocDatId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPhongDuocDatId(),
                            root -> root.join(PhongDat_.phongDuocDat, JoinType.LEFT).get(PhongDuocDat_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
