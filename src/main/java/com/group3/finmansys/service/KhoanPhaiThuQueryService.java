package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.KhoanPhaiThu;
import com.group3.finmansys.repository.KhoanPhaiThuRepository;
import com.group3.finmansys.service.criteria.KhoanPhaiThuCriteria;
import com.group3.finmansys.service.dto.KhoanPhaiThuDTO;
import com.group3.finmansys.service.mapper.KhoanPhaiThuMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link KhoanPhaiThu} entities in the database.
 * The main input is a {@link KhoanPhaiThuCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link KhoanPhaiThuDTO} or a {@link Page} of {@link KhoanPhaiThuDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class KhoanPhaiThuQueryService extends QueryService<KhoanPhaiThu> {

    private final Logger log = LoggerFactory.getLogger(KhoanPhaiThuQueryService.class);

    private final KhoanPhaiThuRepository khoanPhaiThuRepository;

    private final KhoanPhaiThuMapper khoanPhaiThuMapper;

    public KhoanPhaiThuQueryService(KhoanPhaiThuRepository khoanPhaiThuRepository, KhoanPhaiThuMapper khoanPhaiThuMapper) {
        this.khoanPhaiThuRepository = khoanPhaiThuRepository;
        this.khoanPhaiThuMapper = khoanPhaiThuMapper;
    }

    /**
     * Return a {@link List} of {@link KhoanPhaiThuDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<KhoanPhaiThuDTO> findByCriteria(KhoanPhaiThuCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<KhoanPhaiThu> specification = createSpecification(criteria);
        return khoanPhaiThuMapper.toDto(khoanPhaiThuRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link KhoanPhaiThuDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<KhoanPhaiThuDTO> findByCriteria(KhoanPhaiThuCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<KhoanPhaiThu> specification = createSpecification(criteria);
        return khoanPhaiThuRepository.findAll(specification, page).map(khoanPhaiThuMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(KhoanPhaiThuCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<KhoanPhaiThu> specification = createSpecification(criteria);
        return khoanPhaiThuRepository.count(specification);
    }

    /**
     * Function to convert {@link KhoanPhaiThuCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<KhoanPhaiThu> createSpecification(KhoanPhaiThuCriteria criteria) {
        Specification<KhoanPhaiThu> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), KhoanPhaiThu_.id));
            }
            if (criteria.getSoTien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoTien(), KhoanPhaiThu_.soTien));
            }
            if (criteria.getNgayTao() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayTao(), KhoanPhaiThu_.ngayTao));
            }
            if (criteria.getNgayPhaiThu() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayPhaiThu(), KhoanPhaiThu_.ngayPhaiThu));
            }
            if (criteria.getGhiChu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGhiChu(), KhoanPhaiThu_.ghiChu));
            }
            if (criteria.getKhachHangId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKhachHangId(),
                            root -> root.join(KhoanPhaiThu_.khachHang, JoinType.LEFT).get(KhachHang_.id)
                        )
                    );
            }
            if (criteria.getMaKeToanId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getMaKeToanId(),
                            root -> root.join(KhoanPhaiThu_.maKeToan, JoinType.LEFT).get(MaKeToan_.id)
                        )
                    );
            }
            if (criteria.getPhieuThuId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPhieuThuId(),
                            root -> root.join(KhoanPhaiThu_.phieuThus, JoinType.LEFT).get(PhieuThu_.id)
                        )
                    );
            }
            if (criteria.getPhongDuocDatId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPhongDuocDatId(),
                            root -> root.join(KhoanPhaiThu_.phongDuocDat, JoinType.LEFT).get(PhongDuocDat_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
