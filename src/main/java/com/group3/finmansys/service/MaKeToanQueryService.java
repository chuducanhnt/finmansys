package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.MaKeToan;
import com.group3.finmansys.repository.MaKeToanRepository;
import com.group3.finmansys.service.criteria.MaKeToanCriteria;
import com.group3.finmansys.service.dto.MaKeToanDTO;
import com.group3.finmansys.service.mapper.MaKeToanMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link MaKeToan} entities in the database.
 * The main input is a {@link MaKeToanCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MaKeToanDTO} or a {@link Page} of {@link MaKeToanDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MaKeToanQueryService extends QueryService<MaKeToan> {

    private final Logger log = LoggerFactory.getLogger(MaKeToanQueryService.class);

    private final MaKeToanRepository maKeToanRepository;

    private final MaKeToanMapper maKeToanMapper;

    public MaKeToanQueryService(MaKeToanRepository maKeToanRepository, MaKeToanMapper maKeToanMapper) {
        this.maKeToanRepository = maKeToanRepository;
        this.maKeToanMapper = maKeToanMapper;
    }

    /**
     * Return a {@link List} of {@link MaKeToanDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MaKeToanDTO> findByCriteria(MaKeToanCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MaKeToan> specification = createSpecification(criteria);
        return maKeToanMapper.toDto(maKeToanRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link MaKeToanDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MaKeToanDTO> findByCriteria(MaKeToanCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MaKeToan> specification = createSpecification(criteria);
        return maKeToanRepository.findAll(specification, page).map(maKeToanMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MaKeToanCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<MaKeToan> specification = createSpecification(criteria);
        return maKeToanRepository.count(specification);
    }

    /**
     * Function to convert {@link MaKeToanCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<MaKeToan> createSpecification(MaKeToanCriteria criteria) {
        Specification<MaKeToan> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), MaKeToan_.id));
            }
            if (criteria.getSoHieu() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoHieu(), MaKeToan_.soHieu));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), MaKeToan_.ten));
            }
        }
        return specification;
    }
}
