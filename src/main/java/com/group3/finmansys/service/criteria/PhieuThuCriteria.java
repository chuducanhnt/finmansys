package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.PhieuThu} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.PhieuThuResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /phieu-thus?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PhieuThuCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter ngayChungTu;

    private DoubleFilter soTien;

    private LongFilter khoanPhaiThuId;

    private LongFilter quyTienId;

    private LongFilter khachHangId;

    public PhieuThuCriteria() {}

    public PhieuThuCriteria(PhieuThuCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ngayChungTu = other.ngayChungTu == null ? null : other.ngayChungTu.copy();
        this.soTien = other.soTien == null ? null : other.soTien.copy();
        this.khoanPhaiThuId = other.khoanPhaiThuId == null ? null : other.khoanPhaiThuId.copy();
        this.quyTienId = other.quyTienId == null ? null : other.quyTienId.copy();
        this.khachHangId = other.khachHangId == null ? null : other.khachHangId.copy();
    }

    @Override
    public PhieuThuCriteria copy() {
        return new PhieuThuCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getNgayChungTu() {
        return ngayChungTu;
    }

    public InstantFilter ngayChungTu() {
        if (ngayChungTu == null) {
            ngayChungTu = new InstantFilter();
        }
        return ngayChungTu;
    }

    public void setNgayChungTu(InstantFilter ngayChungTu) {
        this.ngayChungTu = ngayChungTu;
    }

    public DoubleFilter getSoTien() {
        return soTien;
    }

    public DoubleFilter soTien() {
        if (soTien == null) {
            soTien = new DoubleFilter();
        }
        return soTien;
    }

    public void setSoTien(DoubleFilter soTien) {
        this.soTien = soTien;
    }

    public LongFilter getKhoanPhaiThuId() {
        return khoanPhaiThuId;
    }

    public LongFilter khoanPhaiThuId() {
        if (khoanPhaiThuId == null) {
            khoanPhaiThuId = new LongFilter();
        }
        return khoanPhaiThuId;
    }

    public void setKhoanPhaiThuId(LongFilter khoanPhaiThuId) {
        this.khoanPhaiThuId = khoanPhaiThuId;
    }

    public LongFilter getQuyTienId() {
        return quyTienId;
    }

    public LongFilter quyTienId() {
        if (quyTienId == null) {
            quyTienId = new LongFilter();
        }
        return quyTienId;
    }

    public void setQuyTienId(LongFilter quyTienId) {
        this.quyTienId = quyTienId;
    }

    public LongFilter getKhachHangId() {
        return khachHangId;
    }

    public LongFilter khachHangId() {
        if (khachHangId == null) {
            khachHangId = new LongFilter();
        }
        return khachHangId;
    }

    public void setKhachHangId(LongFilter khachHangId) {
        this.khachHangId = khachHangId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PhieuThuCriteria that = (PhieuThuCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(ngayChungTu, that.ngayChungTu) &&
            Objects.equals(soTien, that.soTien) &&
            Objects.equals(khoanPhaiThuId, that.khoanPhaiThuId) &&
            Objects.equals(quyTienId, that.quyTienId) &&
            Objects.equals(khachHangId, that.khachHangId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ngayChungTu, soTien, khoanPhaiThuId, quyTienId, khachHangId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhieuThuCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (ngayChungTu != null ? "ngayChungTu=" + ngayChungTu + ", " : "") +
            (soTien != null ? "soTien=" + soTien + ", " : "") +
            (khoanPhaiThuId != null ? "khoanPhaiThuId=" + khoanPhaiThuId + ", " : "") +
            (quyTienId != null ? "quyTienId=" + quyTienId + ", " : "") +
            (khachHangId != null ? "khachHangId=" + khachHangId + ", " : "") +
            "}";
    }
}
