package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.NganSachThang} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.NganSachThangResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /ngan-sach-thangs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NganSachThangCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter thang;

    private DoubleFilter soTien;

    private LongFilter nganSachNamId;

    private LongFilter khoanMucChiPhiId;

    public NganSachThangCriteria() {}

    public NganSachThangCriteria(NganSachThangCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.thang = other.thang == null ? null : other.thang.copy();
        this.soTien = other.soTien == null ? null : other.soTien.copy();
        this.nganSachNamId = other.nganSachNamId == null ? null : other.nganSachNamId.copy();
        this.khoanMucChiPhiId = other.khoanMucChiPhiId == null ? null : other.khoanMucChiPhiId.copy();
    }

    @Override
    public NganSachThangCriteria copy() {
        return new NganSachThangCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getThang() {
        return thang;
    }

    public IntegerFilter thang() {
        if (thang == null) {
            thang = new IntegerFilter();
        }
        return thang;
    }

    public void setThang(IntegerFilter thang) {
        this.thang = thang;
    }

    public DoubleFilter getSoTien() {
        return soTien;
    }

    public DoubleFilter soTien() {
        if (soTien == null) {
            soTien = new DoubleFilter();
        }
        return soTien;
    }

    public void setSoTien(DoubleFilter soTien) {
        this.soTien = soTien;
    }

    public LongFilter getNganSachNamId() {
        return nganSachNamId;
    }

    public LongFilter nganSachNamId() {
        if (nganSachNamId == null) {
            nganSachNamId = new LongFilter();
        }
        return nganSachNamId;
    }

    public void setNganSachNamId(LongFilter nganSachNamId) {
        this.nganSachNamId = nganSachNamId;
    }

    public LongFilter getKhoanMucChiPhiId() {
        return khoanMucChiPhiId;
    }

    public LongFilter khoanMucChiPhiId() {
        if (khoanMucChiPhiId == null) {
            khoanMucChiPhiId = new LongFilter();
        }
        return khoanMucChiPhiId;
    }

    public void setKhoanMucChiPhiId(LongFilter khoanMucChiPhiId) {
        this.khoanMucChiPhiId = khoanMucChiPhiId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NganSachThangCriteria that = (NganSachThangCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(thang, that.thang) &&
            Objects.equals(soTien, that.soTien) &&
            Objects.equals(nganSachNamId, that.nganSachNamId) &&
            Objects.equals(khoanMucChiPhiId, that.khoanMucChiPhiId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, thang, soTien, nganSachNamId, khoanMucChiPhiId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NganSachThangCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (thang != null ? "thang=" + thang + ", " : "") +
            (soTien != null ? "soTien=" + soTien + ", " : "") +
            (nganSachNamId != null ? "nganSachNamId=" + nganSachNamId + ", " : "") +
            (khoanMucChiPhiId != null ? "khoanMucChiPhiId=" + khoanMucChiPhiId + ", " : "") +
            "}";
    }
}
