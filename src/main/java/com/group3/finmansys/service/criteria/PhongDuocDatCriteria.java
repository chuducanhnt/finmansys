package com.group3.finmansys.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.group3.finmansys.domain.PhongDuocDat} entity. This class is used
 * in {@link com.group3.finmansys.web.rest.PhongDuocDatResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /phong-duoc-dats?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PhongDuocDatCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter ngayDat;

    private DoubleFilter khuyenMai;

    private InstantFilter checkIn;

    private InstantFilter checkOut;

    private DoubleFilter gia;

    private LongFilter khachHangId;

    private LongFilter phongDatId;

    private LongFilter dichVuSuDungId;

    public PhongDuocDatCriteria() {}

    public PhongDuocDatCriteria(PhongDuocDatCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ngayDat = other.ngayDat == null ? null : other.ngayDat.copy();
        this.khuyenMai = other.khuyenMai == null ? null : other.khuyenMai.copy();
        this.checkIn = other.checkIn == null ? null : other.checkIn.copy();
        this.checkOut = other.checkOut == null ? null : other.checkOut.copy();
        this.gia = other.gia == null ? null : other.gia.copy();
        this.khachHangId = other.khachHangId == null ? null : other.khachHangId.copy();
        this.phongDatId = other.phongDatId == null ? null : other.phongDatId.copy();
        this.dichVuSuDungId = other.dichVuSuDungId == null ? null : other.dichVuSuDungId.copy();
    }

    @Override
    public PhongDuocDatCriteria copy() {
        return new PhongDuocDatCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getNgayDat() {
        return ngayDat;
    }

    public InstantFilter ngayDat() {
        if (ngayDat == null) {
            ngayDat = new InstantFilter();
        }
        return ngayDat;
    }

    public void setNgayDat(InstantFilter ngayDat) {
        this.ngayDat = ngayDat;
    }

    public DoubleFilter getKhuyenMai() {
        return khuyenMai;
    }

    public DoubleFilter khuyenMai() {
        if (khuyenMai == null) {
            khuyenMai = new DoubleFilter();
        }
        return khuyenMai;
    }

    public void setKhuyenMai(DoubleFilter khuyenMai) {
        this.khuyenMai = khuyenMai;
    }

    public InstantFilter getCheckIn() {
        return checkIn;
    }

    public InstantFilter checkIn() {
        if (checkIn == null) {
            checkIn = new InstantFilter();
        }
        return checkIn;
    }

    public void setCheckIn(InstantFilter checkIn) {
        this.checkIn = checkIn;
    }

    public InstantFilter getCheckOut() {
        return checkOut;
    }

    public InstantFilter checkOut() {
        if (checkOut == null) {
            checkOut = new InstantFilter();
        }
        return checkOut;
    }

    public void setCheckOut(InstantFilter checkOut) {
        this.checkOut = checkOut;
    }

    public DoubleFilter getGia() {
        return gia;
    }

    public DoubleFilter gia() {
        if (gia == null) {
            gia = new DoubleFilter();
        }
        return gia;
    }

    public void setGia(DoubleFilter gia) {
        this.gia = gia;
    }

    public LongFilter getKhachHangId() {
        return khachHangId;
    }

    public LongFilter khachHangId() {
        if (khachHangId == null) {
            khachHangId = new LongFilter();
        }
        return khachHangId;
    }

    public void setKhachHangId(LongFilter khachHangId) {
        this.khachHangId = khachHangId;
    }

    public LongFilter getPhongDatId() {
        return phongDatId;
    }

    public LongFilter phongDatId() {
        if (phongDatId == null) {
            phongDatId = new LongFilter();
        }
        return phongDatId;
    }

    public void setPhongDatId(LongFilter phongDatId) {
        this.phongDatId = phongDatId;
    }

    public LongFilter getDichVuSuDungId() {
        return dichVuSuDungId;
    }

    public LongFilter dichVuSuDungId() {
        if (dichVuSuDungId == null) {
            dichVuSuDungId = new LongFilter();
        }
        return dichVuSuDungId;
    }

    public void setDichVuSuDungId(LongFilter dichVuSuDungId) {
        this.dichVuSuDungId = dichVuSuDungId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PhongDuocDatCriteria that = (PhongDuocDatCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(ngayDat, that.ngayDat) &&
            Objects.equals(khuyenMai, that.khuyenMai) &&
            Objects.equals(checkIn, that.checkIn) &&
            Objects.equals(checkOut, that.checkOut) &&
            Objects.equals(gia, that.gia) &&
            Objects.equals(khachHangId, that.khachHangId) &&
            Objects.equals(phongDatId, that.phongDatId) &&
            Objects.equals(dichVuSuDungId, that.dichVuSuDungId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ngayDat, khuyenMai, checkIn, checkOut, gia, khachHangId, phongDatId, dichVuSuDungId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhongDuocDatCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (ngayDat != null ? "ngayDat=" + ngayDat + ", " : "") +
            (khuyenMai != null ? "khuyenMai=" + khuyenMai + ", " : "") +
            (checkIn != null ? "checkIn=" + checkIn + ", " : "") +
            (checkOut != null ? "checkOut=" + checkOut + ", " : "") +
            (gia != null ? "gia=" + gia + ", " : "") +
            (khachHangId != null ? "khachHangId=" + khachHangId + ", " : "") +
            (phongDatId != null ? "phongDatId=" + phongDatId + ", " : "") +
            (dichVuSuDungId != null ? "dichVuSuDungId=" + dichVuSuDungId + ", " : "") +
            "}";
    }
}
