package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.NganSachNam;
import com.group3.finmansys.repository.NganSachNamRepository;
import com.group3.finmansys.service.criteria.NganSachNamCriteria;
import com.group3.finmansys.service.dto.NganSachNamDTO;
import com.group3.finmansys.service.mapper.NganSachNamMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link NganSachNam} entities in the database.
 * The main input is a {@link NganSachNamCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link NganSachNamDTO} or a {@link Page} of {@link NganSachNamDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class NganSachNamQueryService extends QueryService<NganSachNam> {

    private final Logger log = LoggerFactory.getLogger(NganSachNamQueryService.class);

    private final NganSachNamRepository nganSachNamRepository;

    private final NganSachNamMapper nganSachNamMapper;

    public NganSachNamQueryService(NganSachNamRepository nganSachNamRepository, NganSachNamMapper nganSachNamMapper) {
        this.nganSachNamRepository = nganSachNamRepository;
        this.nganSachNamMapper = nganSachNamMapper;
    }

    /**
     * Return a {@link List} of {@link NganSachNamDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NganSachNamDTO> findByCriteria(NganSachNamCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<NganSachNam> specification = createSpecification(criteria);
        return nganSachNamMapper.toDto(nganSachNamRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link NganSachNamDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NganSachNamDTO> findByCriteria(NganSachNamCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<NganSachNam> specification = createSpecification(criteria);
        return nganSachNamRepository.findAll(specification, page).map(nganSachNamMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(NganSachNamCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<NganSachNam> specification = createSpecification(criteria);
        return nganSachNamRepository.count(specification);
    }

    /**
     * Function to convert {@link NganSachNamCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<NganSachNam> createSpecification(NganSachNamCriteria criteria) {
        Specification<NganSachNam> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), NganSachNam_.id));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), NganSachNam_.ten));
            }
            if (criteria.getNam() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNam(), NganSachNam_.nam));
            }
            if (criteria.getSoTien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoTien(), NganSachNam_.soTien));
            }
            if (criteria.getKhachSanId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKhachSanId(),
                            root -> root.join(NganSachNam_.khachSan, JoinType.LEFT).get(KhachSan_.id)
                        )
                    );
            }
            if (criteria.getNganSachThangId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getNganSachThangId(),
                            root -> root.join(NganSachNam_.nganSachThangs, JoinType.LEFT).get(NganSachThang_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
