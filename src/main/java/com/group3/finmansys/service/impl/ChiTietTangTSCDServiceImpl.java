package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.ChiTietTangTSCD;
import com.group3.finmansys.repository.ChiTietTangTSCDRepository;
import com.group3.finmansys.service.ChiTietTangTSCDService;
import com.group3.finmansys.service.dto.ChiTietTangTSCDDTO;
import com.group3.finmansys.service.mapper.ChiTietTangTSCDMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ChiTietTangTSCD}.
 */
@Service
@Transactional
public class ChiTietTangTSCDServiceImpl implements ChiTietTangTSCDService {

    private final Logger log = LoggerFactory.getLogger(ChiTietTangTSCDServiceImpl.class);

    private final ChiTietTangTSCDRepository chiTietTangTSCDRepository;

    private final ChiTietTangTSCDMapper chiTietTangTSCDMapper;

    public ChiTietTangTSCDServiceImpl(ChiTietTangTSCDRepository chiTietTangTSCDRepository, ChiTietTangTSCDMapper chiTietTangTSCDMapper) {
        this.chiTietTangTSCDRepository = chiTietTangTSCDRepository;
        this.chiTietTangTSCDMapper = chiTietTangTSCDMapper;
    }

    @Override
    public ChiTietTangTSCDDTO save(ChiTietTangTSCDDTO chiTietTangTSCDDTO) {
        log.debug("Request to save ChiTietTangTSCD : {}", chiTietTangTSCDDTO);
        ChiTietTangTSCD chiTietTangTSCD = chiTietTangTSCDMapper.toEntity(chiTietTangTSCDDTO);
        chiTietTangTSCD = chiTietTangTSCDRepository.save(chiTietTangTSCD);
        return chiTietTangTSCDMapper.toDto(chiTietTangTSCD);
    }

    @Override
    public Optional<ChiTietTangTSCDDTO> partialUpdate(ChiTietTangTSCDDTO chiTietTangTSCDDTO) {
        log.debug("Request to partially update ChiTietTangTSCD : {}", chiTietTangTSCDDTO);

        return chiTietTangTSCDRepository
            .findById(chiTietTangTSCDDTO.getId())
            .map(
                existingChiTietTangTSCD -> {
                    chiTietTangTSCDMapper.partialUpdate(existingChiTietTangTSCD, chiTietTangTSCDDTO);
                    return existingChiTietTangTSCD;
                }
            )
            .map(chiTietTangTSCDRepository::save)
            .map(chiTietTangTSCDMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ChiTietTangTSCDDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ChiTietTangTSCDS");
        return chiTietTangTSCDRepository.findAll(pageable).map(chiTietTangTSCDMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ChiTietTangTSCDDTO> findOne(Long id) {
        log.debug("Request to get ChiTietTangTSCD : {}", id);
        return chiTietTangTSCDRepository.findById(id).map(chiTietTangTSCDMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ChiTietTangTSCD : {}", id);
        chiTietTangTSCDRepository.deleteById(id);
    }
}
