package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.LoaiQuy;
import com.group3.finmansys.repository.LoaiQuyRepository;
import com.group3.finmansys.service.LoaiQuyService;
import com.group3.finmansys.service.dto.LoaiQuyDTO;
import com.group3.finmansys.service.mapper.LoaiQuyMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link LoaiQuy}.
 */
@Service
@Transactional
public class LoaiQuyServiceImpl implements LoaiQuyService {

    private final Logger log = LoggerFactory.getLogger(LoaiQuyServiceImpl.class);

    private final LoaiQuyRepository loaiQuyRepository;

    private final LoaiQuyMapper loaiQuyMapper;

    public LoaiQuyServiceImpl(LoaiQuyRepository loaiQuyRepository, LoaiQuyMapper loaiQuyMapper) {
        this.loaiQuyRepository = loaiQuyRepository;
        this.loaiQuyMapper = loaiQuyMapper;
    }

    @Override
    public LoaiQuyDTO save(LoaiQuyDTO loaiQuyDTO) {
        log.debug("Request to save LoaiQuy : {}", loaiQuyDTO);
        LoaiQuy loaiQuy = loaiQuyMapper.toEntity(loaiQuyDTO);
        loaiQuy = loaiQuyRepository.save(loaiQuy);
        return loaiQuyMapper.toDto(loaiQuy);
    }

    @Override
    public Optional<LoaiQuyDTO> partialUpdate(LoaiQuyDTO loaiQuyDTO) {
        log.debug("Request to partially update LoaiQuy : {}", loaiQuyDTO);

        return loaiQuyRepository
            .findById(loaiQuyDTO.getId())
            .map(
                existingLoaiQuy -> {
                    loaiQuyMapper.partialUpdate(existingLoaiQuy, loaiQuyDTO);
                    return existingLoaiQuy;
                }
            )
            .map(loaiQuyRepository::save)
            .map(loaiQuyMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<LoaiQuyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LoaiQuies");
        return loaiQuyRepository.findAll(pageable).map(loaiQuyMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<LoaiQuyDTO> findOne(Long id) {
        log.debug("Request to get LoaiQuy : {}", id);
        return loaiQuyRepository.findById(id).map(loaiQuyMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete LoaiQuy : {}", id);
        loaiQuyRepository.deleteById(id);
    }
}
