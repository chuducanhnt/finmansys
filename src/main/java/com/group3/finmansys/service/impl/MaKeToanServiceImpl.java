package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.MaKeToan;
import com.group3.finmansys.repository.MaKeToanRepository;
import com.group3.finmansys.service.MaKeToanService;
import com.group3.finmansys.service.dto.MaKeToanDTO;
import com.group3.finmansys.service.mapper.MaKeToanMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link MaKeToan}.
 */
@Service
@Transactional
public class MaKeToanServiceImpl implements MaKeToanService {

    private final Logger log = LoggerFactory.getLogger(MaKeToanServiceImpl.class);

    private final MaKeToanRepository maKeToanRepository;

    private final MaKeToanMapper maKeToanMapper;

    public MaKeToanServiceImpl(MaKeToanRepository maKeToanRepository, MaKeToanMapper maKeToanMapper) {
        this.maKeToanRepository = maKeToanRepository;
        this.maKeToanMapper = maKeToanMapper;
    }

    @Override
    public MaKeToanDTO save(MaKeToanDTO maKeToanDTO) {
        log.debug("Request to save MaKeToan : {}", maKeToanDTO);
        MaKeToan maKeToan = maKeToanMapper.toEntity(maKeToanDTO);
        maKeToan = maKeToanRepository.save(maKeToan);
        return maKeToanMapper.toDto(maKeToan);
    }

    @Override
    public Optional<MaKeToanDTO> partialUpdate(MaKeToanDTO maKeToanDTO) {
        log.debug("Request to partially update MaKeToan : {}", maKeToanDTO);

        return maKeToanRepository
            .findById(maKeToanDTO.getId())
            .map(
                existingMaKeToan -> {
                    maKeToanMapper.partialUpdate(existingMaKeToan, maKeToanDTO);
                    return existingMaKeToan;
                }
            )
            .map(maKeToanRepository::save)
            .map(maKeToanMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MaKeToanDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MaKeToans");
        return maKeToanRepository.findAll(pageable).map(maKeToanMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<MaKeToanDTO> findOne(Long id) {
        log.debug("Request to get MaKeToan : {}", id);
        return maKeToanRepository.findById(id).map(maKeToanMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete MaKeToan : {}", id);
        maKeToanRepository.deleteById(id);
    }
}
