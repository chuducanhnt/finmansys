package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.DichVuSuDung;
import com.group3.finmansys.repository.DichVuSuDungRepository;
import com.group3.finmansys.service.DichVuSuDungService;
import com.group3.finmansys.service.dto.DichVuSuDungDTO;
import com.group3.finmansys.service.mapper.DichVuSuDungMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link DichVuSuDung}.
 */
@Service
@Transactional
public class DichVuSuDungServiceImpl implements DichVuSuDungService {

    private final Logger log = LoggerFactory.getLogger(DichVuSuDungServiceImpl.class);

    private final DichVuSuDungRepository dichVuSuDungRepository;

    private final DichVuSuDungMapper dichVuSuDungMapper;

    public DichVuSuDungServiceImpl(DichVuSuDungRepository dichVuSuDungRepository, DichVuSuDungMapper dichVuSuDungMapper) {
        this.dichVuSuDungRepository = dichVuSuDungRepository;
        this.dichVuSuDungMapper = dichVuSuDungMapper;
    }

    @Override
    public DichVuSuDungDTO save(DichVuSuDungDTO dichVuSuDungDTO) {
        log.debug("Request to save DichVuSuDung : {}", dichVuSuDungDTO);
        DichVuSuDung dichVuSuDung = dichVuSuDungMapper.toEntity(dichVuSuDungDTO);
        dichVuSuDung = dichVuSuDungRepository.save(dichVuSuDung);
        return dichVuSuDungMapper.toDto(dichVuSuDung);
    }

    @Override
    public Optional<DichVuSuDungDTO> partialUpdate(DichVuSuDungDTO dichVuSuDungDTO) {
        log.debug("Request to partially update DichVuSuDung : {}", dichVuSuDungDTO);

        return dichVuSuDungRepository
            .findById(dichVuSuDungDTO.getId())
            .map(
                existingDichVuSuDung -> {
                    dichVuSuDungMapper.partialUpdate(existingDichVuSuDung, dichVuSuDungDTO);
                    return existingDichVuSuDung;
                }
            )
            .map(dichVuSuDungRepository::save)
            .map(dichVuSuDungMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DichVuSuDungDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DichVuSuDungs");
        return dichVuSuDungRepository.findAll(pageable).map(dichVuSuDungMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DichVuSuDungDTO> findOne(Long id) {
        log.debug("Request to get DichVuSuDung : {}", id);
        return dichVuSuDungRepository.findById(id).map(dichVuSuDungMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete DichVuSuDung : {}", id);
        dichVuSuDungRepository.deleteById(id);
    }
}
