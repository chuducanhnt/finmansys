package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.ToanTu;
import com.group3.finmansys.repository.ToanTuRepository;
import com.group3.finmansys.service.ToanTuService;
import com.group3.finmansys.service.dto.ToanTuDTO;
import com.group3.finmansys.service.mapper.ToanTuMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ToanTu}.
 */
@Service
@Transactional
public class ToanTuServiceImpl implements ToanTuService {

    private final Logger log = LoggerFactory.getLogger(ToanTuServiceImpl.class);

    private final ToanTuRepository toanTuRepository;

    private final ToanTuMapper toanTuMapper;

    public ToanTuServiceImpl(ToanTuRepository toanTuRepository, ToanTuMapper toanTuMapper) {
        this.toanTuRepository = toanTuRepository;
        this.toanTuMapper = toanTuMapper;
    }

    @Override
    public ToanTuDTO save(ToanTuDTO toanTuDTO) {
        log.debug("Request to save ToanTu : {}", toanTuDTO);
        ToanTu toanTu = toanTuMapper.toEntity(toanTuDTO);
        toanTu = toanTuRepository.save(toanTu);
        return toanTuMapper.toDto(toanTu);
    }

    @Override
    public Optional<ToanTuDTO> partialUpdate(ToanTuDTO toanTuDTO) {
        log.debug("Request to partially update ToanTu : {}", toanTuDTO);

        return toanTuRepository
            .findById(toanTuDTO.getId())
            .map(
                existingToanTu -> {
                    toanTuMapper.partialUpdate(existingToanTu, toanTuDTO);
                    return existingToanTu;
                }
            )
            .map(toanTuRepository::save)
            .map(toanTuMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ToanTuDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ToanTus");
        return toanTuRepository.findAll(pageable).map(toanTuMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ToanTuDTO> findOne(Long id) {
        log.debug("Request to get ToanTu : {}", id);
        return toanTuRepository.findById(id).map(toanTuMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ToanTu : {}", id);
        toanTuRepository.deleteById(id);
    }
}
