package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.QuyTien;
import com.group3.finmansys.repository.QuyTienRepository;
import com.group3.finmansys.service.QuyTienService;
import com.group3.finmansys.service.dto.QuyTienDTO;
import com.group3.finmansys.service.mapper.QuyTienMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link QuyTien}.
 */
@Service
@Transactional
public class QuyTienServiceImpl implements QuyTienService {

    private final Logger log = LoggerFactory.getLogger(QuyTienServiceImpl.class);

    private final QuyTienRepository quyTienRepository;

    private final QuyTienMapper quyTienMapper;

    public QuyTienServiceImpl(QuyTienRepository quyTienRepository, QuyTienMapper quyTienMapper) {
        this.quyTienRepository = quyTienRepository;
        this.quyTienMapper = quyTienMapper;
    }

    @Override
    public QuyTienDTO save(QuyTienDTO quyTienDTO) {
        log.debug("Request to save QuyTien : {}", quyTienDTO);
        QuyTien quyTien = quyTienMapper.toEntity(quyTienDTO);
        quyTien = quyTienRepository.save(quyTien);
        return quyTienMapper.toDto(quyTien);
    }

    @Override
    public Optional<QuyTienDTO> partialUpdate(QuyTienDTO quyTienDTO) {
        log.debug("Request to partially update QuyTien : {}", quyTienDTO);

        return quyTienRepository
            .findById(quyTienDTO.getId())
            .map(
                existingQuyTien -> {
                    quyTienMapper.partialUpdate(existingQuyTien, quyTienDTO);
                    return existingQuyTien;
                }
            )
            .map(quyTienRepository::save)
            .map(quyTienMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<QuyTienDTO> findAll(Pageable pageable) {
        log.debug("Request to get all QuyTiens");
        return quyTienRepository.findAll(pageable).map(quyTienMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<QuyTienDTO> findOne(Long id) {
        log.debug("Request to get QuyTien : {}", id);
        return quyTienRepository.findById(id).map(quyTienMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete QuyTien : {}", id);
        quyTienRepository.deleteById(id);
    }
}
