package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.TaiSanCD;
import com.group3.finmansys.repository.TaiSanCDRepository;
import com.group3.finmansys.service.TaiSanCDService;
import com.group3.finmansys.service.dto.TaiSanCDDTO;
import com.group3.finmansys.service.mapper.TaiSanCDMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link TaiSanCD}.
 */
@Service
@Transactional
public class TaiSanCDServiceImpl implements TaiSanCDService {

    private final Logger log = LoggerFactory.getLogger(TaiSanCDServiceImpl.class);

    private final TaiSanCDRepository taiSanCDRepository;

    private final TaiSanCDMapper taiSanCDMapper;

    public TaiSanCDServiceImpl(TaiSanCDRepository taiSanCDRepository, TaiSanCDMapper taiSanCDMapper) {
        this.taiSanCDRepository = taiSanCDRepository;
        this.taiSanCDMapper = taiSanCDMapper;
    }

    @Override
    public TaiSanCDDTO save(TaiSanCDDTO taiSanCDDTO) {
        log.debug("Request to save TaiSanCD : {}", taiSanCDDTO);
        TaiSanCD taiSanCD = taiSanCDMapper.toEntity(taiSanCDDTO);
        taiSanCD = taiSanCDRepository.save(taiSanCD);
        return taiSanCDMapper.toDto(taiSanCD);
    }

    @Override
    public Optional<TaiSanCDDTO> partialUpdate(TaiSanCDDTO taiSanCDDTO) {
        log.debug("Request to partially update TaiSanCD : {}", taiSanCDDTO);

        return taiSanCDRepository
            .findById(taiSanCDDTO.getId())
            .map(
                existingTaiSanCD -> {
                    taiSanCDMapper.partialUpdate(existingTaiSanCD, taiSanCDDTO);
                    return existingTaiSanCD;
                }
            )
            .map(taiSanCDRepository::save)
            .map(taiSanCDMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TaiSanCDDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TaiSanCDS");
        return taiSanCDRepository.findAll(pageable).map(taiSanCDMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TaiSanCDDTO> findOne(Long id) {
        log.debug("Request to get TaiSanCD : {}", id);
        return taiSanCDRepository.findById(id).map(taiSanCDMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete TaiSanCD : {}", id);
        taiSanCDRepository.deleteById(id);
    }
}
