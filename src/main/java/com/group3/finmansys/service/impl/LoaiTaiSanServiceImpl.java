package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.LoaiTaiSan;
import com.group3.finmansys.repository.LoaiTaiSanRepository;
import com.group3.finmansys.service.LoaiTaiSanService;
import com.group3.finmansys.service.dto.LoaiTaiSanDTO;
import com.group3.finmansys.service.mapper.LoaiTaiSanMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link LoaiTaiSan}.
 */
@Service
@Transactional
public class LoaiTaiSanServiceImpl implements LoaiTaiSanService {

    private final Logger log = LoggerFactory.getLogger(LoaiTaiSanServiceImpl.class);

    private final LoaiTaiSanRepository loaiTaiSanRepository;

    private final LoaiTaiSanMapper loaiTaiSanMapper;

    public LoaiTaiSanServiceImpl(LoaiTaiSanRepository loaiTaiSanRepository, LoaiTaiSanMapper loaiTaiSanMapper) {
        this.loaiTaiSanRepository = loaiTaiSanRepository;
        this.loaiTaiSanMapper = loaiTaiSanMapper;
    }

    @Override
    public LoaiTaiSanDTO save(LoaiTaiSanDTO loaiTaiSanDTO) {
        log.debug("Request to save LoaiTaiSan : {}", loaiTaiSanDTO);
        LoaiTaiSan loaiTaiSan = loaiTaiSanMapper.toEntity(loaiTaiSanDTO);
        loaiTaiSan = loaiTaiSanRepository.save(loaiTaiSan);
        return loaiTaiSanMapper.toDto(loaiTaiSan);
    }

    @Override
    public Optional<LoaiTaiSanDTO> partialUpdate(LoaiTaiSanDTO loaiTaiSanDTO) {
        log.debug("Request to partially update LoaiTaiSan : {}", loaiTaiSanDTO);

        return loaiTaiSanRepository
            .findById(loaiTaiSanDTO.getId())
            .map(
                existingLoaiTaiSan -> {
                    loaiTaiSanMapper.partialUpdate(existingLoaiTaiSan, loaiTaiSanDTO);
                    return existingLoaiTaiSan;
                }
            )
            .map(loaiTaiSanRepository::save)
            .map(loaiTaiSanMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<LoaiTaiSanDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LoaiTaiSans");
        return loaiTaiSanRepository.findAll(pageable).map(loaiTaiSanMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<LoaiTaiSanDTO> findOne(Long id) {
        log.debug("Request to get LoaiTaiSan : {}", id);
        return loaiTaiSanRepository.findById(id).map(loaiTaiSanMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete LoaiTaiSan : {}", id);
        loaiTaiSanRepository.deleteById(id);
    }
}
