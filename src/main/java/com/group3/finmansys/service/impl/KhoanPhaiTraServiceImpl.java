package com.group3.finmansys.service.impl;

import com.group3.finmansys.domain.KhoanPhaiTra;
import com.group3.finmansys.repository.KhoanPhaiTraRepository;
import com.group3.finmansys.service.KhoanPhaiTraService;
import com.group3.finmansys.service.dto.KhoanPhaiTraDTO;
import com.group3.finmansys.service.mapper.KhoanPhaiTraMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link KhoanPhaiTra}.
 */
@Service
@Transactional
public class KhoanPhaiTraServiceImpl implements KhoanPhaiTraService {

    private final Logger log = LoggerFactory.getLogger(KhoanPhaiTraServiceImpl.class);

    private final KhoanPhaiTraRepository khoanPhaiTraRepository;

    private final KhoanPhaiTraMapper khoanPhaiTraMapper;

    public KhoanPhaiTraServiceImpl(KhoanPhaiTraRepository khoanPhaiTraRepository, KhoanPhaiTraMapper khoanPhaiTraMapper) {
        this.khoanPhaiTraRepository = khoanPhaiTraRepository;
        this.khoanPhaiTraMapper = khoanPhaiTraMapper;
    }

    @Override
    public KhoanPhaiTraDTO save(KhoanPhaiTraDTO khoanPhaiTraDTO) {
        log.debug("Request to save KhoanPhaiTra : {}", khoanPhaiTraDTO);
        KhoanPhaiTra khoanPhaiTra = khoanPhaiTraMapper.toEntity(khoanPhaiTraDTO);
        khoanPhaiTra = khoanPhaiTraRepository.save(khoanPhaiTra);
        return khoanPhaiTraMapper.toDto(khoanPhaiTra);
    }

    @Override
    public Optional<KhoanPhaiTraDTO> partialUpdate(KhoanPhaiTraDTO khoanPhaiTraDTO) {
        log.debug("Request to partially update KhoanPhaiTra : {}", khoanPhaiTraDTO);

        return khoanPhaiTraRepository
            .findById(khoanPhaiTraDTO.getId())
            .map(
                existingKhoanPhaiTra -> {
                    khoanPhaiTraMapper.partialUpdate(existingKhoanPhaiTra, khoanPhaiTraDTO);
                    return existingKhoanPhaiTra;
                }
            )
            .map(khoanPhaiTraRepository::save)
            .map(khoanPhaiTraMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<KhoanPhaiTraDTO> findAll(Pageable pageable) {
        log.debug("Request to get all KhoanPhaiTras");
        return khoanPhaiTraRepository.findAll(pageable).map(khoanPhaiTraMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<KhoanPhaiTraDTO> findOne(Long id) {
        log.debug("Request to get KhoanPhaiTra : {}", id);
        return khoanPhaiTraRepository.findById(id).map(khoanPhaiTraMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete KhoanPhaiTra : {}", id);
        khoanPhaiTraRepository.deleteById(id);
    }
}
