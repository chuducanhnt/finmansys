package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.LoaiQuy;
import com.group3.finmansys.repository.LoaiQuyRepository;
import com.group3.finmansys.service.criteria.LoaiQuyCriteria;
import com.group3.finmansys.service.dto.LoaiQuyDTO;
import com.group3.finmansys.service.mapper.LoaiQuyMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link LoaiQuy} entities in the database.
 * The main input is a {@link LoaiQuyCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LoaiQuyDTO} or a {@link Page} of {@link LoaiQuyDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LoaiQuyQueryService extends QueryService<LoaiQuy> {

    private final Logger log = LoggerFactory.getLogger(LoaiQuyQueryService.class);

    private final LoaiQuyRepository loaiQuyRepository;

    private final LoaiQuyMapper loaiQuyMapper;

    public LoaiQuyQueryService(LoaiQuyRepository loaiQuyRepository, LoaiQuyMapper loaiQuyMapper) {
        this.loaiQuyRepository = loaiQuyRepository;
        this.loaiQuyMapper = loaiQuyMapper;
    }

    /**
     * Return a {@link List} of {@link LoaiQuyDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LoaiQuyDTO> findByCriteria(LoaiQuyCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<LoaiQuy> specification = createSpecification(criteria);
        return loaiQuyMapper.toDto(loaiQuyRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link LoaiQuyDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LoaiQuyDTO> findByCriteria(LoaiQuyCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<LoaiQuy> specification = createSpecification(criteria);
        return loaiQuyRepository.findAll(specification, page).map(loaiQuyMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LoaiQuyCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<LoaiQuy> specification = createSpecification(criteria);
        return loaiQuyRepository.count(specification);
    }

    /**
     * Function to convert {@link LoaiQuyCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<LoaiQuy> createSpecification(LoaiQuyCriteria criteria) {
        Specification<LoaiQuy> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), LoaiQuy_.id));
            }
            if (criteria.getMa() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMa(), LoaiQuy_.ma));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), LoaiQuy_.ten));
            }
        }
        return specification;
    }
}
