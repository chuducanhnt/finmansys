package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.QuyTienDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.QuyTien}.
 */
public interface QuyTienService {
    /**
     * Save a quyTien.
     *
     * @param quyTienDTO the entity to save.
     * @return the persisted entity.
     */
    QuyTienDTO save(QuyTienDTO quyTienDTO);

    /**
     * Partially updates a quyTien.
     *
     * @param quyTienDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<QuyTienDTO> partialUpdate(QuyTienDTO quyTienDTO);

    /**
     * Get all the quyTiens.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<QuyTienDTO> findAll(Pageable pageable);

    /**
     * Get the "id" quyTien.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<QuyTienDTO> findOne(Long id);

    /**
     * Delete the "id" quyTien.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
