package com.group3.finmansys.service;

import com.group3.finmansys.service.dto.PhongDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.group3.finmansys.domain.Phong}.
 */
public interface PhongService {
    /**
     * Save a phong.
     *
     * @param phongDTO the entity to save.
     * @return the persisted entity.
     */
    PhongDTO save(PhongDTO phongDTO);

    /**
     * Partially updates a phong.
     *
     * @param phongDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PhongDTO> partialUpdate(PhongDTO phongDTO);

    /**
     * Get all the phongs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PhongDTO> findAll(Pageable pageable);

    /**
     * Get the "id" phong.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PhongDTO> findOne(Long id);

    /**
     * Delete the "id" phong.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
