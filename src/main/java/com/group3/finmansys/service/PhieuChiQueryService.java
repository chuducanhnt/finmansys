package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.PhieuChi;
import com.group3.finmansys.repository.PhieuChiRepository;
import com.group3.finmansys.service.criteria.PhieuChiCriteria;
import com.group3.finmansys.service.dto.PhieuChiDTO;
import com.group3.finmansys.service.mapper.PhieuChiMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link PhieuChi} entities in the database.
 * The main input is a {@link PhieuChiCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PhieuChiDTO} or a {@link Page} of {@link PhieuChiDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PhieuChiQueryService extends QueryService<PhieuChi> {

    private final Logger log = LoggerFactory.getLogger(PhieuChiQueryService.class);

    private final PhieuChiRepository phieuChiRepository;

    private final PhieuChiMapper phieuChiMapper;

    public PhieuChiQueryService(PhieuChiRepository phieuChiRepository, PhieuChiMapper phieuChiMapper) {
        this.phieuChiRepository = phieuChiRepository;
        this.phieuChiMapper = phieuChiMapper;
    }

    /**
     * Return a {@link List} of {@link PhieuChiDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PhieuChiDTO> findByCriteria(PhieuChiCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PhieuChi> specification = createSpecification(criteria);
        return phieuChiMapper.toDto(phieuChiRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PhieuChiDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PhieuChiDTO> findByCriteria(PhieuChiCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PhieuChi> specification = createSpecification(criteria);
        return phieuChiRepository.findAll(specification, page).map(phieuChiMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PhieuChiCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PhieuChi> specification = createSpecification(criteria);
        return phieuChiRepository.count(specification);
    }

    /**
     * Function to convert {@link PhieuChiCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PhieuChi> createSpecification(PhieuChiCriteria criteria) {
        Specification<PhieuChi> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PhieuChi_.id));
            }
            if (criteria.getNgayChungTu() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNgayChungTu(), PhieuChi_.ngayChungTu));
            }
            if (criteria.getSoTien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoTien(), PhieuChi_.soTien));
            }
            if (criteria.getKhachHangId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKhachHangId(),
                            root -> root.join(PhieuChi_.khachHang, JoinType.LEFT).get(KhachHang_.id)
                        )
                    );
            }
            if (criteria.getKhoanMucChiPhiId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKhoanMucChiPhiId(),
                            root -> root.join(PhieuChi_.khoanMucChiPhi, JoinType.LEFT).get(KhoanMucChiPhi_.id)
                        )
                    );
            }
            if (criteria.getKhoanPhaiTraId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKhoanPhaiTraId(),
                            root -> root.join(PhieuChi_.khoanPhaiTra, JoinType.LEFT).get(KhoanPhaiTra_.id)
                        )
                    );
            }
            if (criteria.getQuyTienId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getQuyTienId(), root -> root.join(PhieuChi_.quyTien, JoinType.LEFT).get(QuyTien_.id))
                    );
            }
        }
        return specification;
    }
}
