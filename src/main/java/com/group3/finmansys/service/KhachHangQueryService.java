package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.KhachHang;
import com.group3.finmansys.repository.KhachHangRepository;
import com.group3.finmansys.service.criteria.KhachHangCriteria;
import com.group3.finmansys.service.dto.KhachHangDTO;
import com.group3.finmansys.service.mapper.KhachHangMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link KhachHang} entities in the database.
 * The main input is a {@link KhachHangCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link KhachHangDTO} or a {@link Page} of {@link KhachHangDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class KhachHangQueryService extends QueryService<KhachHang> {

    private final Logger log = LoggerFactory.getLogger(KhachHangQueryService.class);

    private final KhachHangRepository khachHangRepository;

    private final KhachHangMapper khachHangMapper;

    public KhachHangQueryService(KhachHangRepository khachHangRepository, KhachHangMapper khachHangMapper) {
        this.khachHangRepository = khachHangRepository;
        this.khachHangMapper = khachHangMapper;
    }

    /**
     * Return a {@link List} of {@link KhachHangDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<KhachHangDTO> findByCriteria(KhachHangCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<KhachHang> specification = createSpecification(criteria);
        return khachHangMapper.toDto(khachHangRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link KhachHangDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<KhachHangDTO> findByCriteria(KhachHangCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<KhachHang> specification = createSpecification(criteria);
        return khachHangRepository.findAll(specification, page).map(khachHangMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(KhachHangCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<KhachHang> specification = createSpecification(criteria);
        return khachHangRepository.count(specification);
    }

    /**
     * Function to convert {@link KhachHangCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<KhachHang> createSpecification(KhachHangCriteria criteria) {
        Specification<KhachHang> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), KhachHang_.id));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), KhachHang_.ten));
            }
            if (criteria.getDiaChi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDiaChi(), KhachHang_.diaChi));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), KhachHang_.email));
            }
            if (criteria.getSdt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSdt(), KhachHang_.sdt));
            }
        }
        return specification;
    }
}
