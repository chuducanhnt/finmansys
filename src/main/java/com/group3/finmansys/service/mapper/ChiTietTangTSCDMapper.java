package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.ChiTietTangTSCDDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ChiTietTangTSCD} and its DTO {@link ChiTietTangTSCDDTO}.
 */
@Mapper(componentModel = "spring", uses = { TaiSanCDMapper.class })
public interface ChiTietTangTSCDMapper extends EntityMapper<ChiTietTangTSCDDTO, ChiTietTangTSCD> {
    @Mapping(target = "taiSanCD", source = "taiSanCD", qualifiedByName = "ten")
    ChiTietTangTSCDDTO toDto(ChiTietTangTSCD s);
}
