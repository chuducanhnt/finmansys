package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.KhoanMucChiPhiDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link KhoanMucChiPhi} and its DTO {@link KhoanMucChiPhiDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface KhoanMucChiPhiMapper extends EntityMapper<KhoanMucChiPhiDTO, KhoanMucChiPhi> {
    @Named("ten")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ten", source = "ten")
    KhoanMucChiPhiDTO toDtoTen(KhoanMucChiPhi khoanMucChiPhi);
}
