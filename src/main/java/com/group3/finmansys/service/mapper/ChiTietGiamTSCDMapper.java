package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.ChiTietGiamTSCDDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ChiTietGiamTSCD} and its DTO {@link ChiTietGiamTSCDDTO}.
 */
@Mapper(componentModel = "spring", uses = { TaiSanCDMapper.class })
public interface ChiTietGiamTSCDMapper extends EntityMapper<ChiTietGiamTSCDDTO, ChiTietGiamTSCD> {
    @Mapping(target = "taiSanCD", source = "taiSanCD", qualifiedByName = "ten")
    ChiTietGiamTSCDDTO toDto(ChiTietGiamTSCD s);
}
