package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.KhoanPhaiTraDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link KhoanPhaiTra} and its DTO {@link KhoanPhaiTraDTO}.
 */
@Mapper(componentModel = "spring", uses = { KhachHangMapper.class, MaKeToanMapper.class, PhieuChiMapper.class })
public interface KhoanPhaiTraMapper extends EntityMapper<KhoanPhaiTraDTO, KhoanPhaiTra> {
    @Mapping(target = "khachHang", source = "khachHang", qualifiedByName = "ten")
    @Mapping(target = "maKeToan", source = "maKeToan")
    KhoanPhaiTraDTO toDto(KhoanPhaiTra s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ghiChu", source = "ghiChu")
    KhoanPhaiTraDTO toDtoId(KhoanPhaiTra khoanPhaiTra);
}
