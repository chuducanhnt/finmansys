package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.KhoanPhaiThuDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link KhoanPhaiThu} and its DTO {@link KhoanPhaiThuDTO}.
 */
@Mapper(componentModel = "spring", uses = { KhachHangMapper.class, MaKeToanMapper.class, PhongDuocDatMapper.class, PhieuThuMapper.class })
public interface KhoanPhaiThuMapper extends EntityMapper<KhoanPhaiThuDTO, KhoanPhaiThu> {
    @Mapping(target = "khachHang", source = "khachHang", qualifiedByName = "ten")
    @Mapping(target = "maKeToan", source = "maKeToan")
    @Mapping(target = "phongDuocDat", source = "phongDuocDat")
    KhoanPhaiThuDTO toDto(KhoanPhaiThu s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ghiChu", source = "ghiChu")
    KhoanPhaiThuDTO toDtoId(KhoanPhaiThu khoanPhaiThu);
}
