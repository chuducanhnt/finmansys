package com.group3.finmansys.service.mapper;

import com.group3.finmansys.domain.*;
import com.group3.finmansys.service.dto.TaiSanCDDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link TaiSanCD} and its DTO {@link TaiSanCDDTO}.
 */
@Mapper(componentModel = "spring", uses = { KhachSanMapper.class, LoaiTaiSanMapper.class })
public interface TaiSanCDMapper extends EntityMapper<TaiSanCDDTO, TaiSanCD> {
    @Mapping(target = "khachSan", source = "khachSan", qualifiedByName = "ten")
    @Mapping(target = "loaiTaiSan", source = "loaiTaiSan", qualifiedByName = "ten")
    TaiSanCDDTO toDto(TaiSanCD s);

    @Named("ten")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ten", source = "ten")
    TaiSanCDDTO toDtoTen(TaiSanCD taiSanCD);
}
