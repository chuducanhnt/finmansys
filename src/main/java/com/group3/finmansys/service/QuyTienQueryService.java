package com.group3.finmansys.service;

import com.group3.finmansys.domain.*; // for static metamodels
import com.group3.finmansys.domain.QuyTien;
import com.group3.finmansys.repository.QuyTienRepository;
import com.group3.finmansys.service.criteria.QuyTienCriteria;
import com.group3.finmansys.service.dto.QuyTienDTO;
import com.group3.finmansys.service.mapper.QuyTienMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link QuyTien} entities in the database.
 * The main input is a {@link QuyTienCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link QuyTienDTO} or a {@link Page} of {@link QuyTienDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class QuyTienQueryService extends QueryService<QuyTien> {

    private final Logger log = LoggerFactory.getLogger(QuyTienQueryService.class);

    private final QuyTienRepository quyTienRepository;

    private final QuyTienMapper quyTienMapper;

    public QuyTienQueryService(QuyTienRepository quyTienRepository, QuyTienMapper quyTienMapper) {
        this.quyTienRepository = quyTienRepository;
        this.quyTienMapper = quyTienMapper;
    }

    /**
     * Return a {@link List} of {@link QuyTienDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<QuyTienDTO> findByCriteria(QuyTienCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<QuyTien> specification = createSpecification(criteria);
        return quyTienMapper.toDto(quyTienRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link QuyTienDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<QuyTienDTO> findByCriteria(QuyTienCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<QuyTien> specification = createSpecification(criteria);
        return quyTienRepository.findAll(specification, page).map(quyTienMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(QuyTienCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<QuyTien> specification = createSpecification(criteria);
        return quyTienRepository.count(specification);
    }

    /**
     * Function to convert {@link QuyTienCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<QuyTien> createSpecification(QuyTienCriteria criteria) {
        Specification<QuyTien> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), QuyTien_.id));
            }
            if (criteria.getSoTien() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSoTien(), QuyTien_.soTien));
            }
            if (criteria.getGhiChu() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGhiChu(), QuyTien_.ghiChu));
            }
            if (criteria.getTen() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTen(), QuyTien_.ten));
            }
            if (criteria.getLoaiQuyId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getLoaiQuyId(), root -> root.join(QuyTien_.loaiQuy, JoinType.LEFT).get(LoaiQuy_.id))
                    );
            }
            if (criteria.getMaKeToanId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getMaKeToanId(), root -> root.join(QuyTien_.maKeToan, JoinType.LEFT).get(MaKeToan_.id))
                    );
            }
            if (criteria.getKhachSanId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getKhachSanId(), root -> root.join(QuyTien_.khachSan, JoinType.LEFT).get(KhachSan_.id))
                    );
            }
            if (criteria.getPhieuThuId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getPhieuThuId(), root -> root.join(QuyTien_.phieuThus, JoinType.LEFT).get(PhieuThu_.id))
                    );
            }
            if (criteria.getPhieuChiId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getPhieuChiId(), root -> root.join(QuyTien_.phieuChis, JoinType.LEFT).get(PhieuChi_.id))
                    );
            }
        }
        return specification;
    }
}
